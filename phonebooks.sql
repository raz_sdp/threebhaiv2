/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : cabbieappv2

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2018-08-03 17:15:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `phonebooks`
-- ----------------------------
DROP TABLE IF EXISTS `phonebooks`;
CREATE TABLE `phonebooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of phonebooks
-- ----------------------------
INSERT INTO `phonebooks` VALUES ('1', '5', '+15109014307', null, '5 GBP', '2018-08-03 12:04:48', '2018-08-03 12:04:48');
INSERT INTO `phonebooks` VALUES ('2', '5', '+441732496386', null, '5 GBP', '2018-08-03 12:31:02', '2018-08-03 12:31:02');
INSERT INTO `phonebooks` VALUES ('3', '5', '+441174563015', null, '5 GBP', '2018-08-03 12:31:30', '2018-08-03 12:31:30');
