<?php
App::uses('AclsRole', 'Model');

/**
 * AclsRole Test Case
 */
class AclsRoleTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.acls_role',
		'app.role',
		'app.acl'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AclsRole = ClassRegistry::init('AclsRole');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AclsRole);

		parent::tearDown();
	}

}
