<?php
/**
 * AclsRole Fixture
 */
class AclsRoleFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'role_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'acl_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'access' => array('type' => 'boolean', 'null' => true, 'default' => '1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'roleID_2' => array('column' => array('role_id', 'acl_id'), 'unique' => 1),
			'role_id' => array('column' => array('role_id', 'acl_id'), 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB', 'comment' => 'Role Permit ACL')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'role_id' => 1,
			'acl_id' => 1,
			'access' => 1
		),
	);

}
