<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Apilog $Apilog
 * @property DeviceToken $DeviceToken

 * @property PhoneQueue $PhoneQueue
 * @property Queue $Queue
 * @property TopUp $TopUp
 * @property Transaction $Transaction
 * @property Zone $Zone
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'mobile' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_enabled' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'code' => array(
			//'notempty' => array(
				//'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			//),
		),

		'type' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		// 'Apilog' => array(
		// 	'className' => 'Apilog',
		// 	'foreignKey' => 'user_id',
		// 	'dependent' => false,
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => '',
		// 	'limit' => '',
		// 	'offset' => '',
		// 	'exclusive' => '',
		// 	'finderQuery' => '',
		// 	'counterQuery' => ''
		// ),
		'DeviceToken' => array(
			'className' => 'DeviceToken',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
        'Charge' => array(
            'className' => 'Charge',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
		'Payee' => array(
			'className' => 'Payee',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Payer' => array(
			'className' => 'Payer',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'PhoneQueue' => array(
			'className' => 'PhoneQueue',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Queue' => array(
			'className' => 'Queue',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		// 'TopUp' => array(
		// 	'className' => 'TopUp',
		// 	'foreignKey' => 'user_id',
		// 	'dependent' => false,
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => '',
		// 	'limit' => '',
		// 	'offset' => '',
		// 	'exclusive' => '',
		// 	'finderQuery' => '',
		// 	'counterQuery' => ''
		// ),
		'Transaction' => array(
			'className' => 'Transaction',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
        'Wallet' => array(
            'className' => 'Wallet',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
		'Barred' => array(
			'className' => 'Bar',
			'foreignKey' => 'userId1',
			'dependent' => false,
			'conditions' => '',
			'fields' => array('id', 'userId2', 'who_to_whom'),
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Zone' => array(
			'className' => 'Zone',
			'joinTable' => 'users_zones',
			'foreignKey' => 'user_id',
			'associationForeignKey' => 'zone_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
        'Acl' => array(
            'className' => 'Acl',
            'joinTable' => 'users_acls',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'acl_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
        )
	);

	public function getUserFieldById($field, $id) {
		$this->recursive = -1;
		$this->id = $id;
		return $this->field($field);
	}

/**
* @params:
*	$field	: a valid field name
*	$code		: valid `code`
* @return:
*	value of that field
*/
	public function getUserFieldByCode($field, $code) {
		$this->recursive = -1;
		return $this->field($field, array('User.code' => $code));
	}

/**
 * Method to get all available drivers around a driver
 * from driver's position
 *
 * @params:
 *	$lat and $lng of driver
 * @return:
 *	Array of drivers
 */

    public function get_available_drivers_around_driver($driver_id, $lat, $lng, $distance=20, $count=true){
        $drivers = $this->find('all', array(
                'recursive' => 0,
                'joins' => array(
                	array(
                		'table' =>'device_tokens',
                		'alias' =>'DeviceToken',
                		'type' => 'INNER',
                		'conditions' => 'User.id = DeviceToken.user_id'
                		)
                	),
                'fields' => array(
                    'User.id','User.lat','User.lng','User.name','User.email','User.mobile'
                ),
                'conditions' =>  array(
                    am(
                        array('User.type' => 'driver', 'User.is_enabled' => 1, 'User.id !=' => $driver_id),
                        array("ROUND(3963.0*ACOS(SIN('$lat'*PI()/180)*SIN(User.lat*PI()/180)+COS('$lat'*PI()/180)*
					    COS(User.lat*PI()/180)*COS(('$lng'*PI()/180)-(User.lng*PI()/180)))) <= '$distance'")
                    ),
                )
            )
        );
		$driver_count = 0;
        if(!empty($drivers)) {
			$driver_count = count($drivers);
		}
        return $count ? $driver_count  : $drivers;
    }

/*
 * Check the if a valid promoter code or not
 */

    public function check_valid_promoter_code($given_promoter_code){
        $this->displayField = 'promoter_code';
        $codes = $this->find('list');
        return in_array($given_promoter_code, array_values($codes));
    }

    public function create_rand_promoter_code(){
        $r = range('A','Z');
        shuffle($r);
        $str = substr(implode('', $r), 0, 2) .  mt_rand(11, 99) . substr(time(), -2) .  substr(uniqid(), 4, 2);
        return strtoupper($str);
    }


}
