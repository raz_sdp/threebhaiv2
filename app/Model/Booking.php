<?php
App::uses('AppModel', 'Model');
/**
 * Booking Model
 *
 * @property Billing $Billing
 * @property Reject $Reject
 * @property RequestLog $RequestLog
 */
class Booking extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Cache VrSetting and $FareSetting Model Instance
 *
 */
    public $VrSetting;
    public $FareSetting;

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'pick_up' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'destination' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'at_time' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'persons' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'via' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_paid' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'requester' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'acceptor' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'accept_time' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
		'special_requirement' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'bar_driver' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'note_to_driver' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_app_to_app' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_vr' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_pre_book' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(

		'Requester' => array(
			'className' => 'User',
			'foreignKey' => 'requester',
			'conditions' => '',
			'fields' => array('id', 'name', 'email', 'mobile', 'lat', 'lng','type','branch_number'),
			'order' => ''
		),
		'Acceptor' => array(
			'className' => 'User',
			'foreignKey' => 'acceptor',
			'conditions' => '',
			'fields' => array('id', 'name', 'email', 'mobile', 'lat', 'lng', 'type','badge_no','branch_number'),
			'order' => ''
		),
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
	/*	'Billing' => array(
			'className' => 'Billing',
			'foreignKey' => 'booking_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		), */
		'Queue' => array(
			'className' => 'Queue',
			'foreignKey' => 'booking_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	/*	'RequestLog' => array(
			'className' => 'RequestLog',
			'foreignKey' => 'booking_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)*/
	);


/**
 * Check Booking acceptability
 *
 * @param: $booking array
 */
    public function checkBookingAcceptability($booking){
        $is_acceptable = array(
            'success' => false,
            'message' => __('Booking not acceptable'),
            'fare' => null
        );

        if(empty($booking)) return $is_acceptable;


        // Use VrSetting Model
        App::import('Model', 'VrSetting');
        $this->VrSetting = new VrSetting();

        // Use FareSetting Model
        App::import('Model', 'FareSetting');
        $this->FareSetting = new FareSetting();

        // Getting all Booking Settings for current user zone
      /*  
        $settings = $this->VrSetting->get_setting_for_zone($booking['Booking']['pick_up_lat'], $booking['Booking']['pick_up_lng'], $pick_up_date);
        if(empty($settings)){
            $fare = 0; // $this->FareSetting->getFare($booking);
            $is_acceptable = array(
                'success' => true,
                'message' => __('No settings available'),
                'fare' => $fare
            );
            return $is_acceptable;
        }


        // Filter Setting by Date
        $pick_up_date = date('Y-m-d', strtotime($booking['Booking']['at_time']));
        $settings = $this->VrSetting->get_settings_by_date($pick_up_date, $settings);
        if(empty($settings)){
            $fare = 0; // $this->FareSetting->getFare($booking);
            $is_acceptable = array(
                'success' => true,
                'message' => __('No settings available'),
                'fare' => $fare
            );
            return $is_acceptable;
        }

        // Filter Setting by Time
        $pick_up_time = date('H:i:s', strtotime($booking['Booking']['at_time']));
        $setting = $this->VrSetting->get_settings_by_time($pick_up_time, $settings);
        if(empty($setting)){
            $is_acceptable['message'] = __('No Booking available at this time');
            return $is_acceptable;
        }

        // Checking for advance booking and Acceptability
        $advance_booking = $this->VrSetting->is_advance_booking_and_acceptable($booking, $setting);
       /* if($advance_booking['is_advance_booking'] && !$advance_booking['is_acceptable']){
            $is_acceptable = __('Booking Failed. Advance booking not available');
            return $is_acceptable;
        } else if(!$advance_booking['is_advance_booking'] && $advance_booking['is_acceptable']){
            $is_acceptable['message'] = __('Booking failed. Advance Booking must be given before ' . $advance_booking['min_advance_booking_time'] . ' minutes to pick time.');
            return $is_acceptable;
        } */


        // Checking for advance booking
        if(!$advance_booking['is_acceptable']){
            $is_acceptable = __('Booking Failed. Advance booking not available');
            return $is_acceptable;
        } 

        // Get Fare
      //  $fare = 0; // $this->FareSetting->getFare($booking);
        $is_acceptable = array(
            'success' => true,
            'message' => __('Booking Accepted'),
            'fare' => $fare
        );
        return $is_acceptable;
    }

    public function getLastCustomersBookingInfo($passenger_id = null){
        $query = [
            'recursive' => -1,
            'fields' => [
                'Booking.pick_up',
                'Booking.destination',
                'Booking.total_distance',
                'Booking.persons',
                'Booking.no_of_luggages',
                'Booking.at_time',
                'Booking.car_type',
                'Booking.customer_phn_no',
            ],
            'conditions' => [
                'Booking.requester' => $passenger_id,
            ],
            'order' => ['Booking.created' => 'DESC'],
        ];
        return $this->find('first', $query);
    }
}
