<?php
App::uses('AppModel', 'Model');
/**
 * Holiday Model
 *
 * @property Holidaytype $Holidaytype
 */
class Holiday extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Holidaytype' => array(
			'className' => 'Holidaytype',
			'foreignKey' => 'holidaytype_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
