<?php
App::uses('AppModel', 'Model');
/**
 * UsersAcl Model
 *
 * @property User $User
 * @property Acl $Acl
 */
class UsersAcl extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Acl' => array(
			'className' => 'Acl',
			'foreignKey' => 'acl_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
