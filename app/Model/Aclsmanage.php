<?php
App::uses('AppModel', 'Model');
/**
 * Aclsmanage Model
 *
 * @property Acl $Acl
 */
class Aclsmanage extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'aclsmanage';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Acl' => array(
			'className' => 'Acl',
			'foreignKey' => 'acl_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
