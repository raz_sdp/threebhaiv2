<?php
App::uses('AppModel', 'Model');
/**
 * Geophonebook Model
 *
 */
class Geophonebook extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

}
