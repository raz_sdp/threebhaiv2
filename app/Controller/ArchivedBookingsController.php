<?php
App::uses('AppController', 'Controller');
/**
 * ArchivedBookings Controller
 *
 * @property ArchivedBooking $ArchivedBooking
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ArchivedBookingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ArchivedBooking->recursive = 0;
		$this->set('archivedBookings', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ArchivedBooking->exists($id)) {
			throw new NotFoundException(__('Invalid archived booking'));
		}
		$options = array('conditions' => array('ArchivedBooking.' . $this->ArchivedBooking->primaryKey => $id));
		$this->set('archivedBooking', $this->ArchivedBooking->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ArchivedBooking->create();
			if ($this->ArchivedBooking->save($this->request->data)) {
				$this->Session->setFlash(__('The archived booking has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The archived booking could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		}
		$zones = $this->ArchivedBooking->Zone->find('list');
		$this->set(compact('zones'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ArchivedBooking->exists($id)) {
			throw new NotFoundException(__('Invalid archived booking'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ArchivedBooking->save($this->request->data)) {
				$this->Session->setFlash(__('The archived booking has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The archived booking could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('ArchivedBooking.' . $this->ArchivedBooking->primaryKey => $id));
			$this->request->data = $this->ArchivedBooking->find('first', $options);
		}
		$zones = $this->ArchivedBooking->Zone->find('list');
		$this->set(compact('zones'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ArchivedBooking->id = $id;
		if (!$this->ArchivedBooking->exists()) {
			throw new NotFoundException(__('Invalid archived booking'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ArchivedBooking->delete()) {
			$this->Session->setFlash(__('The archived booking has been deleted.'), 'default', array('class' => 'alert alert-success text-center'));
		} else {
			$this->Session->setFlash(__('The archived booking could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		// $this->ArchivedBooking->recursive = 0;
		// $this->set('archivedBookings', $this->Paginator->paginate());
		$this->ArchivedBooking->recursive = 0;
		$this->paginate = array(
	        'limit' => 25,
	        'conditions' => array('ArchivedBooking.pick_up IS NOT NULL'),
	        'order' => 'ArchivedBooking.created DESC',
	    );
	   	$archivedBookings = $this->paginate();
		$this->set(compact('archivedBookings'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ArchivedBooking->exists($id)) {
			throw new NotFoundException(__('Invalid archived booking'));
		}
		$options = array('conditions' => array('ArchivedBooking.' . $this->ArchivedBooking->primaryKey => $id));
		$this->set('archivedBooking', $this->ArchivedBooking->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ArchivedBooking->create();
			if ($this->ArchivedBooking->save($this->request->data)) {
				$this->Session->setFlash(__('The archived booking has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The archived booking could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		}
		$zones = $this->ArchivedBooking->Zone->find('list');
		$this->set(compact('zones'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ArchivedBooking->exists($id)) {
			throw new NotFoundException(__('Invalid archived booking'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ArchivedBooking->save($this->request->data)) {
				$this->Session->setFlash(__('The archived booking has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The archived booking could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('ArchivedBooking.' . $this->ArchivedBooking->primaryKey => $id));
			$this->request->data = $this->ArchivedBooking->find('first', $options);
		}
		$zones = $this->ArchivedBooking->Zone->find('list');
		$this->set(compact('zones'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ArchivedBooking->id = $id;
		if (!$this->ArchivedBooking->exists()) {
			throw new NotFoundException(__('Invalid archived booking'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ArchivedBooking->delete()) {
			$this->Session->setFlash(__('The archived booking has been deleted.'), 'default', array('class' => 'alert alert-success text-center'));
		} else {
			$this->Session->setFlash(__('The archived booking could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	/*
	 * Delete archived booking records function after 30 days
	 */

    public function admin_deleteArchivedBookingRecord($period = 30){
        $this->autoRender = false;
        $this->loadModel('User');
        $user_obj = $this->User->findByCode('', array('id', 'type'));
        if(!empty($user_obj)){
            if($user_obj['User']['type'] === 'admin'){
                if(!empty($period)){
                    $this->ArchivedBooking->query("DELETE FROM archived_bookings WHERE at_time < DATE_SUB(NOW(), INTERVAL $period day)");
                }
            }else{

            }
        }
    }

}
