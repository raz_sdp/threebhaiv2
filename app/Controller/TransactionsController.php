<?php
App::uses('AppController', 'Controller');

/**
 * Transactions Controller
 *
 * @property Transaction $Transaction
 */
class TransactionsController extends AppController
{

    /**
     * admin_index method
     *
     * @return void
     */


    public function admin_index()
    {
        $this->Transaction->recursive = 0;
        //$this->set('transactions', $this->paginate());
        $conditions = [];
        if (AuthComponent::user(['type']) == 'vendor') {
            $conditions = am($conditions, ['User.branch_number' => AuthComponent::user(['branch_number'])]);
        }
        $keyword = "";
        if (!empty($this->request->data['keyword'])) {
            $keyword = $this->request->data['keyword'];
        } elseif (!empty($this->params['named']['keyword'])) {
            $keyword = $this->params['named']['keyword'];
        } elseif (!empty($this->params->query['keyword'])) {
            $keyword = $this->params->query['keyword'];
        }
        if(!empty($keyword)){
            $conditions = am($conditions,['User.type' => $keyword]);
        }
            $this->paginate = array(
                'limit' => 25,
                'conditions' => am($conditions),
                'fields' => [
                    'User.name',
                    'User.type',
                    'Transaction.amount',
                    'Transaction.service',
                    'Transaction.timestamp'
                ],
                'order' => [
                    'Transaction.id DESC'
                ]
            );

        $transactions = $this->paginate();
        //pr($transactions);die;
        $this->set(compact('transactions', 'keyword'));
    }


    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null)
    {
        if (!$this->Transaction->exists($id)) {
            throw new NotFoundException(__('Invalid top up'));
        }
        $this->Transaction->recursive = 0;
        $balance = array(
            'conditions' => array('Transaction.' . $this->Transaction->primaryKey => $id),
            'fields' => array('Transaction.id, Transaction.user_id, Transaction.amount', 'User.name, user.id'),
            'order' => array('Transaction.timestamp DESC')
        );
        $this->set('transactions', $this->Transaction->find('first', $balance));
    }

    public function do_trans()
    {
        $this->autoRender = false;
        if ($this->request->is('post') && !empty($this->request->data)) {

            // Check for valid user
            $this->loadModel('User');
            $user_info = $this->User->findByCode($this->request->data['Transaction']['code']);
            //print_r($user_info['User']['id']);
            //exit;
            if (empty($user_info['User']['id'])) die(json_encode(array('success' => false, 'message' => __('Invalid User'))));

            // Check for booking exists
            $this->loadModel('Booking');
            $booking_id = $this->request->data['Transaction']['booking_id'];
            $fare = $this->request->data['Transaction']['real_fare'];
            // if(empty($this->request->data['Transaction']['real_fare'])){
            // 	$app_fare = $this->Booking->findById($booking_id, array('fare'));
            // 	$fare = $app_fare['Booking']['fare'];
            // }


            if (!$this->Booking->exists($booking_id)) die(json_encode(array('success' => false, 'message' => __('Invalid Booking'))));

            // Check amount has value
            $balance = $this->Transaction->find('first', array(
                'conditions' => array('Transaction.user_id' => $user_info['User']['id']),
                'fields' => array('SUM(Transaction.amount) as balance'),
                'group' => array('Transaction.user_id'),
            ));


            if ($balance[0]['balance'] < $fare && empty($fare) && empty($balance[0]['balance'])) die(json_encode(array('success' => false, 'message' => __('Invalid amount to pay'))));

            // Get booking detail
            $this->Booking->recursive = -1;
            $booking = $this->Booking->find('first', array('conditions' => array('Booking.id' => $booking_id)));

            // check if already paid
            if ($booking['Booking']['is_paid'] == 1) die(json_encode(array('success' => false, 'message' => __('Already paid.'))));

            // if not paid
            $payee_id = $booking['Booking']['acceptor'];
            //print_r($booking['Booking']['acceptor']);
            $service = $this->request->data['Transaction']['service'];
            //print_r($service);
            // Prepare Transaction data and save

            $this->request->data['Transaction']['user_id'] = $user_info['User']['id'];
            $this->request->data['Transaction']['payee'] = $payee_id;
            $this->request->data['Transaction']['short_description'] = 'taxi_fare';
            $this->request->data['Transaction']['amount'] = '-' . $fare;
            $this->request->data['Transaction']['booking_id'] = $booking_id;
            $this->request->data['Transaction']['currency_code'] = CURRENCY;
            $now = date('Y-m-d');
            $this->request->data['Transaction']['timestamp'] = $now;
            //print_r($this->request->data['Transaction']['user_id']);
            $this->Transaction->create();
            if ($this->Transaction->save($this->request->data, false)) {
                // Update booking record is_paid field to 1
                $this->Booking->updateAll(array('Booking.is_paid' => 1, 'Booking.real_fare' => $fare), array('Booking.id' => $booking_id));

                // Insert transaction record for payee
                $data['Transaction']['user_id'] = $payee_id;
                $data['Transaction']['amount'] = $fare;
                $data['Transaction']['short_description'] = 'taxi_fare';
                $data['Transaction']['booking_id'] = $booking_id;
                $data['Transaction']['service'] = $service;
                $now = date('Y-m-d');
                $data['Transaction']['timestamp'] = $now;
                $this->Transaction->create();
                if ($this->Transaction->save($data, false)) {
                    $driver_info = $this->User->findById($payee_id);
                    //	print_r($driver_info);
                    // calculate admin fee
                    $this->_fee_admin_entry($driver_info, $fare, $booking_id, $service);

                    // calculate promoter fee
                    $promoter_of_driver = $this->User->findByPromoterCode($driver_info['User']['ref_promoter_code']);
                    //print_r($promoter_of_driver);
                    if (!empty($promoter_of_driver['User']['id'])) {
                        //pay promoter
                        $this->_pay_promoter($promoter_of_driver, $driver_info['User']['id'], $fare);
                    }
                    if ($service != 'credit_transfer') {
                        if ($service == 'new_paypal') $service = 'paypal';
                        $this->_push($payee_id, 'Payment received successfully via ' . $service . '.', 'CabbieCall');
                    }
                }
                die(json_encode(array('success' => true)));
            } else die(json_encode(array('success' => false, 'message' => __('Payment is failed.'))));
        } else die(json_encode(array('success' => false, 'message' => __('Invalid Request'))));
    }


    //paypal verification MSDK version 2.x

    private function _paypal_verify()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $currency = $this->request->data['Transaction']['currency_code'];
            $total = $this->request->data['Transaction']['amount'];
            $pay_key = $this->request->data['Transaction']['pay_key'];
            //$ch = curl_init();
            $this->loadModel('Setting');
            $paypal_info = $this->Setting->findById(1, array('paypal_clientId', 'paypal_secret', 'paypal_mode', 'sandbox_clientId', 'sandbox_secret'));
            if ($paypal_info['Setting']['paypal_mode'] == 'Sandbox') {
                $clientId = $paypal_info['Setting']['sandbox_clientId']; //"clientID";
                $secret = $paypal_info['Setting']['sandbox_secret']; //"secret";
                $url = "https://api.sandbox.paypal.com/v1/payments/payment/$pay_key";
                $uri = "https://api.sandbox.paypal.com/v1/oauth2/token";

            } elseif ($paypal_info['Setting']['paypal_mode'] == 'Live') {
                $clientId = $paypal_info['Setting']['paypal_clientId']; //"clientID";
                $secret = $paypal_info['Setting']['paypal_secret']; //"secret";
                $url = "https://api.paypal.com/v1/payments/payment/$pay_key";
                $uri = "https://api.paypal.com/v1/oauth2/token";

            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/oauth2/token");
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, $clientId . ":" . $secret);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
            $result = curl_exec($ch);
            $accessToken = null;
            if (empty($result))
                die('invalid access token');
            else {
                $json = json_decode($result);
                $access_token = $json->access_token;
            }
            curl_close($ch);

            /* $res = json_decode($response, true);
             $access_token = $res['access_token'];*/
//             print_r($access_token);
//             exit;

            if (!empty($access_token)) {

                $ch2 = curl_init();
                curl_setopt($ch2, CURLOPT_URL, $url);
                // curl_setopt($ch2, CURLOPT_HEADER, true);
                //curl_setopt($ch2, CURLINFO_HEADER_OUT, true);
                curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
                //curl_setopt($ch2, CURLOPT_POST, true);
                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    "Authorization: Bearer " . $access_token));

                $response = curl_exec($ch2);
                //print_r($response);
                $full_response = json_decode($response, true);
                //print_r($full_response);die;
                //&& $full_response['transactions'][0]['related_resources'][0]['sale']['state']== 'completed'
                curl_close($ch2);
                if ($full_response['state'] == 'approved') {
                    if ($full_response['state'] == 'approved' &&
                        $full_response['transactions'][0]['amount']['currency'] = $currency &&
                            $full_response['transactions'][0]['amount']['total'] = $total
                    ) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }

            }

        }
    }

    /**
     * credit card verification
     **/

    private function _credit_card_pay_verification()
    {
        if ($this->request->is('post')) {
            $currency = $this->request->data['Transaction']['currency_code'];
            $total = $this->request->data['Transaction']['amount'];
            $pay_key = $this->request->data['Transaction']['pay_key'];


            $ch = curl_init();
            $this->loadModel('Setting');
            $paypal_info = $this->Setting->findById(1, array('paypal_clientId', 'paypal_secret', 'paypal_mode', 'sandbox_clientId', 'sandbox_secret'));
            if ($paypal_info['Setting']['paypal_mode'] == 'Sandbox') {
                $clientId = $paypal_info['Setting']['sandbox_clientId']; //"clientID";
                $secret = $paypal_info['Setting']['sandbox_secret']; //"secret";
                $url = "https://api.sandbox.paypal.com/v1/payments/payment/$pay_key ";
                $uri = "https://api.sandbox.paypal.com/v1/oauth2/token";
            } elseif ($paypal_info['Setting']['paypal_mode'] == 'Live') {
                $clientId = $paypal_info['Setting']['paypal_clientId']; //"clientID";
                $secret = $paypal_info['Setting']['paypal_secret']; //"secret";
                $url = "https://api.paypal.com/v1/payments/payment/$pay_key ";
                $uri = "https://api.paypal.com/v1/oauth2/token";
            }
            //print_r($clientId); exit;
            curl_setopt($ch, CURLOPT_URL, $uri);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, $clientId . ":" . $secret);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

            $result = curl_exec($ch);
            curl_close($ch);
            // print_r($result);
            // exit;

            if (empty($result)) die("Error: No response.");
            else {
                $json = json_decode($result);
                $access_token = $json->access_token;
                //print_r($access_token);
                $ch2 = curl_init();
                curl_setopt($ch2, CURLOPT_URL, $url);
                // curl_setopt($ch2, CURLOPT_HEADER, true);
                //curl_setopt($ch2, CURLINFO_HEADER_OUT, true);
                curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
                //curl_setopt($ch2, CURLOPT_POST, true);
                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                    'Content-Type:application/json',
                    "Authorization:Bearer " . $access_token));

                $response = curl_exec($ch2);
                $full_response = json_decode($response, true);
                //print_r($full_response);
                curl_close($ch2);
                if ($full_response['state'] == 'approved') {
                    if ($full_response['state'] == 'approved' &&
                        $full_response['transactions'][0]['amount']['currency'] = $currency &&
                            $full_response['transactions'][0]['amount']['total'] = $total &&
                                $full_response['transactions'][0]['related_resources'][0]['sale']['state'] == 'completed'
                    ) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
    }

    /*
    ** verify paypal payment
    */
    private function _verify_payment()
    {
        if ($this->request->is('post')) {
            $app_id = $this->request->data['Transaction']['app_id'];
            $pay_key = $this->request->data['Transaction']['pay_key'];
            $currency = $this->request->data['Transaction']['currency_code'];
            $amount = $this->request->data['Transaction']['amount'];
            $email = $this->request->data['Transaction']['receiver_email'];
            //PayPal API Credentials
            $this->loadModel('Setting');
            $paypal_info = $this->Setting->findById(1, array('paypal_username_api', 'paypal_password', 'sandbox_password', 'paypal_mode', 'sandbox_username_api', 'sandbox_signature', 'paypal_signature'));
            if ($paypal_info['Setting']['paypal_mode'] == 'Sandbox') {
                $url = 'https://svcs.sandbox.paypal.com/AdaptivePayments/PaymentDetails'; //set PayPal Endpoint to sandbox
                $API_UserName = $paypal_info['Setting']['sandbox_username_api'];
                $API_Password = $paypal_info['Setting']['sandbox_password'];
                $API_Signature = $paypal_info['Setting']['sandbox_signature'];
            } elseif ($paypal_info['Setting']['paypal_mode'] == 'Live') {
                $url = 'https://svcs.paypal.com/AdaptivePayments/PaymentDetails'; //set PayPal Endpoint to sandbox
                $API_UserName = $paypal_info['Setting']['paypal_username_api'];
                $API_Password = $paypal_info['Setting']['paypal_password'];
                $API_Signature = $paypal_info['Setting']['paypal_signature'];
            }
            $API_RequestFormat = 'NV';
            $API_ResponseFormat = 'JSON';
// $API_UserName = 'nick.compton11-facilitator_api1.gmail.com';
// $API_Password =  1377163026; 
// $API_Signature = 'AiPC9BjkCyDFQXbSkoZcgqH3hpacAhZf2MyIIevbkLrfudVe3UTElY5j';

            $bodyparams = array(
                'requestEnvelope.errorLanguage' => 'en_US',
                'payKey' => $pay_key
            );

            $body_data = http_build_query($bodyparams, '', chr(38));

            $params = array(
                "http" => array(
                    "method" => 'POST',
                    "content" => $body_data,
                    "header" => 'X-PAYPAL-SECURITY-USERID: ' . $API_UserName . "\r\n" .
                        'X-PAYPAL-SECURITY-SIGNATURE: ' . $API_Signature . "\r\n" .
                        'X-PAYPAL-SECURITY-PASSWORD: ' . $API_Password . "\r\n" .
                        'X-PAYPAL-APPLICATION-ID: ' . @$app_id . "\r\n" .
                            'X-PAYPAL-REQUEST-DATA-FORMAT: ' . $API_RequestFormat . "\r\n" .
                            'X-PAYPAL-RESPONSE-DATA-FORMAT: ' . $API_ResponseFormat . "\r\n"
                )
            );
            //print_r($params); exit;
            $ctx = stream_context_create($params);
            $fp = @fopen($url, 'r', false, $ctx);
            $response = stream_get_contents($fp);

            if ($response === false) {
                throw new Exception('php error message = ' . '$php_errormsg');
            }
            fclose($fp);
            $res = json_decode($response, true);

            //print_r($res); exit;

            if ($res['responseEnvelope']['ack'] == 'Success') {
                if ($res['status'] == 'COMPLETED' &&
                    $res['currencyCode'] = $currency &&
                        $res['paymentInfoList']['paymentInfo'][0]['receiver']['amount'] == $amount &&
                        $res['paymentInfoList']['paymentInfo'][0]['receiver']['email'] == $email &&
                        $res['paymentInfoList']['paymentInfo'][0]['senderTransactionStatus'] == 'COMPLETED'
                ) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * Save Top-up information
     */
    public function do_top_up()
    {
        $this->autoRender = false;
        if ($this->request->is('post') && !empty($this->request->data)) {
            // Check for valid user
            $this->loadModel('User');
            $booking_id = @$this->request->data['Transaction']['booking_id'];
            $service = $this->request->data['Transaction']['service'];
            $short_description = $this->request->data['Transaction']['short_description'];
            $amount = $this->request->data['Transaction']['amount'];

            $user_info = $this->User->findById($this->request->data['Transaction']['user_id']);

            if (empty($user_info['User']['id'])) die(json_encode(array('success' => false, 'message' => __('Invalid User'))));
            // call do_trans function for transaction(not top up)
            if (!empty($booking_id) && $short_description == 'Taxi fare' && $service == 'credit_transfer') {
                //pay driver
                $this->do_trans();
            }
            // if(!empty($booking_id) && $short_description == 'Taxi fare' && $service == 'cash_in_hand') {
            // 	//pay driver
            // 	$this->cash_in_hand();
            // }
            //check for vaild card payment and vaild paypal payment
            if ($service == 'paypal') {
                // $paypal_validity = $this->_verify_payment();    #Old Method
                $validation = $this->_paypal_verify();
            } else if ($service == 'card') {
                $card_validity = $this->_credit_card_pay_verification();
            } elseif ($service == 'new_paypal') {
                $validation = $this->_paypal_verify();
            }
            if (!empty($paypal_validity) || !empty($card_validity) || !empty($validation)) {

                $user_id = $user_info['User']['id'];
                $currency_code = $this->request->data['Transaction']['currency_code'];
                $app_id = $this->request->data['Transaction']['app_id'];
                $pay_key = $this->request->data['Transaction']['pay_key'];
                $payment_exec_status = $this->request->data['Transaction']['payment_exec_status'];
                $timestamp = $this->request->data['Transaction']['timestamp'];
                //add transaction on transaction table
                if ($this->_add_transactions($user_id,'top_up',null,$amount,$currency_code,$service,$app_id,$pay_key,$payment_exec_status,$timestamp)) {
                    //add transaction on wallet table
                    if($this->_add_wallet($user_info['User']['id'],$amount)){
                    // If booking_id belong to request, pay driver
                    if (!empty($booking_id) && $short_description == 'Taxi fare') {
                        //pay driver
                        $this->do_trans();
                    }

//                    if ($user_info['User']['type'] == 'driver') {
//                        $get_balance = $this->Transaction->getDriverBalance($user_info['User']['id']);
//                        //calculation to subtract admin fee from total balance
//                        $free_credit = $get_balance[1][0]['total'];
//                        $total_fee = abs($get_balance[2][0]['total']);
//                        $total = $get_balance[4][0]['total'];
//                        $balance_value = $this->Transaction->calFreeCredit($free_credit, $total_fee, $total);
//                        $remaining_free_credit = $balance_value['remaining_free_credit'];
//                        $balance = $remaining_free_credit + $total;
//                    } else {
                        $get_balance = $this->_get_wallet_info($user_info['User']['id']);
                        $balance = $get_balance['Wallet']['available_balance'];
                   // }

                    die(json_encode(array('success' => true, 'balance' => $balance)));
                    } else{
                        die(json_encode(array('success' => false, 'message' => __('Sorry, Wallet entry failed'))));
                    }
                    //die(json_encode(array('success' => true)));
                } else die(json_encode(array('success' => false, 'message' => __('Sorry, Transaction entry failed'))));
            } else die(json_encode(array('success' => false, 'message' => __('Payment is not completed'))));
        } else die(json_encode(array('success' => false, 'message' => __('Invalid Request'))));
    }

    //calculate promoter's total profit
    public function profit($code = null)
    {
        $this->autoRender = false;
        $this->loadModel('User');
        $user = $this->User->findByCode($code);
        if (!empty($user)) {
            $this->User->recursive = -1;
            $promoter_profit = $this->User->find('all', array(
                //'fields' => array('*'),
                'fields' => array('SUM(IndividualProfit.amount) AS profit', 'User.name', 'User.id'),
                'joins' => array(
                    array(
                        'table' => 'transactions',
                        'alias' => 'IndividualProfit',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'IndividualProfit.short_description' => 'promoter_profit',
                            'IndividualProfit.user_id' => $user['User']['id'],
                            'IndividualProfit.referred_user_id = User.id',
                        )
                    )
                ),
                'conditions' => array(
                    'User.ref_promoter_code' => $user['User']['promoter_code']
                ),
                'group' => 'User.id'
            ));
            $total_profit = 0;
            $data = array();
            foreach ($promoter_profit as $profit) {
                $total_profit += $profit[0]['profit'];
                $data[] = array('user_id' => $profit['User']['id'], 'name' => $profit['User']['name'], 'profit' => $profit[0]['profit']);
            }
            //print_r($data);

            die(json_encode(array('success' => true, 'total_profit' => $total_profit, 'promoter_profit' => $data)));
        } else die(json_encode(array('success' => false, 'message' => __('Invalid User'))));
    }


    //transaction entry for cash payment
    public function cash_in_hand()
    {
        $this->autoRender = false;
        if ($this->request->is('post') && !empty($this->request->data)) {

            // Check for valid user
            $this->loadModel('User');
            //	$user_info = $this->User->findByCode($this->request->data['Transaction']['code']);
            //print_r($user_info['User']['id']);
            //exit;
            //	if(empty($user_info['User']['id'])) die(json_encode(array('success' => false, 'message' => __('Invalid User'))));

            // Check for booking exists
            $this->loadModel('Booking');
            $booking_id = $this->request->data['Transaction']['booking_id'];
            $fare_booking = $this->Booking->findById($booking_id);
            $fare = $fare_booking['Booking']['fare'];

            //	if(!$this->Booking->exists($booking_id) ) die(json_encode(array('success' => false, 'message' => __('Invalid Booking'))));

            // Get booking detail
            $this->Booking->recursive = -1;
            //	$booking = $this->Booking->find('first', array('conditions' => array('Booking.id' => $booking_id)));

            // check if already paid
            //	if($booking['Booking']['is_paid'] == 1) die(json_encode(array('success' => false, 'message' => __('Already paid.'))));

            // if not paid
            $payee_id = $booking['Booking']['acceptor'];

            // Prepare Transaction data and save

            // Update booking record is_paid field to 1
            $this->Booking->updateAll(array('Booking.is_paid' => 1, 'Booking.real_fare' => $fare), array('Booking.id' => $booking_id));
            $service = $this->request->data['Transaction']['service'];
            // Insert transaction record for payee
            $this->request->data['Transaction']['user_id'] = $payee_id;
            $this->request->data['Transaction']['amount'] = $fare;
            $this->request->data['Transaction']['short_description'] = 'taxi_fare';
            $this->request->data['Transaction']['booking_id'] = $booking_id;
            $now = date('Y-m-d');
            $this->request->data['Transaction']['timestamp'] = $now;
            $this->Transaction->create();
            if ($this->Transaction->save($this->request->data, false)) {
                $driver_info = $this->User->findById($payee_id);
                // calculate admin fee
                $this->_fee_admin_entry($driver_info, $fare, $booking_id, $service);
                // calculate promoter fee
                $promoter_of_driver = $this->User->findByPromoterCode($driver_info['User']['ref_promoter_code']);
                if (!empty($promoter_of_driver['User']['id'])) {
                    //pay promoter
                    $this->_pay_promoter($promoter_of_driver, $driver_info['User']['id'], $fare);
                }
            }
            die(json_encode(array('success' => true)));
        } else die(json_encode(array('success' => false, 'message' => __('Invalid Request'))));
    }

    public function req_transfer_amount()
    {
        //	$this->autoRender = false;
        if ($this->request->is('post') && !empty($this->request->data)) {

            // Check for valid user
            $this->loadModel('User');
            $user_info = $this->User->findByCode($this->request->data['Transaction']['code']);
            if (empty($user_info['User']['id'])) die(json_encode(array('success' => false, 'message' => __('Invalid User'))));
            $this->Transaction->recursive = -1;
            $before_req = $this->Transaction->find('first', array(
                'conditions' => array('short_description' => 'transfer_pending', 'user_id' => $user_info['User']['id'])
            ));

            if (!empty($before_req)) die(json_encode(array('success' => false, 'message' => __('Your previous request is under proecess. Please wait.'))));
            // Insert transaction record for requester
            $amount = $this->request->data['Transaction']['amount'];
            $this->request->data['Transaction']['user_id'] = $user_info['User']['id'];
            $this->request->data['Transaction']['short_description'] = 'transfer_pending';
            $this->request->data['Transaction']['service'] = 'transfer_pending';
            $this->request->data['Transaction']['amount'] = '-' . $amount;
            $now = date('Y-m-d');
            $this->request->data['Transaction']['timestamp'] = $now;
            $this->Transaction->create();
            if ($this->Transaction->save($this->request->data, false)) {
                try {
                    $this->_activateEmail($user_info['User']['id']);
                } catch (Exception $e) {

                }
                $this->_bank_details();
            }
            die(json_encode(array('success' => true)));
        } else die(json_encode(array('success' => false, 'message' => __('Invalid Request'))));

    }

    private function _activateEmail($id = null)
    {
        $this->loadModel('Setting');
        $receiver = $this->Setting->findById(1, array('admin_email'));
        $user = $this->User->findById($id);
        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail('smtp');
        $email->subject('Cabbie App — Balance Transfer Request');
        $email->to($receiver['Setting']['admin_email']);
        $email->send("Admin,\nThe driver named " . $user['User']['name'] . " has requested for balance transfer.\nClick the link to show the request http://cabbieappuk.com/admin/transactions/reqtransferamount/pending.\nClick on the link to view full details of balance http://cabbieappuk.com/admin/users/topup/" . $user['User']['id'] . "\nThanks");
    }

    public function admin_reqtransferamount($type = 'transfer_pending')
    {
        $this->Transaction->recursive = 0;
        //$this->set('transactions', $this->paginate());

        $this->paginate = array(
            'limit' => 25,
            'conditions' => array('Transaction.short_description ' => $type),
            'fields' => array('User.id', 'User.name', 'Transaction.short_description', 'Transaction.id', 'Transaction.amount', 'Transaction.timestamp', 'Transaction.note_box'),
            'order' => array('Transaction.created' => 'desc'),
        );
        $transactions = $this->paginate();
        $this->set(compact('transactions', 'type'));
    }

    public function admin_markaspay($id = null)
    {
        $this->autoRender = false;
        $this->Transaction->id = $id;
        if (!$this->Transaction->exists()) {
            throw new NotFoundException(__('Invalid Request'));
        }
        $transfer = $this->Transaction->findById($id, array('amount'));
        $this->request->data['Transaction']['short_description'] = 'transfer_paid';
        $this->request->data['Transaction']['service'] = 'transfer_paid';
        $now = date('Y-m-d');
        $this->request->data['Transaction']['timestamp'] = $now;
        if ($this->Transaction->save($this->request->data, false)) {
            $this->Session->setFlash(__('The Transaction has been marked as paid'), 'default', array('class' => 'alert alert-success text-center'));
            $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('Cound not set marked as paid. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
        }
        return $this->redirect(array('action' => 'reqtransferamount'));
    }

    /*	public function admin_markaspay_delete($id = null) {
            $this->Transaction->id = $id;
            if (!$this->Transaction->exists()) {
                throw new NotFoundException(__('Invalid Transfer Record'));
            }
            $this->request->onlyAllow('post', 'delete');
            if ($this->Transaction->delete()) {
                $this->Session->setFlash(__('Transfer Record deleted'));
                $this->redirect($this->referer());
            }
            $this->Session->setFlash(__('Transfer Record was not deleted'));
            $this->redirect(array('action' => 'reqtransferamount'));
        } */

    private function _bank_details()
    {
        $this->loadModel('User');
        $user = $this->User->findByCode($this->request->data['Transaction']['code']);
        if (($this->request->is('post') || $this->request->is('put')) && !empty($user)) {
            $this->User->id = $user['User']['id'];
            $this->User->save($this->request->data);
        } else die(json_encode(array('success' => false, 'message' => __('Bank Detail could not Be saved.'))));
    }

    // function to show the admin's fee records
    public function admin_paymentrecord()
    {
        $this->Transaction->recursive = 0;
        $this->paginate = array(
            'limit' => 25,
            'conditions' => array('Transaction.service' => 'admin_payment'),
        );
        $records = $this->paginate();
        $sum_amount = $this->Transaction->query("SELECT
			SUM(amount) AS total
		FROM
			transactions
		WHERE
			service = 'admin_payment'");
        $this->set(compact('records', 'sum_amount'));

    }


    public function g($user_id, $limit_or_from = 30, $to = null)
    {
        $this->autoRender = false;
        $user_info = $this->Transaction->User->findById($user_id);
        if (!empty($user_info)) {
            $conditions = array('Transaction.user_id' => $user_id,
                'Transaction.is_paid' => 1,
                "Transaction.short_description IN (
                        'admin_fee',
                        'promoter_fee')");
            if (!empty($limit_or_from) && !empty($to)) {
                $from = $limit_or_from;
                $conditions = am($conditions, array('DATE(Transaction.created) BETWEEN ? AND ? ' => array($from, $to)));
            } else {
                $limit_created = $limit_or_from;
                $conditions = am($conditions, array('TIMESTAMPDIFF(DAY,Transaction.created,NOW()) <=' => $limit_created));
            }
            $transaction_history = $this->Transaction->find('all', array(
                'recursive' => -1,
                //'conditions' => $conditions,
                'fields' => array('Transaction.amount', 'Transaction.short_description', 'Transaction.created'),
                'order' => array('Transaction.created' => 'DESC'),
            ));
            $total_jobs = $this->Transaction->find('count', array(
                'conditions' => $conditions
            ));
            if (!empty($transaction_history)) {
                $transaction_history = Hash::extract($transaction_history, '{n}.Transaction');
                foreach ($transaction_history as $key => $transaction) {
                    $transaction_history[$key]['amount'] = '£' . trim($transaction['amount'], '-');
                    $transaction_history[$key]['created'] = date_format(date_create($transaction['created']), 'd M Y g:i A');
                }
                die(json_encode(array('success' => true, 'total_jobs' => $total_jobs, 'invoice' => $transaction_history)));
            } else die(json_encode(array('success' => false, 'message' => 'No data found.')));
        } else die(json_encode(array('success' => false, 'message' => 'Invalid User')));
    }

    /**
     * we are not use it
     */
    public function ipn()
    {
        $this->autoRender = false;
        $str = "";
        // read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-validate';

        foreach ($_POST as $key => $value) {
            $value = urlencode(stripslashes($value));
            $req .= "&$key=$value";
        }

        // post back to PayPal system to validate
        $header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
        $header .= "Host: www.paypal.com:80\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
        //$fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);
        $fp = fsockopen('tls://www.sandbox.paypal.com', 443, $errno, $errstr, 30);

        // assign posted variables to local variables
        $item_name = $_POST['item_name'];
        $item_number = $_POST['item_number'];
        $payment_status = $_POST['payment_status'];
        $payment_amount = $_POST['mc_gross'];
        $payment_currency = $_POST['mc_currency'];
        $txn_id = $_POST['txn_id'];
        $receiver_email = $_POST['receiver_email'];
        $payer_email = $_POST['payer_email'];
        $userId = $_POST['custom'];
        $uid = 0;


        if (!$fp) {
            // HTTP ERROR
        } else {
            fputs($fp, $header . $req);
            while (!feof($fp)) {
                $res = fgets($fp, 1024);
                if (strcmp($res, "VERIFIED") == 0) {
                } else if (strcmp($res, "INVALID") == 0) {
                }
            }
            fclose($fp);
        }


        $arrInvalidStatus = array('Denied', 'Expired', 'Failed', 'Voided');
        $ifFlag = 0;
        $msg = '';
        $sql = '';

        if (!in_array($payment_status, $arrInvalidStatus)) {
            // save in db for userId
            $data['Transaction']['user_id'] = $userId;
            $data['Transaction']['amount'] = $payment_amount;
            $data['Transaction']['service'] = 'top_up';
            $data['Transaction']['short_description'] = 'top_up';
            $this->Transaction->create();
            $this->Transaction->save($data, false);

        }
    }

    /**
     * @param null $id
     * it's for admin and vender use
     * information about vendor transaction
     */
    public function admin_invoice($id = null){
        if(AuthComponent::user(['type']) == 'vendor'){
            $user_id = AuthComponent::user(['id']);
        } else {
            $user_id = $id;
        }
        $options = [
            'conditions' => [
                'Transaction.user_id' => $user_id
            ],
            'recursive' => -1
        ];
        //$histories = $this->Transaction->find('all',$options);
        //pr($histories);die;
        $this->paginate = $options;
        $histories = $this->paginate('Transaction');
        $user = $this->Transaction->User->find('first',['conditions' =>['User.id' =>$user_id],'fields' =>['User.name'],'recursive' =>-1]);
        $this->set(compact('histories','user'));

    }
    public function wallet_info($user_id){
        $user_wallet = $this->_get_wallet_info($user_id);
        if(!empty($user_wallet)){
           die(json_encode(['success' => true,'balance' =>floatval($user_wallet['Wallet']['available_balance'])]));
        } else {
            die(json_encode(['success' => true,'balance' =>0.00]));
        }
    }

}
