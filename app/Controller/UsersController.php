﻿<?php
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VoiceGrant;

App::uses('AppController', 'Controller');

/**
 * Users Controller
 * UsersZone Controller
 *
 * @property User $User
 * @property UsersZone $UsersZone
 */
class UsersController extends AppController
{

    //public $components = array('RequestHandler');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index($type = 'driver')
    {
        $conditons = [];
        if (AuthComponent::user(['type']) == 'vendor') {
            $conditons = am($conditons, ['User.branch_number' => AuthComponent::user(['branch_number'])]);
        }
        $this->User->recursive = -1;
        $this->paginate = array(
            'limit' => 25,
            'conditions' => am($conditons, array('User.type' => $type)),
            'order' => array('User.name' => 'ASC'),
        );
        $users = $this->paginate();
        $this->set(compact('users', 'type'));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null, $type = 'driver')
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));

    }

    private function _voip_check($number)
    {
        $pos = stripos($number, '+44');
        if ($pos === false) {
            $number = preg_replace('/^0/', '+44', $number, 1);
        }
        return $number;
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add($type = 'driver')
    {

//		Configure::write('debug', 2);
        $type = $this->params['pass'][0];
        if ($this->request->is('post')) {
            //pr($this->request->data);die;
            $this->User->recursive = -1;
            @$email_exist = $this->User->findByEmail($this->request->data['User']['email'], array('id'));
            $this->User->recursive = -1;
            $username_exist = $this->User->findByUsername(@$this->request->data['User']['username'], array('id'));
//            pr($this->request->data);
//            pr($email_exist);
//            pr($username_exist);die;
            if (empty($email_exist) && empty($username_exist)) {
                $mobile_exist = $this->User->findByMobile($this->request->data['User']['mobile'], array('id'));
                if (empty($mobile_exist)) {

//					$code = String::uuid();
//					$this->request->data['User']['code'] = $code;

                    $this->request->data['User']['type'] = $type;
                    if ($this->request->data['User']['type'] == 'vendor') {
                        // $this->request->data['User']['password'] = AuthComponent::password($this->_generate_password(8));
                        $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
                        if (!empty($this->request->data['User']['image']['tmp_name'])) {
                            $this->request->data['User']['image'] = $this->_upload($this->request->data['User']['image'], 'vendor_infos');
                        } else {
                            unset($this->request->data['User']['image']);
                        }
                        if (!empty($this->request->data['User']['vendor_license_image']['tmp_name'])) {
                            $this->request->data['User']['vendor_license_image'] = $this->_upload($this->request->data['User']['vendor_license_image'], 'vendor_infos');
                        } else {
                            unset($this->request->data['User']['vendor_license_image']);
                        }
                        if (!empty($this->request->data['User']['vendor_license_exp_date'])) {
                            $myDateTime = DateTime::createFromFormat('d/m/Y', $this->request->data['User']['vendor_license_exp_date']);
                            $this->request->data['User']['vendor_license_exp_date'] = $myDateTime->format('Y-m-d');
                        }

                        if (!empty($this->request->data['User']['address'])) {
                            $lat_lng = $this->get_lat_lng_by_address($this->request->data['User']['address']);
                            $this->request->data['User']['lat'] = $lat_lng['lat'];
                            $this->request->data['User']['lng'] = $lat_lng['lng'];
                        }
                        //pr($this->request->data);
                        $this->User->create();
                        if ($this->User->save($this->request->data)) {
                            $this->Session->setFlash(__('The vendor has been created. Now set charge rate'), 'default', array('class' => 'alert alert-success text-center'));
                            $this->redirect(array('controller' => 'charges','action' => 'add', $this->User->id));
                        } else {
                            $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
                        }
                    } elseif ($type == 'passenger') {
                        $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
                        if (AuthComponent::user(['type']) == 'admin') {
                            $this->request->data['User']['branch_number'];
                        } elseif (AuthComponent::user(['type']) == 'vendor') {
                            $this->request->data['User']['branch_number'] = AuthComponent::user(['branch_number']);
                        };
                        //echo(AuthComponent::user(['branch_number']).'number');
                        //pr($this->request->data);die;
                        $this->User->create();
                        if ($this->User->save($this->request->data)) {
                            $this->Session->setFlash(__('The Passenger has been created'), 'default', array('class' => 'alert alert-success text-center'));
                            $this->redirect(array('action' => 'index', $type));
                        } else {
                            $this->Session->setFlash(__('The Passenger could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
                        }
                    } else if ($type == 'admin') {
                        $this->request->data['User']['type'] == 'admin';
                        $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
                        $this->User->create();
                        if ($this->User->save($this->request->data)) {
                            $this->Session->setFlash(__('The Admin has been created'), 'default', array('class' => 'alert alert-success text-center'));
                            $this->redirect(array('action' => 'index', $type));
                        } else {
                            $this->Session->setFlash(__('The Admin could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
                        }

                    } else {
                        $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
                        if ($this->request->data['User']['type'] != 'passenger') {
                            // $my_promoter_code = $this->User->create_rand_promoter_code();
                            //$this->request->data['User']['promoter_code'] = $my_promoter_code;
                        }
                        if ($this->request->data['User']['type'] == 'driver') {
                            $this->request->data['User']['lat'] = $this->Session->read('lat');
                            $this->request->data['User']['lng'] = $this->Session->read('lng');
                            $this->request->data['User']['is_enabled'] = 0;
                            $this->request->data['User']['insurance_certificate'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->request->data['User']['insurance_certificate'])));
                            if (AuthComponent::user(['type']) == 'admin') {
                                $this->request->data['User']['branch_number'];
                            } elseif (AuthComponent::user(['type']) == 'vendor') {
                                $this->request->data['User']['branch_number'] = AuthComponent::user(['branch_number']);
                            };
                        }
                        $image_upload_user = $this->_upload($this->request->data['User']['imagex']['name']);
                        if (!empty($image_upload_user)) {
                            $this->request->data['User']['image'] = $image_upload_user;
                        } else {
                            unset($this->request->data['User']['imagex']);
                        }

                        //print_r($this->request->data); exit;

                        $image_upload_badge = $this->_upload($this->request->data['User']['badge_imagex']['name']);
                        if (!empty($image_upload_badge)) {
                            $this->request->data['User']['badge_image'] = $image_upload_badge;
                        } else {
                            unset($this->request->data['User']['badge_imagex']);
                        }

                        $image_upload_insurance = $this->_upload($this->request->data['User']['insurance_certificate_imagex']['name']);
                        if (!empty($image_upload_insurance)) {
                            $this->request->data['User']['insurance_certificate_image'] = $image_upload_insurance;
                        } else {
                            unset($this->request->data['User']['insurance_certificate_imagex']);
                        }
                        $image_upload_vehicle = $this->_upload($this->request->data['User']['vehicle_licence_imagex']['name']);
                        if (!empty($image_upload_vehicle)) {
                            $this->request->data['User']['vehicle_licence_image'] = $image_upload_vehicle;
                        } else {
                            unset($this->request->data['User']['vehicle_licence_imagex']);
                        }


                        $this->request->data['User']['voip_no'] = $this->_voip_check($this->request->data['User']['voip_no']);

                        $this->User->create();
                        if ($this->User->save($this->request->data['User'])) {

                            $this->loadModel('Setting');
                            $data_setting = $this->Setting->findById(1);
                            $id = $this->User->id;
                            if ($this->request->data['User']['type'] == 'driver') {
                                /*Save users Zone*/
                                $users_zones = $this->request->data['Zone']['id'];
                                #pr($this->request->data['Zone']);die;
                                foreach ($users_zones as $item) {
                                    $data['UsersZone'] = array(
                                        'user_id' => $this->User->id,
                                        'zone_id' => $item
                                    );
                                    $this->User->UsersZone->create();
                                    $this->User->UsersZone->save($data);
                                }
                                /*/Save users Zone*/
                                $this->_new_driver_in_queue($id);
                                $balance = $data_setting['Setting']['free_credit_driver'];
                                $this->_add_free_credit($id, $balance); // free credit for new sign up
                            } else if ($this->request->data['User']['type'] == 'passenger') {
                                $balance = $data_setting['Setting']['free_credit_passenger'];
                                $this->_add_free_credit($id, $balance); // free credit for new sign up
                            }

                            if($type == 'vendor'){
                                $this->redirect(array('controller' => 'charges','action' => 'add', $id));
                                $this->Session->setFlash(__('The user has been saved. Please add charge rate'), 'default', array('class' => 'alert alert-success text-center'));
                            }
                            else {
                                $this->redirect(array('action' => 'index', $type));
                                $this->Session->setFlash(__('The user has been saved'), 'default', array('class' => 'alert alert-success text-center'));
                            }

                        } else {
                            $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
                        }
                    }
                } else $this->Session->setFlash(__('The Duplicate Mobile No. Please, Try Another.'), 'default', array('class' => 'alert alert-danger text-center'));
            } else $this->Session->setFlash(__('The Duplicate Email or User ID. Please, Try Another.'), 'default', array('class' => 'alert alert-danger text-center'));
        }

        /*$ref_prom = array();
        $ref_promoters = $this->User->find('list', array('conditions' => array('User.type' => 'admin'), 'fields' => array('User.promoter_code')));
        foreach ($ref_promoters as $ref_promoter) {
            $ref_prom[$ref_promoter] = $ref_promoter;
        }*/
        // Vendor List
        $branches = $this->User->find('all', ['fields' => ['User.company_name', 'User.branch_number'], 'conditions' => ['User.type' => 'vendor'], 'recursive' => '-1']);
        $this->loadModel('Zone');
        $vr_zones = $this->Zone->find('list', array('conditions' => array('Zone.type' => 'vr_zone')));
        $phonecall_zones = $this->Zone->find('list', array('conditions' => array('Zone.type' => 'phonecall_zone')));
        $phonecall_gps_zones = $this->Zone->find('list', array('conditions' => array('Zone.type' => 'phonecall_gps_zone')));
        $this->set(compact('vr_zones', 'phonecall_zones', 'phonecall_gps_zones', 'type', 'branches'));
    }


    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null, $type = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $type = $this->params['pass'][1];
            $this->request->data['User']['type'] = $type;
            if ($this->request->data['User']['type'] == 'vendor') {
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);

                if (!empty($this->request->data['User']['image']['tmp_name'])) {
                    $this->request->data['User']['image'] = $this->_upload($this->request->data['User']['image'], 'vendor_infos');
                } else {
                    unset($this->request->data['User']['image']);
                }

                if (!empty($this->request->data['User']['vendor_license_image']['tmp_name'])) {
                    $this->request->data['User']['vendor_license_image'] = $this->_upload($this->request->data['User']['vendor_license_image'], 'vendor_infos');
                } else {
                    unset($this->request->data['User']['vendor_license_image']);
                }
                if (!empty($this->request->data['User']['vendor_license_exp_date'])) {
                    $myDateTime = DateTime::createFromFormat('d/m/Y', $this->request->data['User']['vendor_license_exp_date']);
                    $this->request->data['User']['vendor_license_exp_date'] = $myDateTime->format('Y-m-d');
                }
                if (!empty($this->request->data['User']['address'])) {
                    $lat_lng = $this->get_lat_lng_by_address($this->request->data['User']['address']);
                    $this->request->data['User']['lat'] = $lat_lng['lat'];
                    $this->request->data['User']['lng'] = $lat_lng['lng'];
                }

                $this->User->id = $id;
                if ($this->User->save($this->request->data)) {
                    $this->Session->setFlash(__('The vendor has been Updated'), 'default', array('class' => 'alert alert-success text-center'));
                    $this->redirect(array('action' => 'index', $type));
                } else {
                    $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
                }
            } else if ($type == 'admin') {
                if (!empty($this->request->data['User']['password'])) {
                    $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
                } else {
                    unset($this->request->data['User']['password']);
                }
                //print_r($this->request->data);die;
                $this->User->id = $id;
                if ($this->User->save($this->request->data)) {
                    $this->Session->setFlash(__('The Admin has been Updated'), 'default', array('class' => 'alert alert-success text-center'));
                    $this->redirect(array('action' => 'index', $type));
                } else {
                    $this->Session->setFlash(__('The Admin could not be changed. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
                }
            } else {
                $email_exist = $this->User->find('first', array(
                    'conditions' => array('User.email' => $this->request->data['User']['email'], 'User.id <>' => $id, 'User.type' => $this->request->data['User']['type']),
                    'fields' => array('User.id'),
                ));
                //print_r($email_exist); exit;
                if (empty($email_exist)) {
                    $mobile_exist = $this->User->find('first', array(
                        'conditions' => array('User.mobile' => $this->request->data['User']['mobile'], 'User.id <>' => $id, 'User.type' => $this->request->data['User']['type']),
                        'fields' => array('User.id'),
                    ));
                    if (empty($mobile_exist)) {
                        if (empty($this->request->data['User']['passwordx'])) {
                            unset($this->request->data['User']['passwordx']);
                        } else {
                            $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['passwordx']);
                        }
                        $this->request->data['User']['insurance_certificate'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->request->data['User']['insurance_certificate'])));
                        $image_upload_user = $this->_upload($this->request->data['User']['imagex']);
                        if (!empty($image_upload_user)) {
                            $optionimg = [
                                'conditions' => [
                                    'User.id' => $id
                                ],
                                'fields' => [
                                    'User.image'
                                ]
                            ];
                            $img = $this->User->find('first', $optionimg);
                            @unlink(WWW_ROOT . '/files/vendor_infos/' . $img['User']['image']);
                            $this->request->data['User']['image'] = $image_upload_user;
                        }
                        $image_upload_badge = $this->_upload($this->request->data['User']['badge_imagex']);
                        if (!empty($image_upload_badge)) {
                            $this->request->data['User']['badge_image'] = $image_upload_badge;
                        }
                        $image_upload_insurance = $this->_upload($this->request->data['User']['insurance_certificate_imagex']);
                        if (!empty($image_upload_insurance)) {
                            $this->request->data['User']['insurance_certificate_image'] = $image_upload_insurance;
                        }
                        $image_upload_vehicle = $this->_upload($this->request->data['User']['vehicle_licence_imagex']);
                        if (!empty($image_upload_vehicle)) {
                            $this->request->data['User']['vehicle_licence_image'] = $image_upload_vehicle;
                        }
                        $this->request->data['User']['voip_no'] = $this->_voip_check($this->request->data['User']['voip_no']);
                        if ($this->User->save($this->request->data)) {
                            $this->Session->setFlash(__('The user has been saved'));
                            $this->redirect(array('action' => 'index', $this->request->data['User']['type']));
                        } else {
                            $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
                            $this->redirect(array('action' => 'index', $this->request->data['User']['type']));
                        }
                    } else $this->Session->setFlash(__('The Duplicate Mobile. Please, Try Another.'));
                } else $this->Session->setFlash(__('The Duplicate Email. Please, Try Another.'));
            }
        } else {
            $this->User->Behaviors->load('Containable');
            $options = array(
                'contain' => array(
                    'Zone' => array(
                        'fields' => 'Zone.id',
                    )
                ),
                'conditions' => array('User.' . $this->User->primaryKey => $id)
            );
            $this->request->data = $this->User->find('first', $options);
            unset($this->request->data['User']['password']);
            $zone_ids = Hash::extract($this->request->data, "Zone.{n}.id");
            //pr($zone_ids);die;
        }
        // /print_r($this->request->data);
        $this->loadModel('Zone');
        $this->Zone->recursive = -1;
        $vr_zones = $this->Zone->find('list', array('conditions' => array('Zone.type' => 'vr_zone')));
        $phonecall_zones = $this->Zone->find('list', array('conditions' => array('Zone.type' => 'phonecall_zone')));
        $phonecall_gps_zones = $this->Zone->find('list', array('conditions' => array('Zone.type' => 'phonecall_gps_zone')));
        $branches = $this->User->find('all', ['fields' => ['User.company_name', 'User.branch_number'], 'conditions' => ['User.type' => 'vendor'], 'recursive' => '-1']);
        $this->set(compact('vr_zones', 'phonecall_zones', 'phonecall_gps_zones', 'zone_ids', 'type', 'branches'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->User->delete()) {
            //$this->loadModel('Transaction');
            $this->User->query('DELETE FROM transactions WHERE user_id = \'' . $id . ' \'
			');
            //$this->loadModel('Queue');
            $this->User->query('DELETE FROM queues WHERE user_id = \'' . $id . ' \'
			');
            //$this->loadModel('PhoneQueue');
            $this->User->query('DELETE FROM phone_queues WHERE user_id = \'' . $id . ' \'
			');
            //$this->loadModel('DeviceToken');
            $this->User->query('DELETE FROM device_tokens WHERE user_id = \'' . $id . ' \'
			');
            $this->Session->setFlash(__('User deleted'));
            $this->redirect($this->referer());
        }
        $this->Session->setFlash(__('User was not deleted'));
        $this->redirect(array('action' => 'index', $this->request->data['User']['type']));
    }


    // function to set is_deleted = 1
    /*public function admin_is_deleted($id = null) {
        $this->autoRender = false;
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid User'));
        }
        $user = $this->User->findById($id);
        $this->request->data['User']['is_deleted'] = 1;
        if ($this->User->save($this->request->data, false)) {
            $this->Session->setFlash(__('The user is deleted.'));
            $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('User was not deleted'));
        }
    //	return $this->redirect(array('action' => 'index'));


    //	$this->Session->setFlash(__('User was not deleted'));
        $this->redirect(array('action' => 'index', $this->request->data['User']['type']));
    }
    */

    //admin login
    public function admin_login()
    {
        $this->layout = 'login';
        if ($this->request->is('post')) {
            //print_r(AuthComponent::password($this->request->data['User']['password']));die;
            //print_r($this->request->data); exit;
            if (($this->Auth->login()) && ($this->Auth->User('type') == 'admin' || $this->Auth->User('type') == 'vendor')) {
                //print_r($this->Auth->user()); exit;
                $this->redirect($this->Auth->redirect());
            } else {
                //print_r(AuthComponent::user()); exit;
                $this->Auth->logout();
                $this->Session->setFlash(__('Incorrect Email or Password.'), 'default', array('class' => 'alert alert-danger text-center'));
            }
        }
    }

    //admin logout
    public function admin_logout()
    {
        $this->redirect($this->Auth->logout());
    }

    // passenger login In v2 not using
    /*public function passenger_login(){
        $this->request->data['User']['type'] = 'passenger';
        $this->_login();
    }*/
    // driver login In v2 not using
    /*public function driver_login(){
        $this->request->data['User']['type'] = 'driver';
        $this->_login();
    }*/

    public function login()
    {
        $this->_login();
    }

    public function logout()
    {
        $this->redirect($this->Auth->logout());

    }

    //users login
    private function _login()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            #die(AuthComponent::password($this->request->data['User']['password']));
            $this->request->data['User']['passwordx'] = AuthComponent::password($this->request->data['User']['password']);
            $time_pre = microtime(true);
            $user = $this->User->find('first', array(
                'conditions' => array(
                    'OR' => array(
                        'User.email' => $this->request->data['User']['login'],
                        'User.mobile' => $this->request->data['User']['login'],
                        'User.username' => $this->request->data['User']['login']
                    ),
                    'User.password' => $this->request->data['User']['passwordx'],
                    //'User.type' => $this->request->data['User']['type']
                ),
                'recursive' => -1
            ));
            //print_r($user);die;
            if (!empty($user)) {
                $this->request->data['User']['email'] = $this->request->data['User']['login'];
                //static user code
                /*if (empty($user['User']['code'])) {
                    $user['User']['code'] = String::uuid();
                    $this->User->id = $user['User']['id'];
                    $this->User->saveField('code', $user['User']['code']);
                }*/
                if (!empty($this->request->data['User']['device_token'])) {
                    if (empty($this->request->data['User']['device_type'])) {
                        $this->request->data['User']['device_type'] = 'android';
                    }
                    $this->_device_token(
                        $user['User']['id'],
                        $this->request->data['User']['device_token'],
                        $this->request->data['User']['device_type'],
                        $this->request->data['Application']['stage'],
                        '1'
                    );
                }

                //device token array
                $devices = array();
                if (!empty($user['DeviceToken'])) {
                    foreach ($user['DeviceToken'] as $device_tokens) {
                        $devices[] = $device_tokens['device_token'];
                    }
                }

                $this->User->id = $user['User']['id'];
                $this->User->saveField('last_activity', date('Y-m-d H:i:s'));

                //$this->User->saveField('device_token', $this->request->data['User']['device_token']);
                $company_name = $this->get_company_name($user['User']['branch_number']);
                if ($user['User']['type'] == 'driver') {
                    //checks for the zones in where diver is permitted
                    //$isdav = $this->isdav($user['User']['code'], 'all', 'yes', true);
                    echo json_encode(array(
                        'success' => true,
                        'User' => array(
                            'id' => $user['User']['id'],
                            'code' => $user['User']['code'],
                            'name' => $user['User']['name'],
                            'promoter_code' => $user['User']['promoter_code'],

                            'ref_promoter_code' => $user['User']['ref_promoter_code'],
                            'email' => $user['User']['email'],
                            'username' => $user['User']['username'],
                            'mobile' => $user['User']['mobile'],
                            'branch_number' => $user['User']['branch_number'],
                            'company_name' => $company_name,

                            'address' => $user['User']['address'],
                            'home_no' => $user['User']['home_no'],
                            'cab_type' => $user['User']['cab_type'],
                            'vehicle_type' => $user['User']['vehicle_type'],
                            'is_wheelchair' => $user['User']['is_wheelchair'],

                            'post_code' => $user['User']['post_code'],
                            'driving_licence_no' => $user['User']['driving_licence_no'],
                            'driving_licence_dateExpiry' => $user['User']['driving_licence_dateExpiry'],

                            'vehicle_licence_no' => $user['User']['vehicle_licence_no'],

                            'badge_no' => $user['User']['badge_no'],
                            'vehicle_licence_image' => !empty($user['User']['vehicle_licence_image']) ? Router::url('/files/driver_infos/' . $user['User']['vehicle_licence_image'], true) : null,
                            'image' => !empty($user['User']['image']) ? Router::url('/files/driver_infos/' . $user['User']['image'], true) : null,
                            'badge_image' => !empty($user['User']['badge_image']) ? Router::url('/files/driver_infos/' . $user['User']['badge_image'], true) : null,
                            'insurance_certificate_image' => !empty($user['User']['insurance_certificate_image']) ? Router::url('/files/driver_infos/' . $user['User']['insurance_certificate_image'], true) : null,
                            'drivers_licence_image_front' => !empty($user['User']['drivers_licence_image_front']) ? Router::url('/files/driver_infos/' . $user['User']['drivers_licence_image_front'], true) : null,
                            'drivers_licence_image_back' => !empty($user['User']['drivers_licence_image_back']) ? Router::url('/files/driver_infos/' . $user['User']['drivers_licence_image_back'], true) : null,

                            'registration_plate_no' => $user['User']['registration_plate_no'],
                            'insurance_certificate' => $user['User']['insurance_certificate'],
                            'type' => $user['User']['type'],
                            'device_token' => $devices,
                            'no_of_seat' => $user['User']['no_of_seat'],
                            'voip_no' => $user['User']['voip_no'],
                            'running_distance' => $user['User']['running_distance'],
                            'is_enabled' => $user['User']['is_enabled'],
                            'account_holder_name' => $user['User']['account_holder_name'],
                            'account_no' => $user['User']['account_no'],
                            'sort_code' => $user['User']['sort_code'],
                            'iban' => $user['User']['iban'],
                            'swift' => $user['User']['swift']
                        ),
                        'isdav' => array(
                            'success' => true,
                            'vr' => false,
                            'vr_position' => null,
                            'vr_zone_id' => null,
                            'vr_zone' => null,
                            'vr_btn' => 'N/A',
                            'phone_gps' => false,
                            'phone_gps_position' => null,
                            'phone_gps_zone_id' => null,
                            'phone_gps_zone' => null,
                            'phone_gps_btn' => 'N/A',
                            'phone' => false,
                            'phone_position' => null,
                            'phone_zone_id' => null,
                            'phone_zone' => null,
                            'phone_btn' => 'N/A',
                            'msg' => ''
                        )
                    ));
                } else {
                    // for log in via brower
                    echo json_encode(array(
                        'success' => true,
                        'User' => array(
                            'id' => $user['User']['id'],
                            'code' => $user['User']['code'],
                            'name' => $user['User']['name'],
                            'username' => $user['User']['username'],
                            'email' => $user['User']['email'],
                            'mobile' => $user['User']['mobile'],
                            'branch_number' => $user['User']['branch_number'],
                            'company_name' => $company_name,
                            'image' => !empty($user['User']['image']) ? Router::url('/files/vendor_infos/' . $user['User']['image'], true) : null,

                            'address' => $user['User']['address'],
                            'home_no' => $user['User']['home_no'],
                            'type' => $user['User']['type'],
                            'device_token' => $devices
                        )
                    ));
                }
                $response = $user['User']['id'];
                // API log
                $this->_apiLog(
                    $user['User']['id'],
                    var_export($this->request->data, true),
                    $response,
                    1,
                    $time_pre);

                // for log in via brower
                $this->Auth->login();
            } else {
                echo json_encode(array(
                    'success' => false,
                    'message' => 'Not valid email or password'
                ));
            }
        }
    }
public function get_company_name($branch_number){
    $company = $this->User->findByBranchNumberAndType($branch_number,'vendor',['company_name']);
    return $company['User']['company_name'];
}
    public function user_logout()
    {
        $this->autoRender = false;
        if ($this->request->is('post') || $this->request->is('put')) {
            $code = $this->request->data['User']['code'];
            $user = $this->User->findByCode($code);
            $this->request->data['User']['id'] = $user['User']['id'];
            $this->request->data['User']['phonecall_available'] = 'no';
            $this->request->data['User']['phonecall_gps_available'] = 'no';
            $this->request->data['User']['vr_available'] = 'no';
            if ($this->User->save($this->request->data, false)) {
                die(json_encode(array('success' => true, 'message' => 'You\'ve been logout')));
            } else {
                die(json_encode(array('success' => true, 'message' => 'Action failed')));
            }
        } else die(json_encode(array('success' => false, 'message' => 'Invalid request')));
    }

    /*public function signup(){

    }*/

    //driver/passenger registration
    public function register()
    {
        //      print_r($this->request->data);die;
        header('Content-Type: application/json');
        $this->autoRender = false;
        $this->autoLayout = false;
//		Configure::write('debug', 2);
        $this->User->recursive = -1;
        if ($this->request->is('post') || $this->request->is('put') && !empty($this->request->data)) {
            //print_r($this->request->data);die;
            if ($this->request->data['User']['type'] == 'driver') {
                $email_check = $this->User->find('first', array(
                    'conditions' => array('User.email' => $this->request->data['User']['email'], 'User.type' => 'driver')
                ));
                $mobile_check = $this->User->find('first', array(
                    'conditions' => array('User.mobile' => $this->request->data['User']['mobile'], 'User.type' => 'driver')
                ));
                $username_check = $this->User->find('first', array(
                    'conditions' => array('User.username' => $this->request->data['User']['username'], 'User.type' => 'driver')
                ));
                if (empty($email_check) && empty($mobile_check) && empty($username_check)) {
                    /*
                    * Check for promoter code validity for driver
                    */
                    //$is_valid_promoter_code = $this->User->check_valid_promoter_code(trim($this->request->data['User']['ref_promoter_code']));
                    /*if(!$is_valid_promoter_code) {
                        die(json_encode(array('success' => false, 'message' => __('Not a valid promoter code.'))));
                    } else {*/
                    // set promoter code
                    /*$my_promoter_code = $this->User->create_rand_promoter_code();
                    $this->request->data['User']['promoter_code'] = $my_promoter_code;*/

                    // set is_enabled = 0 so that new driver can not log-in without admin's permission, by default every passenger is enabled
                    if ($this->request->data['User']['type'] == 'driver') {
                        $this->request->data['User']['is_enabled'] = 0;
                    } else {
                        $this->request->data['User']['is_enabled'] = 1;
                    }
                    $this->_save_user();

                } elseif (!empty($email_check)) {
                    die(json_encode(array('success' => false, 'message' => 'This Email already exists.')));
                } elseif (!empty($username_check)) {
                    die(json_encode(array('success' => false, 'message' => 'This Username already exists.')));
                } else die(json_encode(array('success' => false, 'message' => 'This Mobile already exists.')));

            } elseif ($this->request->data['User']['type'] == 'passenger') {
                $email_check = $this->User->find('first', array(
                    'conditions' => array('User.email' => $this->request->data['User']['email'], 'User.type' => 'passenger')
                ));
                $mobile_check = $this->User->find('first', array(
                    'conditions' => array('User.mobile' => $this->request->data['User']['mobile'], 'User.type' => 'passenger')
                ));
                $username_check = $this->User->find('first', array(
                    'conditions' => array('User.username' => $this->request->data['User']['username'], 'User.type' => 'passenger')
                ));

                if (empty($email_check) && empty($mobile_check) && empty($username_check)) {
                    $this->_save_user();
                } elseif (!empty($email_check)) {
                    die(json_encode(array('success' => false, 'message' => 'This Email already exists.')));
                } elseif (!empty($username_check)) {
                    die(json_encode(array('success' => false, 'message' => 'This Username already exists.')));
                } else die(json_encode(array('success' => false, 'message' => 'This Mobile already exists.')));
            }
        } else die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
    }

    public function sendto_passenger()
    {
        $this->autoRender = false;
        //$this->_sendSms($this->request->data['User']['mobile'], 'CabbieAppUK! - Account created successfully');
        try {
            $this->_sendEmail(
                $this->request->data['User']['email'],
                "Welcome to CabbieAppUK!",
                "Dear " . $this->request->data['User']['name'] . ",<br/>Your account has been created succesfully<br/><br/>Regards,<br/><br/>The CabbieApp Team",
                'welcome',
                array('title' => 'Thanks for registering with CabbieAppUK!')
            );
        } catch (Exception $e) {

        }
    }


    // new driver in queue
    private function _new_driver_in_queue($id)
    {

        $this->loadModel('Queue');
        $data['Queue']['user_id'] = $id;
        $data['Queue']['booking_id'] = null;
        $now = date("Y-m-d H:i:s");
        $data['Queue']['request_time'] = $now;
        $this->Queue->create();
        $this->Queue->save($data, false);
        $this->loadModel('PhoneQueue');
        $phonedata['PhoneQueue']['user_id'] = $id;
        $phonedata['PhoneQueue']['request_time'] = $now;
        $this->PhoneQueue->create();
        $this->PhoneQueue->save($phonedata, false);


    }


    //free credit for new passengers/drivers
    private function _add_free_credit($id, $balance)
    {
        $now = date('Y-m-d');
       if($this->_add_transactions($id,'free_credit',null,$balance,null,free_credit,null,null,null,$now)){
           $this->_add_wallet($id,$balance);
       }
    }


    //edit driver/passenger via app
    public function update($id = null)
    {
        $this->autoRender = false;
        $user = $this->User->findById($id);
        //print_r($user); exit;
        $time_pre = microtime(true);
        $this->User->recursive = -1;
        $email_check = $this->User->find('all', array(
            'conditions' => array('User.id <>' => $user['User']['id'],
                'User.email' => $this->request->data['User']['email'],
                'User.type' => $user['User']['type']),
            'fields' => array('User.id'),
        ));
        $mobile_check = $this->User->find('all', array(
            'conditions' => array('User.id <>' => $user['User']['id'],
                'User.mobile' => $this->request->data['User']['mobile'],
                'User.type' => $user['User']['type']),
            'fields' => array('User.id'),
        ));

        $username_check = $this->User->find('first', array(
            'fields' => array('User.id'),
            'conditions' => array(
                'User.id <>' => $user['User']['id'],
                'User.username' => $this->request->data['User']['username'],
                'User.type' => $user['User']['type']
            )
        ));
        if (($this->request->is('post') || $this->request->is('put')) && !empty($this->request->data) && !empty($user) && empty($email_check) && empty($mobile_check) && empty($username_check)) {
            //print_r($this->request->data);
            $user_type = $user['User']['type'];
            $user_id = $user['User']['id'];
            if (empty($this->request->data['User']['password'])) {
                unset($this->request->data['User']['password']);
            } else {
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
            }
            if (empty($this->request->data['User']['voip_no'])) {
                unset($this->request->data['User']['voip_no']);
            }

            //device token array
            $devices = array();
            if (!empty($user['DeviceToken'])) {
                foreach ($user['DeviceToken'] as $device_tokens) {
                    $devices[] = $device_tokens['device_token'];
                }
            }

            if ($user_type == 'driver') {

                if (!empty($this->request->data['User']['imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['imagex']['tmp_name'])) {
                    $this->request->data['User']['image'] = $this->_upload($this->request->data['User']['imagex']);
                } else {
                    unset($this->request->data['User']['image']);
                }

                if (!empty($this->request->data['User']['badge_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['badge_imagex']['tmp_name'])) {
                    $this->request->data['User']['badge_image'] = $this->_upload($this->request->data['User']['badge_imagex']);
                } else {
                    unset($this->request->data['User']['badge_image']);
                }
                if (!empty($this->request->data['User']['insurance_certificate_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['insurance_certificate_imagex']['tmp_name'])) {
                    $this->request->data['User']['insurance_certificate_image'] = $this->_upload($this->request->data['User']['insurance_certificate_imagex']);

                } else {
                    unset($this->request->data['User']['insurance_certificate_image']);
                }

                if (!empty($this->request->data['User']['vehicle_licence_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['vehicle_licence_imagex']['tmp_name'])) {
                    $this->request->data['User']['vehicle_licence_image'] = $this->_upload($this->request->data['User']['vehicle_licence_imagex']);
                } else {
                    unset($this->request->data['User']['vehicle_licence_image']);
                }
                /*In v2 added this */
                if (!empty($this->request->data['User']['drivers_licence_imagex_front']['tmp_name']) && is_uploaded_file($this->request->data['User']['drivers_licence_imagex_front']['tmp_name'])) {
                    $this->request->data['User']['drivers_licence_image_front'] = $this->_upload($this->request->data['User']['drivers_licence_imagex_front']);
                } else {
                    unset($this->request->data['User']['drivers_licence_image_front']);
                }
                if (!empty($this->request->data['User']['drivers_licence_imagex_back']['tmp_name']) && is_uploaded_file($this->request->data['User']['drivers_licence_imagex_back']['tmp_name'])) {
                    $this->request->data['User']['drivers_licence_image_back'] = $this->_upload($this->request->data['User']['drivers_licence_imagex_back']);
                } else {
                    unset($this->request->data['User']['drivers_licence_image_back']);
                }
                /*if(!empty($this->request->data['User']['insurance_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['insurance_imagex']['tmp_name'])){
                    $this->request->data['User']['insurance_image'] = $this->_upload($this->request->data['User']['insurance_imagex']);
                }*/

                //$this->request->data['User']['voip_no'] = $this->_voip_check($this->request->data['User']['voip_no']);
                if ($user['User']['is_enabled'] == 0) {
                    $this->request->data['User']['first_update'] = 1;
                }
                $this->User->id = $user_id;
                $this->User->save($this->request->data, false);
                $userInfo = am($user['User'], $this->request->data['User']);

                if ($user['User']['is_enabled'] == 0) {
                    $msg = 'Please wait. You will receive a e-mail notification you have been activated';
                    try {
                        //$this->_activateEmail($user_id, true);
                    } catch (Exception $e) {

                    }
                } else {
                    $msg = 'Update Successful';
                }

                $response = json_encode(array(
                    'success' => true,
                    'msg' => $msg,
                    'User' => array(
                        'id' => $userInfo['id'],
                        'code' => $userInfo['code'],
                        'name' => $userInfo['name'],
                        'promoter_code' => $userInfo['promoter_code'],
                        'ref_promoter_code' => $userInfo['ref_promoter_code'],
                        'email' => $userInfo['email'],
                        'mobile' => $userInfo['mobile'],
                        'address' => $userInfo['address'],
                        'home_no' => $userInfo['home_no'],
                        'cab_type' => $userInfo['cab_type'],
                        'vehicle_type' => $userInfo['vehicle_type'],
                        'is_wheelchair' => $userInfo['is_wheelchair'],
                        'vehicle_licence_no' => $userInfo['vehicle_licence_no'],

                        'badge_no' => $userInfo['badge_no'],

                        'registration_plate_no' => $userInfo['registration_plate_no'],
                        'insurance_certificate' => $userInfo['insurance_certificate'],
                        'vehicle_licence_image' => !empty($userInfo['vehicle_licence_image']) ? Router::url('/files/driver_infos/' . $userInfo['vehicle_licence_image'], true) : null,
                        'image' => !empty($userInfo['User']['image']) ? Router::url('/files/driver_infos/' . $userInfo['image'], true) : null,
                        'badge_image' => !empty($userInfo['badge_image']) ? Router::url('/files/driver_infos/' . $userInfo['badge_image'], true) : null,
                        'insurance_certificate_image' => !empty($userInfo['insurance_certificate_image']) ? Router::url('/files/driver_infos/' . $userInfo['insurance_certificate_image'], true) : null,

                        'drivers_licence_image_front' => !empty($userInfo['drivers_licence_image_front']) ? Router::url('/files/driver_infos/' . $userInfo['drivers_licence_image_front'], true) : null,
                        'drivers_licence_image_back' => !empty($userInfo['drivers_licence_image_back']) ? Router::url('/files/driver_infos/' . $userInfo['drivers_licence_image_back'], true) : null,
                        'type' => $userInfo['type'],
                        'device_token' => $devices,
                        'no_of_seat' => $userInfo['no_of_seat'],
                        'voip_no' => $userInfo['voip_no'],

                        'branch_number' => $userInfo['branch_number'],
                        'driving_licence_no' => $userInfo['driving_licence_no'],

                        'driving_licence_dateExpiry' => $userInfo['driving_licence_dateExpiry'],

                        'running_distance' => $userInfo['running_distance'])
                ));

                // API log
                $this->_apiLog(
                    $user_id,
                    var_export($this->request->data, true),
                    $response,
                    1,
                    $time_pre);

                die($response);
                //echo json_encode(array('success' => true, 'User' => array('id' => $userInfo['id'], 'code' => $userInfo['code'],'name' => $userInfo['name'],'promoter_code' => $userInfo['promoter_code'], 'ref_promoter_code' => $userInfo['ref_promoter_code'], 'email' => $userInfo['email'], 'mobile' => $userInfo['mobile'], 'address' => $userInfo['address'], 'home_no' => $userInfo['home_no'], 'cab_type' => $userInfo['cab_type'], 'vehicle_type' => $userInfo['vehicle_type'], 'is_wheelchair' => $userInfo['is_wheelchair'], 'vehicle_licence_no' => $userInfo['vehicle_licence_no'], 'vehicle_licence_image' => $userInfo['vehicle_licence_image'], 'image' => $userInfo['image'], 'badge_no' => $userInfo['badge_no'], 'badge_image' => $userInfo['badge_image'], 'registration_plate_no' => $userInfo['registration_plate_no'], 'insurance_certificate' => $userInfo['insurance_certificate'], 'insurance_certificate_image' => $userInfo['insurance_certificate_image'], 'type' => $userInfo['type'], 'device_token' => $userInfo['device_token'], 'no_of_seat' => $userInfo['no_of_seat'],'voip_no' => $userInfo['voip_no'],'running_distance' => $userInfo['running_distance']) ));
            } else {
                $this->User->id = $user_id;
                $this->User->save($this->request->data, false);
                $userInfo = am($user['User'], $this->request->data['User']);
                echo json_encode(array(
                    'success' => true,
                    'User' => array(
                        'id' => $userInfo['id'],
                        'code' => $userInfo['code'],
                        'name' => $userInfo['name'],
                        'email' => $userInfo['email'],
                        'mobile' => $userInfo['mobile'],
                        'address' => $userInfo['address'],
                        'home_no' => $userInfo['home_no'],
                        'type' => $userInfo['type'],
                        'device_token' => $devices)
                ));
            }
        } elseif (!empty($email_check)) {
            echo json_encode(array(
                'success' => false,
                'msg' => 'Duplicate Email.',
            ));
        } else {
            echo json_encode(array(
                'success' => false,
                'msg' => 'Duplicate Mobile.',
            ));
        }
        exit;
    }

    //forgot password email
    public function iforgot()
    {
        $this->autoRender = false;
        $this->User->recursive = -1;
        $login = $this->request->data['User']['login'];
        if ($this->request->is('post')) {
            $user = $this->User->findByEmailOrMobile($this->request->data['User']['login'], $this->request->data['User']['login'], array('User.id', 'User.email'));
            //print_r($user); exit;
            if (!empty($user)) {
                $forgot_pwd_token = String::uuid();

                $this->User->id = $user['User']['id'];
                $this->User->saveField('forgot_pwd_token', $forgot_pwd_token);

                App::uses('CakeEmail', 'Network/Email');
                $Email = new CakeEmail('smtp');
                $Email->viewVars(array('forgot_pwd_token' => $forgot_pwd_token));
                $Email->template('iforgot');
                $Email->to($user['User']['email']);
                $Email->subject('Reset password');
                $Email->send();
                echo json_encode(array('success' => true));
            } else {
                echo json_encode(array('success' => false, 'msg' => 'Sorry. User Not Found.'));
            }
        }
    }

    //forgot password link
    public function pwreset($forgot_pwd_token = null)
    {
        $this->layout = 'admin';
        $this->User->recursive = -1;
        if ($this->request->is('post')) {
            if (!empty($forgot_pwd_token) && strlen($this->request->data['User']['password']) >= 6
                && $this->request->data['User']['password'] == $this->request->data['User']['confirm_password']
            ) {
                $checkuser = $this->User->findByForgotPwdToken($forgot_pwd_token, array('User.id'));
                if (!empty($checkuser)) {
                    $data['User']['forgot_pwd_token'] = null;
                    $data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
                    $this->User->id = $checkuser['User']['id'];
                    if ($this->User->save($data)) {
                        $this->Session->setFlash(__('Your password is successfully reset. Please go back to your app and login with your new password.'));
                        $this->redirect(array('action' => 'pwreset', 'success'));
                    }
                } else {
                    $this->Session->setFlash(__('Invalid password reset token.'), 'default', array('class' => 'error-message message'));
                    $this->redirect(array('action' => 'pwreset'));
                }
            } else {
                $this->Session->setFlash(__('Password must be at least 6 character and make sure it matches with confirm password.'), 'default', array('class' => 'error-message message'));
                $this->redirect($this->referer());
            }
        }
        $this->set(compact('forgot_pwd_token'));
    }


    //device token added
    private function _device_token($user_id, $device_token, $type, $stage,$is_login)
    {

        $this->User->DeviceToken->recursive = -1;
        $check_device = $this->User->DeviceToken->findByDeviceToken($device_token, array('DeviceToken.id', 'DeviceToken.device_token'));
        //print_r($check_device_token);die;
        if (!empty($check_device)) {
            $check_device_token = array();
            $check_device_token['DeviceToken']['user_id'] = $user_id;
            $check_device_token['DeviceToken']['device_type'] = $type;
            $check_device_token['DeviceToken']['stage'] = empty($stage) ? 'production' : $stage;
            $check_device_token['DeviceToken']['device_token'] = $device_token;
            $check_device_token['DeviceToken']['is_login'] = $is_login;
            $this->User->DeviceToken->id = $check_device['DeviceToken']['id'];
            $this->User->DeviceToken->save($check_device_token);

        } else {
            $this->User->DeviceToken->create();
            $check_device_token = array();
            $check_device_token['DeviceToken']['user_id'] = $user_id;
            $check_device_token['DeviceToken']['device_type'] = $type;
            $check_device_token['DeviceToken']['device_token'] = $device_token;
            $check_device_token['DeviceToken']['is_login'] = $is_login;
            $check_device_token['DeviceToken']['stage'] = empty($stage) ? 'production' : $stage;
            $this->User->DeviceToken->save($check_device_token);
        }
    }

    public function update_device_token()
    {
        $this->autoRender = false;
        $user_id = $this->request->data['User']['id'];
        $device_token = $this->request->data['User']['device_token'];
        $type = $this->request->data['User']['type'];
        $stage = $this->request->data['User']['stage'];
        $old_device_token = $this->request->data['User']['old_device_token'];
        $this->_device_token($user_id, $device_token, $type, $stage,null, $old_device_token);
    }

    //upload image
    private function _upload($file, $folder = 'driver_infos')
    {
        App::import('Vendor', 'phpthumb', array('file' => 'phpthumb' . DS . 'phpthumb.class.php'));
        if (is_uploaded_file($file['tmp_name'])) {
            $img_arr = explode('.', $file['name']);
            $ext = strtolower(array_pop($img_arr));
            if ($ext == 'txt') $ext = 'jpg';
            $fileName = time() . rand(1, 999) . '.' . $ext;
            if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                $uplodFile = WWW_ROOT . 'files' . DS . $folder . DS . $fileName;
                if (move_uploaded_file($file['tmp_name'], $uplodFile)) {
                    $dest_small = WWW_ROOT . 'files' . DS . $folder . DS . 'small' . DS . $fileName;
                    if ($this->_resize($uplodFile, $dest_small)) {
                        return $fileName;
                    } //echo(json_encode(array('filename' => $fileName) ));die;
                    else die(json_encode(array('success' => false, 'msg' => "Image couldn't resize.")));
                }
            }
        }
    }

    //image resize
    private function _resize($src, $dest_small)
    {
        $phpThumb = new phpThumb();
        $phpThumb->resetObject();
        $capture_raw_data = false;
        $phpThumb->setSourceFilename($src);
        $phpThumb->setParameter('w', 200);
        //$phpThumb->setParameter('w', 1184);
        //$phpThumb->setParameter('h', 852);
        $phpThumb->setParameter('h', 150);
        //$phpThumb->setParameter('zc', 1);
        $phpThumb->GenerateThumbnail();
        $phpThumb->RenderToFile($dest_small);
        return true;
    }

    /**
     * Update User's Position
     */
    /*
        public function utrac() {
            $this->autoRender = false;
            if($this->request->is('post') && !empty($this->request->data) && !empty($this->request->data['User']['id'])) {
                // Prevent hack for any field update
                $data['User'] = array(
                'id' => $this->request->data['User']['id'],
                'lat' => $this->request->data['User']['lat'],
                'lng' => $this->request->data['User']['lng'],
                );
                if($this->User->save($data, false)) {
                    // get drivers count if this request from a driver
                    $user_type = $this->User->findById($this->request->data['User']['id'], array('type'));
                    if($user_type['User']['type'] == 'driver'){
                        $driver_count = $this->User->get_available_drivers_around_driver($this->request->data['User']['id'], $this->request->data['User']['lat'], $this->request->data['User']['lng']);
                        die(json_encode(array('success' => true, 'no_of_drivers' => $driver_count)));
                        } else {
                        die(json_encode(array('success' => true)));
                    }
                } else die(json_encode(array('success' => false, 'message' => 'Lat-Lng Update failed.')));
            } else die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
        }
        */

    //searching driver to bar
    public function search_driver()
    {

    }

    /**
     *    Search Drive for Bar Action
     */
    public function sdvr()
    {
        $this->autoRender = false;
        if ($this->request->is('post') && !empty($this->request->data)) {
            $this->User->recursive = -1;

            // Check for Query parameters
            if (!empty($this->request->data['User']['name']) && empty($this->request->data['User']['badge_no'])) unset($this->request->data['User']['badge_no']);
            else if (empty($this->request->data['User']['name']) && !empty($this->request->data['User']['badge_no'])) unset($this->request->data['User']['name']);
            else if (empty($this->request->data['User']['name']) && empty($this->request->data['User']['badge_no'])) die(json_encode(array('success' => false, 'message' => 'Invalid Search')));
            else if (empty($this->request->data['User']['code'])) die(json_encode(array('success' => false, 'message' => 'Invalid Search')));

            $passenger = $this->User->findByCode($this->request->data['User']['code'], array('id'));
            // Get previous barred drivers by this passenger
            //$already_barred = $this->User->Barred->find('all', array('fields' => array('Barred.userId2'), 'conditions' => array('Barred.userId1' => $passenger['User']['id'], 'Barred.who_to_whom' => 'p2d')));

            if (empty($this->request->data['User']['name']) && !empty($this->request->data['User']['badge_no'])) {
                $con = array(
                    'User.type' => 'driver',
                    'User.badge_no LIKE' => '%' . $this->request->data['User']['badge_no'] . '%'
                );
            } else if (empty($this->request->data['User']['badge_no']) && !empty($this->request->data['User']['name'])) {
                $con = array(
                    'User.type' => 'driver',
                    'User.name LIKE ' => '%' . $this->request->data['User']['name'] . '%'
                );
            } else if (!empty($this->request->data['User']['badge_no']) && !empty($this->request->data['User']['name'])) {
                $con = array(
                    'User.type' => 'driver',
                    'OR' => array(
                        'User.name LIKE ' => '%' . $this->request->data['User']['name'] . '%',
                        'User.badge_no LIKE ' => '%' . $this->request->data['User']['badge_no'] . '%'

                    )
                );
            }

            // Find Drivers
            $users = $this->User->find('all', array(
                'fields' => array('User.id', 'User.name', 'User.badge_no', 'User.email', 'User.image'),
                'conditions' => array(
                    'User.type' => 'driver',
                    'User.name LIKE' => '%' . $this->request->data['User']['name'] . '%',
                    'User.badge_no LIKE' => '%' . $this->request->data['User']['badge_no'] . '%'
                ),
                'order' => array('User.name' => 'desc')));

            if (empty($users)) {
                die(json_encode(array('success' => false, 'message' => 'No records found')));
            } else {
                $drivers = array();
                foreach ($users as $user) {
                    $drivers[]['Driver'] = $user['User'];
                }
                die(json_encode(array('success' => true, 'drivers' => $drivers)));
            }
        } else die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
    }

    /**
     *    Search Passengers for Bar action
     */
    public function spsngrs()
    {
        $this->autoRender = false;
        if ($this->request->is('post') && !empty($this->request->data)) {
            /*$this->request->data['User']['name'] = 'tam';
            $this->request->data['User']['mobile'] = '234234';
            $this->request->data['User']['code'] = '528c5d2d-7f8c-48aa-a67b-08a41603f7ec'; */
            $this->User->recursive = -1;
            //print_r($this->request->data);

            // Check for Query parameters
            if (!empty($this->request->data['User']['name']) && empty($this->request->data['User']['mobile'])) unset($this->request->data['User']['mobile']);
            else if (empty($this->request->data['User']['name']) && !empty($this->request->data['User']['mobile'])) unset($this->request->data['User']['name']);
            else if (empty($this->request->data['User']['name']) && empty($this->request->data['User']['mobile'])) die(json_encode(array('success' => false, 'message' => 'Invalid Search')));
            else if (empty($this->request->data['User']['code'])) die(json_encode(array('success' => false, 'message' => 'Invalid Search')));


            $driver_id = $this->User->findByCode($this->request->data['User']['code'], array('id'));

            // Get previous barred passengers by this driver
            //$already_barred = $this->User->Barred->find('all', array('fields' => array('Barred.userId2'), 'conditions' => array('Barred.userId1' => $driver_id['User']['id'], 'Barred.who_to_whom' => 'd2p')));
            if (!empty($this->request->data['User']['mobile']) && empty($this->request->data['User']['name'])) {
                $con = array(
                    'User.type' => 'passenger',
                    'User.mobile LIKE' => '%' . $this->request->data['User']['mobile'] . '%'
                );
            } else if (empty($this->request->data['User']['mobile']) && !empty($this->request->data['User']['name'])) {
                $con = array(
                    'User.type' => 'passenger',
                    'User.name LIKE' => '%' . $this->request->data['User']['name'] . '%'
                );
            } else if (!empty($this->request->data['User']['mobile']) && !empty($this->request->data['User']['name'])) {
                $con = array(
                    'User.type' => 'passenger',
                    'OR' => array(
                        'User.name LIKE' => '%' . $this->request->data['User']['name'] . '%',
                        'User.mobile LIKE' => '%' . $this->request->data['User']['mobile'] . '%'
                    )
                );
            }

            // Find passengers
            $users = $this->User->find('all', array(
                'fields' => array('User.id', 'User.name', 'User.mobile', 'User.email'),
                'conditions' => $con,
                'order' => array('User.name' => 'desc')));

            if (empty($users)) {
                die(json_encode(array('success' => false, 'message' => 'No records found')));
            } else {
                $passengers = array();
                foreach ($users as $user) {
                    $passengers[]['Passenger'] = $user['User'];
                }
                die(json_encode(array('success' => true, 'passengers' => $passengers)));
            }
        } else die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
    }

    /**
     *    Get list of all Bar drivers/passengers
     */
    public function gbard($code = null, $type = 'driver')
    {
        $this->autoRender = false;
        if (empty($code)) die(json_encode(array('success' => false, 'message' => 'Invalid Request')));

        $this->User->recursive = -1;
        $user = $this->User->findByCode($code, array('id', 'type'));
        if (empty($user)) die(json_encode(array('success' => false, 'message' => 'Invalid User')));

        // Switch user type
        $flag = ($type == 'driver') ? 'p2d' : 'd2p';
        $barred = $this->User->Barred->find('all', array('fields' => array('id', 'userId2', 'who_to_whom'), 'conditions' => array('Barred.who_to_whom' => $flag, 'Barred.userId1' => $user['User']['id'])));
        if (empty($barred)) die(json_encode(array('success' => false, 'message' => 'No record found')));
        else {
            $barred_ids = Hash::extract($barred, '{n}.Barred.userId2');
            if ($type == 'driver') {
                $barred_users = $this->User->find('all', array('fields' => array('id', 'name', 'email', 'mobile', 'badge_no', 'code'), 'conditions' => array('User.id' => $barred_ids)));
            } else {
                $barred_users = $this->User->find('all', array('fields' => array('id', 'name', 'email', 'mobile', 'code'), 'conditions' => array('User.id' => $barred_ids)));
            }

            foreach ($barred_users as $index => $user) {
                $barred[$index]['Barred']['User'] = $user['User'];
            }
            die(json_encode(array('success' => true, 'allBars' => $barred)));
        }
    }


    /**
     *    Check the position of an existing driver in queues and a new driver in the users table
     */
    private function _position_phone($user_id = null, $availablilty_type = 'phonecall_available', $zone_id = null)
    {

        $zone_field = str_replace('available', 'zone', $availablilty_type);
        $booking_request_count = $this->User->query("SELECT COUNT(id) AS total_request FROM phone_queues WHERE user_id='$user_id'");
        if ($booking_request_count[0][0]['total_request'] > 0) {

            $position = $this->User->query('
			SELECT
			COUNT(tt.id)+ 1 AS position
			FROM
			(
			SELECT
			t.*
			FROM
			(
			SELECT
			q.*
			FROM
			phone_queues q
			INNER JOIN users u ON (q.user_id=u.id)
			WHERE u.' . $availablilty_type . ' = \'yes\' AND u.is_enabled = 1 AND u.' . $zone_field . ' = \'' . $zone_id . '\'
			ORDER BY
			request_time DESC
			)AS t
			GROUP BY
			t.user_id
			ORDER BY
			t.request_time
			)AS tt
			WHERE
			tt.request_time <(
			SELECT
			MAX(request_time)
			FROM
			phone_queues
			WHERE
			user_id =  \'' . $user_id . ' \'
			)'
            );
            return $position[0][0]['position'];


        } else { // this will never run

            $total_in_queue = $this->User->query('SELECT
			COUNT(DISTINCT q.user_id)AS total
			FROM
			queues q
			INNER JOIN users u ON(
			q.user_id = u.id
			AND u.is_enabled = 1
			AND u.' . $availablilty_type . '= \'yes\'
			AND u.' . $zone_field . ' = \'' . $zone_id . '\'
			)'
            );
        }
        return null;

    }

    private function _position($user_id = null, $availablilty_type = 'vr_available', $zone_id = null)
    {

        //$zone_field = str_replace('available','zone',$availablilty_type);
        $booking_request_count = $this->User->query("SELECT COUNT(id) AS total_request FROM queues WHERE user_id='$user_id'");
        //print_r($user_id);
        if ($booking_request_count[0][0]['total_request'] > 0) {

            $position = $this->User->query("SELECT
			COUNT(tt.id)+ 1 AS position
			FROM
			(
			SELECT
			t.*
			FROM
			(
			SELECT
			q.*
			FROM
			queues q
			INNER JOIN users u ON (q.user_id=u.id)
			WHERE u.vr_available  = 'yes' AND u.is_enabled = '1' AND u.vr_zone = '$zone_id'
			ORDER BY
			request_time DESC
			)AS t
			GROUP BY
			t.user_id
			ORDER BY
			t.request_time
			)AS tt
			WHERE
			tt.request_time <(
			SELECT
			MAX(request_time)
			FROM
			queues
			WHERE
			user_id =  '$user_id'
			)"
            );
            return $position[0][0]['position'];


        } else { // this will never run

            //print_r('what the hell???');

            $total_in_queue = $this->User->query('SELECT
			COUNT(DISTINCT q.user_id)AS total
			FROM
			queues q
			INNER JOIN users u ON(
			q.user_id = u.id
			AND u.is_enabled = 1
			AND u.' . $availablilty_type . '= \'yes\'
			AND u.' . $zone_field . ' = \'' . $zone_id . '\'
			)'
            );
        }
        return null;

    }


    private function _zone_info($type_zone, $lat = null, $lng = null, $user_id)
    {

        if (empty($lat) && empty($lng)) {
            return null;
        }
        $conditions = array(
            "CONTAINS (GEOMFROMTEXT(Zone.polygon), GEOMFROMTEXT('POINT($lat $lng)'))",
            'Zone.type' => $type_zone
        );
        if ($type_zone == 'vr_zone') {
            $join = array(
                'table' => 'users_zones',
                'alias' => 'UsersZone',
                'type' => 'LEFT',
                'conditions' => array('UsersZone.user_id = \'' . $user_id . '\' AND Zone.id = UsersZone.zone_id')
            );
            $conditions['UsersZone.id'] = null;
        } else { //phonecall_gps_zone
            $join = array(
                'table' => 'users_zones',
                'alias' => 'UsersZone',
                'type' => 'INNER',
                'conditions' => array('UsersZone.user_id = \'' . $user_id . '\' AND Zone.id = UsersZone.zone_id')
            );
        }
        $this->loadModel('Zone');
        $zones = $this->Zone->find('all', array(
                'recursive' => -1,
                'joins' => array($join),
                'conditions' => $conditions,
                'fields' => array(
                    'Zone.id', 'Zone.polygon'
                )
            )
        );
        foreach ($zones as $key => $zone) {
            $points_string = str_replace(array('POLYGON', '(', ')', ','), array('', '', '', ','), $zone['Zone']['polygon']);
            $points = explode(',', $points_string);
            $result = $this->_inside_zone($points, $lat, $lng);
            if ($result) {
                $id = $zone['Zone']['id'];
                break;
            }
        }
        if ($result) {
            return $id;
        } else {
            return null;
        }

    }

    /**
     *    Check if driver available and return driver's position
     */
    public function isdav($code = null, $availability_type = null, $set_driver_availability = null, $return = false)
    {
        //Configure::write('debug', 2);
        $this->autoRender = false;
        if (empty($code)) die(json_encode(array('success' => true, 'message' => 'Invalid Request')));
        $now = date("Y-m-d H:i:s");
        $time_pre = microtime(true);
        $this->User->recursive = -1;
        $user = $this->User->findByCode($code);
        $user_id = $user['User']['id'];

        $this->User->id = $user_id; // for update

        $driver_zone = $user['User']['vr_zone'];
        $driver_zone_phone = $user['User']['phonecall_zone'];
        $driver_zone_gps = $user['User']['phonecall_gps_zone'];
        if ($user['User']['type'] == 'driver') {

            //check if the driver / passenger is enabled or not
            if ($user['User']['is_enabled'] == 0) {
                $response = array(
                    'success' => false,
                    'message' => 'You are not enabled yet. Please wait. You will be notified after activation. Thank You.'
                );
            } else {

                if ($this->request->is('post') && !empty($this->request->data)) {
                    // /print_r('expression'); exit;
                    $lat = $this->request->data['User']['lat'];
                    $lng = $this->request->data['User']['lng'];

                    $user_data = array();
                    if ($this->request->data['User']['is_vr'] == 1) {
                        //print_r('is_vr'); exit;
                        $zone_id = $this->_zone_info('vr_zone', $lat, $lng, $user_id);
                        $user_data = array('User' => array(
                            //'vr_available'=>empty($zone_id) ? 'no' : 'yes',
                            'vr_available' => 'yes',
                            'vr_zone' => $zone_id,
                            'lat' => $lat,
                            'lng' => $lng,
                            'last_activity' => $now
                        ));
                        $this->User->save($user_data);
//						$this->_position_driver($zone_id);
//						$update_queue = $this->User->query('UPDATE queues SET response = "ignore" WHERE id = "$user['User']['id']" AND response = "timeout"');
                    }
                    if ($this->request->data['User']['is_phone_gps'] == 1) {
                        $zone_id = $this->_zone_info('phonecall_gps_zone', $lat, $lng, $user_id);
                        $user_data = array('User' => array(
                            //'phonecall_gps_available'=>empty($zone_id) ? 'no' : 'yes',
                            'phonecall_gps_available' => 'yes',
                            'phonecall_gps_zone' => $zone_id,
                            'lat' => $lat,
                            'lng' => $lng,
                            'last_activity' => $now
                        ));
                        $this->User->save($user_data);
                    }
                    if ($this->request->data['User']['is_phone'] == 1) {
                        if (!empty($user['User']['phonecall_zone']))
                            $user_data = array('User' => array('phonecall_available' => 'yes',
                                'lat' => $lat,
                                'lng' => $lng,
                                'last_activity' => $now));
                        else {
                            $msg = 'You are not assigned to any phone-call zone.';
                            $user_data = array('User' => array('phonecall_available' => 'no',
                                'lat' => $lat,
                                'lng' => $lng));
                        }
                        $this->User->save($user_data);
                    }
                    $user_data['User']['lat'] = $lat;
                    $user_data['User']['lng'] = $lng;
                    $user_data['User']['last_activity'] = $now;
                    $this->User->save($user_data);

                }

                if (!empty($set_driver_availability) && in_array(trim($set_driver_availability), array('yes', 'no'))) {
                    //print_r('step 2'); exit;
                    $lat = $lng = null;
                    if ($this->request->is('post') && !empty($this->request->data)) {
                        $lat = $this->request->data['User']['lat'];
                        $lng = $this->request->data['User']['lng'];
                    }
                    $user_data = array();
                    switch ($availability_type) {
                        case 'vr':
                            $zone_id = $this->_zone_info('vr_zone', $lat, $lng, $user_id);
                            $user_data = array('User' => array(
                                //'vr_available'=>empty($zone_id) ? 'no' : trim($set_driver_availability),
                                'vr_available' => trim($set_driver_availability),
                                'vr_zone' => $zone_id
                            ));
                            break;
                        case 'phone_gps':
                            $zone_id = $this->_zone_info('phonecall_gps_zone', $lat, $lng, $user_id);
                            $user_data = array('User' => array(
                                //'phonecall_gps_available'=>empty($zone_id) ? 'no' : trim($set_driver_availability),
                                'phonecall_gps_available' => trim($set_driver_availability),
                                'phonecall_gps_zone' => $zone_id
                            ));
                            break;
                        case 'phone':
                            if (!empty($user['User']['phonecall_zone']))
                                $user_data = array('User' => array('phonecall_available' => trim($set_driver_availability)));
                            else {
                                $msg = 'You are not assigned to any phone-call zone.';
                                $user_data = array('User' => array('phonecall_available' => 'no'));
                            }
                            break;
                        case 'all':
                        default:
                            $vr_zone_id = $this->_zone_info('vr_zone', $lat, $lng, $user_id);
                            $phonecall_gps_zone_id = $this->_zone_info('phonecall_gps_zone', $lat, $lng, $user_id);
                            $user_data = array('User' => array(
                                //'vr_available'=>empty($vr_zone_id) ? 'no' : trim($set_driver_availability),
                                'vr_available' => trim($set_driver_availability),
                                'vr_zone' => $vr_zone_id,
                                //'phonecall_gps_available'=>empty($phonecall_gps_zone_id) ? 'no' : trim($set_driver_availability),
                                'phonecall_gps_available' => trim($set_driver_availability),
                                'phonecall_gps_zone' => $phonecall_gps_zone_id,
                                'phonecall_available' => empty($user['User']['phonecall_zone']) ? 'no' : trim($set_driver_availability)
                            ));
                            break;
                    }
                    if (!empty($lat) && !empty($lng)) {
                        $user_data['User']['lat'] = $lat;
                        $user_data['User']['lng'] = $lng;
                        $user_data['User']['last_activity'] = $now;
                    }

                    //$this->User->id = $user_id;
                    $this->User->save($user_data);
                }

                //update user's position and send push to positioned changed drivers
                $this->_position_driver($driver_zone);
                $this->_position_driver_phone($driver_zone_phone);
                $this->_position_driver_gps($driver_zone_gps);

                $user = $this->User->find('first', array(
                    'fields' => array('*'),
                    'joins' => array(array(
                        'table' => 'zones',
                        'alias' => 'VRZone',
                        'type' => 'LEFT',
                        'conditions' => array('User.vr_zone = VRZone.id')
                    ), array(
                        'table' => 'zones',
                        'alias' => 'PhoneGPSZone',
                        'type' => 'LEFT',
                        'conditions' => array('User.phonecall_gps_zone = PhoneGPSZone.id')
                    ), array(
                        'table' => 'zones',
                        'alias' => 'PhoneZone',
                        'type' => 'LEFT',
                        'conditions' => array('User.phonecall_zone = PhoneZone.id')
                    )),
                    'conditions' => array('User.code' => $code)
                ));
                $vr_available = $user['User']['vr_available'] == 'yes' ? true : false;
                $vr_position = $vr_available ? $this->_position($user_id, 'vr_available', $user['User']['vr_zone']) : null;

                $phonecall_gps_available = $user['User']['phonecall_gps_available'] == 'yes' ? true : false;
                $phonecall_gps_position = $phonecall_gps_available ? $this->_position_phone($user_id, 'phonecall_gps_available', $user['User']['phonecall_gps_zone']) : null;

                $phonecall_available = $user['User']['phonecall_available'] == 'yes' ? true : false;
                $phonecall_position = $phonecall_available ? $this->_position_phone($user_id, 'phonecall_available', $user['User']['phonecall_zone']) : null;

                if ($user['User']['vr_available'] == 'yes') {
                    $update_queue = $this->User->query("UPDATE queues SET response = 'ignored' WHERE user_id = '$user_id' AND response = 'timeout'");
                    //	$this->_position_driver($user['User']['vr_zone']);
                }
                if ($user['User']['phonecall_available'] == 'yes') {
                    $update_queue = $this->User->query("UPDATE phone_queues SET status = 'ignored' WHERE user_id = '$user_id' AND status IS NULL AND completed IS NULL AND DATE_ADD(created, INTERVAL 1 MINUTE)< '$now'");
                    //	$this->_position_driver($user['User']['vr_zone']);
                }
                if ($user['User']['phonecall_gps_available'] == 'yes') {
                    $update_queue = $this->User->query("UPDATE phone_queues SET status = 'ignored' WHERE user_id = '$user_id' AND status IS NULL AND completed IS NULL AND DATE_ADD(created, INTERVAL 1 MINUTE)< '$now'");
                    //	$this->_position_driver($user['User']['vr_zone']);
                }

                if ($driver_zone != $user['User']['vr_zone'] && $vr_available == 'yes') { //checks if the driver has moved into another zone
                    //update user's position and send push to positioned changed drivers
                    $this->_position_driver($user['User']['vr_zone']);
                }

                if ($driver_zone != $user['User']['phonecall_gps_zone'] && $phonecall_gps_available == 'yes') { //checks if the driver has moved into another zone
                    //update user's position and send push to positioned changed drivers
                    $this->_position_driver_gps($user['User']['phonecall_gps_zone']);
                }

//print_r(var_export($this->request->data, true)); exit;

                $response = array(
                    'success' => true,
                    'vr' => $vr_available,
                    'vr_position' => $vr_position,
                    'vr_zone_id' => $user['VRZone']['id'],
                    'vr_zone' => $user['VRZone']['name'],
                    'vr_btn' => (empty($user['VRZone']['name']) ? 'Nearest Driver' : $user['VRZone']['name']) . ((empty($user['VRZone']['name']) || empty($vr_position)) ? '' : ' - P' . $vr_position),
                    'phone_gps' => $phonecall_gps_available,
                    'phone_gps_position' => $phonecall_gps_position,
                    'phone_gps_zone_id' => $user['PhoneGPSZone']['id'],
                    'phone_gps_zone' => $user['PhoneGPSZone']['name'],
                    'phone_gps_btn' => (empty($user['PhoneGPSZone']['name']) ? 'N/A' : $user['PhoneGPSZone']['name']) . ((empty($user['PhoneGPSZone']['name']) || empty($phonecall_gps_position)) ? '' : ' - P' . $phonecall_gps_position),
                    'phone' => $phonecall_available,
                    'phone_position' => $phonecall_position,
                    'phone_zone_id' => $user['PhoneZone']['id'],
                    'phone_zone' => $user['PhoneZone']['name'],
                    'phone_btn' => (empty($user['PhoneZone']['name']) ? 'N/A' : $user['PhoneZone']['name']) . (empty($phonecall_position) ? '' : ' - P' . $phonecall_position),
                    'msg' => ''
                );
            }
            // API log
            $this->_apiLog(
                $user_id,
                var_export($this->request->data, true),
                var_export($response, true),
                1,
                $time_pre);

            if ($return)
                return $response;
            else
                die(json_encode($response));
        } else die(json_encode(array('success' => false, 'message' => 'You are not a driver.')));
    }

    // private function _push($device_token, $message, $app){
    // 	App::import('Vendor', 'UA', array('file' => 'UA' . DS . 'push.php'));
    // 	$sendPush = new SendPush();
    // 	$sendPush->push($device_token, $message, $app);
    // }

    /**
     *    Emergency button
     */
    public function emergency($is_over = 0)
    {
        $this->autoRender = false;
        if ($this->request->is('post') && !empty($this->request->data)) {
            $driver = array();
            $driver_id = $this->request->data['User']['id'];
            $driver_info = $this->User->findById($driver_id);
            $lat = $this->request->data['User']['lat'];
            $lng = $this->request->data['User']['lng'];
            $address = (strlen($this->request->data['User']['address']) > 50) ? substr($this->request->data['User']['address'], 0, 50) . '...' : $this->request->data['User']['address'];
            $driver['name'] = $driver_info['User']['name'];
            $driver['address'] = $address;
            $driver['lat'] = $lat;
            $driver['lng'] = $lng;
            $driver['mobile'] = $driver_info['User']['mobile'];

            $available_drivers = $this->User->get_available_drivers_around_driver($driver_id, $lat, $lng, 1, false);
            $is_over = trim($is_over);
            if (!empty($available_drivers)) {
                // $device_tokens = Hash::extract($available_drivers, '{n}.User.id');
                // foreach($device_tokens as $device_token) {
                // 	$devices[] = $device_token;
                // }
                $driver_ids = Hash::extract($available_drivers, '{n}.User.id');
                foreach ($driver_ids as $id) {
                    if ($is_over == 1 || $is_over == 'cancel') {
                        $this->_push($id, 'I am ' . $driver_info['User']['name'] . '(' . $driver_info['User']['mobile'] . ') was in emergency at ' . $address . '.Right now emergency is over.', 'CabbieCall', null, null, null, null, null, null, null, null, null, null, true, $driver_id);
                    } else {
                        $this->_push($id, 'I am ' . $driver_info['User']['name'] . 'in danger, at: ' . $address . '. Please help me.', 'CabbieCall', null, null, null, null, null, null, null, null, null, null, true, $driver_id);
                    }
                }

                die(json_encode(array('success' => true, 'danger' => $driver)));
            } else die(json_encode(array('success' => false, 'message' => __('No Driver to send notification'))));
        } else die(json_encode(array('success' => false, 'message' => __('Invalid request'))));
    }

    //locate driver in emergency by other drivers
    public function driver_in_danger_info($id, $info = null)
    {
        $this->autoRender = false;
        $driver_id = $id;
        $driver_info = $this->User->findById($driver_id);
        $driver['name'] = $driver_info['User']['name'];
        $driver['lat'] = $driver_info['User']['lat'];
        $driver['lng'] = $driver_info['User']['lng'];
        $driver['mobile'] = $driver_info['User']['mobile'];
        if ($info == 'info') die(json_encode(array('success' => true, 'danger' => $driver)));
        if ($info == 'coordinate') die(json_encode(array('success' => true, 'lat' => $driver['lat'], 'lng' => $driver['lng'])));
    }


    /**
     *    Get User's active balance
     */
    public function guab($code)
    {
        $this->autoRender = false;
        if (empty($code)) die(json_encode(array('success' => false, 'message' => __('Invalid request'))));
        $user = $this->User->findByCode($code, array('id', 'type'));
        if (empty($user['User']['id'])) die(json_encode(array('success' => false, 'message' => __('Invalid User'))));
        //$active_balance = $this->User->getUserActiveBalance($user['User']['id']);
        $this->loadModel('Transaction');
        if ($user['User']['type'] == 'driver') {
            $balance = $this->Transaction->getDriverBalance($user['User']['id']);
            //pr($balance);
        } else {
            $balance = $this->Transaction->getPassengerBalance($user['User']['id']);
        }
        //print_r($balance);
        //$earning = empty($balance[2][0]['total']) ? 0 : $balance[2][0]['total'];
        $this->loadModel('Setting');
        $min = $this->Setting->findById(1);
        $min_transferable_balance = $min['Setting']['balance_trans'];
        $driver_msg = $min['Setting']['driver_msg'];
        $free_credit = $balance[1][0]['total'];
        $total_fee = abs($balance[2][0]['total']);
        $total = $balance[4][0]['total'];
        $balance_value = $this->Transaction->calFreeCredit($free_credit, $total_fee, $total);
        $remaining_free_credit = $balance_value['remaining_free_credit'];
        $total = empty($balance_value['total']) ? 0 : $balance_value['total'];
        if ($user['User']['type'] == 'driver') {
            echo json_encode(array('success' => true, 'balance' => $total, 'min_transferable_balance' => $min_transferable_balance, 'driver_msg' => $driver_msg, 'free_credit' => $remaining_free_credit));
        } else {
            echo json_encode(array('success' => true, 'balance' => $balance[0][0]['total']));
        }
    }

    /**
     *    Verify user's number via Twilio
     */

    private function _twilio_verify($number, $name)
    {
        App::import('Vendor', 'Twilio', array('file' => 'Twilio' . DS . 'Twilio.php'));
        $sid = 'ACced6e5e9b0ba4d6dabf280cf2bd05b64';
        $token = '7c36c41e156b77ef4c40db7bca2ed432';
        $client = new Services_Twilio($sid, $token);
        $caller_id = $client->account->outgoing_caller_ids->create($number, array(
            "FriendlyName" => $name . "\'s Number"
        ));
        return $caller_id->sid;
    }

    public function vun()
    {
        $this->autoRender = false;
        if ($this->request->is('post') && !empty($this->request->data)) {
            $number = $this->request->data['User']['number'];
            $name = $this->request->data['User']['name'];
            $sid = $this->_twilio_verify($number, $name);
            die(json_encode(array('success' => true, 'sid' => $sid)));
        } else die(json_encode(array('success' => false, 'message' => __('Invalid Request.'))));
    }

    // view of user's top up record

    public function admin_topup($user_id = null, $limit_or_from = 30, $to = null)
    {
        // /Configure::write('debug', 2);
        if (!$this->User->exists($user_id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->User->useTable = 'transactions';
        //$this->User->recursive = -1;

        if (!empty($limit_or_from) && !empty($to)) {
            $from = $limit_or_from;
            $conditions = array('Transaction.is_paid' => 1, 'User.id' => $user_id, 'DATE(Transaction.created) BETWEEN ? AND ? ' => array($from, $to));
        } else {
            $limit_created = $limit_or_from;
            $conditions = array('Transaction.is_paid' => 1, 'User.id' => $user_id, 'TIMESTAMPDIFF(DAY,Transaction.created,NOW()) <=' => $limit_created);
        }

        $this->paginate = array(
            'limit' => 25,
            'joins' => array(
                array(
                    'table' => 'transactions',
                    'alias' => 'Transaction',
                    'type' => 'left',
                    'foreignKey' => false,
                    'conditions' => array('User.id = Transaction.user_id')
                ), array(
                    'table' => 'users',
                    'alias' => 'ReferredUser',
                    'type' => 'left',
                    'foreignKey' => false,
                    'conditions' => array('Transaction.referred_user_id = ReferredUser.id')
                )),
            'conditions' => $conditions,
            'fields' => array('Transaction.*', 'User.name', 'User.id',
                'User.type', 'User.account_holder_name',
                'User.account_no', 'User.swift', 'User.iban', 'User.sort_code', 'ReferredUser.name', 'ReferredUser.id'),
            'order' => array('Transaction.created' => 'desc'),
        );
        $user = $this->User->find('first', array(
            'recursive' => -1,
            'conditions' => array('User.id' => $user_id),
            'fields' => array('type', 'admin_percentage', 'name'),
        ));
        if ($user['User']['type'] == 'driver') {
            $sum_amount = $this->User->Transaction->getDriverBalance($user_id);
        } else {
            $sum_amount = $this->User->Transaction->getPassengerBalance($user_id);
        }
        $free_credit = $sum_amount[1][0]['total'];
        $total_fee = abs($sum_amount[2][0]['total']);
        $actual_total = $sum_amount[0][0]['total'];
        $actual_balance = $this->User->Transaction->calFreeCredit($free_credit, $total_fee, $actual_total);
        $available_total = $sum_amount[4][0]['total'];
        $available_balance = $this->User->Transaction->calFreeCredit($free_credit, $total_fee, $available_total);
        $transactions = $this->paginate();
        $this->set(compact('transactions'));
        $this->set(compact('user_id', 'limit_created', 'from', 'to', 'sum_amount', 'actual_balance', 'available_balance', 'user'));
    }

    //give free credit to any user

    public function admin_freetoup($id)
    {

        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->loadModel('Transaction');
            $short_description = $this->request->data['Transaction']['short_description'];
            $service = $this->request->data['Transaction']['service'];
            $amount = $this->request->data['Transaction']['amount'];
            $timestamp = date('Y-m-d');
            $this->Transaction->create();
            if ($this->_add_transactions($id,$short_description,null,$amount,null,$service,null,null,null,$timestamp)) {
                $this->_add_wallet($id,$amount);
                $this->Session->setFlash(__('Free Credit has been given successfully.'), 'default', array('class' => 'alert alert-success text-center'));
                $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('Free Credit could not be saved. Please, try again.'));
            }
        }
        $this->User->recursive = -1;
        $user = $this->User->find('all', array(
            'conditions' => array('User.id' => $id),
            'fields' => array('User.name', 'User.type')
        ));
        $this->set(compact('user'));
    }

    //enable new driver
    public function admin_enable_driver($id = null)
    {
        $this->autoRender = false;
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid User'));
        }
        $user = $this->User->findById($id, array('email', 'name', 'type'));
        $this->request->data['User']['is_enabled'] = 1;
        $this->request->data['User']['first_update'] = 0;
        if ($this->User->save($this->request->data, false)) {
            //print_r($user['User']['email']);
            //exit;
            try {
                $this->_sendEmail(
                    $user['User']['email'],
                    'Cabbie App',
                    "Dear " . $user['User']['name'] . ",<br/><br/>Your account has been activated.<br/><br/>Thanks.<br/><br/>Admin",
                    'default'
                );
            } catch (Exception $e) {

            }

            if ($user['User']['type'] == 'driver') {
                $this->_push($id, 'Your account has been activated.', 'CabbieCall');
            } else {
                $this->_push($id, 'Your account has been activated.', 'CabbieApp');
            }
            $this->Session->setFlash(__('The user has been enabled.'));
            $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The user could not be enabled. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    //disable any user
    public function admin_disable_driver($id = null)
    {
        $this->autoRender = false;
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid User'));
        }
        $user = $this->User->findById($id, array('email', 'name', 'type'));
        $this->request->data['User']['is_enabled'] = 0;
        $this->request->data['User']['phonecall_available'] = 'no';
        $this->request->data['User']['phonecall_gps_available'] = 'no';
        $this->request->data['User']['vr_available'] = 'no';
        $this->request->data['User']['vr_zone'] = null;
        $this->request->data['User']['phonecall_gps_zone'] = null;
        $this->request->data['User']['phonecall_zone'] = null;

        if ($this->User->save($this->request->data, false)) {
            //print_r($user['User']['email']);
            //exit;
            try {
                $this->_sendEmail(
                    $user['User']['email'],
                    'Cabbie App',
                    "Dear " . $user['User']['name'] . ",<br></br>Your account has been deactivated.<br/><br/>Thanks.<br/><br/>Admin",
                    'default'
                );
            } catch (Exception $e) {

            }

            //print_r($user['User']['email']);
            if ($user['User']['type'] == 'driver') {
                $this->_push($id, 'Your account has been deactivated. Please contact Admin.', 'CabbieCall');
            } else {
                $this->_push($id, 'Your account has been deactivated. Please contact Admin.', 'CabbieApp');
            }
            $this->Session->setFlash(__('The user has been disabled.'));
            $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The user could not be disabled. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    // send email to the admin when new driver sign up
    private function _activateEmail($id = null, $is_update = null)
    {
        $this->loadModel('Setting');
        $receiver = $this->Setting->findById(1, array('admin_email'));
        $user = $this->User->findById($id);
        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail('smtp');
        $email->subject('Cabbie App — Activate new Driver');
        $email->to($receiver['Setting']['admin_email']);
        $path = Router::url(array('controller' => 'users', 'action' => 'index', 'admin' => true, 'driver'), true);
        if ($is_update) {
            $email->send("Admin, <br/> Please activate the driver named " . $user['User']['name'] . ".<br/> The driver has updated 'Edit My Details'. <br/> To activate, please click on " . $path . "<br/> Thanks");
        } else {
            $email->send("Admin, <br/> Please activate the driver named " . $user['User']['name'] . ".<br/> To activate, please click on " . $path . "<br/> Thanks");
        }

    }

    public function req_fr_promoter()
    {
        $this->autoRender = false;
        $promoter = $this->User->find('first', array(
            'conditions' => array('User.type' => 'admin'),
            'fields' => array('User.promoter_code')
        ));
        die(json_encode(array('success' => true, 'promoter' => $promoter['User']['promoter_code'])));
    }

    //public function test($type_zone, $lat=null, $lng=null, $user_id) {
    public function test($id = null)
    {

        //echo Router::url(array('controller' => 'users', 'action' => 'index', 'admin' => true, driver), true );
        $this->autoRender = false;
        $this->_push($id, 'driver push.', 'CabbieCall');
        //$this->_push($id, 'testing push by Belicia.','CabbieApp');
        // $payee_id = '144';
        // $service == 'new_paypal';
        // if($service != 'credit_transfer') {
        //                       if($service == 'new_paypal') $service = 'paypal';
        // 				$this->_push($payee_id, 'Payment received successfully via '.$service. '.','CabbieCall');
        // 			}

        // //print_r($type_zone);
        // if(empty($lat) && empty($lng))
        // {
        // 	return null;
        // }
        // $conditions = array(
        // 	"CONTAINS (GEOMFROMTEXT(Zone.polygon), GEOMFROMTEXT('POINT($lat $lng)'))",
        // 	'Zone.type'=> $type_zone
        // 	);
        // if ($type_zone == 'vr_zone'){
        // 	$join = array(
        // 		'table' => 'users_zones',
        // 		'alias'=> 'UsersZone',
        // 		'type' => 'LEFT',
        // 		'conditions' => array('UsersZone.user_id = \''. $user_id . '\' AND Zone.id = UsersZone.zone_id')
        // 		);
        // 	$conditions['UsersZone.id'] = null;
        // } else { //phonecall_gps_zone
        // 	$join = array(
        // 		'table' => 'users_zones',
        // 		'alias'=> 'UsersZone',
        // 		'type' => 'INNER',
        // 		'conditions' => array('UsersZone.user_id = \''. $user_id . '\' AND Zone.id = UsersZone.zone_id')
        // 		);
        // }
        // $this->loadModel('Zone');
        // $zones = $this->Zone->find('all',array(
        // 	'recursive' => -1,
        // 	'joins' => array($join),
        // 	'conditions' => $conditions,
        // 	'fields' => array(
        // 	'Zone.id', 'Zone.polygon'
        // 	)
        // 	)
        // );
        // //print_r($zones);
        // foreach ($zones as $key => $zone) {
        // 	$points_string = str_replace(array('POLYGON', '(', ')', ','), array('','', '', ','), $zone['Zone']['polygon']);
        // 	$points = explode(',', $points_string);
        // 	$result =  $this->_inside_zone($points, $lat, $lng);
        // 	if($result){
        // 		$id = $zone['Zone']['id'];
        // 		break;
        // 	}
        // }
        // if ($result){
        // 	return $id;
        // } else {
        // 	return null;
        // }

    }

    private function _inside_zone($polygon_points, $lat, $lng)
    {
        App::import('Vendor', 'polygon', array('file' => 'polygon' . DS . 'point_in_polygon.php'));
        $point = "$lat $lng";
        $polygon = $polygon_points;
        // print_r($point);
        // print_r($polygon);
        $pointLocation = new pointLocation();
        //print "Point in " . $pointLocation->pointInPolygon($point, $polygon) . " the polygon";
        if ($pointLocation->pointInPolygon($point, $polygon) === 'inside') {
            return true;
        } else return null;

    }


    private function _save_user($my_promoter_code = null)
    {
        //$this->request->data['User']['code'] = String::uuid();
        $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
        if ($this->request->data['User']['type'] == 'driver') {
            /*File Upload*/
            //pr($this->request->data);die;
            if (!empty($this->request->data['User']['imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['imagex']['tmp_name'])) {
                $this->request->data['User']['image'] = $this->_upload($this->request->data['User']['imagex']);
            }

            if (!empty($this->request->data['User']['badge_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['badge_imagex']['tmp_name'])) {
                $this->request->data['User']['badge_image'] = $this->_upload($this->request->data['User']['badge_imagex']);
                //print_r($this->request->data);die;
            }
            if (!empty($this->request->data['User']['insurance_certificate_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['insurance_certificate_imagex']['tmp_name'])) {
                $this->request->data['User']['insurance_certificate_image'] = $this->_upload($this->request->data['User']['insurance_certificate_imagex']);

            }
            if (!empty($this->request->data['User']['vehicle_licence_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['vehicle_licence_imagex']['tmp_name'])) {
                $this->request->data['User']['vehicle_licence_image'] = $this->_upload($this->request->data['User']['vehicle_licence_imagex']);
            }
            /*In v2 added this */
            if (!empty($this->request->data['User']['drivers_licence_imagex_front']['tmp_name']) && is_uploaded_file($this->request->data['User']['drivers_licence_imagex_front']['tmp_name'])) {
                $this->request->data['User']['drivers_licence_image_front'] = $this->_upload($this->request->data['User']['drivers_licence_imagex_front']);
            }
            if (!empty($this->request->data['User']['drivers_licence_imagex_back']['tmp_name']) && is_uploaded_file($this->request->data['User']['drivers_licence_imagex_back']['tmp_name'])) {
                $this->request->data['User']['drivers_licence_image_back'] = $this->_upload($this->request->data['User']['drivers_licence_imagex_back']);
            }

            /* if(!empty($this->request->data['User']['insurance_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['insurance_imagex']['tmp_name'])){
                 $this->request->data['User']['insurance_image'] = $this->_upload($this->request->data['User']['insurance_imagex']);
             }*/
            /*End File Upload*/
        }

        $userInfo = $this->request->data['User'];
        //pr($this->request->data);die;
        $this->User->create();
        if ($this->User->save($this->request->data)) {

            $id = $this->User->id;
            if (!empty($this->request->data['User']['device_token'])) {
                if (empty($this->request->data['User']['device_type'])) {
                    $this->request->data['User']['device_type'] = 'android';
                }
                $this->_device_token($id, $this->request->data['User']['device_token'],
                    $this->request->data['User']['device_type'], $this->request->data['Application']['stage'],'1');
            }
            // free credit for new sign up
            $this->loadModel('Setting');
            $data_setting = $this->Setting->findById(1);
            if ($this->request->data['User']['type'] == 'driver') {
                $this->_new_driver_in_queue($id); // add new driver in queue
                $balance = $data_setting['Setting']['free_credit_driver'];
                try {
                    //send email to admin for activation
                    $this->_activateEmail($id);
                } catch (Exception $e) {

                }
            } else {
                $balance = $data_setting['Setting']['free_credit_passenger'];
            }
            $this->_add_free_credit($id, $balance);

            if ($this->request->data['User']['type'] == 'driver') {
                die(json_encode(array(
                    'success' => true,
                    'User' => array(
                        'id' => $this->User->id,
                        'code' => $this->request->data['User']['code'],
                        'promoter_code' => $my_promoter_code,

                        'name' => $userInfo['name'],
                        'ref_promoter_code' => $userInfo['ref_promoter_code'],
                        'email' => $userInfo['email'],
                        'username' => $userInfo['username'],
                        'mobile' => $userInfo['mobile'],
                        'address' => $userInfo['address'],
                        'home_no' => $userInfo['home_no'],
                        'post_code' => $userInfo['post_code'],
                        'driving_licence_no' => $userInfo['User']['driving_licence_no'],
                        'driving_licence_dateExpiry' => $userInfo['User']['driving_licence_dateExpiry'],

                        'cab_type' => $userInfo['cab_type'],
                        'vehicle_type' => $userInfo['vehicle_type'],
                        'is_wheelchair' => $userInfo['is_wheelchair'],
                        'vehicle_licence_no' => $userInfo['vehicle_licence_no'],

                        //'vehicle_licence_image' => !empty($userInfo['vehicle_licence_image']) ? Router::url('/files/driver_infos/'.$userInfo['vehicle_licence_image'], true) : null,
                        'image' => !empty($userInfo['image']) ? Router::url('/files/driver_infos/' . $userInfo['image'], true) : null,
                        'badge_image' => !empty($userInfo['badge_image']) ? Router::url('/files/driver_infos/' . $userInfo['badge_image'], true) : null,
                        'insurance_certificate_image' => !empty($userInfo['insurance_certificate_image']) ? Router::url('/files/driver_infos/' . $userInfo['insurance_certificate_image'], true) : null,

                        'badge_no' => $userInfo['badge_no'],
                        'registration_plate_no' => $userInfo['registration_plate_no'],
                        'insurance_certificate' => $userInfo['insurance_certificate'],
                        'type' => $userInfo['type'],

                        'no_of_seat' => $userInfo['no_of_seat'],
                        'voip_no' => $userInfo['voip_no'],

                        'branch_number' => $userInfo['branch_number'],
                        'driving_licence_no' => $userInfo['driving_licence_no'],
                        'drivers_licence_image_front' => !empty($userInfo['drivers_licence_image_front']) ? Router::url('/files/driver_infos/' . $userInfo['drivers_licence_image_front'], true) : null,
                        'drivers_licence_image_back' => !empty($userInfo['drivers_licence_image_back']) ? Router::url('/files/driver_infos/' . $userInfo['drivers_licence_image_back'], true) : null,
                        'driving_licence_dateExpiry' => $userInfo['driving_licence_dateExpiry'],

                        'running_distance' => $userInfo['running_distance']
                    ))));
            } elseif ($this->request->data['User']['type'] == 'passenger') {
                $query = [
                    'recursive' => -1,
                    'fields' => 'User.company_name',
                    'conditions' => [
                        'User.branch_number' => $this->request->data['User']['branch_number'],
                        'User.type' => 'vendor',
                    ],
                ];
                $company_name = $this->User->find('first', $query);
                $userInfo['company_name'] = @$company_name['User']['company_name'];
                die(json_encode(array('success' => true, 'user_data' => $userInfo)));
            } else {
                die(json_encode(array(
                    'success' => true,
                    'User' => array(
                        'id' => $this->User->id,
                        'code' => $this->request->data['User']['code'],
                    ))));
            }
        } else die(json_encode(array('success' => false, 'message' => 'Registration failed.')));
    }

    public function is_updated_info($code)
    {
        $this->autoRender = false;
        $user_obj = $this->User->find('first', array(
            'recursive' => -1,
            'conditions' => array('User.code' => $code,
                'User.first_update' => 0,
                'User.is_enabled' => 0
            ),
            'fields' => array('User.id')
        ));
        //print_r($user_obj);
        if (empty($user_obj)) die(json_encode(array('success' => true)));
        else die(json_encode(array('success' => false))); // pop up appears on App
    }

    public function balance_check($user_id)
    {
        $this->autoRender = false;
        $user_obj = $this->User->findById($user_id);
        if (!empty($user_obj) && $user_obj['User']['type'] == 'driver') {
            $this->loadModel('Setting');
            $driver_min_balance = $this->Setting->findById(1, array('driver_min_balance'));
            $min_balance = $driver_min_balance['Setting']['driver_min_balance'];
            $balance = $this->User->query("SELECT
					*
				FROM
					users AS q
				WHERE
					(
						SELECT
							SUM(amount)
						FROM
							transactions
						WHERE
							user_id = q.id
						AND is_paid = 1
						AND service <> 'cash_in_hand'
					)>= '$min_balance' AND q.id = '$user_id'
			");

            if (empty($balance)) die(json_encode(array('success' => false, 'message' => 'Low balance, Please top-up.')));
            else die(json_encode(array('success' => true)));
        } else die(json_encode(array('success' => false, 'message' => 'Invalid user')));
    }

    /*********====================New Functions=========================*********/
    public function vendor_registration()
    {
        $this->autoRender = false;
//        echo '<pre>';
//        print_r($this->request->data['User']['picture']);die;
//        echo '</pre>';
        $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
        if ($this->request->data['User']['type'] == 'vendor') {
            $this->request->data['User']['name'] = $this->request->data['User']['company_name'];
            $this->request->data['User']['vendor_license_image'] = $this->_upload($this->request->data['User']['license_picture'], 'vendor_infos');
        }
        $this->request->data['User']['created'] = date('Y-m-d H:i:s');
        $this->User->create();

        if ($this->User->save($this->request->data['User'])) {
            $this->Session->setFlash(__('Registration Successful.'), 'default', array('class' => 'alert alert-success'));
        } else {
            $this->Session->setFlash(__('Registration Not Successful'), 'default', array('class' => 'alert alert-danger'));
        }
        $this->redirect($this->referer());
        $this->admin_login($this->request->data);
    }

    public function near_by_drivers($lat, $lng, $user_id)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $vr_zone_id = $this->_zone_info('vr_zone', $lat, $lng, $user_id);
        //echo $vr_zone_id;die;
        $this->loadModel('UsersZone');
        $query = array(
            //'recursive' => 1,
            /* 'joins' => array(
                 array(
                     'table' => 'users',
                     'alias'=> 'User',
                     'type' => 'LEFT',
                     'conditions' => array('User.id = UsersZone.user_id')
                 )
             ),
             'fields' => array(
                 'User.*'
             ),*/
            'conditions' => array(
                'UsersZone.zone_id' => $vr_zone_id,
                'not' => [
                    'UsersZone.user_id' => $user_id,
                ],
            )
        );
        $data = $this->UsersZone->find('all', $query);
        if (!empty($data)) {
            $result = Hash::extract($data, '{n}.UsersZone');
            die(json_encode(array('success' => true, 'drivers' => $result)));
        } else die(json_encode(array('success' => false, 'msg' => 'No Result found.')));
    }

    public function validate_input()
    {
        $this->autoRender = false;
        $this->autoLayout = false;
        // pr(key($this->params->query['data']));die;
        $key = key($this->params->query['data']['User']);
        $username = $this->params->query['data']['User'][$key];

        /*if($key=='email') {
            if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
                return $this->response->statusCode(400);
            }
        }*/

        $this->User->recursive = -1;
        $is_exist = $this->User->find('first', [
            'conditions' => ['User.' . $key => $username]
        ]);
        if (!empty($is_exist)) {
            return $this->response->statusCode(400);
        } else {
            return $this->response->statusCode(200);
        }
    }
    public function validate_address(){
        $this->autoRender = false;
        $this->autoLayout = false;
        // pr(key($this->params->query['data']));die;
        $key = key($this->params->query['data']['User']);
        $address = $this->params->query['data']['User'][$key];
        $lat = null;
        $lng = null;
        App::uses('HttpSocket', 'Network/Http');
        $address = str_replace(" ", "+", $address);

        $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=AIzaSyBhlUzD8xgohlHKuw7QJ2y62MO2zjbDuP8");   //&region='uk'
		//die($json);
        $json = json_decode($json);
        if(!empty($json)){

        $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
        $lng = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
            //pr($lat);
            //pr($lng);
        }


        if (empty($lat)) {
            return $this->response->statusCode(400);
        } else {
            $this->Session->write('lat',$lat);
            $this->Session->write('lng',$lng);
//            pr($this->Session->read('lat'));
//            pr($this->Session->read('lng'));
            return $this->response->statusCode(200);
        }

    }

    public function admin_despatch_job()
    {
        $auth = $this->Auth->user();
        $ss = $this->Session->read('Auth');
//        echo '<pre>';
//        print_r($ss);
//        echo '</pre>';
        $user_id = $ss['User']['id'];
        if ($ss['User']['type'] == 'admin') { // this will be vendor instead of admin
            $branch_number = $ss['User']['branch_number'];
            $this->User->recursive = -1;
            $driver_list = $this->User->find('all', ['conditions' => ['User.type' => 'driver', 'User.branch_number' => $branch_number]]);
        }

//        $pageContents = $this->Staticpage->findBySlug($slug);
//        $page = $pageContents['Staticpage'];

        $this->loadModel('Staticpage');
        $footerContents = $this->Staticpage->findByMenuTitle('footer');
        $footer = $footerContents['Staticpage'];
        $links = $this->Staticpage->find('all', array(
                'fields' => array('Staticpage.slug', 'Staticpage.menu_title', 'Staticpage.sort_order'),
                'conditions' => array('Staticpage.slug <> ' => 'footer'),
                'order' => array('Staticpage.sort_order' => 'asc')
            )
        );

        $this->loadModel('Setting');
        $header_links = $this->Setting->find('all', array(
                'fields' => array('Setting.itunes_link', 'Setting.googleplay_link', 'Setting.facebook_link',
                    'Setting.twitter_link', 'Setting.website_contact_email')
            )
        );
        $this->set(compact('auth', 'page', 'driver_list', 'subpage', 'title_for_layout', 'footer', 'links', 'page', 'header_links', 'dynamic_info', 'home_content'));
        $view = '/Pages/despatch_job';
        $this->render($view);
    }

    /*With respect to v2 requirements*/

    public function generic_app_call()
    {
        set_time_limit(360000000000);
        header('Content-Type: application/json');
        $this->autoRender = false;
        $this->autoLayout = false;
        //Configure::write('debug', 2);
        if ($this->request->is('post')) {
            if($this->request->data['User']['device'] =='android') {
                define('PUSH_CREDENTIAL_SID', 'CRaaa15d7e3080eb3c0a74afc9a9780b35');
            } else {
                define('PUSH_CREDENTIAL_SID', 'CRbfd7a54c9c0ad00586a05d8faeb07fd2');
            }
            $action = $this->request->data['User']['action'];
            switch ($action) {
                case 'around_you':
                    $this->_around_you();
                    break;
                case 'call_driver':
                    $msg = 'Driver is not available now. Please try again later. Thank you.';
                    $this->_call_to_single_user($this->request->data['user'], $msg);
                    break;
                case 'call_passenger':
                    $msg = 'Customer is not available now. Please try again later. Thank you.';
                    $this->_call_to_single_user($this->request->data['user'], $msg);
                    break;

            }
        } else die(json_encode(array('success' => false, 'msg' => 'Invalid Request.')));
    }

    private function _around_you()
    {
        //print_r($this->request->data);die;
        $branch_no = !empty($this->request->data['User']['branch_number']) ? $this->request->data['User']['branch_number'] : '1234';
        $lat = !empty($this->request->data['User']['lat']) ? $this->request->data['User']['lat'] : "22.8186678"; // 52.951905
        $lng = !empty($this->request->data['User']['lng']) ? $this->request->data['User']['lng'] : "89.5450991"; // -1.142797


        $vendor_id = $this->User->findByBranchNumberAndType($this->request->data['User']['branch_number'],'vendor',['User.id']);
        $call_to = $this->User->Charge->findByUserId($vendor_id['User']['id']);
        if(empty($call_to)){
            $call = 'driver';
        }else $call = $call_to['Charge']['call_allow'];
//print_r($lat);
//print_r($lng);die;
        $conditions = [];
        $conditions = array(
            '((ACOS(
            SIN(\'' . $lat . '\' * PI() / 180)* SIN(User.lat * PI() / 180)+
                        COS(\'' . $lat . '\' * PI() / 180)* COS(User.lat * PI() / 180)* COS(
                        (\'' . $lng . '\' * PI() / 180)-(User.lng * PI() / 180))
                        ))*   ' . 3959 . ') <=' => $call_to['Charge']['radious'],
            'User.type' => $call,
            'User.branch_number' => $branch_no,
            'User.is_login' => '1'
        );

        if (!empty($this->request->data['User']['vehicle_type'])) {
            $conditions = am(
                $conditions, array(
                    'User.vehicle_type' => $this->request->data['User']['vehicle_type'],
                )
            );
        }
        $cancel_driver_ids = $this->request->data['User']['cancel_driver_ids'];
        if(!empty($cancel_driver_ids)){
            $conditions = am($conditions,["User.id NOT IN $cancel_driver_ids"]);
        }
        //print_r($conditions);die;
        $query = array(
            'recursive' => -1,
            'fields' => array(
                'User.id',
                'User.username',
                /*'User.lat',
                'User.lng',*/
                '((ACOS(
                SIN(\'' . $lat . '\' * PI() / 180)* SIN(User.lat * PI() / 180)+
                        COS(\'' . $lat . '\' * PI() / 180)* COS(User.lat * PI() / 180)* COS(
                        (\'' . $lng . '\' * PI() / 180)-(User.lng * PI() / 180))
                        ))*   ' . 3959 . ') AS distance'),

            'conditions' => $conditions,
            'order' => ['(ACOS(
                        SIN(\'' . $lat . '\' * PI() / 180)* SIN(User.lat * PI() / 180)+
                        COS(\'' . $lat . '\' * PI() / 180)* COS(User.lat * PI() / 180)* COS(
                        (\'' . $lng . '\' * PI() / 180)-(User.lng * PI() / 180))
                        )*   ' . 3959 . ')' => 'ASC'],
            'limit' => 1
        );

        #This query calculate the miles
        /*Specification*/
        # 6371000 ==> Meters
        # 3959  ==> Miles
        # 6371 ==> KM
        /*Specification*/

        $drivers = $this->User->find('all', $query);
        if($call == 'driver' && empty($drivers)){
            $conditions = [
                'User.type' => 'vendor',
                'User.branch_number' => $branch_no,
                'User.is_login' => '1'
            ];
            if(!empty($cancel_driver_ids)){
                $conditions = am($conditions,["User.id NOT IN $cancel_driver_ids"]);
            }

            $option = [
                'conditions' => $conditions,
                'fields' => [
                    'User.id',
                    'User.username'
                ],
                'recursive' => -1
            ];
            $drivers = $this->User->find('all', $option);


        }
        if(empty($drivers)){
            $this->_push_number_to_passenger($this->request->data['User']['from'],$lat,$lng);
        }
	//print_r($drivers);die;
        $this->_call_nearby_drivers($drivers);
    }

    private function _call_nearby_drivers($drivers = [])
    {
        App::import('Vendor', 'twilio_call_v2', array('file' => 'twilio_call_v2/vendor/autoload.php'));
        App::import('Vendor', 'twilio_call_v2', array('file' => 'twilio_call_v2/config.php'));

        /*
       * Use a valid Twilio number by adding to your account via https://www.twilio.com/console/phone-numbers/verified
       */
        $callerNumber = '+447481339107';
        $response = new Twilio\Twiml();

        $callerId = 'client:' . $this->request->data['User']['from'];
        //$callerId = 'client:quick_start';
        //print_r($callerId);die;

        if (!empty($drivers)) {
            $driver = array_shift($drivers);
            //print_r($driver);die;
            //foreach($drivers as $driver){
            $to = @$driver['User']['username'];

            if (!isset($to) || empty($to)) {
                $response->say('Congratulations! You have just made your first call! Good bye.');
            } else if (is_numeric($to)) {
                $dial = $response->dial(
                    array(
                        'callerId' => $callerNumber
                    ));
                $dial->number($to);
            } else {
                $dial = $response->dial(
                    array(
                        'callerId' => $callerId
                    ));
                $dial->client($to);
            }
            print $response;
            //}

            /*$this->_test_call($users);
            $users = Hash::extract($users, '{n}.User');
            echo '<pre>';
            print_r($users);die;
            die(json_encode(['success' => true, 'nearby_users' => @$users]));*/
        } else {
            $response->say("Sorry! Your Nearest Drivers are not available right now. Please try again later. Thank you.", array('voice' => 'woman'));
            print $response;
        }
    }

    private function _test_call($drivers = [])
    {
        App::import('Vendor', 'twilio_call_v2', array('file' => 'twilio_call_v2/vendor/autoload.php'));
        App::import('Vendor', 'twilio_call_v2', array('file' => 'twilio_call_v2/config.php'));

        $identity = 'alice';
        $callerNumber = '1234567890';
        $callerId = 'client:quick_start';
        //print_r($drivers);die;
        foreach ($drivers as $driver) {
            /*
         * Use a valid Twilio number by adding to your account via https://www.twilio.com/console/phone-numbers/verified
         */

            /*to = isset($_GET["to"]) ? $_GET["to"] : "raz123";
            if (!isset($to) || empty($to)) {
                $to = isset($this->request->data["to"]) ? $this->request->data["to"] : "r@g.com";
            }*/

            $to = @$driver['User']['username'];
            $client = new Twilio\Rest\Client(API_KEY, API_KEY_SECRET, ACCOUNT_SID);

            $call = NULL;
            if (!isset($to) || empty($to)) {
                $call = $client->calls->create(
                    'client:alice', // Call this number
                    $callerId, // From a valid Twilio number
                    array(
                        'url' => 'https://' . $_SERVER['HTTP_HOST'] . '/incoming'
                    )
                );
            } else if (is_numeric($to)) {
                $call = $client->calls->create(
                    $to, // Call this number
                    $callerNumber, // From a valid Twilio number
                    array(
                        'url' => 'https://' . $_SERVER['HTTP_HOST'] . '/incoming'
                    )
                );
            } else {
                $call = $client->calls->create(
                    'client:' . $to, // Call this number
                    $callerId, // From a valid Twilio number
                    array(
                        'url' => 'https://' . $_SERVER['HTTP_HOST'] . '/incoming'
                    )
                );
            }
        }

        print $call . sid;
    }

    public function place_call()
    {
        Configure::write('debug', 2);
        App::import('Vendor', 'twilio_call_v2', array('file' => 'twilio_call_v2/vendor/autoload.php'));
        App::import('Vendor', 'twilio_call_v2', array('file' => 'twilio_call_v2/config.php'));

        $identity = 'alice';
        $callerNumber = '1234567890';
        $callerId = 'client:quick_start';
        $to = isset($_GET["to"]) ? $_GET["to"] : "raz123";
        if (!isset($to) || empty($to)) {
            $to = isset($this->request->data["to"]) ? $this->request->data["to"] : "r@g.com";
        }
        //die(ACCOUNT_SID);
        $client = new Twilio\Rest\Client(API_KEY, API_KEY_SECRET, ACCOUNT_SID);

        $call = NULL;
        if (!isset($to) || empty($to)) {
            $call = $client->calls->create(
                'client:alice', // Call this number
                $callerId, // From a valid Twilio number
                array(
                    'url' => 'https://' . $_SERVER['HTTP_HOST'] . '/incoming'
                )
            );
        } else if (is_numeric($to)) {
            $call = $client->calls->create(
                $to, // Call this number
                $callerNumber, // From a valid Twilio number
                array(
                    'url' => 'https://' . $_SERVER['HTTP_HOST'] . '/incoming'
                )
            );
        } else {
            $call = $client->calls->create(
                'client:' . $to, // Call this number
                $callerId, // From a valid Twilio number
                array(
                    'url' => 'https://' . $_SERVER['HTTP_HOST'] . '/incoming'
                )
            );
        }

        print $call . sid;
    }

    /*
 * Creates an endpoint that plays back a greeting.
 */
    public function incoming()
    {
        App::import('Vendor', 'twilio_call_v2', array('file' => 'twilio_call_v2/vendor/autoload.php'));
        App::import('Vendor', 'twilio_call_v2', array('file' => 'twilio_call_v2/config.php'));

        $response = new Twilio\Twiml();
        $response->say('Congratulations! You have received your first inbound call! Good bye.');
        print $response;
    }

    public function access_token()
    {
        header('Content-Type: application/json');
        $this->autoRender = false;
        $this->autoLayout = false;
        App::import('Vendor', 'twilio_call_v2', array('file' => 'twilio_call_v2/vendor/autoload.php'));
        App::import('Vendor', 'twilio_call_v2', array('file' => 'twilio_call_v2/config.php'));
        //if ($this->request->is('post')) {
            // Use identity and room from query string if provided
            $identity = $this->request->data['identity'];
            $device_type = $this->request->data['device_type'];

            if (empty($identity) && empty($device_type)) {
                $device_type = $this->params->query["device_type"];
                $identity = $this->params->query["identity"];
            }

            // Create access token, which we will serialize and send to the client
            $token = new AccessToken(ACCOUNT_SID,
                API_KEY,
                API_KEY_SECRET,
                3600,
                $identity
            );
            if($device_type =='android') {
                $PUSH_CREDENTIAL_SID = 'CR57d3fb5967f272a8249c26216424dadc';
            } else {
                $PUSH_CREDENTIAL_SID = 'CRbfd7a54c9c0ad00586a05d8faeb07fd2';
            }
            #$PUSH_CREDENTIAL_SID = 'CR57d3fb5967f272a8249c26216424dadc';
            // Grant access to Video
            $grant = new VoiceGrant();
            $grant->setOutgoingApplicationSid(APP_SID);
            $grant->setPushCredentialSid($PUSH_CREDENTIAL_SID);
            $token->addGrant($grant);

            echo $token->toJWT();
        //}
    }

    /**
     * @param int $length
     * @param bool $add_dashes
     * @param string $available_sets
     * @return string
     * create a unique new password
     */
    private function _generate_password($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';

        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];

        $password = str_shuffle($password);

        if (!$add_dashes)
            return $password;

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    /**
     * it's use for admin and vendor dashboard
     */
    public function admin_dashboard()
    {
        $conditions = [];
        $type = AuthComponent::user(['type']);
        if ($type == 'vendor') {
            $branch_number = AuthComponent::user(['branch_number']);
            $conditions = am($conditions, [
                'User.branch_number' => $branch_number
            ]);
        }
        $query = [
            'conditions' => [
                'User.type' => 'vendor'
            ]
        ];
        $branchusers = $this->User->find('count', $query);
        $query1 = am($conditions, ['User.type' => 'passenger']);
        $passengers = $this->User->find('count', ['conditions' => $query1]);
        $query2 = am($conditions, ['User.type' => 'driver']);
        $drivers = $this->User->find('count', ['conditions' => $query2]);
        if ($type == 'admin') {
            $zones = $this->User->Zone->find('count');
        }

        $conditions = [];
        if (AuthComponent::user(['type']) == 'vendor') {
            $conditions = am($conditions, ['User.branch_number' => AuthComponent::user(['branch_number'])]);
        }

        $qury_transaction = array(
            'conditions' => $conditions,
            'fields' => [
                'Transaction.id'
            ]
        );

        $transactions = $this->User->Transaction->find('count',$qury_transaction);
        $this->loadModel('Booking');
        if ($type == 'vendor') {
            $conditions = am($conditions, ['User.type' => 'driver']);
            $users = $this->User->find('list', ['fields' => ['User.id'], 'conditions' => $conditions]);
            $user_ids = implode(',', $users);
            if (empty($user_ids)) {
                $bookings = '0';
            } else {
                $totalBookings = $this->Booking->query("
            SELECT id FROM bookings WHERE acceptor IN ($user_ids)
            ");
            $bookings = count($totalBookings);
            }
            
        } else {
            $bookings = $this->Booking->find('count', ['conditions' => $conditions]);
        }
        $this->set(compact('branchusers', 'passengers', 'drivers', 'zones', 'transactions', 'bookings'));
        //pr($bookings);

    }

    /**
     * it's for all user profile
     */
    public function admin_profile()
    {
        $id = AuthComponent::user(['id']);

        //print_r(AuthComponent::user());die;
        if ($this->request->is('post')) {
            if (!empty($this->request->data['User']['password'])) {
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
            } else {
                unset($this->request->data['User']['password']);
            }
            if (!empty($this->request->data['User']['image']['tmp_name'])) {
                $this->request->data['User']['image'] = $this->_upload($this->request->data['User']['image'], 'vendor_infos');
            } else {
                unset($this->request->data['User']['image']);
            }
            $this->User->id = $id;
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('Account updated successfully.'), 'default', array('class' => 'alert alert-success text-center'));
            }
        }
        $query = [
            'conditions' => [
                'User.id' => $id
            ],
            'recursive' => -1
        ];
        $user = $this->User->find('first', $query);
        unset($user['User']['password']);

        $this->set(compact('user','wallet'));
    }

    /**
     * @param null $id
     * @param null $value
     * @param null $type
     * for using it for Enable or Disable a member
     */
    public function admin_EnableOrDisable_member($id = null, $value = null, $type = null)
    {
        $status = $value == '1' ? 'Enabled' : 'Disabled';
        $this->User->id = $id;
        if ($this->User->saveField('is_enabled', $value)) {
            $this->Session->setFlash(__('The member has been ' . $status . '.'), 'default', array('class' => 'alert alert-success text-center'));
            $this->redirect(['action' => 'index', $type]);
        }
    }

    /**
     * @param $branch_number
     * vendor dashboard data
     * who driver near the own vendor
     */
    public function vendor_near_by_driver($branch_number)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $options = [
            'conditions' => [
                'User.branch_number' => $branch_number,
                'User.type' => 'driver',
                'User.is_login' => '1'
            ],
            'recursive' => -1,
            'fields' => [
                'User.id',
                'User.lat',
                'User.lng'
            ]
        ];
        $drivers = $this->User->find('all', $options);
        //$driver_id = Hash::extract($driver, '{n}.User');
        if(!empty($drivers)){
            foreach($drivers as $driver){
            $details = $this->_get_status($driver['User']['id']);
                $driver_id[] = [
                    'id' => '1000'.$driver['User']['id'],
                    'lat' => $driver['User']['lat'],
                    'lng' => $driver['User']['lng'],
                    'status' => $details
                ];
            }
        }
        //pr($driver_id);die;


        if (!empty($driver_id)) {
            die(json_encode(array('success' => true, 'drivers' => $driver_id)));
        } else die(json_encode(array('success' => false, 'msg' => 'No Result found.')));
    }
    private function _get_status($id){
        $query = [
            'conditions' => [
                'Queue.user_id' => $id,
                'Queue.booking_id IS NOT NULL'
            ],
            'recursive' => -1,
            'fields' => [
                'Queue.response'
            ],
            'order' => [
                'Queue.id DESC'
            ]
        ];
        $response = $this->User->Queue->find('first',$query);
        if(empty($response)) {
            return 'Free';
        }else {
            if($response['Queue']['response'] == 'cleared' || $response['Queue']['response'] == 'rejected') return 'Free';
            else return 'Busy';

        }
        //pr($response);
    }


    /**
     * when paypal redirect
     * And payment successfull
     * then call it
     * it's for paypal payment method varify
     * and save data to mysql
     */
    public function admin_paypal_success()
    {
        $this->autoRender = false;
        $this->autoLayout = false;
        //print_r($_GET);
        if(isset($_GET['tx']))
        {
            $tx = $_GET['tx'];
            // Init cURL
            $request = curl_init();

// Set request options
            curl_setopt_array($request, array
            (
                CURLOPT_URL => 'https://www.sandbox.paypal.com/cgi-bin/webscr',
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => http_build_query(array
                (
                    'cmd' => '_notify-synch',
                    'tx' => $tx,
                    'at' => 'KLZERzb0KnFduKs07XjQGZxuZCosPgKo3Q5cnEPlmFrQvJ6uSGr_Ej4zms0',
                )),
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                // CURLOPT_SSL_VERIFYPEER => TRUE,
                // CURLOPT_CAINFO => 'cacert.pem',
            ));

// Execute request and get response and status code
            $response = curl_exec($request);
            $status   = curl_getinfo($request, CURLINFO_HTTP_CODE);

// Close connection
            curl_close($request);
//print_r($response);
//print_r($status);
        }
        if($status == 200 AND strpos($response, 'SUCCESS') === 0)
        {
            // Remove SUCCESS part (7 characters long)
            $response = substr($response, 7);

// URL decode
            $response = urldecode($response);

// Turn into associative array
            preg_match_all('/^([^=\s]++)=(.*+)/m', $response, $m, PREG_PATTERN_ORDER);
            $response = array_combine($m[1], $m[2]);

// Fix character encoding if different from UTF-8 (in my case)
            if(isset($response['charset']) AND strtoupper($response['charset']) !== 'UTF-8')
            {
                foreach($response as $key => &$value)
                {
                    $value = mb_convert_encoding($value, 'UTF-8', $response['charset']);
                }
                $response['charset_original'] = $response['charset'];
                $response['charset'] = 'UTF-8';
            }

// Sort on keys for readability (handy when debugging)
            ksort($response);
            //print_r($response);die;
            if(!empty($response)){
              $savedata = $this->_saveTopupData($response);
                if($savedata){
                    $this->Session->setFlash(__('You top-up '.$response['mc_gross'].' '.$response['mc_currency'].' successfully'), 'default', array('class' => 'alert alert-success text-center'));

                } else {
                    $this->Session->setFlash(__('Sorry! Something went worng. Please try again.'), 'default', array('class' => 'alert alert-danger text-center'));
                }
                $this->redirect(['action' => 'profile']);
            }
        }
        else
        {
            $this->Session->setFlash(__('Sorry! Your information is wrong'), 'default', array('class' => 'alert alert-danger text-center'));
            $this->redirect(['action' => 'profile']);
        }
    }

    /**
     * @param array $response
     * @return bool
     * when top up from paypal done
     * then call it
     */
    private function _saveTopupData($response = []){
        $user_id = AuthComponent::user(['id']);
        $tx = $response['txn_id'];
        $options = [
            'recursive' => -1,
            'conditions' => [
                'Transaction.pay_key' => $tx
            ]
        ];
        $exit_tx = $this->User->Transaction->find('first',$options);
        if(empty($exit_tx)){
            $date_time = date_create($response['payment_date']);
            $date = date_format($date_time,'Y-m-d H:i:s');

            if($this->_add_transactions($user_id,'top_up',null,$response['mc_gross'],$response['mc_currency'],'paypal',null,$tx,$response['payment_status'],$date)){

                if($this->_add_wallet($user_id,$response['mc_gross'])){
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }

    }

    /**
     * when payment isn't full fill done
     * or transaction cancel by user
     */
    public function admin_paypal_cancel(){
        $this->autoRender = false;
        $this->autoLayout = false;
        $this->Session->setFlash(__('Sorry! Your top-up cancel'), 'default', array('class' => 'alert alert-danger text-center'));
        $this->redirect(['action' => 'profile']);
    }

    /**
     * when request log out from app
     *then request it
     * for update device token which given by post method
     */
    public function app_logout(){
        if ($this->request->is('post')) {
        if (!empty($this->request->data['User']['device_token'])) {
            if (empty($this->request->data['User']['device_type'])) {
                $this->request->data['User']['device_type'] = 'android';
            }
            $this->_device_token(
                $this->request->data['User']['id'],
                $this->request->data['User']['device_token'],
                $this->request->data['User']['device_type'],
                $this->request->data['Application']['stage'],
                '0'
            );
            die(json_encode(array('success' => true)));
        }
    } else {
            die(json_encode(array('success' => false,'message' => 'Sorry! Something went wrong')));
        }
    }

    /**
     * @param $user_id
     * get update place
     * where have a driver
     */
    public function get_place($user_id){
        $user_info = $this->User->findById($user_id,['lat','lng']);
        if(!empty($user_info)){
            die(json_encode(['success' => true,'data'=>$user_info['User']]));
        } else {
            die(json_encode(['success' => false]));
        }
    }

    /**
     * driver app send update location
     * per minute
     * method post
     * data[User][id]
     * data[User][lat]
     * data[User][lng]
     * data[User][address]
     */
    public function update_place(){
        $this->autoLayout = false;
        $this->autoRender = false;
        $user_id = $this->request->data['User']['id'];
        $this->User->id = $user_id;
        $this->User->save($this->request->data);die;
    }

    /**
     * @param null $user_id
     * @param null $branch_number
     * when make a call......
     * it will hit per minute
     */
    public function charge_for_twillo($user_id = null,$branch_number = null){
        $this->autoLayout = false;
        $this->autoRender = false;
                $vendor_id = $this->User->findByBranchNumberAndType($branch_number,'vendor',['User.id']);

        $query = [
            'conditions' => [
                'Charge.user_id' => $vendor_id['User']['id']
            ],
            'fields' => [
                'Charge.per_minute_charge'
            ]
        ];
        $charge = $this->User->Charge->find('first',$query);
//                pr($charge);die;

        if(empty($charge)) die(json_encode(['success' => false]));
        $wallet = $this->_get_wallet_info($user_id);
        if(!empty($wallet['Wallet']['available_balance'])){
           if($wallet['Wallet']['available_balance'] >= $charge['Charge']['per_minute_charge']){
               if($this->_minus_wallet($user_id,$charge['Charge']['per_minute_charge'])){
                   die(json_encode(['success' => true]));
               } else{
                   die(json_encode(['success' => false]));
               }
           } else {
               die(json_encode(['success' => false]));
           }
        } else {
            die(json_encode(['success' => false]));
        }
    }
    private  function _push_number_to_passenger($address,$lat,$lng){        
        $data = explode('_',$address);
        $user_id = array_pop($data);
        //$concate = implode('+',$data);  

        $json = file_get_contents("https://maps.googleapis.com/maps/api/place/textsearch/json?query=$concate&location=$lat,$lng&radius=10000000&&type=doctor&key=AIzaSyBhlUzD8xgohlHKuw7QJ2y62MO2zjbDuP8");
        $d = json_decode($json);
        foreach($d->results as $result){

            $data = file_get_contents("https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyBhlUzD8xgohlHKuw7QJ2y62MO2zjbDuP8&sensor=false&fields=formatted_phone_number,name,formatted_address&reference=".$result->reference);
            $number = json_decode($data);
            //pr($number);die;
            $numbers[] = [
                'name' => $number->result->name,
                'number' => $number->result->formatted_phone_number,
                'address' => $number->result->formatted_address
            ];


        };
        $finalData = [
            'status' => 'not_found',
            'number' => $numbers
        ];        
        $this->_pushAll($user_id,$finalData);
    }
    public function get_address_by_latlang($lat,$lng){
        $json = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&key=AIzaSyBhlUzD8xgohlHKuw7QJ2y62MO2zjbDuP8");
        $data = json_decode($json);
        $first_element = array_shift($data->results);
        die(json_encode(['location'=>$first_element->formatted_address]));
    }
    public function get_access_despatch_job($driver_id){
        $access = $this->User->findByIdAndType($driver_id,'driver',['despatch_job']);
        if($access['User']['despatch_job'] == '1') die(json_encode(['success' => true]));
        else die(json_encode(['success' => false,'message' => 'You are not eligible for booking a job. Please contact with Branch Office']));

    }

    public function test_p(){
    Configure::write('debug',2);
        $this->_push_number_to_passenger('Sonadanga_House_384_Khulna_Division_735','22.819298333333336','89.54549999999999');
        die;
    }




}
