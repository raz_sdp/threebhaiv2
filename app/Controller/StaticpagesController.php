<?php
App::uses('AppController', 'Controller');
/**
 * Staticpages Controller
 *
 * @property Staticpage $Staticpage
 * @property PaginatorComponent $Paginator
 */
class StaticpagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Staticpage->recursive = 0;
		$this->set('staticpages', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Staticpage->exists($id)) {
			throw new NotFoundException(__('Invalid staticpage'));
		}
		$options = array('conditions' => array('Staticpage.' . $this->Staticpage->primaryKey => $id));
		$this->set('staticpage', $this->Staticpage->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			if(empty($this->request->data['Staticpage']['slug'])){
				$this->request->data['Staticpage']['slug'] = Inflector::slug(strtolower($this->request->data['Staticpage']['title']), $replacement = '_');
			} 
			$slug_check = $this->Staticpage->findBySlug($this->request->data['Staticpage']['slug'], array('id'));
			if(empty($slug_check)) {
				$this->Staticpage->create();
				if ($this->Staticpage->save($this->request->data)) {
					$this->Session->setFlash(__('The staticpage has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The staticpage could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
				}
			} else {
				$this->Session->setFlash(__('The duplicate Slug name. Please try another Slug.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
			
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Staticpage->exists($id)) {
			throw new NotFoundException(__('Invalid staticpage'));
		}
		if ($this->request->is('put') || $this->request->is('post')) {
		//print_r($this->request->data['Staticpage']); exit;
			if(empty($this->request->data['Staticpage']['slug'])){
				$this->request->data['Staticpage']['slug'] = Inflector::slug(strtolower($this->request->data['Staticpage']['title']), $replacement = '_');
			}
//            $slug_check = $this->Staticpage->findBySlug($this->request->data['Staticpage']['slug'], array('id'));
//			$slug_check = $this->Staticpage->find('first', array(
//				'conditions' => array('slug' => $this->request->data['Staticpage']['slug'], 'id <>'=>$id),
//			));
			if(empty($slug_check)) {
                $this->Staticpage->id = $id;
				if ($this->Staticpage->save($this->request->data)) {
					$this->Session->setFlash(__('The staticpage has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The staticpage could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
				}
			} else {
				$this->Session->setFlash(__('The duplicate Slug name. Please try another Slug.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('Staticpage.' . $this->Staticpage->primaryKey => $id));
			$this->request->data = $this->Staticpage->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Staticpage->id = $id;
		if (!$this->Staticpage->exists()) {
			throw new NotFoundException(__('Invalid staticpage'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Staticpage->delete()) {
			$this->Session->setFlash(__('The staticpage has been deleted.'), 'default', array('class' => 'alert alert-success text-center'));
		} else {
			$this->Session->setFlash(__('The staticpage could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
