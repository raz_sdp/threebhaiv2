<?php
App::uses('AppController', 'Controller');
/**
 * DriverDeclineReasons Controller
 *
 * @property DriverDeclineReason $DriverDeclineReason
 */
class DriverDeclineReasonsController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->DriverDeclineReason->recursive = 0;
		$this->set('driverDeclineReasons', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->DriverDeclineReason->exists($id)) {
			throw new NotFoundException(__('Invalid driver decline reason'));
		}
		$options = array('conditions' => array('DriverDeclineReason.' . $this->DriverDeclineReason->primaryKey => $id));
		$this->set('driverDeclineReason', $this->DriverDeclineReason->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->DriverDeclineReason->create();
			if ($this->DriverDeclineReason->save($this->request->data)) {
				$this->Session->setFlash(__('The driver decline reason has been saved'), 'default', array('class' => 'alert alert-success text-center'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The driver decline reason could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->DriverDeclineReason->exists($id)) {
			throw new NotFoundException(__('Invalid driver decline reason'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->DriverDeclineReason->save($this->request->data)) {
				$this->Session->setFlash(__('The driver decline reason has been saved'), 'default', array('class' => 'alert alert-success text-center'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The driver decline reason could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('DriverDeclineReason.' . $this->DriverDeclineReason->primaryKey => $id));
			$this->request->data = $this->DriverDeclineReason->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->DriverDeclineReason->id = $id;
		if (!$this->DriverDeclineReason->exists()) {
			throw new NotFoundException(__('Invalid driver decline reason'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->DriverDeclineReason->delete()) {
			$this->Session->setFlash(__('Driver decline reason deleted'), 'default', array('class' => 'alert alert-success text-center'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Driver decline reason was not deleted'), 'default', array('class' => 'alert alert-danger text-center'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function g() {
		$this->autoRender = false;
		$reasons = $this->DriverDeclineReason->get_reasons();
		$res = array();
		if(!empty($reasons)) {
			foreach($reasons as $id => $reason) {
				$res[] = array('id' => $id, 'reason' => $reason);
			}
			die(json_encode(array('success' => true, 'DriverDeclineReasons' => $res)));
		} else die(json_encode(array('success' => true, 'message' => __('Sorry, No reasons setting found'))));
	}

	public function admin_active_reason($id = null){
		$this->autoRender = false;
		$this->DriverDeclineReason->id = $id;
		if (!$this->DriverDeclineReason->exists()) {
			throw new NotFoundException(__('Invalid Reason'));
		}
		$this->request->data['DriverDeclineReason']['is_active'] = 1;		
		if ($this->DriverDeclineReason->save($this->request->data, false)) {
			$this->Session->setFlash(__('The reason has been activated.'));
			$this->redirect($this->referer());
		} else {
			$this->Session->setFlash(__('The reason could not be activate. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	//disable any user
	public function admin_deactive_reason($id = null){
		$this->autoRender = false;
		$this->DriverDeclineReason->id = $id;
		if (!$this->DriverDeclineReason->exists()) {
			throw new NotFoundException(__('Invalid Reason'));
		}
		$this->request->data['DriverDeclineReason']['is_active'] = 0;		
		if ($this->DriverDeclineReason->save($this->request->data, false)) {
			$this->Session->setFlash(__('The reason has been deactivated.'));
			$this->redirect($this->referer());
		} else {
			$this->Session->setFlash(__('The reason could not be deactivated. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
