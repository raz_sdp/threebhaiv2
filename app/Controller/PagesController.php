<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
//App::uses('User', 'Model');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Pages';

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    /**
     * Displays a view
     *
     * @param mixed What page to display
     * @return void
     */
    public function display() {
        /*$path = func_get_args();

        $count = count($path);
        if (!$count) {
            $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }*/
        $branches = [];
        $auth = $this->Auth->user();
        $this->loadModel('Staticpage');

        $slug = $this->params['pass'][0];

        $company_name = $this->params['pass'][1];

        $this->loadModel('User');
        $this->loadModel('HomePage');
        $home_content = $this->HomePage->find('all', ['recursive'=>-1, 'conditions'=>['HomePage.id'=>'admin', 'HomePage.status'=>'active']]);

        $this->Staticpage->recursive=-1;
        $view = $slug;
        $defined_slug = ['login', 'topup', 'cancel', 'success'];

        if($slug == 'home' || $slug == ''){
            $view = 'home';
        }
        else if($slug == 'vendor_registration'){
            $pageContents = $this->Staticpage->findBySlug($slug);
            $page = @$pageContents['Staticpage'];
            $view = 'vendor_registration';
        }
//        else if($slug == 'despatch_job'){
//            $pageContents = $this->Staticpage->findBySlug($slug);
//            $page = $pageContents['Staticpage'];
//            $view = 'despatch_job';
//        }
        else if($slug == 'distributor_registration'){
            $pageContents = $this->Staticpage->findBySlug($slug);
            $page = @$pageContents['Staticpage'];
            $view = 'distributor_registration';
        }
        else if (in_array($slug, $defined_slug)) {
            //$view = 'login';
        }
        elseif($slug=='branches') {
            $this->loadModel('User');
            $query = [
                'recursive' => -1,
                'fields' => [
                    'User.company_name', 'User.branch_number'
                ],
                'conditions' => [
                    'User.type' => 'vendor'
                ]
            ];
            $data = $this->User->find('all', $query);
            $branches = Hash::extract($data, '{n}.User');
        }
        else {
            $this->loadModel('CompanyInformation');
            $pageContents = $this->Staticpage->findBySlug($company_name);

            $page = $pageContents['Staticpage'];

            if ($this->User->hasAny(['company_slug'=>$company_name])) {
//                $user_infos = $this->User->query("select id from users where company_slug = '$slug'");
                $user_infos = $this->User->find('first', ['recursive'=>-1, 'conditions'=>['User.company_slug'=>$company_name]]);
                $user_id = $user_infos['User']['id'];
                $dynamic_info = $this->CompanyInformation->find('first', ['recursive'=>-1, 'conditions'=>['CompanyInformation.user_id'=>$user_id]]);
                $view = 'company';
            }
            else{
                $view = 'home';
            }
        }
        if(isset($dynamic_info) && !empty($dynamic_info)){
            $footer = $dynamic_info['CompanyInformation'];
        }else{
            $footerContents = $this->Staticpage->findByMenuTitle('footer');
            $footer = $footerContents['Staticpage'];
        }

//        echo '<pre>';
//        print_r($footer);
//        echo '</pre>';
//        die();

        $links = $this->Staticpage->find('all', array(
                'fields' => array('Staticpage.slug', 'Staticpage.menu_title', 'Staticpage.sort_order'),
                'conditions' => array('Staticpage.slug <> ' => 'footer'),
                'order' => array('Staticpage.sort_order' => 'asc')
            )
        );

        $this->loadModel('Setting');
        $header_links = $this->Setting->find('all', array(
                'fields' => array('Setting.itunes_link', 'Setting.googleplay_link', 'Setting.facebook_link',
                    'Setting.twitter_link', 'Setting.website_contact_email')
            )
        );

        //print_r($header_links);

        $this->set(compact('auth', 'page', 'subpage', 'title_for_layout', 'footer', 'links', 'page', 'header_links', 'dynamic_info', 'home_content','branches'));
        $this->render($view);
    }





}
