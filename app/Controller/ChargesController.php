<?php

App::uses('AppController', 'Controller');

/**

 * Charges Controller

 *

 * @property Charge $Charge

 * @property PaginatorComponent $Paginator

 */

class ChargesController extends AppController {



/**

 * Components

 *

 * @var array

 */

	public $components = array('Paginator');



/**

 * admin_index method

 *

 * @return void

 */

	public function admin_index() {

		$this->Charge->recursive = 0;

		$this->set('charges', $this->Paginator->paginate());

	}



/**

 * admin_view method

 *

 * @throws NotFoundException

 * @param string $id

 * @return void

 */

	public function admin_view($id = null) {

		if (!$this->Charge->exists($id)) {

			throw new NotFoundException(__('Invalid charge'));

		}

		$options = array('conditions' => array('Charge.' . $this->Charge->primaryKey => $id));

		$this->set('charge', $this->Charge->find('first', $options));

	}



/**

 * admin_add method

 *

 * @return void

 */

	public function admin_add() {

		if ($this->request->is('post')) {

			$this->Charge->create();

            $user_id = $this->request->data['Charge']['user_id'];

            $query = [

                'conditions' => [

                    'Charge.user_id' => $user_id

                ],

                'recursive' => -1

            ];

            $exitUser = $this->Charge->find('first',$query);

            if(empty($exitUser)){

			if ($this->Charge->save($this->request->data)) {

                $this->Session->setFlash(__('The charge has been saved.'), 'default', array('class' => 'alert alert-success text-center'));

				$this->redirect(array('controller'=>'users','action' => 'index','vendor'));

			} else {

                $this->Session->setFlash(__('The charge could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));

			}

            } else {

                $this->Session->setFlash(__('The charge already saved for this user.'), 'default', array('class' => 'alert alert-danger text-center'));

            }

		}

        $options = [

            'conditions' => [

                'User.type' => 'vendor'

            ],

            'fields' => [

                'User.id',

                'User.company_name'

            ]

        ];

		$users = $this->Charge->User->find('list',$options);

		$this->set(compact('users'));

	}



/**

 * admin_edit method

 *

 * @throws NotFoundException

 * @param string $id

 * @return void

 */

	public function admin_edit($id = null) {

		if (!$this->Charge->exists($id)) {

			throw new NotFoundException(__('Invalid charge'));

		}

		if ($this->request->is(array('post', 'put'))) {

			if ($this->Charge->save($this->request->data)) {

                $this->Session->setFlash(__('The charge has been saved.'), 'default', array('class' => 'alert alert-success text-center'));

				//return $this->redirect(array('action' => 'index'));

			} else {

                $this->Session->setFlash(__('The charge could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));

			}

		} else {

			$options = array('conditions' => array('Charge.' . $this->Charge->primaryKey => $id),'recursive' => 0);

			$this->request->data = $this->Charge->find('first', $options);

		}

		$users = $this->Charge->User->find('list');

		$this->set(compact('users'));

	}



/**

 * admin_delete method

 *

 * @throws NotFoundException

 * @param string $id

 * @return void

 */

	public function admin_delete($id = null) {

		$this->Charge->id = $id;

		if (!$this->Charge->exists()) {

			throw new NotFoundException(__('Invalid charge'));

		}

		$this->request->allowMethod('post', 'delete');

		if ($this->Charge->delete()) {

            $this->Session->setFlash(__('The charge has been deleted.'), 'default', array('class' => 'alert alert-success text-center'));

		} else {

            $this->Session->setFlash(__('The charge could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));

		}

		return $this->redirect(array('action' => 'index'));

	}

}

