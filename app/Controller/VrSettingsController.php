<?php
App::uses('AppController', 'Controller');
/**
 * VrSettings Controller
 *
 * @property VrSetting $BookingSetting
 */
class VrSettingsController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->VrSetting->recursive = 0;
		$this->set('vrSettings', $this->paginate());
	}


/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->VrSetting->exists($id)) {
			throw new NotFoundException(__('Invalid booking setting'));
		}
		$options = array('conditions' => array('VrSetting.' . $this->VrSetting->primaryKey => $id));
		$this->set('vrSetting', $this->VrSetting->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
            //pr($this->request->data);die;
			if($this->request->data['VrSetting']['week_no'] != Null) {
				$comma_separated = implode("#", $this->request->data['VrSetting']['week_no']);
				$this->request->data['VrSetting']['week_no'] = '#'. $comma_separated . '#';
			}
			if($this->request->data['VrSetting']['holidaytype_id'] != Null) {
				$this->request->data['VrSetting']['start_date'] = Null;
				$this->request->data['VrSetting']['end_date'] = Null;
			}

			$this->VrSetting->create();
			if ($this->VrSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The booking setting has been saved'), 'default', array('class' => 'alert alert-success text-center'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The booking setting could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		}
		$this->loadModel('Zone');
		$zones = $this->Zone->find('list',array(
			'conditions' => array('Zone.type' => 'vr_zone')
			));
		$this->loadModel('Holidaytype');
		$holidaytypes = $this->Holidaytype->find('list');
		$this->set(compact('zones','holidaytypes'));
	}


/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->VrSetting->exists($id)) {
			throw new NotFoundException(__('Invalid booking setting'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->request->data['VrSetting']['week_no'] != Null) {
				$comma_separated = implode("#", $this->request->data['VrSetting']['week_no']);
				$this->request->data['VrSetting']['week_no'] = '#'. $comma_separated . '#';
			}
			if($this->request->data['VrSetting']['holidaytype_id'] != Null) {
				$this->request->data['VrSetting']['start_date'] = Null;
				$this->request->data['VrSetting']['end_date'] = Null;
			}
			if($this->request->data['VrSetting']['is_over_job_notification'] != 1) {
				$this->request->data['VrSetting']['over_job_amount'] = Null;
			}
			if ($this->VrSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The booking setting has been saved'), 'default', array('class' => 'alert alert-success text-center'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The booking setting could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('VrSetting.' . $this->VrSetting->primaryKey => $id));
			$this->request->data = $this->VrSetting->find('first', $options);
		}
		$this->loadModel('Zone');
		$zones = $this->Zone->find('list',array(
			'conditions' => array('Zone.type' => 'vr_zone')
			));
		$this->loadModel('Holidaytype');
		$holidaytypes = $this->Holidaytype->find('list');
		$this->set(compact('zones','holidaytypes'));
	}

	
/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->VrSetting->id = $id;
		if (!$this->VrSetting->exists()) {
			throw new NotFoundException(__('Invalid booking setting'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->VrSetting->delete()) {
			$this->Session->setFlash(__('Booking setting deleted'), 'default', array('class' => 'alert alert-success text-center'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Booking setting was not deleted'), 'default', array('class' => 'alert alert-danger text-center'));
		$this->redirect(array('action' => 'index'));
	}
	
/**
 * g method
 * Method to get booking settings
 */	
	public function g() {
		$this->autoRender = false;
        if($this->request->is('post') && !empty($this->request->data)){
            $lat = $this->request->data['Booking']['lat'];
            $lng = $this->request->data['Booking']['lng'];
            $at_time = $this->request->data['Booking']['at_time'];
            $requester = $this->request->data['Booking']['requester'];
            $booking_setting = $this->get_vr_setting($lat, $lng, $at_time, $requester);
            //$booking_setting = $this->getVrSettingDetail($lat, $lng, $at_time, $requester);
            if(empty($booking_setting)) {
                die(json_encode(array('success' => false, 'message' => 'No settings found')));
            } else {
                //die(json_encode(array('success' => true, 'VrSetting' => array_pop($booking_setting))));
                die(json_encode(array('success' => true, 'VrSetting' => $booking_setting)));
            }
        } else die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
	}



	

/**
 * Get Booking Setting detail
 */
 /*   public function getVrSettingDetail($lat, $lng, $at_time, $requester){

        // Getting all Booking Settings for current user zone
        $pick_up_day = date('w', strtotime($at_time)); // get weekday of the booking
        $z_settings = $this->get_setting_for_zone($lat, $lng, $pick_up_day); // get vr_settings for this zone
        $zone_id = $z_settings[0]['VrSetting']['zone_id'];
        //find the nearest driver
        //App::import('Model','Queue');
        //$this->Queue = new Queue();
        $nearest_driver = $this->_driver($lat, $lng, $zone_id, -1, $requester);
        $s_lat = $nearest_driver['Driver']['lat'];
        $s_lng = $nearest_driver['Driver']['lng'];
        $get_taxi_in_sec = $this->VrSetting->get_driving_time($s_lat, $s_lng, $lat, $lng);
       
        $settings = null; //initialize settings
        if(!empty($z_settings)) {
            $settings = $z_settings; // assign vr_settings' zone output for that particular zone
            // Filter Setting by Date
            $pick_up_date = date('Y-m-d', strtotime($at_time));
            $d_settings = $this->get_settings_by_date($pick_up_date, $settings); // assign vr_settings' zone + date output for that particular zone
            if(!empty($d_settings)) {
                $settings = $d_settings; // assign vr_settings' zone + date output for that particular zone
                // Filter Setting by Time
                $pick_up_time = date('H:i:s', strtotime($at_time));
                $t_settings = $this->get_settings_by_time($pick_up_time, $settings); // assign vr_settings' zone+ date + time output for that particular zone
                if(!empty($t_settings)) 
                    $settings = $t_settings; // assign vr_settings' zone + time + date output for that particular zone
            }
        }
        if(empty($settings))  $settings = $this->get_default_setting(); // get vr_settings for default zone when no particular settigns found for that zone

        $settings[0]['get_taxi_in_sec'] = $get_taxi_in_sec;
        return $settings;
    }
*/

/**
 * Check for Zone and get setting
 *
 * @param: $lat and $lng
 */
 /*   public function get_default_setting(){
        $setting_from_zone = $this->VrSetting->find('all', array(
            'recursive' => -1, 
            'conditions' => array('VrSetting.zone_id IS NULL')));
    
        //print_r($setting_from_zone[0]['VrSetting']['zone_id']);
        return $setting_from_zone;
    }

    public function get_setting_for_zone($lat, $lng, $pick_up_day){
        $this->loadModel('Zone');
        $zone = $this->Zone->getZone($lat, $lng);
        $setting_from_zone = $this->VrSetting->find('all', array(
            'recursive' => -1, 
            'conditions' => array(
                   'VrSetting.zone_id' => $zone['Zone']['id'],
                   'VrSetting.week_no LIKE ' => '%#'.$pick_up_day.'#%'
                )));

        //print_r($setting_from_zone[0]['VrSetting']['zone_id']);
        return $setting_from_zone;
    }

*/
/**
 * Get Setting by pick date matching
 *
 * @param: pick up date and setting for zone
 */
/*    public function get_settings_by_date($pick_up_date,$setting_from_zone){
        $results = array();
        $pick_up_date = strtotime($pick_up_date);
        foreach($setting_from_zone as $setting){
            $start_date = strtotime($setting['VrSetting']['start_date']);
            $end_date = strtotime($setting['VrSetting']['end_date']);
            if($start_date == $end_date && $pick_up_date == $start_date){
                $result[] = $setting;
                break;
            } else if($start_date != $end_date && ($pick_up_date >= $start_date && $pick_up_date <= $end_date)){
                $results[] = $setting;
            }
        }
        return $results;
    }
*/

/**
 * Get Setting by pick TIME matching
 *
 * @param: pick up date and setting for zone
 */
/*    public function get_settings_by_time($pick_up_time,$settings_get_by_date){
        $result = array();
        $pick_up_time = strtotime(date('H:i:s', strtotime($pick_up_time)));
        foreach($settings_get_by_date as $setting){
            $start_time = strtotime($setting['VrSetting']['start_time']);
            $end_time = strtotime($setting['VrSetting']['end_time']);
            if($start_time == $end_time && $pick_up_time == $start_time){
                $result = $setting;
                break;
            } else if($start_time != $end_time && ($pick_up_time >= $start_time && $pick_up_time <= $end_time)){
                $result = $setting;
                break;
            } else if($start_time != $end_time && ($pick_up_time >= $end_time && $pick_up_time <= $start_time)){
                $result = $setting;
                break;
            }
        }
        return $result;
    }
*/

/**
 * Check for Advance Booking
 *
 * @param: $lat and $lng
 */
    public function is_advance_booking_and_acceptable($booking, $setting){
        $interval_in_minute = round(abs(strtotime($booking['Booking']['at_time']) - strtotime('now')) / 60);
       // $min_advance_booking_time = $setting['VrSetting']['min_advance_booking_time'];
     //   $is_advance_booking = $setting['VrSetting']['min_advance_booking_time'] < $interval_in_minute;
        $is_acceptable = $setting['VrSetting']['is_advance_booking_on'];
       // return compact('is_advance_booking', 'is_acceptable', 'min_advance_booking_time');
         return compact('is_acceptable');
    }


    public function get_vr_setting($lat, $lng, $at_time, $requester){
        $this->autoRender = false;
        //print_r($at_time);
       // $at_time = date_create($at_time);
        $time = date('H:i:s', strtotime($at_time));
        $day = date('Y-m-d', strtotime($at_time));
        $weekday = date('w', strtotime($at_time));
        $nearest_driver = $this->_driver($lat, $lng, $zone_id, -1, $requester);
       // print_r($nearest_driver);
        $s_lat = $nearest_driver['Driver']['lat'];
        $s_lng = $nearest_driver['Driver']['lng'];
        $get_taxi_in_sec = $this->VrSetting->get_driving_time($s_lat, $s_lng, $lat, $lng);
        $this->loadModel('Zone');
        $this->loadModel('Holiday');
        $zone = $this->Zone->getZone($lat, $lng);
        $zone_id = $zone['Zone']['id'];
        $is_holiday = $this->Holiday->findByHoliday($day, array('Holiday.holidaytype_id'));
        $settings = null;
        if(!empty($is_holiday)){
            $settings = $this->VrSetting->find('all', array(
                    'recursive' => -1,
                    'conditions' => array(
                        'VrSetting.zone_id' => $zone_id,
                        'VrSetting.start_time <=' => $time,
                        'VrSetting.end_time >=' => $time,
                        'VrSetting.holidaytype_id' => $is_holiday['Holiday']['holidaytype_id']
                    ),
                    'fields' => array('VrSetting.is_advance_booking_on'),
                ));
        } if(empty($is_holiday)) {
                $settings = $this->VrSetting->find('all',array(
                        'recursive' => -1,
                        'conditions' => array(
                            'VrSetting.zone_id' => $zone_id,
                            'VrSetting.start_time <=' => $time,
                            'VrSetting.end_time >=' => $time,
                            'VrSetting.week_no LIKE' => '%#'.$weekday.'#%'
                        ),
                        'fields' => array('VrSetting.is_advance_booking_on'),
                    ));
                if(empty($settings)) {
                    $settings = $this->VrSetting->find('all',array(
                        'recursive' => -1,
                        'conditions' => array(
                            'VrSetting.zone_id IS NULL'
                        ),
                        'fields' => array('VrSetting.is_advance_booking_on'),
                    ));
                }
            }
            // print_r($settings[0]['VrSetting']['is_advance_booking_on']);
            $settings[0]['VrSetting']['get_taxi_in_sec'] = $get_taxi_in_sec;
        //    print_r($settings[0]['VrSetting']);
            return ($settings[0]['VrSetting']);
        
        
    }

}
