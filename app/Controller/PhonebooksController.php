<?php
App::uses('AppController', 'Controller');
/**
 * Phonebooks Controller
 *
 * @property Phonebook $Phonebook
 * @property PaginatorComponent $Paginator
 */
class PhonebooksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Phonebook->recursive = 0;
		$this->set('phonebooks', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Phonebook->exists($id)) {
			throw new NotFoundException(__('Invalid phonebook'));
		}
		$options = array('conditions' => array('Phonebook.' . $this->Phonebook->primaryKey => $id));
		$this->set('phonebook', $this->Phonebook->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Phonebook->create();
			if ($this->Phonebook->save($this->request->data)) {
				$this->Flash->success(__('The phonebook has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The phonebook could not be saved. Please, try again.'));
			}
		}
		$users = $this->Phonebook->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Phonebook->exists($id)) {
			throw new NotFoundException(__('Invalid phonebook'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Phonebook->save($this->request->data)) {
				$this->Flash->success(__('The phonebook has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The phonebook could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Phonebook.' . $this->Phonebook->primaryKey => $id));
			$this->request->data = $this->Phonebook->find('first', $options);
		}
		$users = $this->Phonebook->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Phonebook->id = $id;
		if (!$this->Phonebook->exists()) {
			throw new NotFoundException(__('Invalid phonebook'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Phonebook->delete()) {
			$this->Flash->success(__('The phonebook has been deleted.'));
		} else {
			$this->Flash->error(__('The phonebook could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
