<?php
App::uses('AppController', 'Controller');
/**
 * Aclsmanages Controller
 *
 * @property Aclsmanage $Aclsmanage
 * @property PaginatorComponent $Paginator
 */
class AclsmanagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		/*$this->Aclsmanage->recursive = 0;
		$this->set('aclsmanages', $this->Paginator->paginate());*/
       // pr($this->params['pass']);die;

        $type = $this->params['pass'][0];
        $this->Aclsmanage->recursive = 0;
        $options = array('fields' => ['Aclsmanage.acl_id'],'conditions' => ['Aclsmanage.type'  => $type,'Aclsmanage.access'  => 1]);
        $selected_acls = $this->Aclsmanage->find('list', $options);
        $this->loadModel('Acl');
        $aclsmanages = $this->Acl->find('all', [ 'recursive' => -1]);
        $this->set(compact('aclsmanages','selected_acls'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Aclsmanage->exists($id)) {
			throw new NotFoundException(__('Invalid aclsmanage'));
		}
		$options = array('conditions' => array('Aclsmanage.' . $this->Aclsmanage->primaryKey => $id));
		$this->set('aclsmanage', $this->Aclsmanage->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
        if ($this->request->is('post')) {
            $type  = $this->params['pass'][0];
            //pr($this->request->data);die;

            //delete previous
            $this->Aclsmanage->deleteAll(['Aclsmanage.type' => $type]);
            $acls = $this->request->data['Aclsmanage']['acl_id'];

            if(!empty($acls)) {
                foreach($acls as $acl) {
                    $data['Aclsmanage'] = [
                        'type' => $type,
                        'acl_id' => $acl,
                        'access' => 1,
                    ];
                    $this->Aclsmanage->create();
                    $this->Aclsmanage->save($data);
                }
                $this->Session->setFlash(__('Permission has been granted.'), 'default', array('class' => 'alert alert-success text-center'));
                return $this->redirect(array('action' => 'index',$type));
            } else {
                $this->Session->setFlash(__('You didn\'t select any menus.'), 'default', array('class' => 'alert alert-warning text-center'));
                return $this->redirect(array('action' => 'add',$type));
            }
        }
		/*$acls = $this->Aclsmanage->Acl->find('list', array('fields'=>array('Acl.permission_name')));
        //pr($acls);die;
		$this->set(compact('acls'));*/
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Aclsmanage->exists($id)) {
			throw new NotFoundException(__('Invalid aclsmanage'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Aclsmanage->save($this->request->data)) {
				$this->Flash->success(__('The aclsmanage has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The aclsmanage could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('Aclsmanage.' . $this->Aclsmanage->primaryKey => $id));
			$this->request->data = $this->Aclsmanage->find('first', $options);
		}
		$acls = $this->Aclsmanage->Acl->find('list', array('fields'=>array('Acl.permission_name')));
		$this->set(compact('acls'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Aclsmanage->id = $id;
		if (!$this->Aclsmanage->exists()) {
			throw new NotFoundException(__('Invalid aclsmanage'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Aclsmanage->delete()) {
			$this->Flash->success(__('The aclsmanage has been deleted.'), 'default', array('class' => 'alert alert-success text-center'));
		} else {
			$this->Flash->error(__('The aclsmanage could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
