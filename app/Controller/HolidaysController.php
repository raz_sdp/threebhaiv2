<?php
App::uses('AppController', 'Controller');
/**
 * Holidays Controller
 *
 * @property Holiday $Holiday
 * @property PaginatorComponent $Paginator
 */
class HolidaysController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Holiday->recursive = 0;
		$this->set('holidays', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Holiday->exists($id)) {
			throw new NotFoundException(__('Invalid holiday'));
		}
		$options = array('conditions' => array('Holiday.' . $this->Holiday->primaryKey => $id));
		$this->set('holiday', $this->Holiday->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Holiday->create();
			if ($this->Holiday->save($this->request->data)) {
                $this->Session->setFlash(__('The holiday has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The holiday could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		}
		$holidaytypes = $this->Holiday->Holidaytype->find('list');
		$this->set(compact('holidaytypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Holiday->exists($id)) {
			throw new NotFoundException(__('Invalid holiday'));
		}
		if ($this->request->is(array('post','put'))) {


           //print_r($this->request->data);die;
			if ($this->Holiday->save($this->request->data)) {
               // print_r($this->request->data);die;
				$this->Session->setFlash(__('The holiday has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The holiday could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('Holiday.' . $this->Holiday->primaryKey => $id));
			$this->request->data = $this->Holiday->find('first', $options);
		}
		$holidaytypes = $this->Holiday->Holidaytype->find('list');
		$this->set(compact('holidaytypes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Holiday->id = $id;
		if (!$this->Holiday->exists()) {
			throw new NotFoundException(__('Invalid holiday'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Holiday->delete()) {
			$this->Session->setFlash(__('The holiday has been deleted.'), 'default', array('class' => 'alert alert-success text-center'));
		} else {
			$this->Session->setFlash(__('The holiday could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	// public function g(){
		
 //        $this->autoRender = false;
 //        $str = "";
 //        // read the post from PayPal system and add 'cmd'
 //        $req = 'cmd=_notify-validate';

 //        foreach ($_POST as $key => $value) 
 //        {
 //            $value = urlencode(stripslashes($value));
 //            $req .= "&$key=$value";
 //        }

 //        // post back to PayPal system to validate
 //        $header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
 //        $header .= "Host: www.paypal.com:80\r\n";
 //        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
 //        $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
 //        //$fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);
 //        $fp = fsockopen('tls://www.sandbox.paypal.com', 443, $errno, $errstr, 30);

 //        // assign posted variables to local variables
 //        $item_name          = $_POST['item_name'];
 //        $item_number        = $_POST['item_number'];
 //        $payment_status     = $_POST['payment_status'];
 //        $payment_amount     = $_POST['mc_gross'];
 //        $payment_currency   = $_POST['mc_currency'];
 //        $txn_id             = $_POST['txn_id'];
 //        $receiver_email     = $_POST['receiver_email'];
 //        $payer_email        = $_POST['payer_email'];
 //        $userId             = $_POST['custom'];
 //        $uid = 0;


 //        if (!$fp){
 //            // HTTP ERROR
 //        } else{
 //            fputs ($fp, $header . $req);
 //            while (!feof($fp))
 //            {
 //                $res = fgets ($fp, 1024);
 //                if (strcmp ($res, "VERIFIED") == 0) 
 //                {
 //                }
 //                else if (strcmp ($res, "INVALID") == 0)
 //                {
 //                }
 //            }
 //            fclose ($fp);
 //        }


 //        $arrInvalidStatus = array('Denied', 'Expired', 'Failed', 'Voided');
 //        $ifFlag           = 0;
 //        $msg              = '';
 //        $sql              = '';

 //        if ( !in_array($payment_status, $arrInvalidStatus) )
 //        {
 //            // save in db for userId
 //                $data['Transaction']['user_id'] = $userId;
 //                $data['Transaction']['amount'] = $payment_amount;
 //                $data['Transaction']['service'] = 'top_up';
 //                $this->Transaction->create();
 //                $this->Transaction->save($data, false);

 //        }
 //    }

}
