<?php
App::uses('AppController', 'Controller');
/**
 * Settings Controller
 *
 * @property Setting $Setting
 * @property PaginatorComponent $Paginator
 */
class PhoneSettingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		//$this->Setting->recursive = -1;
		$this->PhoneSetting->recursive = 0;
		$this->paginate = array(
				'conditions' => array('PhoneSetting.zone_id NOT IN(1044, 1051)'),
			);
		//$this->set('phoneQueues', $this->paginate());
		$phoneSettings = $this->paginate();
		$this->set(compact('phoneSettings'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PhoneSetting->exists($id)) {
			throw new NotFoundException(__('Invalid setting'));
		}
		$options = array('conditions' => array('PhoneSetting.' . $this->PhoneSetting->primaryKey => $id));
		$this->set('phoneSetting', $this->PhoneSetting->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			//print_r($this->request->data); exit;
			if ($this->request->data['PhoneSetting']['restriction'] != Null) {
						$comma_separated = implode("#", $this->request->data['PhoneSetting']['restriction']);
						$this->request->data['PhoneSetting']['restriction'] = '#'. $comma_separated . '#';
			}
			$zone_duplicate = $this->PhoneSetting->find('first', array(
					'conditions' => array('zone_id' => $this->request->data['PhoneSetting']['zone_id'])
				));
			//print_r($zone_duplicate); exit;
			if(!empty($zone_duplicate['PhoneSetting']['zone_id'])) {
				$this->Session->setFlash(__('The setting for this zone is already exists.'), 'default', array('class' => 'alert alert-warning text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->PhoneSetting->create();
				if ($this->PhoneSetting->save($this->request->data)) {
					$this->Session->setFlash(__('The setting has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The setting could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
				}
			}
			
		}
		$this->loadModel('Zone');
		$zones = $this->Zone->find('list',array('conditions' => array('Zone.type <>' => 'vr_zone')));
		$this->set(compact('zones'));
		
		
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PhoneSetting->exists($id)) {
			throw new NotFoundException(__('Invalid setting'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->request->data['PhoneSetting']['restriction'] != Null) {
				$comma_separated = implode("#", $this->request->data['PhoneSetting']['restriction']);
				$this->request->data['PhoneSetting']['restriction'] = '#'. $comma_separated . '#';
			}
			if ($this->PhoneSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The setting has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index',$this->request->data['PhoneSetting']['setting_type']));
			} else {
				$this->Session->setFlash(__('The setting could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('PhoneSetting.' . $this->PhoneSetting->primaryKey => $id));
			$this->request->data = $this->PhoneSetting->find('first', $options);
		}
		$this->loadModel('Zone');
		$this->recursive = -1;		
		$zones = $this->Zone->find('list',array('conditions' => array('Zone.type <>' => 'vr_zone')));
		$this->set(compact('zones'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PhoneSetting->id = $id;
		if (!$this->PhoneSetting->exists()) {
			throw new NotFoundException(__('Invalid setting'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PhoneSetting->delete()) {
			$this->Session->setFlash(__('The setting has been deleted.'), 'default', array('class' => 'alert alert-success text-center'));
		} else {
			$this->Session->setFlash(__('The setting could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
		}
		return $this->redirect($this->referer());
	}}
