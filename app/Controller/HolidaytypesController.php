<?php
App::uses('AppController', 'Controller');
/**
 * Holidaytypes Controller
 *
 * @property Holidaytype $Holidaytype
 * @property PaginatorComponent $Paginator
 */
class HolidaytypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Holidaytype->recursive = 1;
		$holidaytypes = $this->paginate();
		$this->set(compact('holidaytypes'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Holidaytype->exists($id)) {
			throw new NotFoundException(__('Invalid holidaytype'));
		}
		$options = array('conditions' => array('Holidaytype.' . $this->Holidaytype->primaryKey => $id));
		$this->set('holidaytype', $this->Holidaytype->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post') && !empty($this->request->data['Holiday']['holiday'][0])) {
			$this->Holidaytype->create();
			if ($this->Holidaytype->save($this->request->data)) {
				if(!empty($this->request->data['Holidaytype']['is_repeat'])) {
					// repeat this date for next ten years
					for ($i=0; $i<10; $i++) {
						$holiday['Holiday']['holiday'] = $i == 0 ? date('Y-m-d', strtotime(str_replace('/', '-', $this->request->data['Holiday']['holiday'][0]))) : 
						date('Y-m-d', strtotime(str_replace('/', '-', $this->request->data['Holiday']['holiday'][0]) . ' +' . $i . ' years'));
						$holiday['Holiday']['holidaytype_id'] = $this->Holidaytype->id; 
						$this->Holidaytype->Holiday->create();
						$this->Holidaytype->Holiday->save($holiday);
					}
				} else {
					foreach ($this->request->data['Holiday']['holiday'] as $day) {
						if(!empty($day)) {
							$holiday['Holiday']['holiday'] = date('Y-m-d', strtotime(str_replace('/', '-', $day)));
							$holiday['Holiday']['holidaytype_id'] = $this->Holidaytype->id; 
							$this->Holidaytype->Holiday->create();
							$this->Holidaytype->Holiday->save($holiday);
						}
					}
				} 				
				$this->Session->setFlash(__('The holiday has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The holiday could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
				return $this->redirect(array('action' => 'index'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Holidaytype->exists($id)) {
			throw new NotFoundException(__('Invalid holidaytype'));
		}
		if ($this->request->is('post') || $this->request->is('put') && !empty($this->request->data['Holiday']['holiday'][0])) {
            $this->Holidaytype->id=$id;
			if ($this->Holidaytype->save($this->request->data)) {
                //pr($this->request->data);  die;
				// at first remove all existing holidays for this holiday type
				$this->Holidaytype->Holiday->deleteAll(array('Holiday.holidaytype_id' => $id), false);
				if(!empty($this->request->data['Holidaytype']['is_repeat'])) {
					// repeat this date for next ten years
					for ($i=0; $i<10; $i++) {
                        $date = str_replace('/', '-', @$this->request->data['Holiday']['holiday'][0]);
						$holiday['Holiday']['holiday'] = $i == 0 ? date_format( date_create($date), 'Y-m-d') :
                            date('Y-m-d',strtotime(str_replace('/', '-', @$this->request->data['Holiday']['holiday'][$i]) . ' +' . $i . ' years'));
						$holiday['Holiday']['holidaytype_id'] = $this->Holidaytype->id;

                        //pr($holiday);
						$this->Holidaytype->Holiday->create();
						$this->Holidaytype->Holiday->save($holiday);
					}
				} else {
					foreach ($this->request->data['Holiday']['holiday'] as $day) {
						if(!empty($day)) {
							$holiday['Holiday']['holiday'] = date('Y-m-d', strtotime(str_replace('/', '-', $day)));
							$holiday['Holiday']['holidaytype_id'] = $this->Holidaytype->id; 
							$this->Holidaytype->Holiday->create();
							$this->Holidaytype->Holiday->save($holiday);
						}
					}
				} 	
				$this->Session->setFlash(__('The holiday has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The holiday could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('Holidaytype.' . $this->Holidaytype->primaryKey => $id));
			$this->request->data = $this->Holidaytype->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Holidaytype->id = $id;
		if (!$this->Holidaytype->exists()) {
			throw new NotFoundException(__('Invalid holidaytype'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Holidaytype->delete()) {
			$this->loadModel('Holiday');
			$this->Holidaytype->query('DELETE FROM holidays WHERE holidaytype_id = \'' . $id . ' \'
			');
			$this->Session->setFlash(__('The holidaytype has been deleted.'), 'default', array('class' => 'alert alert-success text-center'));
		} else {
			$this->Session->setFlash(__('The holidaytype could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
