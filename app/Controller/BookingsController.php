<?php
App::uses('AppController', 'Controller');
/**
 * Bookings Controller
 *
 * @property Booking $Booking
 * @property User $User
 */
class BookingsController extends AppController {

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $conditions = [];
        if(AuthComponent::user(['type']) == 'vendor'){
            $this->loadModel('User');
            $conditions = am($conditions, ['User.type' => 'driver']);
            $users = $this->User->find('list', ['fields' => ['User.id'],'conditions' => $conditions]);
            //$user_ids = implode(',',$users);
            /*$bookings = $this->Booking->paginate("
            SELECT * FROM bookings WHERE acceptor IN ($user_ids)
            ");*/
            $this->paginate = [
                'recursive' => 0,
                'conditions' => [
                    'Booking.acceptor IN' => $users
                ]
            ];

            $bookings = $this->paginate();


//            $bookings = count($totalBookings);
            //$bookings = $this->paginate();
        } else {
            $this->Booking->recursive = 0;
            $this->paginate = array(
                'limit' => 25,
                'conditions' => array('Booking.pick_up IS NOT NULL'),
                'order' => 'Booking.created DESC',
            );
            $bookings = $this->paginate();

        }
        //pr($bookings);die;
        $booking_list = $this->Booking->find('all');
        $this->set(compact('bookings', 'booking_list'));
    }


    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        if (!$this->Booking->exists($id)) {
            throw new NotFoundException(__('Invalid booking'));
        }
        $options = array('conditions' => array('Booking.' . $this->Booking->primaryKey => $id));
        $this->set('booking', $this->Booking->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Booking->create();
            if ($this->Booking->save($this->request->data)) {
                $this->Session->setFlash(__('The booking has been saved'), 'default', array('class' => 'alert alert-success text-center'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The booking could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
            }
        }
        //$areaCodes = $this->Booking->Areacode->find('list');
        //$this->set(compact('areaCodes'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        if (!$this->Booking->exists($id)) {
            throw new NotFoundException(__('Invalid booking'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Booking->save($this->request->data)) {
                $this->Session->setFlash(__('The booking has been saved'), 'default', array('class' => 'alert alert-success text-center'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The booking could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
            }
        } else {
            $options = array('conditions' => array('Booking.' . $this->Booking->primaryKey => $id));
            $this->request->data = $this->Booking->find('first', $options);
        }
        //$areacodes = $this->Booking->Areacode->find('list');
        //$this->set(compact('areaCodes'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        $this->Booking->id = $id;
        if (!$this->Booking->exists()) {
            throw new NotFoundException(__('Invalid booking'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Booking->delete()) {
            $this->loadModel('Transaction');
            $this->Transaction->query('DELETE FROM transactions WHERE booking_id = \'' . $id . ' \'
			');
            $this->Session->setFlash(__('Booking deleted'), 'default', array('class' => 'alert alert-success text-center'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Booking was not deleted'), 'default', array('class' => 'alert alert-danger text-center'));
        $this->redirect(array('action' => 'index'));
    }


    // check for booking settings
    private function _check_booking_acceptability($booking) {
        $booking_setting = $this->Booking->checkBookingAcceptability($booking);
        return $booking_setting;
    }

    public function book_a_taxi(){
        ini_set('max_execution_time', '10000');
        set_time_limit(360000000000);
        $this->autoRender = false;
        // $this->request->data['Booking'] = array(
        // 	'pick_up' => '9 Montague Close, London SE1, UK',
        // 	// 'pick_up' => 'Khulna, Bangladesh',
        // 	'destination' => '16 Monument St, London EC3R, UK',
        // 	// 'destination' => 'Barisal, Bangladesh',
        // 	'dest_via' => array('110-143 Cannon St, London EC4N 5BP, UK'),
        // 	'at_time' => '2014-01-10 06:04:04',
        // 	'persons' => '2',
        // 	'car_type' => '6 Seats',
        // 	'no_of_luggages' => '3',
        // 	'note_to_driver' => '',
        // 	'from_web' => '1'
        // 	);

        //print_r($this->request->data['Booking']);
        //exit();
        if($this->request->is('post') || $this->request->is('put')){
			//print_r($this->request->data);
            $pick_up = $this->request->data['Booking']['pick_up'];
            $destination = $this->request->data['Booking']['destination'];
            $dest_via = !empty($this->request->data['Booking']['dest_via'])?$this->request->data['Booking']['dest_via']:'';

            $address_json = $this->_get_lat_lng($pick_up, $destination);

            $waypoints = array();
            foreach ($address_json['routes'][0]['legs'][0]['via_waypoint'] as  $key => $waypoint) {
                $waypoints[] = array(
                    'lat' => $waypoint['location']['lat'],
                    'lng' => $waypoint['location']['lng'],
                    'address' => $dest_via[$key],
                );
            }

            //pickup address is airport?
            if(strpos($address_json['routes'][0]['legs'][0]['start_address'], 'Airport') !== false) {
                $pick_up_airport = 1;
            } else{
                $pick_up_airport = 0;
            }

            //destination address is airport?
            if(strpos($address_json['routes'][0]['legs'][0]['end_address'], 'Airport') !== false) {
                $destination_airport = 1;
            } else{
                $destination_airport = 0;
            }

//		print_r($address_json);
//        exit;

            if(empty($address_json['routes'])) {
                echo json_encode(array('message' => 'There is a problem with your pick up point or destination, please check and try again'));
            } else {
                $this->request->data['Booking'] = array(
                    //'pick_up' => $address_json['routes'][0]['legs'][0]['start_address'],
                    'mobile' => $this->request->data['Booking']['mobile'],
                    'pick_up' => $this->request->data['Booking']['pick_up'],
                    'pick_up_lat' => $address_json['routes'][0]['legs'][0]['start_location']['lat'],
                    'pick_up_lng' => $address_json['routes'][0]['legs'][0]['start_location']['lng'],
                    'destination' => $this->request->data['Booking']['destination'],
                    'destination_lat' => $address_json['routes'][0]['legs'][0]['end_location']['lat'],
                    'destination_lng' => $address_json['routes'][0]['legs'][0]['end_location']['lng'],
                    'via' => json_encode($waypoints),
                    'distance_google' => $address_json['routes'][0]['legs'][0]['distance']['text'],
                    'at_time' => $this->request->data['Booking']['at_time'],
                    'pick_up_airport' => $pick_up_airport,
                    'destination_airport' => $destination_airport,
                    'is_sap_booking' => $this->request->data['Booking']['sap'],
                    'persons' => $this->request->data['Booking']['persons'],
                    'car_type' => $this->request->data['Booking']['car_type'],
                    'no_of_luggages' => $this->request->data['Booking']['no_of_luggages'],
                    'note_to_driver' => $this->request->data['Booking']['note_to_driver'],
                    'from_web' => $this->request->data['Booking']['from_web'],
                    'pick_up_postcode' => @$this->request->data['Booking']['pick_up_postcode'],
                    'destination_postcode' => @$this->request->data['Booking']['destination_postcode'],
                    'branch_number' => $this->request->data['Booking']['branch_number'],
                    'driver_number' => $this->request->data['Booking']['driver_number'],
                    'method_1' => empty($this->request->data['Booking']['method_1'])?null:$this->request->data['Booking']['method_1'],
                    'method_2' => empty($this->request->data['Booking']['method_2'])?null:$this->request->data['Booking']['method_2'],
                    'method_3' => empty($this->request->data['Booking']['method_3'])?null:$this->request->data['Booking']['method_3'],
                    'fare' => $this->request->data['Booking']['fare'],
                    'creator' => $this->request->data['Booking']['creator']
                );
//                print_r($this->request->data);
//                exit;
                //echo json_encode(array('success' => true,'output' => $data));
                $this->booking();
            }

        }
    }


    // add new booking
    public function booking() {

        //	$IS_POST = $this->request->is('post');
        $time_pre = microtime(true);

        //pr($this->request->data); exit;

        /*	// test only
            if($is_test == 'yes'){
                $IS_POST = true;
                $this->request->data['Booking'] = array(

                    'requester' => 137,

                   'persons' => 4,

               //	 'total_distance' => 72.08,
                  'distance_google' => '1.1 mi',

                   'no_of_luggages' => 3,

                   'is_paid' => '',

                   'destination_postcode' => 'IG8 0QG',

                   'route' => '',

                   'pick_up' => '28 Victoria Road, Buckhurst Hill, Essex IG9 5ES, UK',

                   'pick_up_lng' => 0.04608260000000001,
                 //	'pick_up_lng' => -0.14958009123802185,

                   'destination_lng' => 0.0309841,
                 //'destination_lng' => -0.1489792764186859,

                   'note_to_driver' => 'Dsfdsafsdg',

                   'recurring' => 'Monthly',

                //   'destination_airport' => 1,
                  'destination_airport' => 0,

                   //'destination' => "76A Queen's Road, Buckhurst Hill, Essex IG9, UK",
                    'destination' => '24 Woodland Way, Woodford, Woodford Green, Greater London IG8 0QG, UK',

                   'fare' => 10,

                   'pick_up_lat' => 51.6267332,
                 // 'pick_up_lat' => 51.50304905202606,

                   'car_type' => 'ANY',

                   'destination_lat' => 51.6204745,

                   'pick_up_airport' => 0,
                //   'pick_up_airport' => 1,

                   'at_time' => '2014-02-27 21:06:55',

                   'is_pre_book' => 0,

                   'recurring_start' => '2014-02-27 21:06:55',

                   'pick_up_postcode' => 'IG9 5ES',

                   'acceptor' => '',

                   'via' => '[]',

                   'accept_time' => '',

                   'bar_driver' => '[]',

                   'special_requirement' => 'Asdewfdsafdadsf',

                   'recurring_end' => '2014-01-29 14:51:00',

                   'booking_type' => 'vr',

                   'customer_phn_no' => '123456',

                   'is_sap_booking' => 1,
                );
            } */
        // end of test data
       // print_r($this->request->data);
        $is_sap_booking = $this->request->data['Booking']['is_sap_booking'];


        //print_r($this->request->data); exit;

        $this->autoRender = false;
        $this->loadModel('User');
            #$this->request->data['Booking']['requester'] = $this->Auth->user('id');    //For old version
            $this->request->data['Booking']['requester'] = $this->_getPassengerIdByMobile( $this->request->data['Booking']['mobile']);


        $user = $this->User->findById($this->request->data['Booking']['requester']);

        if(empty($user)) die(json_encode(array('success' => false, 'message' => 'Invalid User.')));
        else {
            if($this->request->is('post') || $this->request->is('put') && !empty($this->request->data['Booking'])) {
                //convert distance (mile or ft) to distance mile and trim the unit
                $dis = $this->request->data['Booking']['distance_google'];
                $check_data = strstr($dis, 'ft');
                if (empty($check_data)) {
                    $check_data = strstr($dis, 'mi');
                    if($check_data === 'mi') {
                        $this->request->data['Booking']['total_distance'] = trim($dis, ' mi');
                    } else {
                        $this->request->data['Booking']['total_distance'] = $dis;
                    }
                } else {
                    $this->request->data['Booking']['total_distance'] = (trim($dis, ' ft')/5280);
                }

                $user = $this->User->findById($this->request->data['Booking']['requester']);
                if($user['User']['type'] == 'passenger') {
                    $this->request->data['Booking']['customer_phn_no'] = $user['User']['mobile'];
                }
                if(!empty($this->request->data['Booking']['recurring'])){
                    $this->request->data['Booking']['at_time'] = $this->request->data['Booking']['recurring_start'];
                }

                // check booking settings and save if booking is acceptable
                //   $booking_setting = $this->_check_booking_acceptability($this->request->data);
                // if($booking_setting['success']){
                $this->loadModel('Zone');
                $lat = $this->request->data['Booking']['pick_up_lat'];
                $lng = $this->request->data['Booking']['pick_up_lng'];
                $zone_id = $this->Zone->find('first',array(
                    'recursive' => -1,
                    'conditions' => array("type = 'vr_zone'",
                        "CONTAINS (GEOMFROMTEXT(Zone.polygon), GEOMFROMTEXT('POINT($lat $lng)'))"
                    ),
                    'fields'=> array('Zone.id')
                ));
                $this->request->data['Booking']['zone_id'] = $zone_id['Zone']['id'];

                #stop for new version
                /*$fare_obj = $this->_calculate_fare($this->request->data['Booking']);
                $this->request->data['Booking']['fare'] = $fare_obj['amount'];*/
                $this->Booking->create();

                if($this->Booking->save($this->request->data, false)){
                    // send email and sms to a passenger who created booking via web
                    if($user['User']['type'] == 'passenger') {
                        try{
                            $this->_sendSms($user['User']['mobile'], "Thank you for using CabbieAppUK! Your booking has been received. Your booking number is ". $this->Booking->id);
                            $this->_sendEmail($user['User']['email'], "CabbieAppUK! - Booking Confirmation ". $this->Booking->id, "Dear ".$user['User']['name'].",<br/><br/>Thank you for booking your taxi with CabbieAppUK!<br/><br/>Your booking number is ". $this->Booking->id.".<br/><br/>Regards,<br/><br/>The CabbieApp Team");
                        } catch (Exception $e){

                        }

                    }

                    // sent admin over-job notification
                    $this->notifiy_admin_over_job($this->Booking->id);

                    if($is_sap_booking == 1) {

                        $booking_id = $this->Booking->id;
                        $requester = $this->request->data['Booking']['requester'];
                        $passenger = $this->request->data['Booking']['persons'];
                        $car = $this->request->data['Booking']['car_type'];
                        $branch_number = $this->request->data['Booking']['branch_number'];
                        $method_1 = $this->request->data['Booking']['method_1'];
                        $method_2 = $this->request->data['Booking']['method_2'];
                        $method_3 = $this->request->data['Booking']['method_3'];
                        $driver_number = $this->request->data['Booking']['driver_number'];

                        $search_info =  $this->_search_driver($booking_id, $lat, $lng, $requester, $passenger, $car, $is_sap_booking,$branch_number,$method_1,$method_2,$method_3,$driver_number);
                        //print_r($search_info);

                    }

                    if($is_sap_booking != 1) {
                        $response = json_encode(array(
                            'success' => true,
                            'Booking' => array(
                            'is_sap_booking' => false,
                                //'fare' => $fare_obj['amount'],
                                //	'message' => $booking_setting['message'],
                                // 'fare_msg' => $fare_obj['msg']),
                        )
                        ));
                    } else {
                        $response = json_encode(array(
                            'success' => $search_info['search_driver'],
                            'message' => $search_info['message']

                        ));
                    }

                    // API log
                    $this->_apiLog(
                        $this->request->data['Booking']['requester'],
                        var_export($this->request->data, true),
                        $response,
                        1,
                        $time_pre);

                    die($response);
                }
                // die(json_encode(array('success' => true, 'VrSetting' => array('fare' => $fare_obj['amount'], 'message' => $booking_setting['message'], 'fare_msg' => $fare_obj['msg']))));
                else
                    die(json_encode(array('success' => false, 'message' => 'Sorry, Booking Failed.')));
                //   } else die(json_encode(array('success' => false, 'message' => $booking_setting['message'])));
            } else die(json_encode(array('success' => false, 'message' => 'Invalid Request Method')));
        }
    }

    // update booking
    public function update() {
        $this->autoRender = false;
        if($this->request->is('post') && !empty($this->request->data) && !empty($this->request->data['Booking']['id'])) {
            $this->Booking->save($this->request->data, false);
            echo json_encode(array('success' => true));
        } else die(json_encode(array('success' => false, 'message' => __('Invalid request'))));
    }

    /**
     * Method cncl
     *
     * Cancel a booking by passenger
     * Also delete the booking record
     */
    public function cncl($booking_id) {
        $this->autoRender = false;
        if(empty($booking_id)) die(json_encode(array('success' => false, 'message' => __('Invalid request'))));
        $this->Booking->recursive = -1;
        $booking = $this->Booking->findById($booking_id, array('requester', 'acceptor'));
        if(!empty($booking['Booking']['requester'])) {
            $driver_id = $booking['Booking']['acceptor'];
            $this->Booking->query("UPDATE queues SET response = 'cleared' WHERE user_id = '$driver_id' AND booking_id = '$booking_id'");
            if(!empty($booking['Booking']['acceptor'])) {
                $this->_push($driver_id, 'Your booking has been cancelled by passenger.','CabbieCall', $booking_id, null, true); // notify driver for booking cancellation (c = true),
            }

            $booking_record = $this->Booking->query("INSERT INTO archived_bookings SELECT * FROM bookings WHERE id = '$booking_id'");
            $this->Booking->id = $booking_id;
            if($this->Booking->delete()) echo json_encode(array('success' => true));
        } else die(json_encode(array('success' => false, 'message' => __('You\'re not authorized to delete this booking.'))));
    }

    // when driver accept/rejects a booking
    public function accept(){
        $this->autoRender = false;
        if($this->request->is('post') && !empty($this->request->data)){
            // Cache driver_id and response status
            $driver_id = $this->request->data['Booking']['driver_id'];
            $response = trim($this->request->data['Booking']['response']);

            //FIXME: check if the booking has already been accepted by other driver
            //$acceptance_check = $this->Booking->findById($this->request->data['Booking']['id'], array('Booking.acceptor'));
            //if(!empty($acceptance_check['Booking']['acceptor'])) die(json_encode(array('success' => false, 'message' => 'Booking already accepted by other driver')));

            //check if the booking's response time is over or not
            $driver_check = $this->Booking->Queue->findByUserIdAndBookingId($driver_id, $this->request->data['Booking']['id']);
            if(!empty($driver_check['Queue']['response'])) die(json_encode(array('success' => false, 'message' => 'Request Time Out')));

            if( $response == 'accepted') {
                $this->request->data['Booking']['accept_time'] = date("Y-m-d H:i:s");
                $this->request->data['Booking']['acceptor'] = $driver_id;
                $this->Booking->id = $this->request->data['Booking']['id'];
                if(!$this->Booking->save($this->request->data,false)){
                    die(json_encode(array('success' => false, 'message' => 'Operation Failed')));
                }
            }
            //else {
//                $this->request->data['Booking']['accept_time'] = null;
//                $this->request->data['Booking']['acceptor'] = null;
//            }
//            if(!$this->Booking->save($this->request->data, false)) die(json_encode(array('success' => false, 'message' => 'Operation Failed')));
            // Update Queue table status
            //$queue_id = $this->Booking->Queue->field('id', array('Queue.booking_id' => $this->request->data['Booking']['id'], 'Queue.user_id' => $driver_id));
            /* //check if the booking has already been accepted, cleared or auto_cancelled or the request has been sent to other driver
                $check_response = $this->Booking->Queue->find('first', array(
                    'conditions'=>array(
                            'Queue.response ' => array('accepted'),
                            'Queue.booking_id' => $this->request->data['Booking']['id']
                        )
                ));
                if(!empty($check_response)) return false;
            */
            if($driver_check['Queue']['id']) {
                $this->Booking->Queue->id = $driver_check['Queue']['id'];
                $this->Booking->Queue->saveField('response_time', date('Y-m-d H:i:s'));
                $this->Booking->Queue->saveField('response', $response);
            }
            // send notification on accept booking to passenger
            $this->Booking->recursive = 0;
            $booking = $this->Booking->findById($this->request->data['Booking']['id']);
            if($response == 'rejected' || $response == 'accepted'){
                $booking_id = $booking['Booking']['id'];
                $this->Booking->Queue->id = $driver_check['Queue']['id'];
                $this->Booking->Queue->saveField('status', $response);
                $this->Booking->id = $booking_id;
                $this->Booking->saveField('status', $response);
//                $this->Booking->query("
//					UPDATE bookings
//						SET status = '$response'
//						WHERE
//						id = '$booking_id'
//					");

                //check if the driver will be log-out after specified no of rejection or auto logout after accepting a
                $logout = $this->_should_auto_logout($driver_id, $booking['Booking']['zone_id'], $response);
            }
            //find out if the requester is a driver or not
            $this->loadModel('User');
            $user = $this->User->findById($booking['Requester']['id'], array('type','email', 'mobile', 'name','vehicle_licence_no','branch_number'));
            if($user['User']['type'] == 'passenger') {
                //check if the booking is created via web
                //$booking_check = $this->Booking->findById($this->request->data['Booking']['id'], array('from_web'));
//                if($booking_check['Booking']['from_web'] == 1){
                    try{
                        $push_data = [
                            'booking_time' => $booking['Booking']['at_time'],
                            'booking_id' => $booking_id,
                            'driver_name' => $user['User']['name'],
                            'driver_id' => $driver_id,
                            'vehicle_licence_no' => $user['User']['vehicle_licence_no'],
                            'price' => $booking['Booking']['fare'],
                            'status' => $response
                        ];
                        $this->User->recursive = -1;
                        $vendor = $this->User->findByBranchNumberAndType($user['User']['branch_number'],'driver',['id']);
                        $this->_pushAll($driver_id,$push_data);
                        $this->_pushAll($vendor['User']['id'],$push_data);
//                        $this->_sendSms($user['User']['mobile'], 'CabbieAppUK! - Your request has been '. $response);
//                        $this->_sendEmail($user['User']['email'], "CabbieAppUK! - Booking Confirmation ". $booking_id, "Dear ".$user['User']['name'].",<br/><br/>Your request has been ". $response. "<br/><br/>Regards,<br/><br/>The CabbieApp Team");
                    } catch (Exception $e){

                    }
//                }
//                if($response == 'accepted') {
//                    $this->Booking->recursive = 0;
//                    $this->_push($booking['Requester']['id'], 'Your request has been accepted. View details in "My Account".','CabbieApp', null, null, null, null, null, null, null, null, null, true);
//                } else {
//                    $this->_push($booking['Requester']['id'], 'Your request has been rejected.','CabbieApp');
//                }
            }
            if($response == 'accepted'){
                die(json_encode(array('success' => true, 'Booking' =>  $booking['Booking'], 'Requester' => isset($booking['Requester']) ? $booking['Requester'] : null, 'Acceptor' => isset($booking['Acceptor']) ? $booking['Acceptor'] : null, 'is_logout' => $logout)));
            } elseif ($response == 'rejected') {
                die(json_encode(array('success' => true, 'is_logout' => $logout)));
            }
        } else die(json_encode(array('success' => false, 'message' => 'Invalid Request Method')));
    }

    // Get booking info
    public function gbi($id =null) {
        $this->autoRender = false;
        if(empty($id) || !$this->Booking->exists($id)) die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
        $return = array();
        $return['success'] = true;
        $this->Booking->recursive = 0;
        $booking = $this->Booking->findById($id);
        $keys = array_keys($booking);
        foreach($keys as $key) {
            $return[$key] = $booking[$key];
        }
        echo json_encode($return);
    }

    //view past bookings
    public function past_booking(){

    }

    // getall booking list
    // @params: $code, [$is_paid]
    public function g($user_id=null){
        $this->autoRender = false;
        if (!empty($user_id)) {
            // Get user type
            $this->loadModel('User');
            $user = $this->User->findById($user_id);
            $this->Booking->Behaviors->attach('Containable');
            $this->Booking->contain('Requester', 'Acceptor', 'Queue');
            $this->Booking->Queue->Behaviors->attach('Containable');
            //$this->Booking->Queue->contain('Requester', 'Acceptor');
//            if(!empty($is_paid)) {
//                if($user['User']['type'] == 'driver') {
//                    $booking_list = $this->Booking->findAllByAcceptorAndIsPaid($user['User']['id'], $is_paid);
//                    if (empty($booking_list)) {
//                        $booking_list = $this->Booking->findAllByAcceptorAndStatus($user['User']['id'], 'cleared');
//                    }
//                } else {
//                    //$booking_list = $this->Booking->findAllByRequesterAndIsPaid($user['User']['id'], $is_paid);
//                    $now = date("Y-m-d H:i:s");
//                    $requester = $user['User']['id'];
//                    $booking_list = $this->Booking->find('all', array(
//                        'conditions' => array("(requester = '$requester' AND TIMESTAMPDIFF(MINUTE, at_time, '$now') >= '45')
//								OR (requester = '$requester' AND is_paid = '$is_paid')
//								OR (requester = '$requester' AND status = 'cleared')
//								OR (requester = '$requester' AND status = 'no_driver_found' AND is_sap_booking = '1')")
//                    ));
//                }
//            } else {
                if($user['User']['type'] == 'driver') {
                    //$booking_list = $this->Booking->findAllByAcceptor($user['User']['id']);
                    $booking_lists = $this->Booking->Queue->find('all', array(
                        'contain' => array('Booking' => array('Acceptor', 'Requester')),
                        'conditions' => array(
                            'user_id' => $user['User']['id'],
                            "response IN('accepted')",
                            'Booking.id IS NOT NULL'
                        )
                    ));
                    $booking_list = array();
                    foreach ($booking_lists as $k => $booking) {


                        $booking_list[$k]['Booking'] = $booking['Booking'];
                        unset($booking_list[$k]['Booking']['Requester']);
                        unset($booking_list[$k]['Booking']['Acceptor']);

                        $booking_list[$k]['Queue'][] = $booking['Queue'];
                        $booking_list[$k]['Requester'] = $booking['Booking']['Requester'];
                        $booking_list[$k]['Acceptor'] = $booking['Booking']['Acceptor'];


                    }
                } else {
                    /*	$booking_list = $this->Booking->find('all', array(
                                'conditions' => array(
                                    'is_paid' => 0,
                                    'status <>' => 'cleared',
                                    'requester' => $user['User']['id'],
                            ))); */
                    $requester = $user['User']['id'];
                    $booking_list = $this->Booking->find('all', array(
                        'conditions' => array("(requester = '$requester' AND is_paid = '0' AND status <> 'cleared' AND (is_sap_booking <> '1' OR is_sap_booking IS NULL))
						OR (requester = '$requester' AND status NOT IN ('no_driver_found', 'cleared') AND is_sap_booking = '1')")
                    ));

                    /*	("SELECT * FROM bookings as Booking
                            WHERE (requester = '$requester' AND is_paid = 0 AND status <> 'cleared')
                            OR (requester = '$requester' AND status NOT IN ('no_driver_found', 'cleared') AND is_sap_booking = '1')");*/
                }
 //           }
            //print_r($booking_list);
            if(!empty($booking_list)){
                echo json_encode(array('success' => true, 'bookings' => $booking_list));
            }else{
                echo json_encode(array('success' => false, 'message'=> 'no bookings found'));
            }
        } else {
            echo json_encode(array('success' => false, 'message'=> 'no valid code found'));
            exit;
        }
    }


    /**
     * Get User's Position
     */

    public function gtrac() {
        $this->autoRender = false;
        if($this->request->is('post') && !empty($this->request->data) && !empty($this->request->data['Booking']['id'])) {
            $this->Booking->recursive = 0;
            $booking = $this->Booking->findById($this->request->data['Booking']['id']);
            if($booking) {
                $driver = $booking['Acceptor'];
                die(json_encode(array('success' => true, 'Acceptor' => $driver)));
            } else die(json_encode(array('success' => false, 'message' => 'No record found')));
        } else die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
    }


    /**
     * Search for driver who accepted a job
     */

    public function hunt() {
        $this->autoRender = false;
        if($this->request->is('post') && !empty($this->request->data)) {
            $this->Booking->recursive = 0;
            $booking = $this->Booking->findByIdAndRequesterAndAcceptor($this->request->data['Booking']['id'], $this->request->data['Booking']['requester'], $this->request->data['Booking']['acceptor'],array('Booking.id', 'Acceptor.lat', 'Acceptor.lng'));
            if(!$booking || empty($booking)) {
                die(json_encode(array('success' => false, 'message' => 'No Booking found.')));
            } else die(json_encode(array('success' => true, 'Acceptor' => $booking['Acceptor'])));
        } else die(json_encode(array('success' => false, 'message' => 'Invalid request')));
    }

    /**
     * Get Pending Bookings of a driver
     */
    public function pndg($driver_id=null) {
        $this->autoRender = false;
        if(empty($driver_id)) die(json_encode(array('success' => false, 'message' => 'Not valid request')));
        //$this->Booking->Queue->recursive = -1;
        $this->Booking->Queue->Behaviors->attach('Containable');
        $results = $this->Booking->Queue->find('all',
            array(
                'contain' => array(
                    'Booking' => array(
                        'Requester'
                    )
                ),
                // Todo: need to implement recurring within condition
                'conditions' => array(
                    'Queue.response' => NULL,
                    'Queue.user_id' => $driver_id,
                    'Queue.booking_id <>' => NULL
                )
            )
        );
        if(empty($results)) die(json_encode(array('success' => false, 'message' => 'No record found')));
        else {
            $bookings = Hash::extract($results, '{n}.Booking');
            $response['success'] = true;
            $response['bookings'] = array();
            foreach($bookings as $booking) {
                $response['bookings'][]['Booking'] = $booking;
            }
            die(json_encode($response));
        }
    }

    /*
     * Method to update queue when drier arrive
     * to passenger's door
     */
    public function ondr()
    {
        $this->autoRender = false;
        if ($this->request->is('post') && !empty($this->request->data)) {
            $booking_id = $this->request->data['Booking']['id'];
            $driver_id = $this->request->data['Booking']['acceptor'];
            $passenger_id = $this->request->data['Booking']['requester'];
            if (empty($booking_id)) die(json_encode(array('success' => false, 'message' => __('This booking no longer exists in the system.'))));
            $time_pre = microtime(true);
            $this->loadModel('User');
            $this->User->Behaviors->load('Containable');
            $options = [
                'contain' => ['DeviceToken'],
                'fields' => array('type', 'email', 'mobile', 'name'),
                'conditions' => [
                    'User.id' => $passenger_id
                ]
            ];
            #$requester = $this->User->findById($passenger_id, array('type', 'email', 'mobile', 'name'));
            $requester = $this->User->find('first', $options);
            // Update Queue table status for On Door
            $queue = $this->Booking->Queue->findByBookingIdAndUserId($booking_id, $driver_id, array('Queue.id'));
            $this->Booking->Queue->id = $queue['Queue']['id'];
            $this->Booking->Queue->saveField('ondoor_time', $this->request->data['Queue']['ondoor_time']);
            //$response = $requester['User']['device_token'];
            // API log	
            /*$this->_apiLog(
              $passenger_id,
              var_export($this->request->data, true),
              $response,
              1,
               $time_pre
          );*/
            //check if the booking has been created via web
            $booking_check = $this->Booking->findById($booking_id, array('from_web'));
            // send notification when requester is passenger
            if ($requester['User']['type'] == 'passenger') {
                //$this->_push($passenger_id, 'Driver has arrived at pick up point','CabbieApp', null, null, null, null, null, null, null, null, true);

                $push_data = [
                    'booking_time' => $this->request->data['Booking']['booking_time'],
                    'booking_id' => $booking_id,
                    'driver_name' => $this->request->data['Booking']['driver_name'],
                    'driver_id' => $driver_id,
                    'vehicle_licence_no' => $this->request->data['Booking']['vehicle_licence_no'],
                    'price' => $this->request->data['Booking']['fare'],
                    'status' => 'arrived'
                ];

                $this->_pushAll($passenger_id,$push_data);
//                if ($booking_check['Booking']['from_web'] == 1) {
//                    try {
//                        $this->_sendSms($requester['User']['mobile'], 'CabbieAppUK! - Driver has arrived at your pick up point.');
//                        $this->_sendEmail($requester['User']['email'], "CabbieAppUK! - Booking Confirmation " . $booking_id, "Dear " . $requester['User']['name'] . ",<br/><br/>Driver has arrived at your pick up point.<br/><br/>Regards,<br/><br/>The CabbieApp Team");
//                    } catch (Exception $e) {
//
//                    }
//                }
                die(json_encode(array('success' => true, 'message' => __('Notification sent'))));
            } else {
                die(json_encode(array('success' => false, 'message' => __('Passenger Not Found.'))));
            }
        } else die(json_encode(array('success' => false, 'message' => __('Invalid Request'))));
    }

    /*
     * Method to update Queue pob_time when
     * Passenger on Board ie. Passenger on cab
     */
    public function pobtm(){
        $this->autoRender = false;
        if($this->request->is('post') && !empty($this->request->data)){
            $booking_id = $this->request->data['Booking']['id'];
            $driver_id = $this->request->data['Acceptor']['id'];
            if(empty($booking_id)) die(json_encode(array('success' => true, 'message' => __('Something went worng'))));
            // Update Queue table status for On Door
            //$queue_id = $this->Booking->Queue->field('id', array('Queue.booking_id' => $booking_id));
            //$this->Booking->Queue->id = $queue_id;
            $queue = $this->Booking->Queue->findByBookingIdAndUserId($booking_id, $driver_id, array('Queue.id'));
            $this->Booking->Queue->id = $queue['Queue']['id'];
            if($this->Booking->Queue->saveField('pob_time', $this->request->data['Queue']['pob_time'])){
                die(json_encode(array('success' => true, 'message' => __('POB updated'))));
            } else die(json_encode(array('success' => false, 'message' => __('POB update failed'))));
        } else die(json_encode(array('success' => false, 'message' => __('Invalid request'))));
    }

    /*
     * Method to update Queue no_show_time if
     * driver can't find the passenger
     */
    public function nshwt(){
        $this->autoRender = false;
        /*    $this->request->data['Booking']['id'] = 3;
            $this->request->data['Queue'] = array(
                    'no_show_time' => '2013-12-22 12:46:31',
                    'driver_decline_reason_id' => 4,
                    'user_comment' => 'xyz',
                ); */
        if($this->request->is('post') && !empty($this->request->data)){
            $booking_id = $this->request->data['Booking']['id'];
            $driver_id = $this->request->data['Acceptor']['id'];
            $this->request->data['Queue']['no_show_time'] = date('Y-m-d H:i:s');
            if(empty($booking_id)) die(json_encode(array('success' => false, 'message' => __('This booking no longer exists in the system.'))));
            // Update Queue table status for On Door
            $this->request->data['Queue']['response'] = 'cancelled';
            array_walk($this->request->data['Queue'], function(&$value, $key){$value="'".$value."'";});
            $this->Booking->Queue->updateAll($this->request->data['Queue'], array('Queue.booking_id' => $booking_id, 'Queue.user_id' => $driver_id));
            // send notification to passenger on decline booking
            $this->loadModel('User');
            //$requester = $this->User->findById($this->request->data['Booking']['requester'], array('User.type','User.email', 'User.mobile','User.name'));
            //$booking_check = $this->Booking->findById($this->request->data['Booking']['id'], array('from_web'));
            // print_r($requester);
//            if(trim($requester['User']['type']) == 'passenger') {
//                $this->_push($this->request->data['Booking']['requester'], 'Sorry your booking was cancelled by the driver. For more information please contact info@cabbieappuk.com','CabbieApp');
//
//                    try{
//                        $this->_sendSms($requester['User']['mobile'], 'CabbieAppUK! - Sorry, your booking was cancelled by the driver. For more information please contact info@cabbieappuk.com');
//                        $this->_sendEmail($requester['User']['email'], "CabbieAppUK! - Booking Confirmation ". $requester['User']['name'] , "Dear User,<br/><br/>Sorry, your booking was cancelled by the driver. For more information please contact info@cabbieappuk.com.<br/><br/>Regards,<br/><br/>The CabbieApp Team");
//                    }catch (Exception $e){
//
//                    }
//
//            }
            $pushData = [
                'status' => 'cancel',
                'msg' => 'Sorry your booking was cancelled by the driver.'
            ];
            $this->_pushAll($this->request->data['Booking']['requester'],$pushData);
            die(json_encode(array('success' => true, 'message' => __('No Show updated'))));
        } else die(json_encode(array('success' => false, 'message' => __('Invalid request'))));
    }

    /*
     * Method to execute when Driver hit the "Clear"
     * button and completed the job successfully
     */

    public function jclr(){
        $this->autoRender = false;
        if($this->request->is('post') && !empty($this->request->data)){
            $response = $this->request->data['Booking']['response'];
            $booking_id = $this->request->data['Booking']['id'];
            $driver_id = $this->request->data['Driver']['id'];
            // getting Booking details
            $booking_details = $this->Booking->findById($booking_id);
            // geting data from Queue table
            $queue = $this->Booking->Queue->findByBookingIdAndUserId($booking_id, $driver_id, array('Queue.id','Queue.response'));
            // checking Booking id
            if(empty($booking_details['Booking']['id'])) die(json_encode(array('success' => false, 'message' => 'This booking no longer exists in the system.')));
            // checking response
            if($queue['Queue']['response']!='accepted') die(json_encode(array('success' => false, 'message' => 'Something went wrong.')));
            // checking hit response
            if($response != 'cleared') die(json_encode(array('success' => false, 'message' => 'Wrong request.')));
            // update query table
            $this->Booking->Queue->id = $queue['Queue']['id'];
            if($this->Booking->Queue->saveField('response',$response)){
                // update Booking table
                $this->Booking->id = $booking_id;
                $this->Booking->saveField('status',$response);
                // Do transaction Driver and vendor
                if($this->_pay_vendor_for_booking($driver_id,$booking_details['Booking']['fare'],$booking_id,$booking_details['Booking']['creator'])){
                    die(json_encode(array('success' => true, 'message' =>'Well Done. Job complete')));
                } else {
                    die(json_encode(array('success' => false, 'message' =>'something error')));
                }

            } else die(json_encode(array('success' => false, 'message' => 'Sorry request failed. Try again')));
        } else die(json_encode(array('success' => false, 'message' => 'Invalid request')));
    }

    /**
     * Method driver_forward
     * Forward a accepted job to some other driver
     * for some obvious reason
     */
    public function driver_forward() {
        $this->autoRender = false;
        if($this->request->is('post') && !empty($this->request->data)){
            $acceptor_id = $this->request->data['Acceptor']['id'];
            $booking_id = $this->request->data['Booking']['id'];
            $this->Booking->recursive = -1;
            $queue = $this->Booking->Queue->findByUserIdAndBookingIdAndResponse($acceptor_id ,$booking_id, 'accepted',array('id'));
            $queue_id = $queue['Queue']['id'];
            if(!empty($queue_id)) {
                $this->Booking->id = $booking_id;
                $this->Booking->updateAll(array('Booking.acceptor' => null,'Booking.status' => "'forwarded'"), array('Booking.id' => $booking_id));
                /*$this->Booking->query("UPDATE bookings
                        SET status = 'forwarded' AND acceptor = NULL
                        WHERE
                        id = '$booking_id'"); */
                $this->Booking->Queue->updateAll(array('Queue.response' => "'forwarded'"), array('Queue.id' => $queue_id));
                die(json_encode(array('success' => true, 'message' => __('Booking Successfully Forwarded'))));
            } else die(json_encode(array('success' => false, 'message' => __('Forward Failed'))));
        } else die(json_encode(array('success' => false, 'message' => __('Invalid request'))));
    }


    /*
     * interrupt method
     *
     * Method should call when Driver wont take an
     * accepted booking due to some reason
     * e.g. Car broken and so on
     *
     * for this request:
     *	1. Cancel the booking
     *	2. Notify the passenger
     *	3. Send request to another driver
     */
    public function interrupt(){
        $this->autoRender = false;
        if($this->request->is('post') && !empty($this->request->data)){
            $driver_id = $this->request->data['Booking']['acceptor'];
            $passenger_id = $this->request->data['Booking']['requester'];
            $booking_id = $this->request->data['Booking']['id'];
            $intr_type = $this->request->data['Booking']['intr_type'];

            // Make corresponding queue entry for this booking as cancelled
            //$this->Booking->Queue->make_cancelled($driver_id, $booking_id);

            // Update booking entry acceptor to  null
            //$this->Booking->id = $booking_id;
            //$this->Booking->saveField('acceptor', null);

            // send notification
            $this->loadModel('User');
            $requester = $this->User->findById($passenger_id, array('type', 'email', 'mobile', 'name'));
            $booking_check = $this->Booking->findById($booking_id, array('from_web'));
            $message = 'Your Booking has been cancelled.';
            if($intr_type == 'traffic') $message = 'Sorry traffic on the way. We are apologizing for the delay';
            if($intr_type == 'broken') {
                $message = 'Sorry, my cab just have some issues. So failed to pick-up.';
                $queue = $this->Booking->Queue->findByUserIdAndBookingIdAndResponse($driver_id ,$booking_id, 'accepted',array('id'));
                $queue_id = $queue['Queue']['id'];
                if(!empty($queue_id)) {
                    $this->Booking->id = $booking_id;
                    $this->Booking->updateAll(array('Booking.acceptor' => null,'Booking.status' => "'forwarded'"), array('Booking.id' => $booking_id));
                    $this->Booking->Queue->updateAll(array('Queue.response' => "'forwarded'"), array('Queue.id' => $queue_id));
                }
            }
            if($requester['User']['type'] == 'passenger') {
                $this->_push($passenger_id, $message,'CabbieApp');
                if($booking_check['Booking']['from_web'] == 1){
                    try{
                        $this->_sendSms($requester['User']['mobile'], 'CabbieAppUK! - '.$message);
                        $this->_sendEmail($requester['User']['email'], "CabbieAppUK - Booking Confirmation ". $booking_id ,"Dear ".$requester['User']['name'].",<br/><br/>". $message. "<br/><br/>Regards,<br/><br/>The CabbieApp Team");
                    }catch (Exception $e){

                    }
                }
            }
            die(json_encode(array('success' => true)));
        } else die(json_encode(array('success' => false, 'message' => __('Invalid request'))));
    }

    /*
    public function test(){
        $this->autoRender = false;
		$requester_device_token = $this->Booking->Requester->findById(1, array('device_token'));
		debug($requester_device_token);
		$exclude_booking_ids = $this->Booking->Queue->get_booking_ids_except_cancelled();
		$r = $this->Booking->find('all', 
							array(
								'recursive' => 0,
								// Todo: need to implement recurring within condition
								'conditions' => array(
									'TIMESTAMPDIFF(MINUTE, NOW(), Booking.at_time) <= 30', // get all booking within next 30 minutes  	
									'Booking.acceptor' => 0, // no driver accepted yet
									'NOT' => array('Booking.id' => $exclude_booking_ids)
								)
							)
						);
		debug($r);
		exit;
        $queue = $this->Booking->Queue->field('id', array('Queue.booking_id' => 1));
        debug($queue); exit;
        $this->Booking->recursive = 0;
        debug(json_encode($this->Booking->findById('1')));

        $booking = array(
            'Booking' => array(
                'pick_up_lat' => '51.58633649681665',
                'pick_up_lng' => '-0.490264892578125',
                'at_time' => '2013-08-22 07:25:00',
                'total_distance' => '11.8'
            )
        );
        $availability = $this->Booking->checkBookingAcceptability($booking);
        debug($availability);
    } */

    private function _airport_name($str) {
        $airport_name = array_shift(preg_split('/\,|\(/',$str));
        return '%'.trim($airport_name).'%';
    }

    private function _calculate_fare($booking){
        //pr($booking);
        $this->loadModel('FareSetting');
        $this->loadModel('Holiday');
        $at_time = date_create($booking['at_time']);
        $time = date_format($at_time, 'H:i:s');
        $day = date_format($at_time, 'Y-m-d');
        $weekday = date_format($at_time, 'w');
        //	print_r($weekday);
        $set_fixed_fare = null;
        $is_holiday = $this->Holiday->findByHoliday($day, array('Holiday.holidaytype_id'));
        //print_r($is_holiday);
        $conditions = array();

        if ($booking['pick_up_airport']) {
            $conditions[] = "FareSetting.pick_up LIKE '" . $this->_airport_name($booking['pick_up']) . "'";
        } else {
            $conditions[] = "'" . $booking['pick_up_postcode'] . "' LIKE CONCAT('%', FareSetting.pick_up ,'%')";
        }

        if ($booking['destination_airport']) {
            $conditions[] = "FareSetting.destination LIKE '" . $this->_airport_name($booking['destination']) . "'";
        } else {
            $conditions[] = "'" . $booking['destination_postcode'] . "' LIKE CONCAT('%', FareSetting.destination ,'%')";
        }

        $holidayConditions = $conditions;

        $holidayConditions[] = "FareSetting.time_from <= '" . $time . "'";
        $holidayConditions[] = "FareSetting.time_to >= '" . $time . "'";
        $holidayConditions[] = "FareSetting.holidaytype_id = '" . $is_holiday['Holiday']['holidaytype_id'] . "'";



        if(!empty($is_holiday)){
            $set_fixed_fare = $this->FareSetting->find('first', array(
                'recursive' => -1,
                'conditions' => $holidayConditions //array(

                /*
                'FareSetting.pick_up LIKE' => $booking['pick_up_airport'] ? $this->_airport_name($booking['pick_up']) : '%'. $booking['pick_up_postcode'] .'%',
                'FareSetting.destination LIKE' => $booking['destination_airport'] ? $this->_airport_name($booking['destination']) : '%'. $booking['destination_postcode'] .'%',
                'FareSetting.time_from <=' => $time,
                'FareSetting.time_to >=' => $time,
                'FareSetting.holidaytype_id' => $is_holiday['Holiday']['holidaytype_id']
                */
                //)
            ));
            //	pr($set_fixed_fare);
        }


        if(empty($set_fixed_fare)) {
            $holidayConditions[] = "FareSetting.weeks LIKE '%#$weekday#%'";

            $set_fixed_fare = $this->FareSetting->find('first', array(
                    'recursive' => -1,
                    'conditions' => $holidayConditions //array(

                    /*
                    'FareSetting.pick_up LIKE' => $booking['pick_up_airport'] ? $this->_airport_name($booking['pick_up']) : '%'.$booking['pick_up_postcode'].'%',
                    "CONCAT('%', FareSetting.destination ,'%')" => $booking['destination_airport'] ? $this->_airport_name($booking['destination']) : '%'.$booking['destination_postcode'].'%',
                    'FareSetting.time_from <=' => $time,
                    'FareSetting.time_to >=' => $time,
                    'FareSetting.weeks LIKE' => '%#'.$weekday.'#%'
                    */
                    //)
                )
            );
            //	pr($set_fixed_fare);

            if(empty($set_fixed_fare)){
                $set_fixed_fare = $this->FareSetting->find('first', array(
                    'recursive' => -1,
                    'conditions' => $conditions
                    /*array(
                        'FareSetting.pick_up LIKE' => $booking['pick_up_airport'] ? $this->_airport_name($booking['pick_up']) : '%'.$booking['pick_up_postcode'].'%',
                        'FareSetting.destination LIKE' => $booking['destination_airport'] ? $this->_airport_name($booking['destination']) : '%'.$booking['destination_postcode'].'%',
                )*/
                ));
                //	pr($set_fixed_fare);
            }
        }
        $fare = null;
        if(!empty($set_fixed_fare)){
            $fare = $this->_fare_by_car_type($booking, $set_fixed_fare);
        } else {
            $lat = $booking['pick_up_lat'];
            $lng = $booking['pick_up_lng'];
            $set_fare_by_miles = null;
            $is_holiday = $this->Holiday->findByHoliday($day, array('Holiday.holidaytype_id'));
            if(!empty($is_holiday)){
                $set_fare_by_miles = $this->FareSetting->find('all', array(
                    'recursive' => -1,
                    'conditions' => array(
                        'FareSetting.zone_id' => $booking['zone_id'],
                        'FareSetting.fare_setting_type' => 'fare_by_miles',
                        'FareSetting.time_from <=' => $time,
                        'FareSetting.time_to >=' => $time,
                        'FareSetting.holidaytype_id' => $is_holiday['Holiday']['holidaytype_id']
                    ),
                    'order' => array('FareSetting.distance')
                ));
                // print_r('holiday ');
                // 	print_r($set_fare_by_miles);
            } if(empty($set_fare_by_miles)) {
                $set_fare_by_miles = $this->FareSetting->find('all',array(
                    'recursive' => -1,
                    'conditions' => array(
                        'FareSetting.zone_id' => $booking['zone_id'],
                        'FareSetting.fare_setting_type' => 'fare_by_miles',
                        'FareSetting.time_from <=' => $time,
                        'FareSetting.time_to >=' => $time,
                        'FareSetting.weeks LIKE' => '%#'.$weekday.'#%'
                    ),
                    'order' => array('FareSetting.distance')
                ));
                // print_r('zone ');
                // print_r($set_fare_by_miles);
                if(empty($set_fare_by_miles)) {
                    $set_fare_by_miles = $this->FareSetting->find('all',array(
                        'recursive' => -1,
                        'conditions' => array(
                            'FareSetting.fare_setting_type' => 'fare_by_miles',
                            'FareSetting.zone_id IS NULL'
                        ),
                        'order' => array('FareSetting.distance')
                    ));
                    // print_r('default ');
                    // print_r($set_fare_by_miles);
                }
            }
            if(!empty($set_fare_by_miles)){
                $fare =	$this->_fare_per_mile($booking, $set_fare_by_miles);
            }
        }
        if(empty($fare)){
            $fare = array(
                'amount' => 'undetermined',
                'msg' => 'Could not retrieve approximate fare.'
            );
        }
        return $fare;
    }

    private function _fare_by_car_type($booking, $set_fixed_fare) {

        $base_fare = $set_fixed_fare['FareSetting']['s_4'];
        $fare = 0;
        $seats = 0;
        switch ($booking['car_type']) {
            case '5 Seats':
                $fare = $booking['persons'] <= 5 ? $set_fixed_fare['FareSetting']['s_5'] : $set_fixed_fare['FareSetting']['s_'.$booking['persons']];
                break;
            case '6 Seats':
                $fare = $booking['persons'] <= 6 ? $set_fixed_fare['FareSetting']['s_6'] : $set_fixed_fare['FareSetting']['s_'.$booking['persons']];
                break;
            case '7 Seats':
                $fare = $booking['persons'] <= 7 ? $set_fixed_fare['FareSetting']['s_7'] : $set_fixed_fare['FareSetting']['s_'.$booking['persons']];;
                break;
            case '8 Seats':
                $fare = $set_fixed_fare['FareSetting']['s_8'];
                break;
            case 'Estate':
                $fare = $booking['persons'] <= 4 ? $set_fixed_fare['FareSetting']['e_4'] : $set_fixed_fare['FareSetting']['e_'.$booking['persons']];
                break;
            case 'Wheelchair':
                $fare = $booking['persons'] <= 3 ? $set_fixed_fare['FareSetting']['w_3'] : $set_fixed_fare['FareSetting']['w_'.min($booking['persons'],6)];
                break;
            case 'ANY':
            case 'Saloon car':
            default:
                $fare = $booking['persons'] <= 4 ? $set_fixed_fare['FareSetting']['s_4'] : $set_fixed_fare['FareSetting']['s_'.$booking['persons']];
                break;
        }
        $percent_check = strstr($fare, '%') ;
        if ($percent_check !== false) {
            $p = floatval(trim($fare, '%'));
            $sub_fare = round(($p * $base_fare) / 100, 2);
            $fare = $base_fare + $sub_fare;
        }
        return array(
            'amount' => $fare,
            'msg' => $set_fixed_fare['FareSetting']['note']
        );
    }


    private function _fare_per_mile($booking, $set_fare_by_miles){
        $min = array_shift($set_fare_by_miles); // array_shift is used so that we can get min fare (1st s_4)
        $fare_by_car = $this->_fare_by_car_type($booking, $min);
        $total_fare = $fare_by_car['amount'];
        // print_r(' total_fare(base fare) ');
        // print_r($total_fare);
        $total_distance = $booking['total_distance'];
        // print_r(' total_distance ');
        // print_r($total_distance);
        if($total_distance > $min['FareSetting']['distance']) {
            $distance_calculated = $min['FareSetting']['distance'];
            // print_r(' distance_calculated ');
            // print_r($distance_calculated);
            foreach ($set_fare_by_miles as $fare) {
                $distance = min($total_distance-$distance_calculated, $fare['FareSetting']['distance']-$distance_calculated);
                // print_r(' distance ');
                // print_r($distance);
                $fare_by_car = $this->_fare_by_car_type($booking, $fare);
                $total_fare += $distance*$fare_by_car['amount'];
                // print_r(' fare step ');
                // print_r($distance*$fare_by_car['amount']);
                // print_r(' total_fare ');
                // print_r($total_fare);
                if($total_distance > $fare['FareSetting']['distance']){
                    $distance_calculated = $fare['FareSetting']['distance'];
                    //print_r('distance_calculated');
                    //print_r($distance_calculated);
                    continue;
                } else {
                    break;
                }
            }
        }
        return array(
            'amount' => $total_fare,
            'msg' => $set_fare_by_miles['FareSetting']['note']
        );
    }

    private function _should_auto_logout($driver_id = null, $zone_id = null, $response = null){
        $logout = array();
        if($response == 'rejected') {
            if(empty($zone_id)){
                $this->loadModel('User');
                $this->User->id = $driver_id;
                $zone_id = $this->User->field('vr_zone');
            }
            $this->loadModel('Zone');
            $reject_threshold_for_zone = $this->Zone->findById($zone_id, array('reject_threshold'));
            $reject_threshold =$reject_threshold_for_zone['Zone']['reject_threshold'];
            if(empty($reject_threshold)) {
                $reject_threshold = 1; //set default threshold value
            }
            $reject_count = $this->Booking->query("SELECT
					COUNT(IF(res.response = 'rejected', 1, NULL))AS cnt
				FROM
					(
						SELECT
							response
						FROM
							queues
						WHERE
							user_id = '$driver_id'
						AND response IS NOT NULL
						ORDER BY
							response_time DESC
						LIMIT $reject_threshold
					)AS res");
            if($reject_count[0][0]['cnt'] >= $reject_threshold) {
                $this->Booking->query("UPDATE users SET vr_available = 'no', vr_zone=NULL WHERE id = '$driver_id'
				");
                $logout['vr'] = 'no';
            }
        } elseif($response == 'accepted') {
            $remain_loggedin_on_accept_zone = $this->Booking->query("SELECT remain_loggedin_on_accept FROM zones
					WHERE id = '$zone_id'");
            $remain_loggedin_on_accept = $remain_loggedin_on_accept_zone[0]['zones']['remain_loggedin_on_accept'];
            if(empty($remain_loggedin_on_accept)) {
                $remain_loggedin_on_accept = 1; //by default, log out a driver after accepting a job
            }
            if($remain_loggedin_on_accept == 1) {
                $this->Booking->query("UPDATE users SET vr_available = 'no', vr_zone=NULL WHERE id = '$driver_id'");
                $logout['vr'] = 'no';
            }
        }

        return $logout;

    }

    public function notifiy_admin_over_job($zone_id = null){
        $this->autoRender = false;
        $this->loadModel('VrSetting');
        $over_job_amount = $this->VrSetting->findByZoneId($zone_id, array('over_job_amount'));
        if(empty($over_job_amount)) {
            $over_job_amount = $this->VrSetting->findByZoneId(null, array('over_job_amount'));
        }
        if(!empty($over_job_amount['VrSetting']['over_job_amount'])){
            $over_job_count = $this->Booking->query("SELECT
				COUNT(id) AS cnt
				FROM(SELECT	id FROM	bookings WHERE
						acceptor IS NULL
					AND zone_id " .  (empty($zone_id) ? ' IS NULL ' : "= '$zone_id' ") . "
					AND TIMESTAMPDIFF(HOUR, at_time, NOW()) > '1'
				)AS res
			");
            if($over_job_count[0][0]['cnt'] >= $over_job_amount['VrSetting']['over_job_amount']) {
                $this->loadModel('Setting');
                $receiver = $this->Setting->findById(1, array('admin_email'));
                App::uses('CakeEmail', 'Network/Email');
                $email = new CakeEmail('smtp');
                $email->subject('Cabbie App — Over Job notification');
                $email->to($receiver['Setting']['admin_email']);
                $email->send("Admin,\nThere are more than ". $over_job_amount['VrSetting']['over_job_amount'] . " bookings in " .  (empty($zone_id) ? ' outside VR zones ' : " Zone No: '$zone_id' ") .".\nThanks");
            }
        }

    }

    public function try_driver_again(){
        $this->autoRender = false;
        if ($this->request->is('post') || $this->request->is('put')) {
            $booking_id = $this->request->data['Booking']['booking_id'];
            $lat = $this->request->data['Booking']['pick_up_lat'];
            $lng = $this->request->data['Booking']['pick_up_lng'];
            $requester = $this->request->data['Booking']['requester'];
            $is_sap_booking = $this->request->data['Booking']['is_sap_booking'];
            $booking_details = $this->Booking->findById($booking_id, array('status', 'persons', 'car_type'));
            if($booking_details['Booking']['status'] == 'cancelled_by_passenger') die(json_encode(array('success' => false, 'message' => __('Booking has already been cancelled'))));
            $passenger = $booking_details['Booking']['persons'];
            $car = $booking_details['Booking']['car_type'];
            $search_info =  $this->_search_driver($booking_id, $lat, $lng, $requester, $passenger, $car, $is_sap_booking);
            echo json_encode(array(
                'success' => true,
                'search_driver' => $search_info['search_driver'],
                'phone' => $search_info['phone'],
            ));
        } else echo json_encode(array('success'=> false));
    }

    public function cancel_sap_booking($booking_id){
        $this->autoRender = false;
        if(empty($booking_id)) die(json_encode(array('success' => false, 'message' => __('Invalid Booking'))));
        $booking_details = $this->Booking->findById($booking_id, array('is_sap_booking', 'status', 'acceptor'));
        if($booking_details['Booking']['is_sap_booking'] != 1) die(json_encode(array('success' => false, 'message' => __('Not A SAP Booking'))));
        if($booking_details['Booking']['status'] == 'cancelled_by_passenger') die(json_encode(array('success' => false, 'message' => __('Booking has already been cancelled'))));
        if(!empty($booking_id) && ($booking_details['Booking']['is_sap_booking'] == 1)) {
            $this->Booking->id = $booking_id;
            $this->Booking->saveField('status', 'cancelled_by_passenger');
            if(!empty($booking_details['Booking']['acceptor'])) {
                $driver_id = $booking_details['Booking']['acceptor'];
                $this->Booking->query("UPDATE queues SET response = 'cleared' WHERE user_id = '$driver_id' AND booking_id = '$booking_id'");
                $this->_push($driver_id, 'Your booking has been cancelled by passenger. For Details Go "My Account" "View Bookings"','CabbieCall', null, null, true);
            }
            die(json_encode(array('success' => true, 'message' => __('SAP Booking is cancelled'))));
        } else die(json_encode(array('success'=> false, 'message' => __('SAP Booking is not cancelled'))));

    }

    public function info($code, $booking_id){
        $this->autoRender = false;
        $this->loadModel('User');
        $user_obj = $this->User->findByCode($code, array('id', 'type'));
        if(!empty($user_obj)){
            if($user_obj['User']['type'] === 'driver'){
                $booking_obj = $this->Booking->findById($booking_id, array('pick_up', 'persons'));
                unset($booking_obj['Booking']['id']);
                if(!empty($booking_obj)) die(json_encode(array('success' => true, 'booking_info' => $booking_obj['Booking'])));
                else die(json_encode(array('success' => false, 'message' => 'Not A valid Booking ID')));
            } else die(json_encode(array('success' => false, 'message' => 'You are not a driver')));
        } else die(json_encode(array('success' => false, 'message' => 'Not A valid User')));
    }

    public function admin_deleteBookingRecord($period = null){
        $this->autoRender = false;
        /*$this->loadModel('User');
        $user_obj = $this->User->findById(AuthComponent::user('id'), array('id', 'type'));
        if(!empty($user_obj)){
            if($user_obj['User']['type'] === 'admin'){*/
                if(!empty($period)){
                    $this->Booking->query("DELETE FROM bookings WHERE at_time < DATE_SUB(NOW(), INTERVAL $period day)");
                }
                if ($this->request->is('post') || $this->request->is('put')) {
                    $from_date = !empty($this->request->data['from_date']) ? $this->request->data['from_date'] . ' 00:00:00': '';
                    $to_date = !empty($this->request->data['to_date']) ? $this->request->data['from_date'] . ' 23:59:59': '';
                    if(!empty($from_date) && !empty($to_date)){
                        $this->Booking->query("DELETE FROM bookings WHERE at_time BETWEEN TIMESTAMP('$from_date') AND TIMESTAMP('$to_date')");
                    }else{
                        $this->Booking->query("DELETE FROM bookings WHERE at_time = TIMESTAMP('$from_date') OR at_time = TIMESTAMP('$to_date')");
                    }
                }
                die(json_encode(['success' => true]));
            /*}else{

            }
        }*/
    }

//    ====================== ============================== According to V2 requirement =============================== ===================================================== =========================
    /**
     * it only use for get last booking information by mobile number
     */
    public function get_last_booking_info_mobile(){
        if($this->request->is('post')) {
            $mobile = $this->request->data['mobile'];
            // Get passenger id by mobile number
            $passenger_id = $this->_getPassengerIdByMobile($mobile);
            if(!empty($passenger_id))
                // Get last Booking information by passenger id
            $result = $this->Booking->getLastCustomersBookingInfo($passenger_id);
            //pr($result);
            if(!empty($result)) {
                die(json_encode(['success' => true, 'booking_data' => $result['Booking']]));
            } else die(json_encode(['success' => false, 'msg' => 'No Previous Booking Found.']));

        } else {
            die(json_encode(['success' => false, 'msg' => 'Invalid Request']));
        }
    }

    /**
     * it only use for get last booking information by user id
     */
    public function get_last_booking_info(){
        if($this->request->is('post')) {
            $passenger_id = $this->request->data['user_id'];
            if(!empty($passenger_id))
                // Get last Booking information by passenger id
                $result = $this->Booking->getLastCustomersBookingInfo($passenger_id);
            if(!empty($result)) {
                die(json_encode(['success' => true, 'booking_data' => $result['Booking']]));
            } else die(json_encode(['success' => false, 'msg' => 'No Previous Booking Found.']));

        } else {
            die(json_encode(['success' => false, 'msg' => 'Invalid Request']));
        }
    }

    /**
     * get mile by pick up address and destination
     */
    public function get_mile(){
        $pickup = $this->request->data['Booking']['pickup'];
        $destination = $this->request->data['Booking']['destination'];
        $amount =$this->request->data['Booking']['cost'];
        // Get lat lng by pickup and destination
        $address_json = $this->_get_lat_lng($pickup,$destination);
        $slat = $address_json['routes'][0]['legs'][0]['start_location']['lat'];
        $slng = $address_json['routes'][0]['legs'][0]['start_location']['lng'];
        $elat = $address_json['routes'][0]['legs'][0]['end_location']['lat'];
        $elng = $address_json['routes'][0]['legs'][0]['end_location']['lng'];
        // Get distance by lat lng
        $distance = $this->_distance_calculation($slat,$slng,$elat,$elng);
        // Calculate fare
        $fare = $distance * $amount;

        die(json_encode(['success' => true,'mile' => $distance,'fare'=>$fare]));
        //pr($fare);
        //pr($distance);die;

        //pr($waypoints);die;
        //$distance = $this
    }

    /**
     * @param $m
     * @param $n
     * it,s only use for query test
     */
    public function test($m=null,$n=null){
        //pr($this->_add_wallet($m,$n));die;
        $json = file_get_contents("https://maps.googleapis.com/maps/api/place/textsearch/json?query=XOR+Software+Solution&location=22.8190863,89.5432898&radius=10000&&type=doctor&key=AIzaSyBhlUzD8xgohlHKuw7QJ2y62MO2zjbDuP8");

        $d = json_decode($json);
        foreach($d->results as $result){

            $data = file_get_contents("https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyCej3iRHphjKOGUxNq0j2bk129bym0sAHY&sensor=false&fields=formatted_phone_number,name,formatted_address&reference=".$result->reference);
            $number = json_decode($data);
            //pr($number);die;
            $numbers[] = [
                'name' => $number->result->name,
                'number' => $number->result->formatted_phone_number,
                'address' => $number->result->formatted_address
            ];


        };
        $finalData = [
            'status' => 'not_found',
            'number' => $numbers
        ];
        $this->_pushAll($m,$finalData);die;
    }
    public function nearBySearch(){
        $json = file_get_contents("https://maps.googleapis.com/maps/api/place/textsearch/json?query=XOR+Software+Solution&location=22.8190863,89.5432898&radius=10000&&type=doctor&key=AIzaSyBhlUzD8xgohlHKuw7QJ2y62MO2zjbDuP8");
        $d = json_decode($json);
        foreach($d->results as $result){

            $data = file_get_contents("https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyBhlUzD8xgohlHKuw7QJ2y62MO2zjbDuP8&sensor=false&fields=formatted_phone_number,name,formatted_address&reference=".$result->reference);
            $number = json_decode($data);
            //pr($number);die;
            $numbers[] = [
                'name' => $number->result->name,
                'number' => $number->result->formatted_phone_number,
                'address' => $number->result->formatted_address
            ];


        };
pr($numbers);die;

    }

}
