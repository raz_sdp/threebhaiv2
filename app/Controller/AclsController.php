<?php
App::uses('AppController', 'Controller');
/**
 * Acls Controller
 *
 * @property Acl $Acl
 * @property PaginatorComponent $Paginator
 */
class AclsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Acl->recursive = 0;
		$this->set('acls', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Acl->exists($id)) {
			throw new NotFoundException(__('Invalid acl'));
		}
		$options = array('conditions' => array('Acl.' . $this->Acl->primaryKey => $id));
		$this->set('acl', $this->Acl->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Acl->create();
			if ($this->Acl->save($this->request->data)) {
				$this->Flash->success(__('The acl has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The acl could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		}
		$roles = $this->Acl->Role->find('list');
		$this->set(compact('roles'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Acl->exists($id)) {
			throw new NotFoundException(__('Invalid acl'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Acl->save($this->request->data)) {
				$this->Flash->success(__('The acl has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The acl could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('Acl.' . $this->Acl->primaryKey => $id));
			$this->request->data = $this->Acl->find('first', $options);
		}
		$roles = $this->Acl->Role->find('list');
		$this->set(compact('roles'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Acl->id = $id;
		if (!$this->Acl->exists()) {
			throw new NotFoundException(__('Invalid acl'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Acl->delete()) {
			$this->Flash->success(__('The acl has been deleted.'), 'default', array('class' => 'alert alert-success text-center'));
		} else {
			$this->Flash->error(__('The acl could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Acl->recursive = 0;
		$this->set('acls', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Acl->exists($id)) {
			throw new NotFoundException(__('Invalid acl'));
		}
		$options = array('conditions' => array('Acl.' . $this->Acl->primaryKey => $id));
		$this->set('acl', $this->Acl->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Acl->create();
			if ($this->Acl->save($this->request->data)) {
				$this->Flash->success(__('The acl has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The acl could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		}
		$roles = $this->Acl->Role->find('list');
		$this->set(compact('roles'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Acl->exists($id)) {
			throw new NotFoundException(__('Invalid acl'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Acl->save($this->request->data)) {
				$this->Flash->success(__('The acl has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The acl could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('Acl.' . $this->Acl->primaryKey => $id));
			$this->request->data = $this->Acl->find('first', $options);
		}
//		$roles = $this->Acl->Role->find('list');
//		$this->set(compact('roles'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Acl->id = $id;
		if (!$this->Acl->exists()) {
			throw new NotFoundException(__('Invalid acl'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Acl->delete()) {
			$this->Flash->success(__('The acl has been deleted.'), 'default', array('class' => 'alert alert-success text-center'));
		} else {
			$this->Flash->error(__('The acl could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
