<?php
App::uses('AppController', 'Controller');
/**
 * UsersAcls Controller
 *
 * @property UsersAcl $UsersAcl
 * @property PaginatorComponent $Paginator
 */
class UsersAclsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		/*$this->UsersAcl->recursive = 0;
		$this->set('usersAcls', $this->Paginator->paginate());*/
        $vendor_id = $this->params['pass'][0];
        #$type = $this->params['pass'][1];
        $this->UsersAcl->recursive = 0;
        $options = array('fields' =>['UsersAcl.acl_id'], 'conditions' => array('UsersAcl.user_id'  => $vendor_id));
        $selected_acls = $this->UsersAcl->find('list', $options);
        $query = [
            'fields' => ['Acl.*'],
        ];
        $aclsmanages = $this->UsersAcl->Acl->find('all', $query);
        $this->set(compact('aclsmanages','selected_acls'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->UsersAcl->exists($id)) {
			throw new NotFoundException(__('Invalid users acl'));
		}
		$options = array('conditions' => array('UsersAcl.' . $this->UsersAcl->primaryKey => $id));
		$this->set('usersAcl', $this->UsersAcl->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
            $vendor_id = $this->params['pass'][0];
            //pr($this->request->data);die;

            //delete previous
            $this->UsersAcl->deleteAll(['UsersAcl.user_id' => $vendor_id]);
            $acls = $this->request->data['UsersAcl']['acl_id'];
            if(!empty($acls)) {
                foreach($acls as $acl) {
                    $data['UsersAcl'] = [
                        'user_id' => $vendor_id,
                        'acl_id' => $acl,
                    ];
                    $this->UsersAcl->create();
                    $this->UsersAcl->save($data);
                }
                $this->Session->setFlash(__('Permission has been granted.'), 'default', array('class' => 'alert alert-success text-center'));
                return $this->redirect(array('action' => 'index',$vendor_id));
            } else {
                $this->Session->setFlash(__('You didn\'t select any menus.'), 'default', array('class' => 'alert alert-warning text-center'));
                return $this->redirect(array('action' => 'add',$vendor_id));
            }
		}
		/*$users = $this->UsersAcl->User->find('list');
		$acls = $this->UsersAcl->Acl->find('list');
		$this->set(compact('users', 'acls'));*/
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->UsersAcl->exists($id)) {
			throw new NotFoundException(__('Invalid users acl'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UsersAcl->save($this->request->data)) {
				$this->Flash->success(__('The users acl has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The users acl could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UsersAcl.' . $this->UsersAcl->primaryKey => $id));
			$this->request->data = $this->UsersAcl->find('first', $options);
		}
		$users = $this->UsersAcl->User->find('list');
		$acls = $this->UsersAcl->Acl->find('list');
		$this->set(compact('users', 'acls'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->UsersAcl->id = $id;
		if (!$this->UsersAcl->exists()) {
			throw new NotFoundException(__('Invalid users acl'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UsersAcl->delete()) {
			$this->Flash->success(__('The users acl has been deleted.'));
		} else {
			$this->Flash->error(__('The users acl could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
