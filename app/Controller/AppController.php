<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 *
 *
 */

/**
 * @property User $User
 */

/*
 * It's all about App to App calling
 * */
// if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', "::1"))) {
//     #Test credential
//     define('ACCOUNT_SID', 'AC380847079144a880d552a768a2968904');
//     define('API_KEY', '');
//     define('API_KEY_SECRET', '');
//     define('PUSH_CREDENTIAL_SID', '');
//     define('APP_SID', '');
// } else {
//     #Live credential
//     define('ACCOUNT_SID', 'ACd5880a6ed2193077c585763c514be2a2');
//     define('API_KEY', 'SKfc9933d283f110b05d157906d06e38bb');
//     define('API_KEY_SECRET', 'AoWsM7Mdm0t40DVaIGqVRGBtLPqjw9Xb');
//     define('PUSH_CREDENTIAL_SID', 'CRd8d1540f4ca234c3882c1d8946ade556');
//     define('APP_SID', 'APcbe2690daddc49b49b79000a71340621');

//     /*define('ACCOUNT_SID', 'ACd5880a6ed2193077c585763c514be2a2');
//     define('API_KEY', 'SKfc9933d283f110b05d157906d06e38bb');
//     define('API_KEY_SECRET', 'AoWsM7Mdm0t40DVaIGqVRGBtLPqjw9Xb');
//     define('PUSH_CREDENTIAL_SID', 'CRa1022bac6da61e4f472e33bbfa9d9e74');
//     define('APP_SID', 'APb2e59be341ef446891fe53837e99af80');*/
// }

define('ACCOUNT_SID', 'ACd5880a6ed2193077c585763c514be2a2');
define('API_KEY', 'SKfc9933d283f110b05d157906d06e38bb');
define('API_KEY_SECRET', 'AoWsM7Mdm0t40DVaIGqVRGBtLPqjw9Xb');
#define('PUSH_CREDENTIAL_SID', 'CR57d3fb5967f272a8249c26216424dadc');
define('APP_SID', 'APd542bd9814e34d9a90fe1ae650cef84a');

class AppController extends Controller
{
    /*
    var $API_KEY = 'SK987328f1a6b6dc30d4028c0d36d80283';
    var $API_KEY_SECRET = 'lW2MXWg9fIOloa0E2ZUsBQyVotOXd5D5';
    var $PUSH_CREDENTIAL_SID = 'CRaaa15d7e3080eb3c0a74afc9a9780b35';
    var $APP_SID = 'APd542bd9814e34d9a90fe1ae650cef84a';*/

    public $components = array(

        'Session', 'Cookie', 'RequestHandler',

        'Auth' => array(

            'loginAction' => array('controller' => 'users', 'action' => 'login', 'admin' => true),

            'loginRedirect' => array('controller' => 'users', 'action' => 'dashboard', 'admin' => true),

            'logoutRedirect' => array('controller' => 'users', 'action' => 'login', 'admin' => true),

            'authError' => 'You are not allowed',

            'authenticate' => array(

                'Form' => array(

                    'fields' => array('username' => 'email', 'password' => 'password')

                )

            )

        )

    );


    public function beforeFilter()
    {
        //pr($this->params);die;
        if ($this->params->prefix == 'admin' && ($this->Auth->User('type') != 'admin' && $this->Auth->User('type') != 'vendor')) $this->Auth->logout();
        if ($this->params['admin']) {

            $controller = $this->params['controller'];

            $action = $this->params['action'];

            if ($controller == 'users' && $action == 'admin_despatch_job') {

                $this->layout = 'default';

            } else {
                $this->layout = 'admin';
            }


        } elseif ($this->params->url == 'pages/login') {

            $this->layout = 'login';

        }
//         if($this->params->prefix == 'admin' && $this->Auth->User('type')!='admin'){
//         if(!$this->isAuthorized($this->Auth->User('type'))){
//             $this->Session->setFlash(__('You are not authorized for this page.'), 'default', array('class' => 'alert alert-danger text-center'));
//             $this->redirect(['controller' => 'users','action' => 'dashboard']);
//             die;
//         }
//         }
        //for get request

        if (!empty($this->params->pass[0])) {

            if (preg_match("/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/", $this->params->pass[0])) {

                $code = $this->params->pass[0];

            }

        }

        //for post rrequest

        if (!empty($this->params->data[$this->modelClass]['code'])) {

            if (preg_match("/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/", $this->params->data[$this->modelClass]['code'])) {

                $code = $this->params->data[$this->modelClass]['code'];

                $this->loadModel('User');

                $this->User->recursive = -1;

                $userExist = $this->User->findByCode($code, array('User.id'));

                if (!empty($userExist)) {

                    $this->User->id = $userExist['User']['id'];

                    $this->User->saveField('last_activity', date('Y-m-d H:i:s'));

                }

            }

        }
        $this->Auth->allow(

            'display', 'register', 'update', 'iforgot', 'pwreset', 'booking_settings', 'distributedBooking', 'driver_forward', 'get_vr_setting',

            'accept', 'booking', 'g', 'zonelists', 'utrac', 'gtrac', 'cncl', 'hunt', 'sdvr', 'pndg', 'gbard', 'dobar', 'login', 'logout',

            'undobar', 'isdav', 'spsngrs', 'gdqp', 'push', 'user_logout', 'ondr', 'pobtm', 'nshwt', 'jclr', 'do_top_up', 'cancel_sap_booking', 'driver_in_danger_info',

            'emergency', 'interrupt', 'udinfo', 'do_trans', 'guab', 'vun', 'position', 'cash_in_hand', 'cronjobZoneSettings', 'bank_details',

            'passenger_login', 'driver_login', 'profit', 'verify_payment', 'credit_card_pay_verification', 'do_pay', 'due_admin', 'twilio_call',

            'twilio_complete', 'twilio_screen', 'req_fr_promoter', 'req_transfer_amount', 'billing_twillio', 'cronjobRejectThreshold', 'try_driver_again', 'signup',

            'book_a_taxi', 'proxy', 'sendto_passenger', 'update_device_token', 'paypal_info', 'is_updated_info', 'info', 'cleanup', 'email_send_cronjob', 'test', 'balance_check', 'test_push',

            'login', 'search_call', 'recurrence_billing', 'rent_transaction', 'sufficient_balance', 'reminder_rent_fee', 'low_balance_email', 'driver_invoice',

            'view_pdf', 'ipn', 'success', 'cancel','get_mile','app_logout','get_last_booking_info_mobile','charge_for_twillo','update_place',

            'vendor_registration', 'distributor_registration', 'branchdisplay', 'near_by_drivers', 'validate_input','wallet_info','get_place',
            /*According to V2 requirements*/
            'generic_app_call', 'place_call', 'incoming', 'access_token', 'vendor_near_by_driver', 'get_last_booking_info', 'book_a_taxi','nearBySearch','get_access_despatch_job','test_p'

        );
        if (!defined('CURRENCY')) {

            //$TEST = array('currency_code' => 'GBP', 'free_credit' => 'Free Credit');

            define('CURRENCY', 'GBP');

        }


    }


    public function isAuthorized($type){
        $this->loadModel('Acl');
        $this->loadModel('Aclsmanage');
        $controller = strtolower($this->params['controller']);
        $action = explode('_', strtolower($this->params['action']));
        if($controller == 'users' && $action['1'] == 'login') return true;
        if($controller == 'users' && $action['1'] == 'paypal') return true;
        if($controller =='settings'){
            $url = $controller.'/'.$action['1'].'/1';
        } elseif($controller =='users'&&($action['1'] =='index'||$action['1'] =='add')) {
            $url = $controller.'/'.$action['1'].'/'.$this->params['pass']['0'];
        } else{
            $url = $controller.'/'.$action['1'];
        }
        //pr($url);
        $this->Acl->recursive = -1;
        $acl = $this->Acl->findByPermissionKey($url,['Acl.id']);
        if(empty($acl)) return false;
        $this->Aclsmanage->recursive = -1;
        $aclsmanage = $this->Aclsmanage->findByAclIdAndTypeAndAccess($acl['Acl']['id'],$type,'1',['id']);
        if(empty($aclsmanage)){
            return false;
        } else {
            return true;
        }

        //pr($aclsmanage);die;


    }
    public function getChargeId($id){

    }

    // api log
    function _apiLog($user_id, $payload, $response, $status, $time_pre)
    {
        $this->loadModel('Apilog');
        $time_post = microtime(true);
        $data = array(
            'Apilog' => array(
                'request_url' => $this->params->url,
                'user_id' => $user_id,
                'payload' => $payload,
                'response' => $response,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'status' => $status,
                'time' => round(($time_post - $time_pre), 4)
            )
        );
        // $this->Apilog->create();
        // $this->Apilog->save($data,false);

        $delete_apilogs = $this->Apilog->query('
            DELETE FROM apilogs WHERE DATE(created) < DATE_SUB(CURDATE(),INTERVAL 4 DAY)');
    }

    //entry admin percentage for a booking 
    function _fee_admin_entry($user_info = null, $fare = null, $booking_id = null, $service = null)
    {

        $this->loadModel('Setting');
        if ($service != 'cash_in_hand') {
            $admin_percentage = $user_info['User']['admin_percentage'];
            if (empty($admin_percentage)) {
                $admin_percentage_settings = $this->Setting->findById(1, array('admin_percentage'));
                $admin_percentage = $admin_percentage_settings['Setting']['admin_percentage'];
            }
        } else {
            $admin_percentage = $user_info['User']['admin_percentage_cash'];
            if (empty($admin_percentage)) {
                $admin_percentage_settings = $this->Setting->findById(1, array('admin_percentage_cash'));
                $admin_percentage = $admin_percentage_settings['Setting']['admin_percentage_cash'];
            }
        }
        $admin_amount = $this->User->Transaction->getAdminEarning($admin_percentage, $fare);
        $data['Transaction']['user_id'] = $user_info['User']['id'];
        $data['Transaction']['amount'] = '-' . $admin_amount;
        $data['Transaction']['short_description'] = 'admin_fee';
        $data['Transaction']['referred_user_id'] = null;
        $now = date('Y-m-d');
        $data['Transaction']['timestamp'] = $now;
        $data['Transaction']['booking_id'] = $booking_id;
        $data['Transaction']['currency_code'] = CURRENCY;
        $data['Transaction']['service'] = 'admin_payment';
        $this->Transaction->create();
        $this->Transaction->save($data, false);
        /*if($this->Transaction->save($data, false)){
            $a_data['Transaction']['user_id'] = $user_info['User']['id'];
            $a_data['Transaction']['amount'] = '-'.$admin_amount;
            $a_data['Transaction']['short_description'] = 'admin_fee';
            $a_data['Transaction']['referred_user_id'] = null;
            $now = date('Y-m-d');
            $a_data['Transaction']['timestamp'] = $now;
            $a_data['Transaction']['booking_id'] = $booking_id;
            $a_data['Transaction']['currency_code'] = CURRENCY;
            $a_data['Transaction']['service'] = 'admin_payment';
            $this->Transaction->create();
            $this->Transaction->save($a_data, false);
        }*/
    }

    // promoter percentage calculation and give the promoter calculated credit 
    function _pay_promoter($promoter_info = null, $user_id = null, $amount = null)
    {

        $promoter_percentage = $promoter_info['User']['promoter_percentage'];
        if (empty($promoter_percentage)) {
            $this->loadModel('Setting');
            $promoter_percentage = $this->Setting->findById(1, array('promoter_percentage'));
            $promoter_amount = ($amount * $promoter_percentage['Setting']['promoter_percentage']) / 100;
        } else {
            $promoter_amount = ($amount * $promoter_percentage) / 100;
        }
        $driver['Transaction']['user_id'] = $user_id;
        $driver['Transaction']['amount'] = '-' . $promoter_amount;
        $driver['Transaction']['payee'] = $promoter_info['User']['id'];
        $driver['Transaction']['short_description'] = 'promoter_fee';
        $now = date('Y-m-d');
        $driver['Transaction']['timestamp'] = $now;
        $driver['Transaction']['service'] = 'promoter_fee';
        $driver['Transaction']['currency_code'] = CURRENCY;
        $this->Transaction->create();
        if ($this->Transaction->save($driver, false)) {
            $data['Transaction']['user_id'] = $promoter_info['User']['id'];
            $data['Transaction']['amount'] = $promoter_amount;
            $data['Transaction']['short_description'] = 'promoter_profit';
            $data['Transaction']['referred_user_id'] = $user_id;
            $now = date('Y-m-d');
            $data['Transaction']['timestamp'] = $now;
            $data['Transaction']['service'] = 'promoter_profit';
            $data['Transaction']['currency_code'] = CURRENCY;
            $this->Transaction->create();
            $this->Transaction->save($data, false);
        }
    }

    function _queue($driver, $booking_id, $zone_id)
    {
        $data = array();
        $this->recursive = -1;
        $data['Queue']['user_id'] = $driver['Driver']['user_id'];
        $data['Queue']['booking_id'] = $booking_id;
        $now = date("Y-m-d H:i:s");
        $data['Queue']['request_time'] = $now;
        $data['Queue']['zone_id'] = $zone_id;
        //$Queue = new Queue();
        $this->loadModel('Booking');
        $booking_details = $this->Booking->findById($booking_id, array('note_to_driver','is_sap_booking','at_time', 'pick_up','pick_up_lat','pick_up_lng', 'persons'));
        $is_sap_booking = $booking_details['Booking']['is_sap_booking'];

        // FIXME: check if the driver alreay got that job request
        $check_driver = $this->Queue->findByBookingIdAndUserId($booking_id, $driver['Driver']['user_id'], array('Queue.id'));
        if (!empty($check_driver)) return false;

        // FIXME: check if the booking has already been accepted, cleared or auto_cancelled or the request has been sent to other driver
        $check_response = $this->Queue->find('first', array(
            'conditions' => array(
                'Queue.response' => array('accepted', 'cleared', 'auto_cancelled'),
                'Queue.response IS NULL',
                'Queue.booking_id' => $booking_id
            ),
            'fields' => array('Queue.id'),
        ));

        if (!empty($check_response)) return false;
        $pick_up = (strlen($booking_details['Booking']['pick_up']) > 50) ? substr($booking_details['Booking']['pick_up'], 0, 50) . '...' : $booking_details['Booking']['pick_up'];
        $passenger = $booking_details['Booking']['persons'];
        if ($is_sap_booking == 1) {
            $this->Booking->id = $booking_id;
            $this->Booking->saveField('status', 'request_sent');
        }
        $this->Queue->create();

        $this->Queue->save($data);
        $distance = $this->_distance_calculation($driver['Driver']['lat'],$driver['Driver']['lng'],$booking_details['Booking']['pick_up_lat'],$booking_details['Booking']['pick_up_lng']);
        $timeData = date_create($booking_details['Booking']['at_time']);
        $time = date_format($timeData,'d/m/Y g:i A');
        $pushData = [
            'booking_id' =>$booking_id,
            'pick_up' => $booking_details['Booking']['pick_up'],
            'request_time' => $time,
            'person' => $passenger,
            'distance' => $distance.' Mi',
            'special' => $booking_details['Booking']['note_to_driver'],
            'status' => 'job'

        ];
        //pr($pushData);
        $this->_pushAll($driver['Driver']['user_id'],$pushData);

        // send notification to driver
        $this->_push($driver['Driver']['user_id'], 'You\'ve got a new booking.', 'CabbieCall', $booking_id, null, null, null, $pick_up, $passenger);

        //$driver_zone = $driver['Driver']['vr_zone'];

        //$this->_position_driver($driver_zone, $driver['Driver']['user_id']);


    }

    function _search_driver($booking_id, $lat, $lng, $requester, $passenger, $car, $is_sap_booking = -1, $branch_number = null, $method_1 = null, $method_2 = null, $method_3 = null, $driver_number = null)
    {
        $this->loadModel('Booking');
        $this->loadModel('Queue');
        $booking = $this->Booking->findById($booking_id, array('Booking.zone_id', 'Booking.status', 'Booking.from_web'));
        $zone_id = $booking['Booking']['zone_id'];
        $driver = [];
        if (!empty($method_1) && empty($driver)) {
            $driver = $this->_find_fixed_driver($driver_number,$branch_number);
        }
        if (!empty($method_2) && empty($driver)) {
            //$driver = $this->_find_vr_nearest_driver($zone_id,$branch_number);
            $driver = $this->_find_vr_nearest_driver($lat, $lng, $zone_id, $booking_id, $requester, $passenger, $car, $branch_number);
        }
        if (!empty($method_3) && empty($driver)) {
            $driver = $this->_find_nearest_driver($lat, $lng, $booking_id, $requester, $passenger, $car, $branch_number);
        }

//print_r($driver);
//        exit;


        $srch_dvr = null;

        if (!empty($driver)) {
            $this->_queue($driver, $booking_id, $zone_id);
            if ($is_sap_booking == 1) {

                $srch_dvr = [
                    'search_driver' => true,
                    'message' => 'Driver found'
                ];
                return $srch_dvr;
            }
        } else {
            //inform passenger that no driver found.

//                $nearest_phone_no = $this->Queue->query("SELECT
//                        g.id, g.phone, g.address,
//                            3963.0 * ACOS(
//                                SIN(b.pick_up_lat * PI() / 180)* SIN(g.lat * PI() / 180)+ COS(b.pick_up_lat * PI() / 180)* COS(g.lat * PI() / 180)* COS(
//                                    (b.pick_up_lng * PI() / 180)-(g.lng * PI() / 180)
//                                )
//                            )AS distance
//                        FROM
//                            geophonebooks AS g
//                        LEFT JOIN bookings b ON b.id = '$booking_id'
//                        ORDER BY
//                            distance ASC
//                        LIMIT 1");
//                $phone = $nearest_phone_no[0]['g']['phone'];
            $requester = $this->Queue->Booking->find('first', array(
                    'recursive' => -1,
                    'joins' => array(
                        array(
                            'table' => 'users',
                            'alias' => 'User',
                            'type' => 'LEFT',
                            'conditions' => 'User.id = Booking.requester'
                        ),
                    ),
                    'conditions' => array(
                        'Booking.id' => $booking_id
                    ),
                    'fields' => array(
                        'User.id', 'User.type', 'User.email', 'User.mobile', 'User.name'
                    )
                )
            );
            if (trim($requester['User']['type']) == 'passenger') {
                if ($booking['Booking']['status'] != 'no_driver_found') {

                    if ($booking['Booking']['from_web'] == 1) {
                        try {
                            $this->_sendSms($requester['User']['mobile'], 'CabbieAppUK! - No Drivers are available, please wait');
                            $this->_sendEmail($requester['User']['email'], "CabbieAppUK! - Booking Confirmation " . $booking_id, "Dear " . $requester['User']['name'] . ",<br/><br/>There are no drivers available right now, if you would like to call to change or cancel you booking please call .<br/><br/>Regards,<br/><br/>The CabbieApp Team");
                        } catch (Exception $e) {

                        }

                    }

                    $driver_not_found_bookings = $this->Session->read('driver_not_found_bookings');

//                        $driver_not_found_bookings[$requester['User']['id']] = array(
//                           'requester' => $requester['User']['id'],
//                            'msg' => 'No Drivers are available, please wait or call '. $phone .". For Detail Go 'Open Bookings'",
//                            'app' => 'CabbieApp',
//                            'booking_id' => $booking_id,
//                            'phone' => $phone
//                        );
//                        $this->Session->write('driver_not_found_bookings', $driver_not_found_bookings);

                }

            }
//            , hunting_no = '$phone'
            $this->Queue->Booking->query("
                    UPDATE bookings SET status = 'no_driver_found'
                    WHERE id = '$booking_id'
                ");

            if ($is_sap_booking == 1) {

//                $srch_dvr['phone'] = $phone;
                $srch_dvr = [
                    'search_driver' => false,
                    'message' => 'No driver found. Wait for while'
                ];

                // print_r($srch_dvr);
                return $srch_dvr;
            }
        }

    }

    /**
     * @param null $driver_number
     * @return array|null
     *
     */
    function _find_fixed_driver($driver_number = null,$branch_number = null)
    {
        $this->loadModel('User');
        $this->loadModel('Queue');
        $this->loadModel('Setting');
        $this->loadModel('Wallet');
//        find the driver id and vr zone by driver mobile number
        $driver_id = preg_replace('[^1000]', '', $driver_number);
        $this->User->recursive = -1;
        $driver = $this->User->findByIdAndTypeAndBranchNumber($driver_id, 'driver',$branch_number);
        $driver_min_balance = $this->Setting->findById(1, array('driver_min_balance'));
        $min_balance = $driver_min_balance['Setting']['driver_min_balance'];
        //pr($min_balance);
        if (!empty($driver)) {
            $options = [
                'conditions' => [
                    'Queue.user_id' => $driver['User']['id'],
                    'Queue.booking_id IS NOT NULL',
                    'OR' => [
                        'Queue.response' => 'accepted',
                        'Queue.response IS NULL',


                    ]
                ]
            ];
//            find the driver available
            $is_available = $this->Queue->find('first', $options);
            if (empty($is_available)) {
//                if he/she isn't busy
                $amount = $this->_get_wallet_info($driver['User']['id']);
                //pr($amount);die;
                if($amount['Wallet']['available_balance']>=$min_balance){
                    $data['Driver'] = [
                        'user_id' => $driver['User']['id'],
                        'vr_zone' => $driver['User']['vr_zone'],
                        'lat' => $driver['User']['lat'],
                        'lng' => $driver['User']['lng']
                    ];
                    //print_r($driver_number);exit;

                    return $data;
                }else{
                    return null;
                }
            } else {
//                if he/she is busy
                return null;
            }
        }

    }

    function _find_vr_nearest_driver($lat, $lng, $zone_id = null, $booking_id, $requester, $passenger, $car, $branch_number)
    {
        $driver = null;
        //print_r($branch_number);
        $car_passenger_cond = '';
        $car_passenger_cond_alt = '';
        //echo('exit here');
//        switch ($car) {
//            case '5 Seats':
//                $car_passenger_cond = " AND u.no_of_seat >= '5' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '5' ";
//                break;
//            case '6 Seats':
//                $car_passenger_cond = " AND u.no_of_seat >= '6' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '6' ";
//                break;
//            case '7 Seats':
//                $car_passenger_cond = " AND u.no_of_seat >= '7' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '7' ";
//                break;
//            case '8 Seats':
//                $car_passenger_cond = " AND u.no_of_seat >= '8' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '8' ";
//                break;
//            case 'Estate':
//                $car_passenger_cond = " AND u.no_of_seat >= '$passenger' AND u.vehicle_type = 'Estate' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '$passenger' AND User.vehicle_type = 'Estate' ";
//                break;
//            case 'Wheelchair':
//                $car_passenger_cond = " AND u.no_of_seat >= '$passenger' AND u.is_wheelchair = '1' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '$passenger' AND User.is_wheelchair = '1' ";
//                break;
//            case 'ANY':
//                $car_passenger_cond = " AND u.no_of_seat >= '$passenger' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '$passenger' ";
//                break;
//            case 'Saloon car':
//                $car_passenger_cond = " AND u.no_of_seat >= '$passenger' AND u.vehicle_type = 'Saloon' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '$passenger' AND User.vehicle_type = 'Saloon' ";
//                break;
//            default:
//                $car_passenger_cond = " ";
//                $car_passenger_cond_alt = " ";
//                break;
//        }
        $this->loadModel('Setting');
        $this->loadModel('Queue');
        $driver_min_balance = $this->Setting->findById(1, array('driver_min_balance'));
        $min_balance = $driver_min_balance['Setting']['driver_min_balance'];
        //pr($zone_id);
        if (!empty($zone_id)) {

            $driver = $this->Queue->query("SELECT
                Driver.user_id as user_id,
                Driver.request_time as request_time,
                Driver.response as response,
                Driver.lat as lat,
                Driver.lng as lng,
                Driver.vr_zone as vr_zone
                    FROM
                        (
                            SELECT
                                q.user_id,
                                q.request_time,
                                q.response,
                                u.lat,
                                u.lng,
                                u.vr_zone
                            FROM
                                queues q
                            INNER JOIN users u ON(
                                u.id = q.user_id
                                AND u.vr_available = 'yes'
                                AND u.vr_zone = '$zone_id'
                                AND u.is_enabled = '1'
                                AND u.type = 'driver'
                                AND u.vehicle_type >= '$car'
                                And u.branch_number = '$branch_number'
                                AND u.id <> '$requester'
                                AND(
                                    (
                                        u.running_distance IS NOT NULL
                                        AND 3963.0 * ACOS(
                                                SIN('$lat' * PI() / 180)* SIN(u.lat * PI() / 180)+ COS('$lat' * PI() / 180)* COS(u.lat * PI() / 180)* COS(
                                                    ('$lng' * PI() / 180)-(u.lng * PI() / 180)
                                                )
                                            ) <= u.running_distance
                                    )
                                    OR(
                                        u.running_distance IS NULL
                                    )
                                )
                            )
                            WHERE q.user_id NOT IN(SELECT user_id AS user_id FROM queues WHERE booking_id = '$booking_id'
                                UNION ALL
                                    SELECT user_id AS user_id FROM queues WHERE response = 'accepted' OR (response IS NULL AND booking_id IS NOT NULL)
                                UNION ALL
                                    SELECT userId1 AS user_id FROM bars WHERE userId1 = q.user_id AND userId2 = '$requester'
                                UNION ALL
                                    SELECT userId2 AS user_id FROM bars WHERE userId1 = '$requester' AND userId2 =q.user_id)
                                AND (SELECT available_balance FROM wallets WHERE user_id = q.user_id) >= '$min_balance'

                            ORDER BY
                                q.request_time DESC
                        )Driver
                    GROUP BY
                        Driver.user_id
                    ORDER BY
                        Driver.request_time ASC
                    LIMIT 1
                ");

//            cut from above
//            AND (SELECT SUM(amount) FROM transactions WHERE user_id = q.user_id
//            AND service <> 'cash_in_hand' AND is_paid = 1) >= '$min_balance'
        }
        // search for nearest driver when there is no one avaliable in the zone
//        if(empty($driver)){
//            $driver = $this->Queue->query("SELECT
//            Driver.user_id as user_id,
//            Driver.request_time as request_time,
//            Driver.response as response,
//            Driver.lat as lat,
//            Driver.lng as lng,
//            Driver.vr_zone as vr_zone
//                FROM
//                    (
//                    SELECT
//                        Queue.user_id,
//                        Queue.request_time,
//                        Queue.response,
//                        User.lat,
//                        User.lng,
//                        User.vr_zone
//                        FROM
//                            users AS User
//                        INNER JOIN queues AS Queue ON(
//                            Queue.user_id = User.id
//                        )
//                        WHERE
//                            (
//                                (
//                                    (
//
//                                        User.running_distance IS NOT NULL
//                                        AND(
//
//                                                3963.0 * ACOS( SIN('$lat'  * PI() / 180)* SIN(User.lat * PI() / 180)+ COS('$lat' * PI() / 180)* COS(User.lat * PI() / 180)* COS(
//                                                ('$lng' * PI()/ 180)-(User.lng * PI() / 180))) <= User.running_distance
//                                        )
//                                    )
//                                )
//                                OR(
//                                    User.running_distance IS NULL
//                                )
//                            )
//                        AND User.is_enabled = '1'
//                        AND User.type = 'driver'
//                        AND User.no_of_seat >= '$car'
//                        And u.branch_number = '$branch_number'
//                        AND User.id <> '$requester'
//                        AND User.vr_available = 'yes'
//                        AND Queue.user_id NOT IN(SELECT user_id AS user_id FROM queues WHERE booking_id='$booking_id'
//                                UNION ALL
//                                    SELECT user_id AS user_id FROM queues WHERE response = 'accepted' OR (response IS NULL AND booking_id IS NOT NULL)
//                                UNION ALL
//                                    SELECT userId1 AS user_id FROM bars WHERE userId1 = Queue.user_id AND userId2 = '$requester'
//                                UNION ALL
//                                    SELECT userId2 AS user_id FROM bars WHERE userId1 = '$requester' AND userId2 = Queue.user_id)
//
//
//                        GROUP BY Queue.user_id
//                        LIMIT 1
//                    )Driver
//                "
//            );
//            cut from above
//            AND (SELECT SUM(amount) FROM transactions WHERE user_id = Queue.user_id
//            AND service <> 'cash_in_hand' AND is_paid = 1) >= '$min_balance'
        //}
        //print_r($driver);
        if (!empty($driver)) {
            return $driver[0];
        } else {
            return null;
        }
    }

    /**
     * @param $lat
     * @param $lng
     * @param null $zone_id
     * @param $booking_id
     * @param $requester
     * @param $passenger
     * @param $car
     * @param $branch_number
     * @return null
     */
    function _find_nearest_driver($lat, $lng, $booking_id, $requester, $passenger, $car, $branch_number)
    {
        $driver = null;
        //print_r($branch_number);
        $car_passenger_cond = '';
        $car_passenger_cond_alt = '';
        //echo('exit here');
//        switch ($car) {
//            case '5 Seats':
//                $car_passenger_cond = " AND u.no_of_seat >= '5' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '5' ";
//                break;
//            case '6 Seats':
//                $car_passenger_cond = " AND u.no_of_seat >= '6' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '6' ";
//                break;
//            case '7 Seats':
//                $car_passenger_cond = " AND u.no_of_seat >= '7' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '7' ";
//                break;
//            case '8 Seats':
//                $car_passenger_cond = " AND u.no_of_seat >= '8' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '8' ";
//                break;
//            case 'Estate':
//                $car_passenger_cond = " AND u.no_of_seat >= '$passenger' AND u.vehicle_type = 'Estate' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '$passenger' AND User.vehicle_type = 'Estate' ";
//                break;
//            case 'Wheelchair':
//                $car_passenger_cond = " AND u.no_of_seat >= '$passenger' AND u.is_wheelchair = '1' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '$passenger' AND User.is_wheelchair = '1' ";
//                break;
//            case 'ANY':
//                $car_passenger_cond = " AND u.no_of_seat >= '$passenger' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '$passenger' ";
//                break;
//            case 'Saloon car':
//                $car_passenger_cond = " AND u.no_of_seat >= '$passenger' AND u.vehicle_type = 'Saloon' ";
//                $car_passenger_cond_alt = " AND User.no_of_seat >= '$passenger' AND User.vehicle_type = 'Saloon' ";
//                break;
//            default:
//                $car_passenger_cond = " ";
//                $car_passenger_cond_alt = " ";
//                break;
//        }
        $this->loadModel('Setting');
        $this->loadModel('Queue');
        $driver_min_balance = $this->Setting->findById(1, array('driver_min_balance'));
        $min_balance = $driver_min_balance['Setting']['driver_min_balance'];
//        if (!empty($zone_id)) {
//            $driver = $this->Queue->query("SELECT
//                Driver.user_id as user_id,
//                Driver.request_time as request_time,
//                Driver.response as response,
//                Driver.lat as lat,
//                Driver.lng as lng,
//                Driver.vr_zone as vr_zone
//                    FROM
//                        (
//                            SELECT
//                                q.user_id,
//                                q.request_time,
//                                q.response,
//                                u.lat,
//                                u.lng,
//                                u.vr_zone
//                            FROM
//                                queues q
//                            INNER JOIN users u ON(
//                                u.id = q.user_id
//                                AND u.vr_available = 'yes'
//                                AND u.is_enabled = '1'
//                                AND u.type = 'driver'
//                                AND u.no_of_seat >= '$car'
//                                And u.branch_number = '$branch_number'
//                                AND u.id <> '$requester'
//                                AND(
//                                    (
//                                        u.running_distance IS NOT NULL
//                                        AND 3963.0 * ACOS(
//                                                SIN('$lat' * PI() / 180)* SIN(u.lat * PI() / 180)+ COS('$lat' * PI() / 180)* COS(u.lat * PI() / 180)* COS(
//                                                    ('$lng' * PI() / 180)-(u.lng * PI() / 180)
//                                                )
//                                            ) <= u.running_distance
//                                    )
//                                    OR(
//                                        u.running_distance IS NULL
//                                    )
//                                )
//                            )
//                            WHERE q.user_id NOT IN(SELECT user_id AS user_id FROM queues WHERE booking_id = '$booking_id'
//                                UNION ALL
//                                    SELECT user_id AS user_id FROM queues WHERE response = 'accepted' OR (response IS NULL AND booking_id IS NOT NULL)
//                                UNION ALL
//                                    SELECT userId1 AS user_id FROM bars WHERE userId1 = q.user_id AND userId2 = '$requester'
//                                UNION ALL
//                                    SELECT userId2 AS user_id FROM bars WHERE userId1 = '$requester' AND userId2 =q.user_id)
//                            AND (SELECT SUM(amount) FROM transactions WHERE user_id = q.user_id
//                                        AND service <> 'cash_in_hand' AND is_paid = 1) >= '$min_balance'
//
//                            ORDER BY
//                                q.request_time DESC
//                        )Driver
//                    GROUP BY
//                        Driver.user_id
//                    ORDER BY
//                        Driver.request_time ASC
//                    LIMIT 1
//                ");
//        }


        // search for nearest driver when there is no one avaliable in the zone
        if (empty($driver)) {
            $driver = $this->Queue->query("SELECT  
            Driver.user_id as user_id,
            Driver.request_time as request_time,
            Driver.response as response,
            Driver.lat as lat,
            Driver.lng as lng,
            Driver.vr_zone as vr_zone
                FROM
                    (
                    SELECT
                        Queue.user_id,
                        Queue.request_time,
                        Queue.response,
                        User.lat,
                        User.lng,
                        User.vr_zone
                        FROM
                            users AS User
                        INNER JOIN queues AS Queue ON(
                            Queue.user_id = User.id                 
                        )
                        WHERE
                            (
                                (
                                    (
                                        
                                        User.running_distance IS NOT NULL
                                        AND(
                                            
                                                3963.0 * ACOS( SIN('$lat'  * PI() / 180)* SIN(User.lat * PI() / 180)+ COS('$lat' * PI() / 180)* COS(User.lat * PI() / 180)* COS(
                                                ('$lng' * PI()/ 180)-(User.lng * PI() / 180))) <= User.running_distance
                                        )
                                    )
                                )
                                OR(
                                    User.running_distance IS NULL
                                )
                            )
                        AND User.is_enabled = '1'
                        AND User.type = 'driver'
                        AND User.branch_number = '$branch_number'
                        AND User.vehicle_type = '$car'
                        AND User.id <> '$requester'
                        AND Queue.user_id NOT IN(SELECT user_id AS user_id FROM queues WHERE booking_id='$booking_id'
                                UNION ALL
                                    SELECT user_id AS user_id FROM queues WHERE response = 'accepted' OR (response IS NULL AND booking_id IS NOT NULL)
                                UNION ALL
                                    SELECT userId1 AS user_id FROM bars WHERE userId1 = Queue.user_id AND userId2 = '$requester'
                                UNION ALL
                                    SELECT userId2 AS user_id FROM bars WHERE userId1 = '$requester' AND userId2 = Queue.user_id) 
                        AND (SELECT available_balance FROM wallets WHERE user_id = Queue.user_id) >= '$min_balance'

                        GROUP BY Queue.user_id
                        LIMIT 1
                    )Driver 
                "
            );
            //            cut from above
//            AND (SELECT SUM(amount) FROM transactions WHERE user_id = Queue.user_id
//            AND service <> 'cash_in_hand' AND is_paid = 1) >= '$min_balance'

        }
        //print_r($driver);
        if (!empty($driver)) {
            return $driver[0];
        } else {
            return null;
        }

    }

    /**
     * @param $receiver_id
     * @param $message
     * @param $app
     * @param null $booking_id
     * @param null $phone
     * @param null $c
     * @param null $v
     * @param null $pick_up
     * @param null $passenger
     * @param null $position
     * @param null $zone
     * @param null $driver_pick
     * @param null $accept
     * @param null $emergency
     * @param null $driver_id
     * @param null $pn
     * @param null $gn
     * @param null $g_position
     * @param null $g_zone
     * @param null $call_position
     * @param null $call_zone
     * @return bool
     *
     * UA Push service using
     */
    function _push($receiver_id, $message, $app,
                   $booking_id = null,
                   $phone = null,
                   $c = null,
                   $v = null,
                   $pick_up = null,
                   $passenger = null,
                   $position = null,
                   $zone = null,
                   $driver_pick = null,
                   $accept = null,
                   $emergency = null,
                   $driver_id = null,
                   $pn = null,
                   $gn = null,
                   $g_position = null,
                   $g_zone = null,
                   $call_position = null,
                   $call_zone = null)
    {
        if ($_SERVER['HTTP_HOST'] == 'localhost') return true;
        App::import('Vendor', 'UA', array('file' => 'UA' . DS . 'push.php'));
        $this->loadModel('DeviceToken');
        $devices = $this->DeviceToken->find('all', array(
                'recursive' => -1,
                'conditions' => array(
                    'DeviceToken.user_id' => $receiver_id
                )
            )
        );
        $sendPush = new SendPush();
        try {
            $sendPush->push($receiver_id, $message, $app, $devices,
                $booking_id, $phone, $c, $v, $pick_up, $passenger,
                $position, $zone, $driver_pick, $accept, $emergency,
                $driver_id, $pn, $gn, $g_position, $g_zone,
                $call_position, $call_zone);
        } catch (Exception $e) {

        }
    }

    //sent verification code to user 
    function _sendSms($phoneNo, $text)
    {
        //  require "Twilio.php";
        App::import('Vendor', 'Twilio', array('file' => 'sms/Twilio.php'));
        //require 
        // set your AccountSid and AuthToken from www.twilio.com/user/account
        if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', "::1"))) {
            #Test credential
            $AccountSid = "AC380847079144a880d552a768a2968904";
            $AuthToken = "1e2d606db297d5d64739e3b99b04aafd";
        } else {
            #Live credential
            $AccountSid = "ACd5880a6ed2193077c585763c514be2a2";
            $AuthToken = "c67580685ab040d98a11612441b87848";
        }

        $client = new Services_Twilio($AccountSid, $AuthToken);

        try {
            $sms = $client->account->sms_messages->create(
                "+44 20 3095 7374", // From this number
                $phoneNo, // "+44 1576 820016", // To this number
                $text//"Test message!"
            );
        } catch (Exception $e) {

        }

        //return $sms;
    }

    function _sendEmail($to, $subject, $content, $template = 'default', $viewVars = array(), $layout = 'default', $attachment = null)
    {
        App::uses('CakeEmail', 'Network/Email');
        if (empty($viewVars)) {
            $viewVars['title'] = $subject;
        }
        $viewVars['body'] = $content;
        $email = new CakeEmail('smtp');
        $email->template($template, $layout);
        $email->viewVars($viewVars);
        $email->emailFormat('both');
        $email->subject($subject);
        $email->to($to);
        if (!empty($attachment)) {
            $email->attachments($attachment);
        }
        try {
            $email->send();
        } catch (Exception $e) {

        }

    }

    function _billing_twillio($id, $amount)
    {
        $this->loadModel('Transaction');
        $data['Transaction']['user_id'] = $id;
        $data['Transaction']['amount'] = '-' . $amount;
        $data['Transaction']['short_description'] = 'billing_twillio';
        $data['Transaction']['service'] = 'billing_twillio';
        $now = date('Y-m-d');
        $data['Transaction']['timestamp'] = $now;
        $data['Transaction']['currency_code'] = CURRENCY;
        $this->Transaction->create();
        $this->Transaction->save($data, false);
    }

    /**
     * @param null $driver_zone
     * @param null $omit
     */

    function _position_driver($driver_zone = null, $omit = null)
    {
        $this->loadModel('Booking');
        $this->loadModel('User');
        if (!empty($driver_zone)) {
            $position = 0;
            $drivers = $this->Booking->query("SELECT
                    t.*
                FROM
                    (
                        SELECT
                            q.*
                        FROM
                            queues q
                        INNER JOIN users u ON(q.user_id = u.id)
                        WHERE
                            u.vr_available = 'yes'
                        AND u.is_enabled = 1
                        AND u.vr_zone = '$driver_zone'
                        ORDER BY
                            request_time DESC
                    )AS t
                GROUP BY
                    t.user_id
                ORDER BY
                    t.request_time");
            $this->loadModel('Zone');
            $zone = $this->Zone->findById($driver_zone, array('name'));
            $zone_name = $zone['Zone']['name'];
            foreach ($drivers as $dvr) {
                $driver_id = $dvr['t']['user_id'];
                $position = $position + 1;
                $this->User->query("UPDATE users SET virtual_rank = '$position' WHERE id = '$driver_id'");
                if ($driver_id != $omit && $this->User->getAffectedRows()) {
                    // send notification to driver except for who got the job offer currently
                    $this->_push($driver_id, "Your current position is " . $position . " in VR Zone.", 'CabbieCall', null, null, null, null, null, null, $position, $zone_name);
                }

            }
        }
    }

    function _position_driver_phone($driver_zone = null, $omit = null)
    {
        $this->loadModel('PhoneQueue');
        $this->loadModel('User');
        if (!empty($driver_zone)) {
            $position = 0;
            $drivers = $this->PhoneQueue->query("SELECT
                    t.*
                FROM
                    (
                        SELECT
                            q.*
                        FROM
                            phone_queues q
                        INNER JOIN users u ON(q.user_id = u.id)
                        WHERE
                            u.phonecall_available = 'yes'
                        AND u.is_enabled = '1'
                        AND u.phonecall_zone = '$driver_zone'
                        ORDER BY
                            request_time DESC
                    )AS t
                GROUP BY
                    t.user_id
                ORDER BY
                    t.request_time");
            $this->loadModel('Zone');
            $zone = $this->Zone->findById($driver_zone, array('name'));
            $zone_name = $zone['Zone']['name'];
            foreach ($drivers as $dvr) {
                $driver_id = $dvr['t']['user_id'];
                $position = $position + 1;
                $this->User->query("UPDATE users SET phone_rank = '$position' WHERE id = '$driver_id'");
                if ($driver_id != $omit && $this->User->getAffectedRows()) {
                    // send notification to driver except for who got the job offer currently
                    $this->_push($driver_id, "Your current position is " . $position . " in Phone Call Zone.", 'CabbieCall',
                        null, null, null, null, null, null, null, null,
                        null, null, null, null, null, null, null, null, $position, $zone_name);
                }

            }
        }
    }

    function _position_driver_gps($driver_zone = null, $omit = null)
    {
        $this->loadModel('PhoneQueue');
        $this->loadModel('User');
        if (!empty($driver_zone)) {
            $position = 0;
            $drivers = $this->PhoneQueue->query("SELECT
                    t.*
                FROM
                    (
                        SELECT
                            q.*
                        FROM
                            phone_queues q
                        INNER JOIN users u ON(q.user_id = u.id)
                        WHERE
                            u.phonecall_gps_available = 'yes'
                        AND u.is_enabled = 1
                        AND u.phonecall_gps_zone = '$driver_zone'
                        ORDER BY
                            request_time DESC
                    )AS t
                GROUP BY
                    t.user_id
                ORDER BY
                    t.request_time");
            $this->loadModel('Zone');
            $zone = $this->Zone->findById($driver_zone, array('name'));
            $zone_name = $zone['Zone']['name'];
            foreach ($drivers as $dvr) {
                $driver_id = $dvr['t']['user_id'];
                $position = $position + 1;
                $this->User->query("UPDATE users SET gps_rank = '$position' WHERE id = '$driver_id'");
                if ($driver_id != $omit && $this->User->getAffectedRows()) {
                    // send notification to driver except for who got the job offer currently
                    $this->_push($driver_id, "Your current position is " . $position . " in Phone GPS Zone.", 'CabbieCall',
                        null, null, null, null, null, null, null,
                        null, null, null, null, null, null, null, $position, $zone_name);
                }

            }
        }
    }

    public function rent_transaction($rent_item_id, $amount, $profit_percent)
    {
        /*$this->autoRender = false;
        $this->loadModel('Payer');
        $this->loadModel('Payee');
        $this->loadModel('Transaction');
        $all_payers = $this->Payer->findAllByRentItemId($rent_item_id, 'Payer.user_id');
        $all_payees = $this->Payee->findAllByRentItemId($rent_item_id, 'Payee.user_id');
        $count_payers = $this->Payer->find('count', array(
            'conditions' => array('Payer.rent_item_id' => $rent_item_id)
        ));
        $count_payees = $this->Payee->find('count', array(
            'conditions' => array('Payee.rent_item_id' => $rent_item_id)
        ));
        $total_amount = $amount * $count_payers;
        $percent_amount = ($total_amount * $profit_percent)/ 100;
        $per_person_amount = $percent_amount / $count_payees;
        // print_r($per_person_amount);
        // print_r(' '.$count_payers);
        // print_r(' '.$count_payees); exit;
        foreach ($all_payers as $key => $payer) {
            $this->Transaction->create();
            $this->Transaction->save(array(
                'Transaction' => array(
                    'user_id' => $all_payers[$key]['Payer']['user_id'],
                    'rent_item_id' => $rent_item_id,
                    'short_description' => 'rent_fee',
                    'timestamp' => date('Y-m-d'),
                    'service' => 'rent_fee',
                    'amount' => '-'. $amount,
                    'currency_code' => CURRENCY,
                    'is_paid' => 0
                    )
                )
            );
        }
        foreach ($all_payees as $key => $payee) {
            $this->Transaction->create();
            $this->Transaction->save(array(
                'Transaction' => array(
                    'user_id' => $all_payees[$key]['Payee']['user_id'],
                    'rent_item_id' => $rent_item_id,
                    'short_description' => 'rent_profit',
                    'service' => 'rent_profit',
                    'timestamp' => date('Y-m-d'),
                    'amount' => $per_person_amount,
                    'currency_code' => CURRENCY,
                    'is_paid' => 0
                    )
                )
            );
        }*/
    }

    /*==================================================================Using for v2===================================================================*/
    /**
     * @param null $user
     * @param array $data
     */
    public function _pushAll($user = null,$data = []){
//        $data = [
//            'booking_id' =>'68',
//            'pick_up' => 'khulna',
//            'request_time' => '2018-08-08 12:13:21',
//            'person' => '1',
//            'distance' => '1 Mi',
//            'special' => 'ahjhdbjsdhb'
//        ];
        $certificate_path_development = WWW_ROOT . 'files' . DS . 'push_certificate' . DS . 'pushcert_dev.pem';
        $certificate_path_production = WWW_ROOT . 'files' . DS . 'push_certificate' . DS . 'pushcert_dist.pem';
        $push_url_development = 'ssl://gateway.sandbox.push.apple.com:2195';
        $push_url_production = 'ssl://gateway.push.apple.com:2195';
        $passphrase = 'a';
        $this->loadModel('DeviceToken');
        $devicequery = [
            'recursive' => -1,
            'conditions' => [
                'DeviceToken.user_id' => $user,
                'DeviceToken.is_login' => '1'
            ],
            'fields' => [
                'DeviceToken.device_token',
                'DeviceToken.device_type',
                'DeviceToken.stage'
            ]
        ];

        $reciver_ids = $this->DeviceToken->find('all',$devicequery);
        //pr($reciver_ids);die;
        foreach ($reciver_ids as $reciver_id) {
            //pr($reciver_id['DeviceToken']);
            if($reciver_id['DeviceToken']['device_type'] =='android'){
                $push_data['push_notification'] = $data;
                $this->_push_firebase($reciver_id['DeviceToken']['device_token'], $push_data);
            } else {
                $body['aps'] = array(
                    'alert' => 'notification',
                    'sound' => 'default',
                    'notification_id' => empty($notification_id) ? '' : $notification_id
                );
                $push_data = am($body,$data);
                if($reciver_id['DeviceToken']['device_type'] == 'production'){
                    $certificate = $certificate_path_production;
                    $url =$push_url_production;
                } else {
                    $certificate = $certificate_path_development;
                    $url =$push_url_development;
                }
                $this->_apns($certificate, $passphrase, $url, $reciver_id['DeviceToken']['device_token'], $push_data);
            }

        }
    }
    /**
     * @param $receiver
     * receiver
     * @param $data
     */
    public function _push_firebase($receiver, $data)
    {
        // Enabling error reporting
        error_reporting(-1);
        ini_set('display_errors', 'On');

        App::import('Vendor', 'firebase', array('file' => 'firebase/firebase.php'));
        App::import('Vendor', 'firebase', array('file' => 'firebase/push.php'));

        $firebase = new Firebase();
        $response = $firebase->send($receiver, $data);
    }

    /**
     * @param $certificate_path
     * @param $passphrase
     * @param $push_url
     * @param $deviceToken
     * @param $body
     */
    private function _apns($certificate_path, $passphrase, $push_url, $deviceToken, $body){
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $certificate_path);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server

        $fp = stream_socket_client($push_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp) exit(0); //      exit("Failed to connect: $err $errstr" . PHP_EOL);
        //echo 'Connected to APNS' . PHP_EOL;

        // Encode the payload as JSON

        $payload = json_encode($body);

        // Build the binary notification

        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server

        $result = fwrite($fp, $msg, strlen($msg));
        // if (!$result) echo 'Message not delivered' . PHP_EOL;
        // else echo 'Message successfully delivered' . PHP_EOL;

        // Close the connection to the server
        fclose($fp);
    }

    public function _call_to_single_user($user = null, $msg = null)
    {
        App::import('Vendor', 'twilio_call_v2', array('file' => 'twilio_call_v2/vendor/autoload.php'));
        App::import('Vendor', 'twilio_call_v2', array('file' => 'twilio_call_v2/config.php'));

        /*
       * Use a valid Twilio number by adding to your account via https://www.twilio.com/console/phone-numbers/verified
       */
        $callerNumber = '+447481339107';
        $response = new Twilio\Twiml();

        $callerId = 'client:' . $this->request->data['from'];
        //$callerId = 'client:quick_start';
        //print_r($callerId);die;

        if (!empty($user)) {
            $to = $user;

            if (!isset($to) || empty($to)) {
                $response->say('Congratulations! You have just made your first call! Good bye.');
            } else if (is_numeric($to)) {
                $dial = $response->dial(
                    array(
                        'callerId' => $callerNumber
                    ));
                $dial->number($to);
            } else {
                $dial = $response->dial(
                    array(
                        'callerId' => $callerId
                    ));
                $dial->client($to);
            }
            print $response;
        } else {
            $response->say($msg, array('voice' => 'woman'));
            print $response;
        }
    }

    /**
     * @param null $mobile
     * @return mixed
     * common function to get user id by mobile number
     */
    public function _getPassengerIdByMobile($mobile = null)
    {
        $this->loadModel('User');
        $this->User->recursive = -1;
        $data = $this->User->findByMobileAndType($mobile, 'passenger');
        //print_r($data);
        if (!empty($data)) {
            $passenger_id = $data['User']['id'];
            return $passenger_id;
        } else die(json_encode(['success' => false, 'message' => 'Invalid User.']));
    }


    /**
     * @param null $pick_up
     * @param null $destination
     * @return mixed
     * get lat lng to pick up from destination
     */
    public function _get_lat_lng($pick_up = null, $destination = null)
    {
        App::uses('HttpSocket', 'Network/Http');
        $addrs = array(
            'origin' => $pick_up,
            'destination' => $destination,
            'sensor' => 'false',
            'units' => 'imperial',
            'region' => 'uk'
        );
        if (!empty($dest_via)) {
            $addrs['waypoints'] = 'optimize:true|via:' . implode('|via:', $dest_via);
        }
        //print_r($addrs);
        $HttpSocket = new HttpSocket();
        $address = $HttpSocket->get('https://maps.googleapis.com/maps/api/directions/json?key=AIzaSyDpt_cHVYOuA_WAmkdjLZ33g9bHrCmXEj8&v=3', $addrs);
        return json_decode($address->body, true);
    }

    /*
Description: Distance calculation from the latitude/longitude of 2 points
Author: Michaël Niessen (2014)
Website: http://AssemblySys.com

If you find this script useful, you can show your
appreciation by getting Michaël a cup of coffee ;)
PayPal: https://www.paypal.me/MichaelNiessen

As long as this notice (including author name and details) is included and
UNALTERED, this code can be freely used and distributed.
*/

    public function _distance_calculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'mi', $decimals = 2)
    {
        // Calculate the distance in degrees
        $degrees = rad2deg(acos((sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($point1_long - $point2_long)))));

        // Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
        switch ($unit) {
            case 'km':
                $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
                break;
            case 'mi':
                $distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
                break;
            case 'nmi':
                $distance = $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
        }
        return round($distance, $decimals);
    }

    /**
     * @param string $address
     * @return array
     */
    public function get_lat_lng_by_address($address = '')
    {
        App::uses('HttpSocket', 'Network/Http');
        $address = str_replace(" ", "+", $address);
        $json_data = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=AIzaSyBhlUzD8xgohlHKuw7QJ2y62MO2zjbDuP8");
        $json = json_decode($json_data);

        if (!empty($json)) {
            return [
                'lat' => $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'},
                'lng' => $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'},
            ];
        }
    }
//=========================Transaction Section Start===============================
    /**
     * @param null $user_id
     * @param null $amount
     * @return bool
     * add all credit amount
     */
    public function _add_wallet($user_id = null,$amount = null){
//        pr($user_id);
//        pr($amount);die;
        $this->loadModel('Wallet');
        if(!empty($user_id) && !empty($amount)){
            $user = $this->Wallet->findByUserId($user_id,['id','total_balance','total_withdraw','available_balance']);
            if(!empty($user)){
                $data['Wallet'] = [
                    'total_balance' => $user['Wallet']['total_balance'] + $amount,
                    'total_withdraw' => $user['Wallet']['total_withdraw'],
                    'available_balance' => $user['Wallet']['available_balance'] + $amount
                ];
                $this->Wallet->id = $user['Wallet']['id'];
                if($this->Wallet->save($data)){
                    return true;
                }
            } else {
                $new_data['Wallet'] = [
                    'user_id' => $user_id,
                    'total_balance' => $amount,
                    'total_withdraw' => '0',
                    'available_balance' => $amount
                ];
                $this->Wallet->create();
                if($this->Wallet->save($new_data)){
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * @param $user_id
     * @param $short_description
     * @param $booking_id
     * @param $amount
     * @param $currency_code
     * @param $service
     * @param $app_id
     * @param $pay_key
     * @param $payment_exec_status
     * @param $timestamp
     * @return bool
     * for entry every transaction
     */
    public function _add_transactions($user_id,$short_description,$booking_id,$amount,$currency_code,$service,$app_id,$pay_key,$payment_exec_status,$timestamp){
        $this->loadModel('Transaction');
        $data['Transaction']['user_id'] = $user_id;
        $data['Transaction']['short_description'] = $short_description;
        $data['Transaction']['booking_id'] = $booking_id;
        $data['Transaction']['amount'] = $amount;
        $data['Transaction']['currency_code'] = $currency_code;
        $data['Transaction']['service'] = $service;
        $data['Transaction']['app_id'] = $app_id;
        $data['Transaction']['pay_key'] = $pay_key;
        $data['Transaction']['payment_exec_status'] = $payment_exec_status;
        $data['Transaction']['timestamp'] = $timestamp;
        $this->Transaction->create();
        if ($this->Transaction->save($data, false)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @param $user_id
     * @param $amount
     * for dabited account
     */
    public function _minus_wallet($user_id,$amount){
        $this->loadModel('Wallet');
        if(!empty($user_id) && !empty($amount)){
            $user = $this->Wallet->findByUserId($user_id,['id','total_balance','total_withdraw','available_balance']);
            if(!empty($user)){
                $data['Wallet'] = [
                    'total_balance' => $user['Wallet']['total_balance'],
                    'total_withdraw' => $user['Wallet']['total_withdraw'] + $amount,
                    'available_balance' => $user['Wallet']['available_balance'] - $amount
                ];
                $this->Wallet->id = $user['Wallet']['id'];
                if($this->Wallet->save($data)){
                    return true;
                } else{
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * @param null $user_id
     * @param null $amount
     * @param null $service
     * @param null $is_paid
     * @return bool
     * for minus every transaction
     */
    public function _debit_account($user_id = null, $amount = null, $service = null)
    {
        $this->loadModel('Wallet');
        $options = [
            'conditions' => [
                'Wallet.user_id' => $user_id
            ],
            'recursive' => -1
        ];
        $wallet = $this->Wallet->find('first', $options);
        if ($wallet['Wallet']['available_balance'] >= $amount) {
            if ($this->_add_transactions($user_id,$service,null,$amount,null,$service,null,null,null,date('Y-m-d'))) {
                if ($this->_minus_wallet($user_id,$amount)) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {

        }
    }

    /**
     * @param $user_id
     * @return mixed
     * get wallet data by user id
     */
    public function _get_wallet_info($user_id){
        $this->loadModel('Wallet');
        // Search user wallet table info
        $user = $this->Wallet->findByUserId($user_id,['id','total_balance','total_withdraw','available_balance']);
        return $user;
    }

    /**
     * @param $driver_id
     * @param $fare
     * @param $booking_id
     * @return bool
     * it's call when driver press job clear button
     */
    public function _pay_vendor_for_booking($driver_id,$fare,$booking_id,$creator = null){
        $this->loadModel('User');
        $this->loadModel('Setting');
        $this->loadModel('Booking');
        $this->loadModel('Charge');
        // get driver information
        $driver_info = $this->User->findById($driver_id,['User.admin_percentage','User.branch_number']);
        // check driver information is not empty
        if(!empty($driver_info)){
            // Set vendor charge percentage
            if(!empty($driver_info['User']['admin_percentage'])){
                $percentage = $driver_info['User']['admin_percentage'];
            } else {
                $p = $this->Setting->findById('1',['admin_percentage']);
                $percentage = $p['Setting']['admin_percentage'];
            }
            // Getting vendor id for transaction
            $vendor_id = $this->User->findByBranchNumberAndType($driver_info['User']['branch_number'],'vendor',['id']);
            // Calculate the net amount of vendor
            $amount = ($fare*$percentage)/100;

            // set current time
            $timestamp = date('Y-m-d H:i:s');
            $creator = $this->User->findById($creator,['type']);
            if($creator['User']['type'] == 'vendor'){
            if(
                $this->_add_transactions($driver_id,'booking_fee',$booking_id,$amount,null,'booking_fee',null,null,null,$timestamp)
                &&
                $this->_minus_wallet($driver_id,$amount)
                &&
                $this->_add_transactions($vendor_id['User']['id'],'booking_fee',$booking_id,$amount,null,'booking_fee',null,null,null,$timestamp)
                &&
                $this->_add_wallet($vendor_id['User']['id'],$amount)
            ){
                // Send push to Vendor
                $push_data = [
                    'status' => 'notice_vendor',
                    'msg' => $booking_id.' is finished'
                ];
                $this->_pushAll($vendor_id['User']['id'],$push_data);
                return true;
            } else return false;
        } else {
                $chargeRate = $this->Charge->findByUserId($vendor_id['User']['id'],['despatch_driver_pocket']);
                $driver_get = ($amount*$chargeRate['Charge']['despatch_driver_pocket'])/100;
                $vendor_get = $amount - $driver_get;
                if(
                    $this->_add_transactions($driver_id,'booking_fee',$booking_id,$amount,null,'booking_fee',null,null,null,$timestamp)
                    &&
                    $this->_minus_wallet($driver_id,$amount)
                    &&
                    $this->_add_transactions($vendor_id['User']['id'],'booking_fee',$booking_id,$vendor_get,null,'booking_fee',null,null,null,$timestamp)
                    &&
                    $this->_add_wallet($vendor_id['User']['id'],$vendor_get)
                    &&
                    $this->_add_transactions($creator,'booking_fee_get',$booking_id,$driver_get,null,'booking_fee_get',null,null,null,$timestamp)
                    &&
                    $this->_add_wallet($creator,$driver_get)
                ){
                    // Send push to Vendor
                    $push_data = [
                        'status' => 'notice_vendor',
                        'msg' => $booking_id.' is finished'
                    ];
                    $this->_pushAll($vendor_id['User']['id'],$push_data);
                    $this->_pushAll($creator['User']['id'],$push_data);
                    return true;
                } else return false;
            }


        } else return false;

    }
    //=========================Transaction Section End===============================


}
