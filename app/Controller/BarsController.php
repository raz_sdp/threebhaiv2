<?php
App::uses('AppController', 'Controller');
/**
 * Bars Controller
 *
 * @property Bar $Bar
 */
class BarsController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Bar->recursive = -1;
		//$this->set('bars', $this->paginate());


		$this->paginate = array(
	        'limit' => 25,
	        'joins' => array(
	            array(
		            'table' => 'users',
		            'alias' => 'User1',
		            'type' => 'INNER',
		            'foreignKey' => false,
		            'conditions' => array( 
		            	'User1.id = Bar.userId1')
		            ),
	            	array(
		            'table' => 'users',
		            'alias' => 'User2',
		            'type' => 'INNER',
		            'foreignKey' => false,
		            'conditions' => array( 
		            	'User2.id = Bar.userId2')
		            )
	            ), 
	        'fields' => array('Bar.*', 'User2.name', 'User2.id','User1.name','User1.id','User1.type','User2.type')
	    );
	    
	    $bars = $this->paginate();
		$this->set(compact('bars'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Bar->exists($id)) {
			throw new NotFoundException(__('Invalid bar'));
		}
		$options = array('conditions' => array('Bar.' . $this->Bar->primaryKey => $id));
		$this->set('bar', $this->Bar->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Bar->create();
			if ($this->Bar->save($this->request->data)) {
				$this->Session->setFlash(__('The bar has been saved'), 'default', array('class' => 'alert alert-success text-center'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bar could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		}
		$user1s = $this->Bar->User1->find('list');
		$user2s = $this->Bar->User2->find('list');
		$this->set(compact('user1s', 'user2s'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Bar->exists($id)) {
			throw new NotFoundException(__('Invalid bar'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Bar->save($this->request->data)) {
				$this->Session->setFlash(__('The bar has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bar could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('Bar.' . $this->Bar->primaryKey => $id));
			$this->request->data = $this->Bar->find('first', $options);
		}
		$user1s = $this->Bar->User1->find('list');
		$user2s = $this->Bar->User2->find('list');
		$this->set(compact('user1s', 'user2s'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Bar->id = $id;
		if (!$this->Bar->exists()) {
			throw new NotFoundException(__('Invalid bar'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Bar->delete()) {
			$this->Session->setFlash(__('Bar deleted'), 'default', array('class' => 'alert alert-success text-center'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Bar was not deleted'), 'default', array('class' => 'alert alert-danger text-center'));
		$this->redirect(array('action' => 'index'));
	}
	
/**
 * _is_already_barred method
 * Method to check that A Driver is already barred by a passenger
 * @params:
 *		$userId1 : who bar
 *		$userId2 : whom to bar
 */	

	private function _is_already_barred($userId1, $userId2) {
		$this->Bar->recursive = -1;
		$res = $this->Bar->find('count', array('conditions' => array('Bar.userId1' => $userId1, 'Bar.userId2' => $userId2)));
		if(!empty($res)) return true;
		else return false;
	}
	
	//bar a driver
	public function bar_driver(){

	}

/**
 * dobar method
 * Method to do any bar action by driver or passenger
 */
	public function dobar() {
		$this->autoRender = false;
		if($this->request->is('post') && !empty($this->request->data)) {
			$data = array();
			//print_r($this->request->data);
			$drivers_to_bar = explode(',', $this->request->data['Bar']['userId2']);
			$status = false;
			$data = array();
			foreach($drivers_to_bar as $userId2) {
				if(!$this->_is_already_barred($this->request->data['Bar']['userId1'], $userId2)) {
					$data = array('Bar' => array(
						'userId1' => $this->request->data['Bar']['userId1'], 
						'userId2' => $userId2, 
						'who_to_whom' => $this->request->data['Bar']['who_to_whom']
					));
					$this->Bar->create();
					if($this->Bar->save($data,false)) $status = true;
				}
			}
			if($status) die(json_encode(array('success' => true)));
			else die(json_encode(array('success' => false, 'message' => 'Bar Action failed')));
		} else die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
	}
	
	//un-do bar 
	public function unbar_driver(){

	}

/**
 * undobar method
 * Method to do any bar action by driver or passenger
 */
	public function undobar($bar_id, $user_code) {
		$this->autoRender = false;
		// print_r($bar_id);
		// print_r($user_code);
		if(empty($bar_id) || empty($user_code)) die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
		$this->loadModel('User');
		$user_id = $this->User->findByCode($user_code, array('id'));
		$is_bar_exits = $this->Bar->find('count', array('recursive' => -1, 'conditions' => array('userId1' => $user_id['User']['id'], 'id' => $bar_id)));
		if($is_bar_exits) {
			$this->Bar->id = $bar_id;
			if($this->Bar->delete()) die(json_encode(array('success' => true)));
			else die(json_encode(array('success' => false, 'message' => 'Un-Bar action failed')));
		} else die(json_encode(array('success' => false, 'message' => 'No record found')));
	}	
	
}
