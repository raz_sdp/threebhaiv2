<?php
App::uses('AppController', 'Controller');
/**
 * Settings Controller
 *
 * @property Setting $Setting
 * @property PaginatorComponent $Paginator
 */
class SettingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Setting->recursive = 0;
		$this->set('settings', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Setting->exists($id)) {
			throw new NotFoundException(__('Invalid setting'));
		}
		$options = array('conditions' => array('Setting.' . $this->Setting->primaryKey => $id));
		$this->set('setting', $this->Setting->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Setting->create();
			if ($this->Setting->save($this->request->data)) {
				$this->Session->setFlash(__('The setting has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The setting could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Setting->exists($id)) {
			throw new NotFoundException(__('Invalid setting'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Setting->save($this->request->data, true)) {
				$this->Session->setFlash(__('The setting has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The setting could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('Setting.' . $this->Setting->primaryKey => $id));
			$this->request->data = $this->Setting->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Setting->id = $id;
		if (!$this->Setting->exists()) {
			throw new NotFoundException(__('Invalid setting'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Setting->delete()) {
			$this->Session->setFlash(__('The setting has been deleted.'), 'default', array('class' => 'alert alert-success text-center'));
		} else {
			$this->Session->setFlash(__('The setting could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function g(){
		$this->autoRender = false;
		$social_msg = $this->Setting->findById(1, array('social_sharing_msg'));
		echo json_encode(array('success' => true, 'social_msg' => $social_msg['Setting']['social_sharing_msg']));
	}

	public function admin_share_links($id = null) {
		if (!$this->Setting->exists($id)) {
			throw new NotFoundException(__('Invalid setting'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Setting->save($this->request->data, true)) {
				$this->Session->setFlash(__('The setting has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The setting could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('Setting.' . $this->Setting->primaryKey => $id));
			$this->request->data = $this->Setting->find('first', $options);
		}
	}

	public function paypal_info(){
		$this->autoRender = false;
        $paypal_info = $this->Setting->findById(1, array('live_username','paypal_clientId', 'sandbox_clientId', 'paypal_mode', 'sandbox_username')); 
        echo json_encode(array('paypal_info' => array(
        		'live_username' => $paypal_info['Setting']['live_username'],
        		'live_clientId' => $paypal_info['Setting']['paypal_clientId'],
        		'sandbox_clientId' => $paypal_info['Setting']['sandbox_clientId'],
        		'sandbox_username' => $paypal_info['Setting']['sandbox_username'],
        		'paypal_mode' => $paypal_info['Setting']['paypal_mode'],
        	)));
	}

	public function admin_paypal($id =null){
		if (!$this->Setting->exists($id)) {
			throw new NotFoundException(__('Invalid setting'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Setting->save($this->request->data, true)) {
				$this->Session->setFlash(__('The setting has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The setting could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('Setting.' . $this->Setting->primaryKey => $id));
			$this->request->data = $this->Setting->find('first', $options);
		}

	}


}
