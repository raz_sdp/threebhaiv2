<?php
App::uses('AppController', 'Controller');
/**
 * PhoneQueues Controller
 *
 * @property PhoneQueue $PhoneQueue
 */
class PhoneQueuesController extends AppController {

/*Twilio call Screening for a list of numbers to call in a specific order.
If the first number doesn't pick up, then a second number should be called, and so on. */
	public function twilio_screen(){
		$this->autoRender = false;
		header("content-type: text/xml"); 
		$res = '<?xml version="1.0" encoding="UTF-8"?><Response><Gather action="http://cabbieappuk.com/phone_queues/twilio_complete/"><Say>Press any key to accept this call</Say></Gather><Hangup/></Response>';
		// CakeLog::write('twilio_log', "Response Twilio Screen\n");
		// CakeLog::write('twilio_log', $res);
		die($res);
	}
	public function twilio_complete(){
		$this->autoRender = false;
		header("content-type: text/xml"); 
		$res = '<?xml version="1.0" encoding="UTF-8"?><Response><Say>Connecting</Say></Response>';
		// CakeLog::write('twilio_log', "Response Twilio Complete\n");
		// CakeLog::write('twilio_log', $res);
		die($res);
	}
	public function twilio_call(){
		$this->autoRender = false;
		//Configure::write('debug', 0);
		header("content-type: text/xml");
		$time_pre = microtime(true);
		$this->_apiLog(
						1, 
						var_export($_POST, true), 
						'PostResponses', 
						1, 
						$time_pre);
		if($this->request->is('post')) {
			// CakeLog::write('twilio_log', "Post\n");
			// CakeLog::write('twilio_log', var_export($this->request->data,true));
			
			//print_r($this->request->data); exit;
			
			$call_sid = $this->request->data['CallSid'];
			$to = $this->request->data['To'];
			$from = $this->request->data['From'];
			$dial_call_status = @$this->request->data['DialCallStatus'];
			$call_status = $this->request->data['CallStatus'];
			if(!empty($this->request->data['CallDuration'])) $call_duration = $this->request->data['CallDuration'];
			if(!empty($this->request->data['RecordingDuration'])) $recording_duration = $this->request->data['RecordingDuration'];
			if(!empty($this->request->data['DialCallDuration'])) $dial_call_duration = $this->request->data['DialCallDuration'];
			$recording_url = @$this->request->data['RecordingUrl'];
			$this->loadModel('Zone');
			$this->Zone->recursive = -1;
			$zone = $this->Zone->findByVoipPhoneNo($to);
			//print_r($zone); exit;
			//print_r($dial_call_status); exit;
			$zone_type = $zone['Zone']['type'];
			$zone_id = $zone['Zone']['id']; //Get Zone id from Twilio Voip phone number

			$settings = $this->PhoneQueue->query("SELECT req_timeout,repeat_call_timeout, bar_private_no 
																 From phone_settings 
																 WHERE zone_id = '$zone_id'
															");
			$bar_private_no = $settings[0]['phone_settings']['bar_private_no'];
			
			if(!empty($bar_private_no)){
				$pos = stripos($from, '+44'); 
				if($pos === false) {
					$res = '<Response>
					<Say voice="woman">Sorry this number does not accept caller withheld numbers. Please call back displaying your number.</Say>
						<Hangup/>
					</Response>';
					// CakeLog::write('twilio_log', "Withheld\n");
					// CakeLog::write('twilio_log', $res);
					die($res);	
					exit;
				}
			}
			$req_timeout = $settings[0]['phone_settings']['req_timeout'];
			$repeat_call_timeout = $settings[0]['phone_settings']['repeat_call_timeout'];

			//check if twillio has already searched driver for that call_sid
			$check_call_sid = $this->PhoneQueue->find('first', array(
				'recursive' => -1,
				'conditions' => array('PhoneQueue.user_id IS NULL', 'call_sid' => $call_sid),
			));
			if(!empty($check_call_sid)){
				//cut the call if twillio has already searched driver for that call_sid
				$this->_apiLog($call_sid, var_export($this->request->data, true), 'Hangup', 1, $time_pre);
				$res = '<Response>
					<Say voice="woman">Sorry, we are very busy. Please call back later.</Say>
						<Hangup/>
					</Response>';
				// CakeLog::write('twilio_log', "Response for user_id null\n");
				// CakeLog::write('twilio_log', $res);
				die($res);
				exit;
			}


			$driver_object = $this->_driver_search($call_sid, $to, $from, $repeat_call_timeout, $zone_id);
			
			//print_r($driver_object); exit;
		
			$driver = $driver_object[0];
		//	$zone_id = $driver_object[1];

			// $repeated_driver_id = $driver_object[0][0]['Driver']['user_id'];
			// //check if same driver got call twice, say no driver found
			// $check_driver_sid = $this->PhoneQueue->find('first', array(
			// 	'recursive' => -1,
			// 	'conditions' => array('PhoneQueue.user_id' => $repeated_driver_id, 'call_sid' => $call_sid),
			// 	'order' => array('PhoneQueue.created' => 'DESC')
			// ));
			// if(!empty($check_driver_sid)){
			// 	//cut the call if twillio has already searched driver for that call_sid
			// 	$this->_apiLog($call_sid, var_export($this->request->data, true), 'Hangup', 1, $time_pre);
			// 	$res = '<Response>
			// 		<Say voice="woman">Sorry no driver is available at this moment</Say>
			// 			<Hangup/>
			// 		</Response>';
			// 	CakeLog::write('twilio_log', "Response same driver got call more than once\n");
			// 	CakeLog::write('twilio_log', $res);
			// 	die($res);
			// 	exit;
			// }


			if($dial_call_status != 'completed' && !empty($driver[0]['Driver']['voip_no'])){
				//complete = The called party answered the call and was connected to the caller.
				// by !completed we means a driver didn't answer and twilio gets back to us for next driver

				//update response time for driver who did not pick phone last time
				$call_sid_check = $this->PhoneQueue->find('first', array(
					'recursive' => -1,
					'conditions' => array(
						'PhoneQueue.call_sid' => $call_sid,
						'PhoneQueue.response_time IS NULL'
					)
				));
				//print_r($call_sid_check['PhoneQueue']['id']); exit;
				if(!empty($call_sid_check)){
					
					//$this->PhoneQueue->saveField('response_time', date('Y-m-d H:i:s'));
					// nasty fix: not a good way. this is to keep the driver in his position if he ignores a call
					// we are setting the request_time for the current call_sid to other *more* recent previous request_time
					
					// $prevReq = $this->PhoneQueue->find('first', array(
					// 	'recursive' => -1,
					// 	'conditions' => array(
					// 		'PhoneQueue.user_id' => $call_sid_check['PhoneQueue']['user_id'],
					// 		'PhoneQueue.request_time < (SELECT MAX(request_time) FROM phone_queues WHERE user_id = \'' . $call_sid_check['PhoneQueue']['user_id'] . '\')',
					// 	),
					// 	'order' => array('PhoneQueue.request_time' => 'desc')
					// ));
					$prevReq = $this->PhoneQueue->find('first', array(
						'recursive' => -1,
						'conditions' => array(
							'PhoneQueue.user_id' => $call_sid_check['PhoneQueue']['user_id'],
						),
						'fields' => array('PhoneQueue.request_time'),
						'order' => array('PhoneQueue.request_time' => 'desc')
					));

					$prev_request_time = $prevReq['PhoneQueue']['request_time'];

					// if($prevReq)
					// 	$prev_request_time = $prevReq['PhoneQueue']['request_time'];
					// else
					// 	$prev_request_time = date('2013-m-d H:i:s'); // !!!!

					$this->PhoneQueue->id = $call_sid_check['PhoneQueue']['id'];
					$this->PhoneQueue->save(array(
						'PhoneQueue' => array(
							'response_time' => $prev_request_time,
							'request_time' => $prev_request_time // not sure what will be affected!
							)
						)
					);
				}
				
				//print_r($call_sid_check); exit;
				// logout driver for dial-call_status = 'no-answer'
					if($dial_call_status == 'no-answer'){
						$list_drivers = $this->PhoneQueue->query("SELECT
							P.user_id, P.zone_id, u.phonecall_available, 
							u.phonecall_gps_available, u.phonecall_gps_zone, u.phonecall_zone
						FROM
							phone_queues P LEFT JOIN users u on (P.user_id = u.id)
						WHERE
							P.completed IS NULL AND call_sid = '$call_sid'
						");
					if(!empty($list_drivers)){
						//log current driver out for ignoring call
						if($zone_type === 'phonecall_zone') {
							$this->_logout_driver_for_ignore($list_drivers, $zone_id);
						} elseif($zone_type === 'phonecall_gps_zone') {
							$this->_logout_driver_for_ignore_gps($list_drivers, $zone_id);
						}
					}
				}

				/*
				Assign oldest request time to driver when create row(call him)
				if he receives the call update the time to current time, else keep it
				this is done so that a driver losses his position only he receives call
				*/
				$prevReq = $this->PhoneQueue->find('first', array(
						'recursive' => -1,
						'conditions' => array(
							'PhoneQueue.user_id' => $driver[0]['Driver']['user_id'],
						),
						'fields' => array('PhoneQueue.request_time'),
						'order' => array('PhoneQueue.request_time' => 'desc')
					));

					$prev_request_time = $prevReq['PhoneQueue']['request_time'];

				//if(!empty($driver)) {
					$this->PhoneQueue->create();
					$this->PhoneQueue->save(array(
						'PhoneQueue'=>array(
							'call_sid'=> $call_sid,
							'to'=> $to ,
							'from'=> $from,
							'user_id'=> $driver[0]['Driver']['user_id'],
						//	'request_time'=> date('Y-m-d H:i:s'),
							'request_time' => $prev_request_time,
							'zone_id' => $zone_id,
							'recording_url' => $recording_url,
							'phone_no' => $driver[0]['Driver']['voip_no']
							)
						)
					);
					if(!empty($req_timeout)){
						$timeout = $req_timeout;
					} else {
						$timeout = '120';
					}
					$res = '<Response><Say voice="woman">Connecting and calls may be recording, please wait.</Say><Dial action="http://cabbieappuk.com/phone_queues/twilio_call/?nextnumber=yes" timeout="'. $timeout .'" record="true"><Number url="http://cabbieappuk.com/phone_queues/twilio_screen/">'. $driver[0]['Driver']['voip_no'] .'</Number></Dial></Response>';
					//$res = '<Response><Dial action="http://74.3.255.228/dev.cabbieapp/phone_queues/twilio_call/?nextnumber=yes" timeout="'. $timeout .'" record="true"><Number url="http://74.3.255.228/dev.cabbieapp/phone_queues/twilio_screen/">'. $driver[0]['Driver']['voip_no'] .'</Number></Dial></Response>';
					die($res);



			}else {
				if($dial_call_status === 'completed'  && ($call_status === 'completed' || $call_status === 'in-progress')) {
				//dial_call_status is completed means: The called party answered the call and was connected to the caller.
				// call_status complete means conversation is complete
				// sometimes we get in-progress as last response from twilio (BUG FIX!)
				$data['PhoneQueue']['completed'] = date('Y-m-d H:i:s');
				$data['PhoneQueue']['status'] = 'completed';
				$call_charge_zone = $this->PhoneQueue->query("SELECT minimum_charge, per_minute_charge, remain_loggedin_on_accept FROM phone_settings WHERE zone_id = '$zone_id'");
				$minimum_call_charge = $call_charge_zone[0]['phone_settings']['minimum_charge'];
				$per_minute_call_charge = $call_charge_zone[0]['phone_settings']['per_minute_charge'];
				$remain_loggedin_on_accept = $call_charge_zone[0]['phone_settings']['remain_loggedin_on_accept'];

				$data['PhoneQueue']['call_duration'] = $call_duration;	
				$data['PhoneQueue']['recording_duration'] = $recording_duration;
				$data['PhoneQueue']['dial_call_duration'] = $dial_call_duration;

				$driver_obj = $this->PhoneQueue->find('first', array(
						'fields' => array('PhoneQueue.user_id', 'PhoneQueue.id'),
						'conditions' => array('PhoneQueue.call_sid' => $call_sid),
						'order' => array('PhoneQueue.created' => 'DESC')
				));
				$driver_id = $driver_obj['PhoneQueue']['user_id'];
				if($dial_call_duration <= 10 && !empty($data['PhoneQueue']['completed'])) {
					$total_charge = $minimum_call_charge;
					$this->_billing_twillio($driver_id, $total_charge);
				} elseif($dial_call_duration > 10 && !empty($data['PhoneQueue']['completed'])) {
					$per_second_call_charge = $per_minute_call_charge / 60;
					$total_charge = $dial_call_duration * $per_second_call_charge;
					$this->_billing_twillio($driver_id, $total_charge);
				}
				$data['PhoneQueue']['call_charge'] = $total_charge;
				$this->PhoneQueue->id = $driver_obj['PhoneQueue']['id'];
				$data['PhoneQueue']['request_time'] = date('Y-m-d H:i:s');
				$data['PhoneQueue']['response_time'] = date('Y-m-d H:i:s');
				$data['PhoneQueue']['recording_url'] = $recording_url;
				$this->PhoneQueue->save($data);
				if(empty($remain_loggedin_on_accept)){
					// log driver out after accept a job or not
					if($zone_type === 'phonecall_zone') {
						$this->_logout_driver_on_accept($driver_id);
					} elseif($zone_type === 'phonecall_gps_zone'){
						$this->_logout_driver_on_accept_gps($driver_id);
					}
					
				}
					//check who should be logged out
					$list_drivers = $this->PhoneQueue->query("SELECT
						P.user_id, P.zone_id, u.phonecall_available, 
						u.phonecall_gps_available, u.phonecall_gps_zone, u.phonecall_zone
					FROM
						phone_queues AS P LEFT JOIN users u on (P.user_id = u.id)
					WHERE
						P.completed IS NULL AND call_sid = '$call_sid'
					");

					if(!empty($list_drivers)){
						//print_r($list_drivers);
						//log drivers out for ignoring call
						if($zone_type === 'phonecall_zone') {
							$this->_logout_driver_for_ignore($list_drivers, $zone_id);
						} elseif($zone_type === 'phonecall_gps_zone') {
							$this->_logout_driver_for_ignore_gps($list_drivers, $zone_id);
						}
					}
				
					$res = '<Response/>';
					// CakeLog::write('twilio_log', "Response\n");
					// CakeLog::write('twilio_log', $res);
					die($res);
					exit;
			}
				
				//check who should be logged out
				// according to Azid/Khan's feedback - do not log driver out if call is ignored
				
				$list_drivers = $this->PhoneQueue->query("SELECT
					P.user_id, P.zone_id, u.phonecall_available, 
					u.phonecall_gps_available, u.phonecall_gps_zone, u.phonecall_zone
				FROM
					phone_queues P LEFT JOIN users u on (P.user_id = u.id)
				WHERE
					P.completed IS NULL AND call_sid = '$call_sid'
				");

//print_r('voip');
				if(!empty($list_drivers)){
					//log drivers out for ignoring call
					if($zone_type === 'phonecall_zone') {
						$this->_logout_driver_for_ignore($list_drivers, $zone_id);
					} elseif($zone_type === 'phonecall_gps_zone') {
						$this->_logout_driver_for_ignore_gps($list_drivers, $zone_id);
					}
				} 
				// uncomment above section if they change their minds again!

				$this->_apiLog($call_sid, var_export($this->request->data, true), 'Hangup', 1, $time_pre);
				$res = '<Response>
					<Say voice="woman">Sorry, we are very busy. Please call back later.</Say>
						<Hangup/>
					</Response>';
				// CakeLog::write('twilio_log', "Response\n");
				// CakeLog::write('twilio_log', $res);
				die($res);

			}
		}
	} 


	private function _logout_driver_for_ignore($drivers, $zone_id){
		//log out driver for ignore a phone call
		$reject_threshold_for_zone = $this->PhoneQueue->query("SELECT reject_threshold FROM phone_settings WHERE zone_id = $zone_id");
		$reject_threshold =$reject_threshold_for_zone[0]['phone_settings']['reject_threshold'];
		if(empty($reject_threshold)) $reject_threshold = 3;// use default value in case there is no settings for the given zone.
		foreach ($drivers as $driver) {
			$driver_zone_phone = $driver['u']['phonecall_zone'];
			$driver_zone_gps = $driver['u']['phonecall_gps_zone'];
			$driver_id = $driver['P']['user_id'];
			$reject_count = $this->PhoneQueue->query("SELECT
				COUNT(IF(res.completed IS NULL, 1, NULL))AS cnt
			FROM
				(
					SELECT
						user_id, id, completed
					FROM
						phone_queues
					WHERE
						user_id = '$driver_id'
					AND zone_id = '$zone_id'
					AND status IS NULL
					ORDER BY
						created DESC
					LIMIT $reject_threshold
				)AS res");
				
			if($reject_count[0][0]['cnt'] >= $reject_threshold) {
				if ($driver['u']['phonecall_available'] == 'yes') {
					// send notification to driver
    				$this->_push($driver_id, 'You have been logged-out for ignoring the Phone call.','CabbieCall', 
    			 		null, null, null, null, null, null, 
    			 		null, null, null, null, null, null, true);
    				$this->PhoneQueue->query("UPDATE users SET phonecall_available  = 'no'
						WHERE id = '$driver_id' AND phonecall_available = 'yes'");
				}
			$this->_position_driver_phone($zone_id, $driver_id);

			} 

		}
	}

	private function _logout_driver_for_ignore_gps($drivers, $zone_id){
		//log out driver for ignore a phone call
		$reject_threshold_for_zone = $this->PhoneQueue->query("SELECT reject_threshold FROM phone_settings WHERE zone_id = $zone_id");
		$reject_threshold =$reject_threshold_for_zone[0]['phone_settings']['reject_threshold'];
		if(empty($reject_threshold)) $reject_threshold = 3;// use default value in case there is no settings for the given zone.
		foreach ($drivers as $driver) {
			$driver_zone_phone = $driver['u']['phonecall_zone'];
			$driver_zone_gps = $driver['u']['phonecall_gps_zone'];
			$driver_id = $driver['P']['user_id'];
			$reject_count = $this->PhoneQueue->query("SELECT
				COUNT(IF(res.completed IS NULL, 1, NULL))AS cnt
			FROM
				(
					SELECT
						user_id, id, completed
					FROM
						phone_queues
					WHERE
						user_id = '$driver_id'
					AND zone_id = '$zone_id'
					AND status IS NULL
					ORDER BY
						created DESC
					LIMIT $reject_threshold
				)AS res");
				
			if($reject_count[0][0]['cnt'] >= $reject_threshold) {
				if ($driver['u']['phonecall_gps_available'] == 'yes') {
					// send notification to driver
    				$this->_push($driver_id, 'You have been logged-out for ignoring the Phone call.','CabbieCall',
    					null, null, null, null, null, null, 
    					null, null, null, null, null, null, null, true);
    				$this->PhoneQueue->query("UPDATE users SET phonecall_gps_available = 'no', phonecall_gps_zone = NULL 
						WHERE id = '$driver_id' AND phonecall_gps_available = 'yes'");
				}
				$this->_position_driver_gps($zone_id, $driver_id);
			} 

		}
	}

	private function _logout_driver_on_accept_gps($driver_id = null){
			$this->loadModel('User');
			$user_obj = $this->User->findById($driver_id, array('phonecall_gps_available'));
			if ($user_obj['User']['phonecall_gps_available'] == 'yes') {
				// send notification to driver
				$this->_push($driver_id, 'You have been logged-out for accepting the phone call.','CabbieCall',
					null, null, null, null, null, null, 
					null, null, null, null, null, null, null, true);
			}
			$this->PhoneQueue->query("UPDATE users SET phonecall_gps_available = 'no', phonecall_gps_zone = NULL 
					WHERE id = '$driver_id' AND phonecall_gps_available = 'yes'");
        $this->_position_driver_gps($zone_id, $driver_id);
	}

	private function _logout_driver_on_accept($driver_id = null){
			$this->loadModel('User');
			$user_obj = $this->User->findById($driver_id, array('phonecall_available'));
			if ($user_obj['User']['phonecall_available'] == 'yes') {
				// send notification to driver
				$this->_push($driver_id, 'You have been logged-out for accepting the phone call.','CabbieCall', 
					null, null, null, null, null, null, 
					null, null, null, null, null, null, true);
			}
			$this->PhoneQueue->query("UPDATE users SET phonecall_available  = 'no'
					WHERE id = '$driver_id' AND phonecall_available = 'yes'");
			$this->_position_driver_phone($zone_id, $driver_id);
	}

	public function test(){

		// $call_sid_check['PhoneQueue']['user_id'] = 129;

		// $prevReq = $this->PhoneQueue->find('first', array(
		// 				'recursive' => -1,
		// 				'conditions' => array(
		// 					'PhoneQueue.user_id' => $call_sid_check['PhoneQueue']['user_id'],
		// 					'PhoneQueue.request_time < (SELECT MAX(request_time) FROM phone_queues WHERE user_id = \'' . $call_sid_check['PhoneQueue']['user_id'] . '\')',
		// 				),
		// 				'order' => array('PhoneQueue.request_time' => 'desc')
		// 			));

		// 			if($prevReq)
		// 				$prev_request_time = $prevReq['PhoneQueue']['request_time'];
		// 			else
		// 				$prev_request_time = date('2013-m-d H:i:s'); // !!!!


		// 			print_r($prev_request_time);

					/*$this->PhoneQueue->id = $call_sid_check['PhoneQueue']['id'];
					$this->PhoneQueue->save(array(
						'PhoneQueue' => array(
							'response_time' => $prev_request_time,
							'request_time' => $prev_request_time // not sure what will be affected!
							)
						)
					);*/
		Configure::write('debug', 2);
		$this->autoRender = false;
		$test = $this->_driver_search('sdfsdfsdfsdfsdfsf','+441278393065','+447939857957', null, '1102');
		print_r($test);
		// $driver = $test[0][0]['Driver']['user_id'];
		// $check_driver_sid = $this->PhoneQueue->find('first', array(
		// 		'recursive' => -1,
		// 		'conditions' => array('PhoneQueue.user_id' => $driver, 'call_sid' => '1234567890987654321'),
		// 		'order' => array('PhoneQueue.created' => 'DESC')
		// 	));
		// print_r($check_driver_sid);
		// 	if(!empty($check_driver_sid)){
		// 		//cut the call if twillio has already searched driver for that call_sid
		// 		print_r('no driver found');
		// 		exit;
		// 	}

	}
	
	private function _driver_search($call_sid, $to, $from, $repeat_call_timeout = null, $zone_id){
	//	if(empty($repeat_call_timeout)) $repeat_call_timeout = '2';

		$time_pre = microtime(true);
		$driver = array();
		$now = date('Y-m-d H:i:s');
		$this->loadModel('Setting');
		$driver_min_balance = $this->Setting->findById(1, array('driver_min_balance'));
		$min_balance = $driver_min_balance['Setting']['driver_min_balance'];
		$original_zone = $zone_id;
		$repeater_call = $this->PhoneQueue->find('all',array(
			'joins'=>array(
				array(
					'table'=> 'users',
					'alias'=> 'Driver',
					'type'=> 'LEFT',
					'conditions'=>'PhoneQueue.user_id = Driver.id'
					)
				),
			'fields' => array('PhoneQueue.to','PhoneQueue.from','User.voip_no', 'User.id'),
			'conditions'=>array(
				'zone_id'=> $zone_id,
				'dial_call_duration >=' => '3',
				'to' => $to,
				'from' => $from,
				'call_sid <>' => $call_sid,
				"TIMESTAMPDIFF(MINUTE , PhoneQueue.request_time, '$now') <= '$repeat_call_timeout' "
				),
			'limit' => 1,
			));
		//$repeater_call_voip_no  = array();
		//print_r($repeater_call); exit(0);
		if(!empty($repeater_call)) {
			$driver[0]['Driver']['voip_no'] = $repeater_call[0]['User']['voip_no'];
			$driver[0]['Driver']['user_id'] = $repeater_call[0]['User']['id'];
		}
		if(!empty($driver[0]['Driver']['user_id']) || !empty($driver[0]['Driver']['voip_no'])){
			return array($driver,$zone_id);
			//return $driver;
		}else{
			if(!empty($zone_id)){
				//find alternative zones for that particular zone
				$this->loadModel('PhoneSetting');
				$alternative_zone = $this->PhoneSetting->findByZoneId($zone_id, array('restriction'));
				$alt_zn = trim($alternative_zone['PhoneSetting']['restriction'],'#');
	            $zns = array_filter(explode('#', $alt_zn)); 
	            //find driver in the particular zone
				$driver = $this->_find_driver_twilio($zone_id, $call_sid, $min_balance);
				if(empty($driver)) {
					while(1){
						if($zone_id == current($zns)) {
							$zone_id = next($zns); 
						}
						else { 
							$zone_id = current($zns);
						}
						if(!!$zone_id){
							$driver = $this->_find_driver_twilio($zone_id, $call_sid, $min_balance);
						}
						if(empty($driver) && next($zns)){
							prev($zns);
							continue;
						} else break;
					} 
					if(empty($driver)){
						$divert_phn = $this->PhoneSetting->findByZoneId($original_zone, array('divert_phoneno'));
						$driver[0]['Driver']['voip_no'] = $divert_phn['PhoneSetting']['divert_phoneno'];
					}
				}
			}
			return array($driver,$zone_id);
		}
	}

	//function to search driver to receive twilio call
	private function _find_driver_twilio($zone_id, $call_sid, $min_balance){
		$driver = $this->PhoneQueue->query("SELECT
							Driver.user_id AS user_id,
							Driver.request_time AS request_time,
							Driver.voip_no AS voip_no,
							Driver.phonecall_zone AS Phonecall_zone,
							Driver.phonecall_gps_zone AS Phonecall_gps_zone
						FROM
							(
								SELECT
									q.user_id,
									q.request_time,
									u.voip_no,
									u.phonecall_zone,
									u.phonecall_gps_zone
								FROM
									phone_queues q
								INNER JOIN users u ON(
									u.id = q.user_id
									AND(
										(
											u.phonecall_zone = '$zone_id'
											AND u.phonecall_available = 'yes'
										)
										OR(
											u.phonecall_gps_zone = '$zone_id'
											AND u.phonecall_gps_available = 'yes'
										)
									)
									AND u.is_enabled = '1'
									AND u.type = 'driver'
								)
								WHERE q.user_id NOT IN(SELECT user_id FROM phone_queues WHERE call_sid='$call_sid') AND 
										(SELECT SUM(amount) FROM transactions WHERE user_id = q.user_id 
											AND service <> 'cash_in_hand' AND is_paid = 1) >= '$min_balance' 
								ORDER BY
									q.request_time DESC
							)Driver
						GROUP BY
							Driver.user_id
						ORDER BY
							Driver.request_time ASC
						LIMIT 1
						");
		return $driver;
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PhoneQueue->recursive = -1;
		$this->paginate = array(
				'conditions' => array('PhoneQueue.call_sid IS NOT NULL'),
				'group' => array('PhoneQueue.call_sid'),
				'order' => array('PhoneQueue.created' => 'desc'),
			);
		//$this->set('phoneQueues', $this->paginate());
		$phoneQueues = $this->paginate();
		$this->set(compact('phoneQueues'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->recursive = -1;
		$this->paginate = array(
	        'limit' => 25,
	        'conditions' => array('PhoneQueue.call_sid' => $id),
	       	'order' => array('PhoneQueue.created' => 'desc'),
	    );
	    $phoneQueues = $this->paginate();
		$this->set(compact('phoneQueues'));

	}


/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PhoneQueue->create();
			if ($this->PhoneQueue->save($this->request->data)) {
				$this->Session->setFlash(__('The phone queue has been saved'), 'default', array('class' => 'alert alert-success text-center'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The phone queue could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PhoneQueue->exists($id)) {
			throw new NotFoundException(__('Invalid phone queue'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->PhoneQueue->save($this->request->data)) {
				$this->Session->setFlash(__('The phone queue has been saved'), 'default', array('class' => 'alert alert-success text-center'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The phone queue could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('PhoneQueue.' . $this->PhoneQueue->primaryKey => $id));
			$this->request->data = $this->PhoneQueue->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PhoneQueue->id = $id;
		if (!$this->PhoneQueue->exists()) {
			throw new NotFoundException(__('Invalid phone queue'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PhoneQueue->delete()) {
			$this->Session->setFlash(__('The call log has been deleted'), 'default', array('class' => 'alert alert-success text-center'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('The call log was not deleted'), 'default', array('class' => 'alert alert-danger text-center'));
		$this->redirect(array('action' => 'index'));
	}

	public function test_log(){
		$this->autoRender = false;
		$log_path = APP.'tmp' . DS . 'logs'. DS .'error.log';
		$fp = fopen($log_path, "w");
		fwrite($fp, " ");
		fclose($fp);
		$debug_log_path = APP.'tmp' . DS . 'logs'. DS .'debug.log';
		$filep = fopen($debug_log_path, "w");
		fwrite($filep, " ");
		fclose($filep);
	}

	public function g($user_id, $limit_or_from = 30, $to = null){
        $this->autoRender = false;
        $user_info = $this->PhoneQueue->User->findById($user_id);
        if(!empty($user_info)){
            $conditions = array('PhoneQueue.user_id' => $user_id, 'PhoneQueue.status' => 'completed');
            if(!empty($limit_or_from) && !empty($to)){
                $from = $limit_or_from;
                $conditions = am($conditions, array('DATE(PhoneQueue.created) BETWEEN ? AND ? ' => array($from, $to)));
            } else {
                $limit_created = $limit_or_from;
                $conditions = am($conditions, array('TIMESTAMPDIFF(DAY,PhoneQueue.created,NOW()) <=' => $limit_created));
            }
            $transaction_history = $this->PhoneQueue->find('all', array(
                'recursive' => -1,
               // 'conditions' => $conditions,
                'fields' => array('PhoneQueue.dial_call_duration', 'PhoneQueue.call_charge', 'PhoneQueue.created', 'PhoneQueue.from'),
                'order' => array('PhoneQueue.created' => 'DESC'),
            ));
            $total_call = $this->PhoneQueue->find('count', array(
            	//'conditions' => $conditions
            ));
            if(!empty($transaction_history)){
                $transaction_history = Hash::extract($transaction_history, '{n}.PhoneQueue');
                foreach ($transaction_history as $key => $transaction) {
                	$transaction_history[$key]['call_charge'] = $transaction['call_charge'];
                	$transaction_history[$key]['dial_call_duration'] = $transaction['dial_call_duration']. ' sec';
                    $transaction_history[$key]['created'] = date_format(date_create($transaction['created']), 'd M Y g:i A');
                }
                die(json_encode(array('success' => true, 'total_call' => $total_call, 'invoice' => $transaction_history)));
            } else die(json_encode(array('success' => false, 'message' => 'No data found.')));
        } else die(json_encode(array('success' => false, 'message' => 'Invalid User')));
    }

    public function test_delete(){
    	$this->autoRender = false;
    	App::import('Vendor', 'Services', array('file' => 'Services' . DS . 'Twilio.php'));	
	    /* Set CabbieApp's AccountSid and AuthToken */
        if(in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', "::1"))){
            #Test credential
            $AccountSid = "AC380847079144a880d552a768a2968904";
            $AuthToken = "1e2d606db297d5d64739e3b99b04aafd";
        } else {
            #Live credential
            $AccountSid = "ACd5880a6ed2193077c585763c514be2a2";
            $AuthToken = "c67580685ab040d98a11612441b87848";
        }

	    $client = new Services_Twilio($AccountSid, $AuthToken);
	    $now = date('Y-m-d H:i:s');
    	$old_records = $this->PhoneQueue->find('all', array(
    		'recursive' => -1,
    		'conditions' => array(
    			"PhoneQueue.created < ('$now' - INTERVAL 3 MONTH)", 
    			'PhoneQueue.call_sid IS NOT NULL', 
    			'PhoneQueue.is_deleted_from_twilio' => 0)
    	));
    	//pr($old_records); exit;
    	foreach ($old_records as $key => $record) {
    		$call_sid = $record['PhoneQueue']['call_sid'];
    		try {
	            //$call = $client->account->calls->delete("$call_sid");
	            //$call = $client->account->calls->delete("CA9db91cf94ca2d3170b1decab8996d378");
    			//	print_r($call_sid); exit;
    // 			$this->PhoneQueue->id = $record['PhoneQueue']['id'];
				// $this->PhoneQueue->saveField('is_deleted_from_twilio', 1);				
        	} catch (Exception $e) {}
    	}
		    
		//echo $call->to;
    }


    public function search_call($driver_id, $phone_no){
    	$this->autoRender = false;
    	$driver_object = $this->PhoneQueue->query("SELECT
			U.id,
			U.type,
			Z. NAME,
			Z.type,
		U.phonecall_gps_available,
		Z2.type,
		U.phonecall_available,
			UZ.id,
			U.phonecall_zone
		FROM
			users AS U
		LEFT JOIN users_zones AS UZ ON(U.id = UZ.user_id)
		LEFT JOIN zones AS Z ON(Z.id = UZ.zone_id)
		LEFT JOIN zones AS Z2 ON(Z2.id = U.phonecall_zone)
		WHERE
			U.id = '$driver_id'
		AND U.type = 'driver'
		AND(
			(
				Z.type = 'phonecall_gps_zone'
				AND U.phonecall_gps_available = 'yes'
			)
			OR(
				Z2.type = 'phonecall_zone'
				AND U.phonecall_available = 'yes'
			)
		)");
    	if(!empty($driver_object)){
    		$phone_no = $this->_voip_check($phone_no);
    		//print_r($phone_no); exit;
    		$call_history = $this->PhoneQueue->find('all', array(
    		// 	'joins' => array(
    		// 		array(
						// 'table' => 'users',
						// 'alias' => 'User',
						// 'type' => 'LEFT',
						// 'conditions' => 'PhoneQueue.user_id = User.id'
						// ),
    		// 	),
    			'conditions' => array('PhoneQueue.from LIKE' => '%'.trim($phone_no).'%'),
    			'fields' => array('PhoneQueue.to', 'User.name', 'PhoneQueue.dial_call_duration', 'PhoneQueue.recording_url', 'PhoneQueue.created'),
    			'order'=> array('PhoneQueue.created' => 'DESC')
    		));
    		//print_r($call_history);
    		$record = array();
    		if(!empty($call_history)){
    			foreach ($call_history as $key => $c_h) {
    				$record[$key]['name'] = $call_history[$key]['User']['name'];
					$record[$key]['to'] = $call_history[$key]['PhoneQueue']['to'];
					$record[$key]['dial_call_duration'] = $call_history[$key]['PhoneQueue']['dial_call_duration'];
					$record[$key]['recording_url'] = $call_history[$key]['PhoneQueue']['recording_url'];
					$record[$key]['created'] = $call_history[$key]['PhoneQueue']['created'];
    			}
    			die(json_encode(array('success' => true, 'record' => $record)));
    		} else die(json_encode(array('success' => false, 'message' => 'No data found')));
    	}else die(json_encode(array('success' => false, 'message' => 'Invalid User')));

    }

    private function _voip_check($number){
		$pos = stripos($number, '+44'); 
		if($pos === false) {
			$number = preg_replace('/^0/', '+44', $number, 1); 
		}
		return $number;
	}


    /*
     * Delete call records queue records function after 30 days
     */

    public function admin_deleteCallRecord($period = 61){
        $this->autoRender = false;
        $this->loadModel('User');
        $user_obj = $this->User->findByCode('', array('id', 'type'));
        if(!empty($user_obj)){
            if($user_obj['User']['type'] === 'admin'){
                if(!empty($period)){
                    $this->ArchivedBooking->query("DELETE FROM phone_queues WHERE created < DATE_SUB(NOW(), INTERVAL $period day)");
                }
            }else{

            }
        }
    }


}
