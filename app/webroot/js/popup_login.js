var loginClicked = true;
$(function() {
	if (loged_in === true) {
		$('.login-link').text('Log Out');
	} else {
		$('.login-link').text('Login');
	}
	$('#loginimg').click(function() {
		var email = $.trim($('#email').val());
		var password = $('#loginpassword').val();
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if (!emailReg.test(email)) {
			alert('Invalid email');
			return;
		}
		if (password.length < 1) {
			alert('Pleaser enter password');
			return;
		}
		$.post(ROOT + 'users/passenger_login', {
			'data[User][type]': 'passenger',
			'data[User][login]': email,
			'data[User][password]': password
		},
		function(res) {
			if (res.success) {
				loged_in = true;
				$('.login-link').text('Log Out');
				$('.popup, #overlay').hide();
				if(loginClicked === false){
					alert('booking');
					createBooking();	
				}						
			} else {
				alert(res.message);
			}
		},
		'json'); 
	});
});
function logout() {
	if (loged_in === true) {
		$.get(ROOT + 'users/logout',
		function(res) {
			loged_in = false;
			$('.login-link').text('Login');
		});
	}
}