
$(function() { //create map
	var ukCenter = new google.maps.LatLng(51.501904, -0.144539);
	var myOptions = {
		zoom: 10,
		center: ukCenter,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	var map = new google.maps.Map(document.getElementById('main-map'), myOptions); //polygon
	var markerBounds = new google.maps.LatLngBounds();
	//console.log(circles);
	// draw all polygons
	$.each(polygons,
	function(i, polygonObj) {
		var polygonCoords = [];
		$.each(polygonObj.poly,
		function(j, poly) {
			var point = new google.maps.LatLng(poly.lat, poly.lng);
			polygonCoords.push(point);
			markerBounds.extend(point);
		});
		var polygon = new google.maps.Polygon({
			paths: polygonCoords,
			strokeColor: '#FF0000',
			strokeOpacity: 0.8,
			strokeWeight: 3,
			fillColor: '#FF0000',
			fillOpacity: 0.35,
			map: map
		}); //polygon.setMap(map);
		var overlayMarker = new MarkerWithLabel({
			position: new google.maps.LatLng(0, 0),
			draggable: false,
			raiseOnDrag: false,
			map: map,
			labelContent: polygonObj.name,
			labelAnchor: new google.maps.Point(30, 20),
			labelClass: "labels",
			// the CSS class for the label
			labelStyle: {
				opacity: 1.0
			},
			icon: "http://placehold.it/1x1",
			visible: false
		});
		google.maps.event.addListener(polygon, "mousemove",
		function(event) {
			overlayMarker.setPosition(event.latLng);
			overlayMarker.setVisible(true);
			$('tr#zone_' + polygonObj.id).css({
				backgroundColor : '#fba'
			});
		});
		google.maps.event.addListener(polygon, "mouseout",
		function(event) {
			overlayMarker.setVisible(false);
			$('tr#zone_' + polygonObj.id).css({
				backgroundColor : '#fff'
			});
		});
	}); // draw all circles
	/*$.each(circles,
	function(i, circle) {
		var center = new google.maps.LatLng(circle.lat, circle.lng);
		var marker = new google.maps.Marker({
			map: map,
			position: center,
			draggable: false,
			//title: 'Drag me!'
		});
		var circle = new google.maps.Circle({
			map: map,
			radius: circle.radius * 1000 // in meter
		});
		circle.bindTo('center', marker, 'position');
		markerBounds.extend(center);
	});*/
	map.fitBounds(markerBounds);
});