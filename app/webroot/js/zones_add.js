var creator = null,
circle = null,
marker = null,
map = null,
earthRadiuses = { // The radius of the earth in various units
	'mi': 3963.1676,
	'km': 6378.1,
	'ft': 20925524.9,
	'mt': 6378100,
	'in': 251106299,
	'yd': 6975174.98,
	'fa': 3487587.49,
	'na': 3443.89849,
	'ch': 317053.408,
	'rd': 1268213.63,
	'fr': 31705.3408
};
var removeAll = function() {
	if (creator !== null) {
		creator.destroy();
		creator = null;
	}
	if (circle !== null && marker !== null) {
		marker.setMap(null);
		circle.setMap(null);
	}
	$('#ZoneCenter').val('');
	$('#ZoneRadius').val('');
	$('#ZonePolygonPoints').val('');
};
function select(buttonId) {
	removeAll();
	document.getElementById("hand_b").className = "unselected";
	document.getElementById("shape_b").className = "unselected"; //document.getElementById("line_b").className = "unselected";
	//document.getElementById("placemark_b").className = "unselected";
	document.getElementById(buttonId).className = "selected";
}
$(function() { //create map
	var ukCenter = new google.maps.LatLng(51.501904, -0.144539);
	var myOptions = {
		zoom: 13,
		center: ukCenter,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	map = new google.maps.Map(document.getElementById('main-map'), myOptions); //polygon
	/////////////////////////////////////////////////////////
	var markerBounds = new google.maps.LatLngBounds();
	
	$.each(polygons,
	function(i, polygonObj) {
		var polygonCoords = [];
		$.each(polygonObj.poly,
		function(j, poly) {
			var point = new google.maps.LatLng(poly.lat, poly.lng);
			polygonCoords.push(point);
			markerBounds.extend(point);
		});
		var polygon = new google.maps.Polygon({
			paths: polygonCoords,
			strokeColor: '#666',
			strokeOpacity: 0.8,
			strokeWeight: 3,
			fillColor: '#999',
			fillOpacity: 0.35,
			map: map
		}); //polygon.setMap(map);
		var overlayMarker = new MarkerWithLabel({
			position: new google.maps.LatLng(0, 0),
			draggable: false,
			raiseOnDrag: false,
			map: map,
			labelContent: polygonObj.name,
			labelAnchor: new google.maps.Point(30, 20),
			labelClass: "labels",
			// the CSS class for the label
			labelStyle: {
				opacity: 1.0
			},
			icon: "http://placehold.it/1x1",
			visible: false
		});
		google.maps.event.addListener(polygon, "mousemove",
		function(event) {
			overlayMarker.setPosition(event.latLng);
			overlayMarker.setVisible(true);
			$('tr#zone_' + polygonObj.id).css({
				backgroundColor: '#fba'
			});
		});
		google.maps.event.addListener(polygon, "mouseout",
		function(event) {
			overlayMarker.setVisible(false);
			$('tr#zone_' + polygonObj.id).css({
				backgroundColor: '#fff'
			});
		});
	});
	map.fitBounds(markerBounds); /////////////////////////////////////////////////////
	var resetPolygon = function() {
		removeAll();
		//creator = new PolygonCreator(map);
		$('#shape_b').click();
	};
	function getData(data) {
		$('#ZonePolygonPoints').val(data[0]);
		$('#ZonePolygon').val('POLYGON((' + data[1] + '))');
	}
	$('#shape_b').click(function() {
		select('shape_b');
		removeAll();
		google.maps.event.clearListeners(map, 'click');
		creator = new PolygonCreator(map, resetPolygon, getData);
	}); //circle
	var addCircle = function(e) { // Create a draggable marker which will later on be binded to a
		removeAll(); // Circle overlay.
		var lat = e.latLng.lat(),
		lng = e.latLng.lng();
		$('#ZoneCenter').val(lat + ',' + lng);
		$('#ZoneRadius').val($('#radiusInput').val());
		marker = new google.maps.Marker({
			map: map,
			position: e.latLng,
			//new google.maps.LatLng(55, 0),
			draggable: true,
			title: 'Drag me!'
		}); // Add a Circle overlay to the map.
		circle = new google.maps.Circle({
			map: map,
			radius: parseFloat($('#radiusInput').val()) * 1000 // in meter
		}); // Since Circle and Marker both extend MVCObject, you can bind them
		// together using MVCObject's bindTo() method.  Here, we're binding
		// the Circle's center to the Marker's position.
		// http://code.google.com/apis/maps/documentation/v3/reference.html#MVCObject
		circle.bindTo('center', marker, 'position');
		/*var removeCircle = function() {
			this.setMap(null);
			marker.setMap(null);
		}*/
		google.maps.event.addListener(circle, 'rightclick', removeAll);
	};
	$('#placemark_b').click(function() {
		select('placemark_b');
		google.maps.event.addListener(map, 'click', addCircle);
	});
	$('#hand_b').click(function() {
		select('hand_b');
		google.maps.event.clearListeners(map, 'click');
	}); //form validation
	$("#ZoneAdminAddForm").submit(function() {
		if ($("#ZonePolygonPoints").val() != "" || $("#ZoneCenter").val() != "") {
			$('input[type="submit"]').removeAttr('disabled');
			return true;
		} else alert("draw a polygon or circle");
		return false;
	});


	$('#shape_b').click();
});