function createBooking() {

	var mobile = $('#mobile_no').val();
	var pickup = $('#pickup').val();
	var destination = $('input.destination').val();
	//var dest_via = $('.dest_via').val();
	var date = $('#datetext').val();
	var time = $('#timetext').val();
	var seat_no = $('.part-three input[type="checkbox"]:checked').val();
	var sap = $('#asap input[type="checkbox"]:checked').val();
	var passngr_no = $('#passngrno').val();
	var luggag_no = $('#luggage').val();
	var note = $('#addNote').val();
	var bookingdate = $("#datetext").datepicker("getDate");
	var bookingtime = $("#timetext").val();
    var branch_number = $('#branch_number').val();
    var method_1 = $('.js-method-1:checked').val();
    var method_2 = $('.js-method-2:checked').val();
    var method_3 = $('.js-method-3:checked').val();
    var driver_number = $('#method_number').val();
    var e_fare = $('#e-fare').val();
    var creator = $('#creator').val();
	if (sap != 1) sap = 0;
		if (!pickup) {
			alert('Please enter a valid pick up point');
			return;
		}
		if (!destination) {
			alert('Please enter a valid destination');
			return;
		}
		if ((!bookingdate || !bookingtime) && sap == 0) {
			alert('Please enter your pick up datetime or checked soon as possible');
			return;
		}
		if (!seat_no) {
			alert('Please select the number of seats required');
			return;
		}
		if (!passngr_no) {
			alert('Please select the number of passengers');
			return;
		}
		if (!luggag_no) {
			alert('Please select the number of luggage');
			return;
		}	
		if (loged_in == false) {
			$(':input', '.popup').val('');
			$('.popup, #overlay').show();
			return;
		} 
			//$('#overlay, .wait-message').show();

		if(sap === 0){
			var bookingtime_parts = bookingtime.split(' ');
			var bookingtime_time_parts = bookingtime_parts[0].split(':');
			var h = parseInt(bookingtime_time_parts[0]);
			h = bookingtime_parts[1] == 'AM' ? h: h + 12;
			h = h < 10 ? ('0' + h) : h;
			var m = parseInt(bookingtime_time_parts[1]);
			m = m < 10 ? ('0' + m) : m;
			bookingdate = bookingdate.getFullYear() + '-' + (bookingdate.getMonth() + 1) + '-' + bookingdate.getDate() + ' ' + h + ':' + m + ':00';
		} else {
			var now = new Date(),
			gmt_offset = now.getTime() - now.getTimezoneOffset() * 60 * 1000,
			gmt_date = new Date(gmt_offset);
			var month = gmt_date.getMonth() + 1;
			month = month < 10 ? ('0' + month) : month;
			var date = gmt_date.getDate();
			date = date < 10 ? ('0' + date) : date;
			var hours = gmt_date.getHours();
			hours = hours < 10 ? ('0' + hours) : hours;
			var minutes = gmt_date.getMinutes();
			minutes = minutes < 10 ? ('0' + minutes) : minutes;		
			bookingdate = gmt_date.getFullYear() + '-' + month + '-' + date + ' ' + hours + ':' + minutes + ':00';
		}
		

	var data = {
		'data[Booking][mobile]': mobile,
        'data[Booking][branch_number]': branch_number,
		'data[Booking][pick_up]': pickup,
		'data[Booking][destination]': destination,
		'data[Booking][at_time]': bookingdate,
		'data[Booking][persons]': passngr_no,
		'data[Booking][car_type]': seat_no,
		'data[Booking][sap]': sap,
		'data[Booking][no_of_luggages]': luggag_no,
		'data[Booking][note_to_driver]': note,
		'data[Booking][from_web]': '1',
		'data[Booking][pick_up_postcode]': pick_up_postcode,
		'data[Booking][destination_postcode]': destination_postcode,
        'data[Booking][driver_number]': driver_number,
        'data[Booking][method_1]': method_1,
        'data[Booking][method_2]': method_2,
        'data[Booking][method_3]': method_3,
        'data[Booking][fare]' : e_fare,
        'data[Booking][creator]' : creator
	};
    //console.log(data)


	if ($('#via input').length) {
		$('#via input').each(function(i, j) {
			var viaVal = $.trim($(this).val());
			if(viaVal.length)
				data['data[Booking][dest_via][' + i + ']'] = viaVal;
		});
	}
	//console.log(data);
	$.post(ROOT + 'bookings/book_a_taxi', data,
	function(res) {
		//console.log(res);
		if (res.success) {
//			$(':input:not(:checkbox)', '.bg-color').val('');
//			$('input[type="checkbox"]:checked').prop('checked', false).prev().hide().prev().show();
//			$('#overlay, .wait-message').hide();
//			$('#via').empty();
			alert('Your booking has been submitted');
		} else if (res.message == 'There is a problem with your pick up point or destination, please check and try again') {
			$('#overlay, .wait-message').hide();
			alert(res.message);
		}
	}, 'json');
}

$(function() {
	$('.buy-now').click(function(e) {
		e.preventDefault();
		//$('.another-des').hide();
		$( "#datetext" ).datepicker('enable');	
		$('#timetext').prop('disabled', false);  
		createBooking();		
	});
});

