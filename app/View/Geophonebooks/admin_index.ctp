<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<!-- <div class="box-header">
					<h3 class="box-title">Users</h3>
				</div> -->
				<div class="box-body">
					<div class="geophonebooks index">
<<<<<<< HEAD
                        <div>
                            <div class="col-md-9"><h2><?php echo __('Geophonebooks'); ?></h2></div>
						<div class="AddNewButton col-md-3"><?php echo $this->Html->link('Add new Geo Phone Book', array('controller' => 'geophonebooks', 'action' => 'add', 'admin' => true, 'prefix' => 'admin'), ['class'=> 'btn btn-warning btn-lg'])?></div>
                        </div>

=======
                        <div class="col-md-10"><h2><?php echo __('Geophonebooks'); ?></h2></div>
                        <div class="col-md-2">
                            <div class="AddNewButton"><?php echo $this->Html->link('Add new Geo Phone Book '. $types[$type], array('controller' => 'geophonebooks', 'action' => 'add', 'admin' => true, 'prefix' => 'admin', $type), ['class'=> 'btn btn-warning btn-lg'])?></div>
                        </div>
>>>>>>> de146323a6d51f88a48cf59803ef4e8d36a49413
						<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
							<tr>
								<th><?php echo $this->Paginator->sort('address'); ?></th>
								<th><?php echo $this->Paginator->sort('phone','Phone Number'); ?></th>
								<th class="actions"><?php echo __('Action'); ?></th>
							</tr>
							<?php foreach ($geophonebooks as $geophonebook): ?>
								<tr>
									<td><?php echo h($geophonebook['Geophonebook']['address']); ?>&nbsp;</td>

									<td><?php echo h($geophonebook['Geophonebook']['phone']); ?>&nbsp;</td>
									<td class="actions">

										<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $geophonebook['Geophonebook']['id']), array('class' => 'btn btn-info')); ?>
										<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $geophonebook['Geophonebook']['id']), array('class' => 'btn btn-danger'), __('Are you sure you want to delete %s?', $geophonebook['Geophonebook']['address'])); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</table>
						<p class="pull-right">
							<?php
							echo $this->Paginator->counter(array(
								'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
							));
							?>	</p>
							<div class="clearfix"></div>
							<div class="paging">
								<ul class="pagination pagination-sm no-margin pull-right">
									<?php
									echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
									echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
									echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
									?>
								</ul>

							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->


				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
