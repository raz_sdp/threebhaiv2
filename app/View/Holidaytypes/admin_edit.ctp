<?php
echo $this->Html->script(array('add_holidays','jquery-ui'));
echo $this->Html->css(array('jquery-ui', 'style-ui'));
?>

    <div class="wrapper">
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo __('Edit Holiday'); ?></h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->


                            <div class="box-body">
                                <?php echo $this->Form->create('Holidaytype', array('type' => 'file',"data-toggle"=>"validator", "role" =>"form")); ?>
                                <div class="form-group has-feedback">
                                    <?php  echo $this->Form->input('holiday_type',array(
                                        'class'=>'form-control',
                                        'required'=>'required',
                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    )); ?>
                                </div>
                                <?php
                                    foreach ($this->request->data['Holiday'] as $holiday) {
                                        // pr($holiday);die;
                                        ?>
                                        <div class="form-group has-feedback">
                                        <?php echo $this->Form->input('holiday',array('label' =>'Date',
                                            'name' => 'data[Holiday][holiday][]',
                                            'id' => false,
                                            'required'=>'required',
                                            'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>',
                                            'value' => date_format(date_create($holiday['holiday']), 'd/m/Y'), 'class' => 'form-control datePicker'));
                                        if(!empty($this->request->data['Holidaytype']['is_repeat'])) break;
                                    }
                                ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $this->Form->input('Add More Dates',array('type' => 'button', 'label' => false, 'value' =>'Add More Date', 'id' => 'AddMoreDates'));?>

                                </div>
                                <div class="form-group">
                                    <?php echo $this->Form->input('is_repeat',array('type' => 'checkbox', 'label' => 'Repeat Every Year'));?>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-primary btn-block">Update</button>
                                    </div>
                                </div>
                                </from>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>






<?php
/*	echo $this->Html->script(array('add_holidays','jquery-ui'));
	echo $this->Html->css(array('jquery-ui', 'style-ui'));
*/?><!--
<div class="holidaytypes form">
<?php /*echo $this->Form->create('Holidaytype'); */?>
	<fieldset>
		<legend><?php /*echo __('Edit Holiday'); */?></legend>
	<?php
/*		echo $this->Form->input('id');
		echo $this->Form->input('holiday_type');
		foreach ($this->request->data['Holiday'] as $holiday) {
			echo $this->Form->input('holiday',array('label' =>'Date', 'name' => 'data[Holiday][holiday][]', 'id' => false, 'class' => 'datePicker', 'value' => date_format(date_create($holiday['holiday']), 'd/m/Y'),'style' => 'width:175px; height:30px; padding:0'));

			if(!empty($this->request->data['Holidaytype']['is_repeat'])) break;
		}
		echo $this->Form->input('Add More Dates',array('type' => 'button', 'label' => false, 'value' =>'Add More Date', 'id' => 'AddMoreDates'));
	
		echo $this->Form->input('is_repeat',array('type' => 'checkbox', 'label' => 'Repeat Every Year'));
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
--><?php /*echo $this->element('menu'); */?>