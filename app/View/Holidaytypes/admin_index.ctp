<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Holidays</h3>
                        <button class="btn btn-warning btn-lg pull-right"><?php echo $this->Html->link('Add new Holiday', array('controller' => 'holidaytypes', 'action' => 'add', 'admin' => true, 'prefix' => 'admin'))?></button>
                    </div>

                    <div class="box-body">
					<div class="holidaytypes index">

                        <div class="table-responsive">
						<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
							<tr>
								<th><?php echo $this->Paginator->sort('holiday_type'); ?></th>
								<th><?php echo $this->Paginator->sort('holiday','Date'); ?></th>
								<th><?php echo $this->Paginator->sort('is_repeat','Repeat Every Year'); ?></th>
								<th class="actions"><?php echo __(''); ?></th>
							</tr>

                            <?php  //pr($holidaytypes);die;?>
							<?php foreach ($holidaytypes as $holidaytype): ?>
                                <?php  //pr($holidaytype);die;?>
                              <?php  //print_r($this->request->data);?>
								<tr>
									<td><?php echo h($holidaytype['Holidaytype']['holiday_type']); ?>&nbsp;</td>
									<td>
										<?php

                                        if(empty($holidaytype['Holidaytype']['is_repeat'])) {
											$dates = array();
											foreach ($holidaytype['Holiday'] as $holiday) {
												$dates[] = date('d/m/Y', strtotime($holiday['holiday']));
												//$dates[] = date_format(date_create($holiday['holiday']), 'd/m/Y');
											}
											echo implode(', ', $dates);
										} else {
                                            //pr(date_format(date_create($holidaytype['Holiday'][0]['holiday']),'d/m'));die;
											echo !empty($holidaytype['Holiday']) ? date_format(date_create(@$holidaytype['Holiday'][0]['holiday']),'d/m') : '-';
										}
										?>
									</td>
									<td>
										<?php
										if(!empty($holidaytype['Holidaytype']['is_repeat'])) {
											echo h('Yes');
										} else {
											echo h('No');
										}
										?>
									</td>
									<td class="actions">
										<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $holidaytype['Holidaytype']['id']), ['class' => 'btn btn-info']); ?>
										<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $holidaytype['Holidaytype']['id']), ['class' => 'btn btn-danger'], __('Are you sure you want to delete %s?', $holidaytype['Holidaytype']['holiday_type'])); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</table>
                            </div>
						<p class="pull-right">
							<?php
							echo $this->Paginator->counter(array(
								'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
							));
							?>	</p>
							<div class="clearfix"></div>
							<div class="paging">
								<ul class="pagination pagination-sm no-margin pull-right">
									<?php
									echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
									echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
									echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
									?>
								</ul>
							</div>
						</div>


					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->


				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
