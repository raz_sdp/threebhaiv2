<style>
    form div.checkbox {
        float:;
        clear:;
    }

    .checkbox {
        width: 20%;
        float: left;
    }

</style>

<?php
echo $this->Html->script('booking_holidays', array('inline'=>false));
?>

<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Edit VR Zone Setting'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->


                        <div class="box-body">
                            <?php echo $this->Form->create('VrSetting', array('type' => 'file',"data-toggle"=>"validator", "role" =>"form")); ?>
                            <div class="form-group has-feedback">
                                <?php  echo $this->Form->input('zone_id', array(
                                    'label' => 'VR Zone',
                                    'class'=>'form-control',
                                    'required'=>'required',
                                    'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                ));; ?>

                            </div>
                            <div class="form-group">
                                <?php  echo $this->Form->input('holidaytype_id', array(
                                    'label' => 'Holiday Type',
                                    'class'=>'form-control',
                                    'empty' => '(Not A Holiday)',
                                    'required'=>'' ))	;?>
                            </div>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('start_date',array(
                                    'dateFormat'=>'DMY',
                                    'minYear'=>date('Y')+20,
                                    'maxYear'=>date('Y')-1,
                                    'required'=>'required',
                                    'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                ))
                                ;?>

                            </div>
                            <div class="form-group has-feedback">
                                <?php   echo $this->Form->input('end_date',array(
                                    'dateFormat'=>'DMY',
                                    'minYear'=>date('Y')+20,
                                    'required'=>'required',
                                    'maxYear'=>date('Y')-1,
                                    'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                ))
                                ;?>
                            </div>
                            <div class="form-group hsa-feedback ">
                                <?php echo $this->Form->input('start_time',array(
                                    'required'=>'required',
                                    'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'

                                ));?>
                            </div>
                            <div class="form-group col-md-12">
                                <?php
                                echo $this->Form->input('end_time');
                                $weekdays =  array('0' => 'Sunday', '1'=> 'Monday','2' => 'Tuesday','3' => 'Wednesday','4' => 'Thrusday','5' => 'Friday','6' => 'Saturday') ;?>
                                <?php
                                if($this->request->data['VrSetting']['week_no'] != Null) {
                                    $day = trim($this->request->data['VrSetting']['week_no'],'#');
                                    $wks = explode('#', $day);

                                    ?>
                            <?php foreach($weekdays as $key => $weekday) { ?>
                                <label class="checkbox-inline days">
                                    <input name="data[VrSetting][week_no][]" type="checkbox" value="<?php echo $key ?>" <?php echo in_array($key,$wks) ? 'checked':'' ?>> <?php echo $weekday?>
                                </label>
                               <?php } } else {
                                    echo $this->Form->input('week_no', array('label'=> false, 'multiple' => 'checkbox','options' => $weekdays, 'div' => false));
                                } ?>

                            </div>


                            <div class="form-group has-feedback">
                                <?php
                                echo $this->Form->input('over_job_amount', array(
                                    'label' => 'Over Job Amount (If empty, no email will be sent)',
                                    'class'=>'form-control',
                                    'required'=>'required',
                                    'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                ));
                                ?>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                            </from>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>









<!--
<style>
   form .days div.checkbox{
        float: left;
        clear: none;
    }
</style>
<?php /*
   echo $this->Html->script('booking_holidays', array('inline'=>false)); 
*/?>
<div class="vrSettings form">
   <?php /*echo $this->Form->create('VrSetting'); */?>
        <fieldset>
            <legend><?php /*echo __( 'Admin Edit VR Zone Setting'); */?></legend>
        <?php /*echo $this->Form->input('id');
            echo $this->Form->input('zone_id', array('label' => 'VR Zone'));
            echo $this->Form->input('holidaytype_id', array('label' => 'Holiday Type','empty' => '(Not A Holiday)')); 
            echo $this->Form->input('start_date',array(
                'dateFormat'=>'DMY',
                'minYear'=>date('Y')+20,
                'maxYear'=>date('Y')-1,
                ));
            echo $this->Form->input('end_date',array(
                'dateFormat'=>'DMY',
                'minYear'=>date('Y')+20,
                'maxYear'=>date('Y')-1,
            ));
            echo $this->Form->input('start_time');
            echo $this->Form->input('end_time'); 
            $weekdays = array('0' => 'Sunday','1' => 'Monday','2' => 'Tuesday','3' => 'Wednesday','4' => 'Thrusday','5'=> 'Friday','6' => 'Saturday'); 
            */?>
            <div class='days'>
            <?php /*
                if($this->request->data['VrSetting']['week_no'] != Null) { 
                    $day = trim($this->request->data['VrSetting']['week_no'],'#');
                    $wks = explode('#', $day); 
                    echo $this->Form->input('week_no', array('label' => false ,'multiple' => 'checkbox', 'selected' => $wks ,'options' => $weekdays, 'div' => false)); 
                } else { 
                    echo $this->Form->input('week_no', array('label'=> false, 'multiple' => 'checkbox','options' => $weekdays, 'div' => false));
                } */?>
                </div>
                <?php /* echo $this->Form->input('is_advance_booking_on'); */?>
                <?php /*// echo $this->Form->input('min_advance_booking_time'); */?>
                <?php /*// echo $this->Form->input('is_over_job_notification'); */?>
                <?php /*echo $this->Form->input('over_job_amount', array('label' => 'Over Job Amount (If empty, no email will be sent)')); */?>
            </fieldset>
            <?php /*echo $this->Form->end(__('Submit')); */?>
</div>
--><?php /*echo $this->element('menu'); */?>