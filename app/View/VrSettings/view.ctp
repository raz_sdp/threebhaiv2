<div class="bookingSettings view">
<h2><?php  echo __('Booking Setting'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($bookingSetting['BookingSetting']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($bookingSetting['User']['name'], array('controller' => 'users', 'action' => 'view', $bookingSetting['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Min Advance Booking Time'); ?></dt>
		<dd>
			<?php echo h($bookingSetting['BookingSetting']['min_advance_booking_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Advance Booking On'); ?></dt>
		<dd>
			<?php echo h($bookingSetting['BookingSetting']['is_advance_booking_on']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Week No'); ?></dt>
		<dd>
			<?php echo h($bookingSetting['BookingSetting']['week_no']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Date'); ?></dt>
		<dd>
			<?php echo h($bookingSetting['BookingSetting']['start_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End Date'); ?></dt>
		<dd>
			<?php echo h($bookingSetting['BookingSetting']['end_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zones'); ?></dt>
		<dd>
			<?php echo $this->Html->link($bookingSetting['Zones']['id'], array('controller' => 'zones', 'action' => 'view', $bookingSetting['Zones']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Over Job Notification'); ?></dt>
		<dd>
			<?php echo h($bookingSetting['BookingSetting']['is_over_job_notification']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Over Job Amount'); ?></dt>
		<dd>
			<?php echo h($bookingSetting['BookingSetting']['over_job_amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Promoter Percentage'); ?></dt>
		<dd>
			<?php echo h($bookingSetting['BookingSetting']['promoter_percentage']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Repeat Call Duration'); ?></dt>
		<dd>
			<?php echo h($bookingSetting['BookingSetting']['repeat_call_duration']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($bookingSetting['BookingSetting']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($bookingSetting['BookingSetting']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Booking Setting'), array('action' => 'edit', $bookingSetting['BookingSetting']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Booking Setting'), array('action' => 'delete', $bookingSetting['BookingSetting']['id']), null, __('Are you sure you want to delete # %s?', $bookingSetting['BookingSetting']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Booking Settings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Booking Setting'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Zones'), array('controller' => 'zones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Zones'), array('controller' => 'zones', 'action' => 'add')); ?> </li>
	</ul>
</div>
