<div class="bookingSettings form">
<?php echo $this->Form->create('BookingSetting'); ?>
	<fieldset>
		<legend><?php echo __('Edit Booking Setting'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('min_advance_booking_time');
		echo $this->Form->input('is_advance_booking_on');
		echo $this->Form->input('week_no');
		echo $this->Form->input('start_date');
		echo $this->Form->input('end_date');
		echo $this->Form->input('zone_ids');
		echo $this->Form->input('is_over_job_notification');
		echo $this->Form->input('over_job_amount');
		echo $this->Form->input('promoter_percentage');
		echo $this->Form->input('repeat_call_duration');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('BookingSetting.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('BookingSetting.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Booking Settings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Zones'), array('controller' => 'zones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Zones'), array('controller' => 'zones', 'action' => 'add')); ?> </li>
	</ul>
</div>
