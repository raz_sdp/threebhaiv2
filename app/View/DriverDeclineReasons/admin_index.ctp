<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Driver Decline Reasons</h3>
                        <button class="btn btn-warning btn-lg pull-right"><?php echo $this->Html->link('Add Driver Decline Reason', array('controller' => 'driver_decline_reasons', 'action' => 'add', 'admin' => true, 'prefix' => 'admin')) ?></button>
                    </div>

                    <div class="box-body">
                        <div class="driverDeclineReasons index">

                            <div class="table-responsive">
                            <table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
                                <tr>
                                    <th><?php echo $this->Paginator->sort('reason'); ?></th>
                                    <th><?php echo $this->Paginator->sort('is_active', 'Active'); ?></th>
                                    <th class="actions"><?php echo __(''); ?></th>
                                </tr>
                                <?php foreach ($driverDeclineReasons as $driverDeclineReason): ?>
                                    <tr>
                                        <td><?php echo h($driverDeclineReason['DriverDeclineReason']['reason']); ?>
                                            &nbsp;</td>
                                        <td><?php
                                            if ($driverDeclineReason['DriverDeclineReason']['is_active'] == '1') {
                                                echo 'Yes';
                                            } else echo 'No';
                                            ?>
                                        </td>
                                        <td class="actions">
                                            <?php
                                            if ($driverDeclineReason['DriverDeclineReason']['is_active'] == 0) {
                                                echo $this->Html->link(__('Active'), array('action' => 'active_reason', $driverDeclineReason['DriverDeclineReason']['id']), array('div' => false, 'class' => 'e_d btn btn-primary', 'style="padding: 2px 13px;"'));
                                            } else {
                                                echo $this->Html->link(__('Deactive'), array('action' => 'deactive_reason', $driverDeclineReason['DriverDeclineReason']['id']), array('div' => false, 'class' => 'd_d btn btn-default'));
                                            }
                                            ?>
                                            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $driverDeclineReason['DriverDeclineReason']['id']), ['class' => 'btn btn-info']); ?>
                                            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $driverDeclineReason['DriverDeclineReason']['id']), ['class' => 'btn btn-danger'], __('Are you sure you want to delete : %s?', $driverDeclineReason['DriverDeclineReason']['reason'])); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                            </div>
                            <p class="pull-right">
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?>    </p>

                            <div class="clearfix"></div>
                            <div class="paging">
                                <ul class="pagination pagination-sm no-margin pull-right">
                                    <?php
                                    echo "<li>" . $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) . "</li>";
                                    echo "<li>" . $this->Paginator->numbers(array('separator' => '')) . "</li>";
                                    echo "<li>" . $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) . "</li>";
                                    ?>
                                </ul>
                            </div>
                        </div>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
