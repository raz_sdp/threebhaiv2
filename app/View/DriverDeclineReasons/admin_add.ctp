<style>
    .checkbox{
        margin-left: 0px;
    }


</style>>

<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Add Driver Decline Reason'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->


                        <div class="box-body">
                            <?php echo $this->Form->create('DriverDeclineReason', array('type' => 'file')); ?>
                            <div class="form-group">
                                <?php  echo $this->Form->input('reason',array('class'=>'form-control')); ?>

                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                <?php 	echo $this->Form->input('is_active',array( 'class' => ''));?>
                            </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                            </from>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>












<!--
<div class="driverDeclineReasons form">
<?php /*echo $this->Form->create('DriverDeclineReason'); */?>
	<fieldset>
		<legend><?php /*echo __('Add Driver Decline Reason'); */?></legend>
	<?php
/*		echo $this->Form->input('reason');
		echo $this->Form->input('is_active');
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
--><?php /*echo $this->element('menu'); */?>
