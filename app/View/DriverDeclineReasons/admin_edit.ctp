<style>

    .checkbox {
        width: 20%;
        float: left;
    }

</style>

<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Edit Driver Decline Reason'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->


                        <div class="box-body">
                            <?php echo $this->Form->create('DriverDeclineReason', array('type' => 'file')); ?>
                            <?php echo $this->Form->input('id');?>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('reason', array('label' => 'reason','class'=>'form-control')); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('is_active', array('label' => 'Active','class'=>'checkbox')); ?>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                            </form>


                        </div>
                    </div>
                </div>
        </section>

    </div>
</div>
<!---->
<!---->
<!---->
<!---->
<!---->
<!---->
<!---->
<!--<div class="driverDeclineReasons form">-->
<?php //echo $this->Form->create('DriverDeclineReason'); ?>
<!--	<fieldset>-->
<!--		<legend>--><?php //echo __('Edit Driver Decline Reason'); ?><!--</legend>-->
<!--	--><?php
//		echo $this->Form->input('id');
//		echo $this->Form->input('reason');
//		echo $this->Form->input('is_active');
//	?>
<!--	</fieldset>-->
<?php //echo $this->Form->end(__('Submit')); ?>
<!--</div>-->
<?php //echo $this->element('menu'); ?>
