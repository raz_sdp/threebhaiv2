<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo 'ThreevhaiV2';//$title_for_layout; ?>
	</title>
	<!--[if lt IE 9]>
	<?php
		echo $this->Html->script(array('html5shiv', 'respond.min'));
	?>
    <![endif]-->
    <!--[if IE]>
    <?php
		 echo $this->Html->css(array('ie'));
	?>
    <![endif]-->

	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->script(array(
            'jQuery-2.2.0.min',
			'jquery-ui',
            'bootstrap.min',
            'bootstrap-timepicker',
			'parallax/jquery.parallax-1.1.3.js',
			'parallax/jquery.localscroll-1.2.7-min',
			'parallax/jquery.scrollTo-1.4.2-min',
			'jScrollPane/jquery.jscrollpane.min',
			'jScrollPane/jquery.mousewheel',
			'website',
			'validator.min',
        ));
		echo $this->Html->css(array(
            'bootstrap.min',
            'bootstrap-theme.min',
            'bootstrap-timepicker.min',
			'jquery.jscrollpane',
            'jquery-ui',
            'style'
            )
        );

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<script>
	var ROOT = '<?php echo $this->Html->url('/', true); ?>';
    var HERE = '<?php echo $this->here; ?>';
	</script>
	

    <meta http-equiv="X-UA-Compatible" content="IE=edge">  
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>
	<?php echo $this->Session->flash(); ?>

	<?php echo $this->fetch('content'); ?>

	<?php echo $this->element('sql_dump'); ?>
</body>
</html>