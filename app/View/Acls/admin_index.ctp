<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<!-- <div class="box-header">
						<h3 class="box-title">Users</h3>
					</div> -->
					<div class="box-body">
						<div class="acls index">
							<h2><?php echo __('Acls'); ?></h2>
                            <div class="table-responsive">
							<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th><?php echo $this->Paginator->sort('id'); ?></th>
										<th><?php echo $this->Paginator->sort('permission_name'); ?></th>
										<th><?php echo $this->Paginator->sort('permission_key'); ?></th>
										<th><?php echo $this->Paginator->sort('orderid'); ?></th>
										<th><?php echo $this->Paginator->sort('parent'); ?></th>
										<th class="actions"><?php echo __('Actions'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($acls as $acl): ?>
										<tr>
											<td><?php echo h($acl['Acl']['id']); ?>&nbsp;</td>
											<td><?php echo h($acl['Acl']['permission_name']); ?>&nbsp;</td>
											<td><?php echo h($acl['Acl']['permission_key']); ?>&nbsp;</td>
											<td><?php echo h($acl['Acl']['orderid']); ?>&nbsp;</td>
											<td><?php echo h($acl['Acl']['parent']); ?>&nbsp;</td>
											<td class="actions">
<!--												--><?php //echo $this->Html->link(__('View'), array('action' => 'view', $acl['Acl']['id'])); ?>
												<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $acl['Acl']['id']), ['class' => 'btn btn-info']); ?>
<!--												--><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $acl['Acl']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $acl['Acl']['id']))); ?>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
                                </div>
							<p class="pull-right">
								<?php
								echo $this->Paginator->counter(array(
									'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
								));
								?>	</p>
								<div class="clearfix"></div>
								<div class="paging">
									<ul class="pagination pagination-sm no-margin pull-right">
										<?php
										echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
										echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
										echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
										?>
									</ul>

								</div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->


					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>


