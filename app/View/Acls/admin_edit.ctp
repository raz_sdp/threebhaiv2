
<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Edit ACL'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->


                        <div class="box-body">
                            <?php echo $this->Form->create('Acl', array('type' => 'text')); ?>
                            <?php echo $this->Form->input('id');?>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('permission_name', array('label' => 'Permission Name','class'=>'form-control')); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('permission_key', array('label' => 'Permission Key','class'=>'form-control','disabled')); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('orderid', array('label' => 'Order','class'=>'form-control')); ?>
                            </div>
                            <div class="col-md-12 form-group ">
                                <?php echo $this->Form->input('parent', array('label' => 'Is Menu','class'=>'checkbox checkboxmargin')); ?>
                            </div>
<!--	--><?php
//		echo $this->Form->input('id');
//		echo $this->Form->input('orderid');
//		echo $this->Form->input('parent');
//		echo $this->Form->input('Role');
//	?>
<!--	</fieldset>-->
                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                            </form>


                        </div>
                    </div>
                </div>
        </section>

    </div>
</div>

