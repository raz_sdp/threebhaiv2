<div class="acls form">
<?php echo $this->Form->create('Acl'); ?>
	<fieldset>
		<legend><?php echo __('Add Acl'); ?></legend>
	<?php
		echo $this->Form->input('permission_name');
		echo $this->Form->input('permission_key');
		echo $this->Form->input('orderid');
		echo $this->Form->input('parent');
		echo $this->Form->input('Role');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Acls'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Roles'), array('controller' => 'roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('controller' => 'roles', 'action' => 'add')); ?> </li>
	</ul>
</div>
