<div class="acls view">
<h2><?php echo __('Acl'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($acl['Acl']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Permission Name'); ?></dt>
		<dd>
			<?php echo h($acl['Acl']['permission_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Permission Key'); ?></dt>
		<dd>
			<?php echo h($acl['Acl']['permission_key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Orderid'); ?></dt>
		<dd>
			<?php echo h($acl['Acl']['orderid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Parent'); ?></dt>
		<dd>
			<?php echo h($acl['Acl']['parent']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Acl'), array('action' => 'edit', $acl['Acl']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Acl'), array('action' => 'delete', $acl['Acl']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $acl['Acl']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Acls'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Acl'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles'), array('controller' => 'roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('controller' => 'roles', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Roles'); ?></h3>
	<?php if (!empty($acl['Role'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Role Name'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($acl['Role'] as $role): ?>
		<tr>
			<td><?php echo $role['id']; ?></td>
			<td><?php echo $role['role_name']; ?></td>
			<td><?php echo $role['status']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'roles', 'action' => 'view', $role['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'roles', 'action' => 'edit', $role['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'roles', 'action' => 'delete', $role['id']), array('confirm' => __('Are you sure you want to delete # %s?', $role['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Role'), array('controller' => 'roles', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
