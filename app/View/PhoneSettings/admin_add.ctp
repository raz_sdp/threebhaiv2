<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add Phonecall and Phone GPS Zones Settings'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('PhoneSetting', array('type' => 'file', "data-toggle"=>"validator", "role" =>"form")); ?>
                        <div class="box-body">
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('zone_id', array('options' => $zones, 'label' => 'Phonecall and Phone GPS Zones', 'class' => 'form-control', 'required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>

                            </div>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('req_timeout', array('label' => 'Try Each Logged In Driver for Number of Seconds', 'class' => 'form-control', 'required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>

                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('remain_loggedin_on_accept', array('label' => 'After Accepting a Job Leave Drivers Logged IN','class' => 'checkboxmargin', 'checked' => true)); ?>
                            </div>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('reject_threshold', array('label' => 'Log Drivers OUT after number of Ignore or Reject Jobs', 'class' => 'form-control','required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
                            </div>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('allow_inactive_time', array('label' => 'Allowed Inactive Time', 'class' => 'form-control','required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
                            </div>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('min_call_duration', array('label' => 'Ignore Calls Shorter then Numer of Seconds', 'class' => 'form-control','required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
                            </div>

                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('minimum_charge', array('label' => 'Minimum Charge', 'class' => 'form-control','required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
                            </div>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('per_minute_charge', array('label' => 'Per Minute Charge', 'class' => 'form-control','required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
                            </div>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('call_cut_off_time', array('label' => 'Cut Calls off If Answered Within Milliseconds of Connection', 'class' => 'form-control','required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
                            </div>

                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('repeat_call_timeout', array('label' => 'Route Repeat Calls to the Same Driver Within', 'class' => 'form-control','required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>

                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('bar_private_no', array('label' => 'Bar all Incoming Private & Withheld Numbers')); ?>
                            </div>
                            <strong>Try Alternate Zones When No One is Available to Take Calls</strong>
                            <?php
                            foreach ($zones as $zone) {
                                ?>
                                <div class="form-group">
                                    <?php echo $this->Form->input('restriction', array('hiddenField' => false, 'name' => 'data[PhoneSetting][restriction][]', 'label' => false, 'type' => 'select', 'options' => $zones, 'empty' => '(No zone)', 'class' => 'form-control')); ?>
                                </div>
                            <?php
                            }
                            ?>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('divert_phoneno', array('label' => 'Divert the call', 'class' => 'form-control','required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                            </from>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
