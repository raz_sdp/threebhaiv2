<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add Phone Queue'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->


                        <div class="box-body">
                            <?php echo $this->Form->create('PhoneQueue', array('type' => 'file')); ?>
                            <div class="form-group">
                                <?php  	echo $this->Form->input('call_sid',array('class' => 'form-control')); ?>

                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('zone_id',array('class' => 'form-control')) ;?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('user_id',array('class' => 'form-control')) ;?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('request_time',array('class' => ''));?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('response_time',array('class' => '')) ; ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('response',array('class' => 'form-control')) ;?>
                            </div>

                            <div class="form-group">
                                <?php echo $this->Form->input('user_comment',array('class' => 'form-control')) ;?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('driver_decline_reason_id',array('class' => 'form-control')) ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('is_reactivated',array('class' => 'form-control')) ?>
                            </div>

                            <div class="form-group">
                                <?php echo $this->Form->input('pob_time',array('class' => 'form-control')) ; ?>

                            </div>
                            <div class="form-group">
                                <?php  echo $this->Form->input('ondoor_time',array('class' => 'form-control'))?>
                            </div>

                            <div class="form-group">
                                <?php  echo $this->Form->input('no_show_time',array('class' => 'form-control'))?>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                            </from>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>













<!--
<div class="phoneQueues form">
<?php /*echo $this->Form->create('PhoneQueue'); */?>
	<fieldset>
		<legend><?php /*echo __('Admin Add Phone Queue'); */?></legend>
	<?php
/*		echo $this->Form->input('call_sid');
		echo $this->Form->input('zone_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('request_time');
		echo $this->Form->input('response_time');
		echo $this->Form->input('response');
		echo $this->Form->input('user_comment');
		echo $this->Form->input('driver_decline_reason_id');
		echo $this->Form->input('is_reactivated');
		echo $this->Form->input('pob_time');
		echo $this->Form->input('ondoor_time');
		echo $this->Form->input('no_show_time');
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>

		<li><?php /*echo $this->Html->link(__('List Phone Queues'), array('action' => 'index')); */?></li>
	</ul>
</div>
-->