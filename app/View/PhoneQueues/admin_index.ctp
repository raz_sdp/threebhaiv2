<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Call Logs</h3>
                    </div>

                    <div class="box-body">
					<div class="phoneQueues index">

                        <div class="table-responsive">
						<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
							<tr>
								<th><?php echo $this->Paginator->sort('call_sid'); ?></th>
								<th><?php echo $this->Paginator->sort('from'); ?></th>
								<th><?php echo $this->Paginator->sort('to'); ?></th>
								<th><?php echo $this->Paginator->sort('created', 'Request Time'); ?></th>
								<th class="actions"><?php echo __(''); ?></th>
							</tr>
							<?php //pr($phoneQueues); ?>
							<?php foreach ($phoneQueues as $phoneQueue): ?>
								<tr>
									<td><?php echo h($phoneQueue['PhoneQueue']['call_sid']); ?>&nbsp;</td>
									<td><?php echo h($phoneQueue['PhoneQueue']['from']); ?>&nbsp;</td>
									<td><?php echo h($phoneQueue['PhoneQueue']['to']); ?>&nbsp;</td>
									<td><?php
									echo date_format(date_create($phoneQueue['PhoneQueue']['created']), 'd/m/Y g:i A');
									?>&nbsp;</td>
									<td class="actions">
										<?php echo $this->Html->link(__('Queue'), array('action' => 'view', $phoneQueue['PhoneQueue']['call_sid']),['class' => 'btn btn-info']); ?>
										<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $phoneQueue['PhoneQueue']['id']), ['class' => 'btn btn-danger'], __('Are you sure you want to delete # %s?', $phoneQueue['PhoneQueue']['call_sid'])); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</table>
                            </div>
						<p class="pull-right">
							<?php
							echo $this->Paginator->counter(array(
								'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
							));
							?>	</p>
							<div class="clearfix"></div>
							<div class="paging">
								<ul class="pagination pagination-sm no-margin pull-right">
									<?php
									echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
									echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
									echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
									?>
								</ul>

							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->


				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
