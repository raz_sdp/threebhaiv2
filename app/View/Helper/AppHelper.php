<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Helper', 'View');
App::uses('Acl', 'Model');
App::uses('User', 'Model');
App::uses('Wallet', 'Model');
App::uses('Phonebook', 'Model');
App::uses('Charge', 'Model');
App::uses('UsersAcl', 'Model');
/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {
//    public static function getMenus(){
//        $role = AuthComponent::user('type');
//        $obj = new Acl();
//        $query = "SELECT A.permission_name, A.permission_key FROM acls as A INNER JOIN aclsmanage as AM ON AM.acl_id = A.id WHERE A.parent='1' AND AM.type='$role' AND AM.access='1'";
//        $data = $obj->query($query);
//        $result = Hash::combine($data, '{n}.A.permission_name', '{n}.A.permission_key');
//        //pr($data);die;
//        return $result;
//    }
    public static function getMenus(){
        $user = AuthComponent::user('id');
        $object = new Acl();
        $obj = new UsersAcl();
        if(AuthComponent::user()['type'] != 'admin'){

        $query = [
            'conditions' =>[
                'UsersAcl.user_id' => $user
            ],
            'fields' => [
                'UsersAcl.acl_id',
            ]
        ];
        $result = $obj->find('all',$query);
        $acl_ids = Hash::extract($result,'{n}.UsersAcl.acl_id');
        if(!empty($acl_ids)){
        $option = [
            'conditions' => [
                'Acl.id IN' => $acl_ids,
                'Acl.parent' => '1'
            ],
            'fields' => [
                'Acl.permission_name',
                'Acl.permission_key'
            ]
        ];
        $res = $object->find('list',$option);
            return $res;
        } else {
            return $res = [];
        }
        } else {
            $option = [
                'conditions' => [
                    'Acl.parent' => '1',
                    'NOT' => [
                        'Acl.id' => '175'
                    ]
                ],
                'fields' => [
                    'Acl.permission_name',
                    'Acl.permission_key'
                ]
            ];
            $res = $object->find('list',$option);
            //print_r($res);die;

            return $res;
        }

        //pr($res);die;
//        $obj = new Acl();
//        $query = "SELECT A.permission_name, A.permission_key FROM acls as A INNER JOIN aclsmanage as AM ON AM.acl_id = A.id WHERE A.parent='1' AND AM.type='$role' AND AM.access='1'";
//        $data = $obj->query($query);
//        $result = Hash::combine($data, '{n}.A.permission_name', '{n}.A.permission_key');

    }
    public static function active_driver($vendor){
    $obj = new User();
    $options = [
        'conditions' => [
            'User.branch_number' => $vendor,
            'User.type' => 'driver',
            'User.is_enabled' => '1'
        ],
        'recursive' => -1,
        'fields' => [
            'User.id'
        ]
    ];
      $active_driver = $obj->find('count',$options);
        if(empty($active_driver)){
            $active = 'N/A';
        } else {
            $active = $active_driver;
        }
        return $active;
}
    public static function wallet($id = null){

        $options = [
            'conditions' => [
                'Wallet.user_id' => $id
            ],
            'recursive' => -1
        ];
        $obj = new Wallet();
        $wallet = $obj->find('first',$options);
        return $wallet;
    }
    public static function active_number($user_id = null){
        $option = [
          'conditions' =>[
              'Phonebook.user_id' => $user_id
          ],
            'recursive' => -1
        ];
        $obj = new Phonebook();
        return $obj->find('count',$option);
    }
    public static function get_charge_id(){
        $id = AuthComponent::user()['id'];
        $obj = new Charge();
        return $obj->find('first',['conditions' => ['Charge.user_id' => $id],'fields' =>['Charge.id']]);
    }
}
