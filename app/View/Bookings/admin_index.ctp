<?php echo $this->Html->script(array('excel/jquery.table2excel.min')); ?>

<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Bookings</h3>

        </div>

        <div class="box-body">
          <div class="bookings index">
            <style>
            .nopadding{
              padding: 0 !important;
            }
            .btn-flat, .no-radius{
              border-radius: 0 !important;
            }
            #table2excel{
              display: none;
            }
            </style>

            <div class="col-md-4 ">
              <div class="col-md-6 nopadding">
                <select class="form-control no-radius" name="period" id="period">
                  <option value="30">30 Days Old</option>
                  <option value="60">60 Days Old</option>
                  <option value="90">90 Days Old</option>
                </select>
              </div>
              <div class="col-md-6 nopadding">
                <button type="submit" class="btn btn-danger btn-block btn-flat" id="period_delete">Delete Records</button>
              </div>
            </div>
            <div class="col-md-6">
              <div class="col-md-4 nopadding"><input type="date" data-date-format="y-m-d" class="form-control no-radius" id="from_date" name="from_date"/></div>
              <div class="col-md-4 nopadding"><input type="date" data-date-format="y-m-d" class="form-control no-radius" id="to_date" name="to_date"/></div>
              <div class="col-md-4 nopadding"><button type="submit" class="btn btn-danger btn-block btn-flat" id="limit_delete">Delete Records</button></div>
            </div>

            <div class="col-md-2"><button class="btn btn-success btn-block btn-flat" id="save_record">Save Records</button></div>
            <script>
            $('#period_delete').on('click', function(){
              var period = $('#period').val();
              if(confirm('Are you sure to delete last '+ period +' days records?')){
                $.ajax({
                  url: '<?php echo $this->Html->url('deleteBookingRecord');?>/' + period,
                  data: {period: period},
                  dataType: 'json',
                  success: function(res) {
                      if(res.success) {
                          //alert('Data Deleted');
                          swal({
                                  title: "Good job",
                                  text: "Booking Data Deleted",
                                  type: "success"
                              },
                              function(){
                                  location.reload();
                              });


                      }

                  },
                  type: 'POST'
                });
              }
            });
            $('#limit_delete').on('click', function(){
              var from_date = $('#from_date').val();
              var to_date = $('#to_date').val();
              if(confirm('Are you sure to delete the records?')){
                $.ajax({
                  url: '<?php echo $this->Html->url('deleteBookingRecord');?>',
                  data: {from_date: from_date, to_date:to_date},
                  dataType: 'json',
                  success: function(res) {
                      if(res.success) {
                          //alert('Data Deleted');
                          swal({
                                  title: "Good job",
                                  text: "Booking Data Deleted",
                                  type: "success"
                              },
                              function(){
                                  location.reload();
                              });


                      }
                  },
                  type: 'POST'
                });
              }
            });
            $('#save_record').on('click', function(){
              $("#table2excel").table2excel({
                exclude: ".noExl",
                name: "Excel Document Name",
                filename: "bookings",
                fileext: ".xls",
                exclude_img: true,
                exclude_links: true,
                exclude_inputs: true
              });
            });
            </script>
            <div class="row"></div>
            <br/>
            <div class="row"></div>
              <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" id="" class="table table-bordered table-hover">
              <tr>
                <th><?php echo $this->Paginator->sort('id'); ?></th>
                <th><?php echo $this->Paginator->sort('pick_up'); ?></th>
                <th><?php echo $this->Paginator->sort('destination'); ?></th>
                <th><?php echo $this->Paginator->sort('is_sap_booking','SAP Booking'); ?></th>
                <th><?php echo $this->Paginator->sort('at_time'); ?></th>
                <th><?php echo $this->Paginator->sort('persons','Passengers'); ?></th>
                <th><?php echo $this->Paginator->sort('fare','Estimated Fare'); ?></th>
                <th><?php echo $this->Paginator->sort('real_fare'); ?></th>
                <th><?php echo $this->Paginator->sort('is_paid','Paid'); ?></th>
                <th><?php echo $this->Paginator->sort('requester'); ?></th>
                <th><?php echo $this->Paginator->sort('acceptor'); ?></th>
                <th><?php echo $this->Paginator->sort('accept_time'); ?></th>
                <th><?php echo $this->Paginator->sort('created'); ?></th>
                <th class="actions"><?php echo __(''); ?></th>
              </tr>
              <?php foreach ($bookings as $booking): ?>
                <?php
                $sts = array('no_driver_found' => 'No Driver Found/Responsed', 'cancelled_by_passenger' => 'Cancelled By Passenger', 'accepted' => 'Accepted','timeout'=> 'Timeout', 'cleared' => 'Cleared', 'rejected' => 'Rejected','forwarded' => 'Forwarded', 'ignored' => 'Timeout','timeout'=> 'Timeout','request_sent' => 'Request Sent');
                ?>
                <tr>
                  <td><?php
                  if(!empty($booking['Booking']['from_web'])) echo $this->Html->image('/img/web.jpg',array('alt'=>'web'));
                  else echo $this->Html->image('/img/app.jpg',array('alt'=>'app'));
                  echo h($booking['Booking']['id']);
                  ?>&nbsp;</td>
                  <td><?php echo h($booking['Booking']['pick_up']); ?>&nbsp;</td>
                  <td><?php echo h($booking['Booking']['destination']); ?>&nbsp;</td>
                  <td><?php if($booking['Booking']['is_sap_booking'] == '1') echo 'Yes';
                  else echo 'No';
                  ?>&nbsp;</td>
                  <td><?php
                  $date_time = date_create($booking['Booking']['at_time']);
                  echo date_format($date_time,'d/m/Y g:i A');
                  ?>&nbsp;</td>
                  <td><?php echo h($booking['Booking']['persons']); ?>&nbsp;</td>
                  <td><?php echo $this->Number->currency($booking['Booking']['fare'], 'GBP'); ?>&nbsp;</td>
                  <td><?php echo $this->Number->currency($booking['Booking']['real_fare'], 'GBP'); ?>&nbsp;</td>
                  <td><?php
                  if($booking['Booking']['is_paid']=='1'){
                    echo 'Yes';
                  } else {
                    echo 'No';
                  }
                  ?>&nbsp;</td>
                  <td><?php if(!empty($booking['Requester']['name'])){
                    echo $this->Html->link($booking['Requester']['name'], array('controller' => 'users', 'action' => 'edit', $booking['Booking']['requester']));
                  } else echo 'User has been deleted'
                  ?>&nbsp;</td>
                  <td><?php
                  if(!empty($booking['Booking']['acceptor'])) {
                    echo $this->Html->link($booking['Acceptor']['name'], array('controller' => 'users', 'action' => 'edit', $booking['Booking']['acceptor']));
                  } else {
                    if(empty($booking['Booking']['status'])) echo 'Queued';
                    else {
                      $val = $booking['Booking']['status'];
                      echo $sts[$val];
                    }
                  }
                  ?>&nbsp;</td>
                  <td><?php
                  if (!empty($booking['Booking']['accept_time'])) {
                    $date_time = date_create($booking['Booking']['accept_time']);
                    echo date_format($date_time,'d/m/Y g:i A');
                  } else {
                    echo 'N/A';
                  }
                  ?>&nbsp;</td>
                  <td><?php
                  $date_time = date_create($booking['Booking']['created']);
                  echo date_format($date_time,'d/m/Y g:i A');
                  ?>&nbsp;
                </td>
                <td class="actions">
                  <?php echo $this->Html->link(__('Queue'), array('controller' => 'queues','admin' => true,'action' => 'index', $booking['Booking']['id']),['class' =>'btn btn-success']); ?>
                  <?php echo $this->Html->link(__('View Details'), array('action' => 'view', $booking['Booking']['id']),['class' =>'btn btn-primary']); ?>
                  <?php echo $this->Form->postLink(__('Delete'), array('action' => 'admin_delete', $booking['Booking']['id']),['class' =>'btn btn-danger'], __('Are you sure you want to delete the booking with: %s?', $booking['Booking']['pick_up'])); ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </table>
              </div>
          <p class="pull-right">
            <?php
            echo $this->Paginator->counter(array(
              'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
            ));
            ?>	</p>
            <div class="clearfix"></div>
            <div class="paging">
              <ul class="pagination pagination-sm no-margin pull-right">
                <?php
                echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
                echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
                echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
                ?>
              </ul>

            </div>
          </div>
          <!--=================================-->
          <table cellpadding="0" cellspacing="0" id="table2excel">
            <tr>
              <th>id</th>
              <th>pick_up</th>
              <th>destination</th>
              <th>SAP Booking</th>
              <th>at_time</th>
              <th>Passengers</th>
              <th>Estimated Fare</th>
              <th>real_fare</th>
              <th>Paid</th>
              <th>requester</th>
              <th>acceptor</th>
              <th>accept_time</th>
              <th>created</th>
            </tr>
            <?php foreach ($booking_list as $booking): ?>
              <?php
              $sts = array('no_driver_found' => 'No Driver Found/Responsed', 'cancelled_by_passenger' => 'Cancelled By Passenger', 'accepted' => 'Accepted','timeout'=> 'Timeout', 'cleared' => 'Cleared', 'rejected' => 'Rejected','forwarded' => 'Forwarded', 'ignored' => 'Timeout','timeout'=> 'Timeout','request_sent' => 'Request Sent');
              ?>
              <tr>
                <td><?php
                echo h($booking['Booking']['id']);
                ?>&nbsp;</td>
                <td><?php echo h($booking['Booking']['pick_up']); ?>&nbsp;</td>
                <td><?php echo h($booking['Booking']['destination']); ?>&nbsp;</td>
                <td><?php if($booking['Booking']['is_sap_booking'] == '1') echo 'Yes';
                else echo 'No';
                ?>&nbsp;</td>
                <td><?php
                $date_time = date_create($booking['Booking']['at_time']);
                echo date_format($date_time,'d/m/Y g:i A');
                ?>&nbsp;</td>
                <td><?php echo h($booking['Booking']['persons']); ?>&nbsp;</td>
                <td><?php echo $this->Number->currency($booking['Booking']['fare'], 'GBP'); ?>&nbsp;</td>
                <td><?php echo $this->Number->currency($booking['Booking']['real_fare'], 'GBP'); ?>&nbsp;</td>
                <td><?php
                if($booking['Booking']['is_paid']=='1'){
                  echo 'Yes';
                } else {
                  echo 'No';
                }
                ?>&nbsp;</td>
                <td><?php if(!empty($booking['Requester']['name'])){
                  echo $this->Html->link($booking['Requester']['name'], array('controller' => 'users', 'action' => 'edit', $booking['Booking']['requester']));
                } else echo 'User has been deleted'
                ?>&nbsp;</td>
                <td><?php
                if(!empty($booking['Booking']['acceptor'])) {
                  echo $this->Html->link($booking['Acceptor']['name'], array('controller' => 'users', 'action' => 'edit', $booking['Booking']['acceptor']));
                } else {
                  if(empty($booking['Booking']['status'])) echo 'Queued';
                  else {
                    $val = $booking['Booking']['status'];
                    echo $sts[$val];
                  }
                }
                ?>&nbsp;</td>
                <td><?php
                if (!empty($booking['Booking']['accept_time'])) {
                  $date_time = date_create($booking['Booking']['accept_time']);
                  echo date_format($date_time,'d/m/Y g:i A');
                } else {
                  echo 'N/A';
                }
                ?>&nbsp;</td>
                <td><?php
                $date_time = date_create($booking['Booking']['created']);
                echo date_format($date_time,'d/m/Y g:i A');
                ?>&nbsp;
              </td>
            </tr>
          <?php endforeach; ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->


    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
