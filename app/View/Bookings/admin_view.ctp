<?php 
		$sts = array('no_driver_found' => 'No Driver Found/Responsed', 'cancelled_by_passenger' => 'Cancelled By Passenger', 'accepted' => 'Accepted','timeout'=> 'Timeout', 'cleared' => 'Cleared', 'rejected' => 'Rejected','forwarded' => 'Forwarded', 'ignored' => 'Timeout','timeout'=> 'Timeout', 'request_sent' => 'Request Sent'); 
	?>
<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Booking Detail</h3>

                    </div>
                    <div class="box-body">

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b><?php echo __('Pick Up'); ?></b> <a class="pull-right"><?php echo h($booking['Booking']['pick_up']); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('Destination'); ?></b> <a class="pull-right"><?php echo h($booking['Booking']['destination']); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('At Time'); ?></b> <a class="pull-right"><?php
                                    if (!empty($booking['Booking']['at_time'])){
                                        $date_time = date_create($booking['Booking']['at_time']);
                                        echo date_format($date_time,'d/m/Y g:i A');
                                    } else {
                                        echo 'N/A';
                                    }
                                    ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('SAP Booking'); ?></b> <a class="pull-right"><?php if(($booking['Booking']['is_sap_booking']) == '1') echo 'Yes';
                                    else 'No';
                                    ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('Passengers'); ?></b> <a class="pull-right"><?php echo h($booking['Booking']['persons']); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('Fare'); ?></b> <a class="pull-right"><?php echo $this->Number->currency($booking['Booking']['fare'], 'GBP'); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('Real Fare'); ?></b> <a class="pull-right"><?php echo $this->Number->currency($booking['Booking']['real_fare'], 'GBP'); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('Requester'); ?></b> <a class="pull-right"><?php echo $this->Html->link($booking['Requester']['name'], array('controller' => 'users', 'action' => 'edit', $booking['Booking']['requester'])); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('Acceptor'); ?></b>
                                    <?php
                                    if(!empty($booking['Booking']['acceptor'])) {
                                        echo $this->Html->link($booking['Acceptor']['name'], array('controller' => 'users', 'action' => 'edit', $booking['Booking']['acceptor']),['class'=>'pull-right']);
                                    } else {
                                        ?>
                                        <a class="pull-right">
                                            <?php
                                        if(empty($booking['Booking']['status'])) echo 'Queued';
                                        elseif ($booking['Booking']['status'] =='no_driver_found' AND $booking['Booking']['is_sap_booking'] == '1' AND $booking['Booking']['from_web']!='1') {
                                            echo 'Cancelled';
                                        }elseif($booking['Booking']['status'] =='no_driver_found' AND $booking['Booking']['is_sap_booking'] == '1' AND $booking['Booking']['from_web']=='1') {
                                            echo 'No Driver Found';
                                        }else {
                                            $val = $booking['Booking']['status'];
                                            echo $sts[$val];
                                        }
                                            ?>
                                            </a>
                                            <?php
                                    }
                                    ?>

                            </li>
                            <?php if(!empty($booking['Booking']['hunting_no'])) { ?>
                            <li class="list-group-item">
                                <b><?php echo __('Inquery No'); ?></b> <a class="pull-right"><?php echo h($booking['Booking']['hunting_no']); ?></a>
                            </li>
                            <?php } ?>
                            <li class="list-group-item">
                                <b><?php echo __('Accept Time'); ?></b> <a class="pull-right"><?php
                                    if (!empty($booking['Booking']['accept_time'])){
                                        $date_time = date_create($booking['Booking']['accept_time']);
                                        echo date_format($date_time,'d/m/Y g:i A');
                                    } else {
                                        echo 'N/A';
                                    }
                                    ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('Advanced Booking'); ?></b> <a class="pull-right"><?php
                                    if ($booking['Booking']['is_pre_book'] == '1'){
                                        echo 'Yes';
                                    } else {
                                        echo 'No';
                                    }
                                    ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('Special Requirement'); ?></b> <a class="pull-right"><?php
                                    if (!empty($booking['Booking']['special_requirement'])){
                                        echo h($booking['Booking']['special_requirement']);
                                    } else {
                                        echo 'N/A';
                                    }
                                    ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('Note To Driver'); ?></b> <a class="pull-right"><?php echo h($booking['Booking']['note_to_driver']); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('Created'); ?></b> <a class="pull-right"><?php
                                    $date_time = date_create($booking['Booking']['created']);
                                    echo date_format($date_time,'d/m/Y g:i A');
                                    ?></a>
                            </li>




                        </ul>



                    </div>
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div
