<div class="charges view">
<h2><?php echo __('Charge'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($charge['Charge']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($charge['User']['name'], array('controller' => 'users', 'action' => 'view', $charge['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Minimum Charge'); ?></dt>
		<dd>
			<?php echo h($charge['Charge']['minimum_charge']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Per Minute Charge'); ?></dt>
		<dd>
			<?php echo h($charge['Charge']['per_minute_charge']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Divert Call Charge'); ?></dt>
		<dd>
			<?php echo h($charge['Charge']['divert_call_charge']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Despatch Rent'); ?></dt>
		<dd>
			<?php echo h($charge['Charge']['despatch_rent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Despatch Rent Type'); ?></dt>
		<dd>
			<?php echo h($charge['Charge']['despatch_rent_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Driver Rent'); ?></dt>
		<dd>
			<?php echo h($charge['Charge']['driver_rent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Driver Rent Type'); ?></dt>
		<dd>
			<?php echo h($charge['Charge']['driver_rent_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($charge['Charge']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($charge['Charge']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Charge'), array('action' => 'edit', $charge['Charge']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Charge'), array('action' => 'delete', $charge['Charge']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $charge['Charge']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Charges'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Charge'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
