<?php $user_id = @$this->params['pass'][0];
//pr($user_id);die;?>
<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add Branch Charges'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->


                        <div class="box-body">
                            <?php echo $this->Form->create('Charge', array("data-toggle" => "validator", "role" => "form")); ?>

                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input(
                                    'user_id',
                                    [
                                        'label' => 'Select Branch',
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>',
                                        'value' => $user_id
                                    ]
                                );
                                ?>
                            </div>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input(
                                    'minimum_charge',
                                    [
                                        'label' => 'Minimum Charge (<span style="color: royalblue">For GPS Call, Call Zone, Call Nearest Driver from App & Website & Drivers Call back to Customer</span>)',
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    ]
                                ); ?>
                            </div>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input(
                                    'per_minute_charge', ['label' => 'Per Minute Charge (<span style="color: royalblue">For GPS Call, Call Zone, Call Nearest Driver from App & Website & Drivers Call back</span>)',
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    ]
                                ); ?>
                            </div>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input(
                                    'divert_call_charge',
                                    ['label' => 'Divert the call charge',
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    ]
                                ); ?>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="box box-warning">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"><?php echo __('Rent for Despatch System'); ?></h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->


                                        <div class="box-body">
                                            <div class="form-group has-feedback">
                                                <?php echo $this->Form->input(
                                                    'despatch_rent',
                                                    [
                                                        'label' => 'Amount',
                                                        'class' => 'form-control',
                                                        'required' => 'required',
                                                        'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                                    ]
                                                ); ?>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <label for="despatch-type">Select Weekly/Monthly</label>
                                                <select class="form-control" id="despatch-type"
                                                        name="data[Charge][despatch_rent_type]">
                                                    <option value="week">Week</option>
                                                    <option value="month">Month</option>
                                                </select>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="box box-warning">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"><?php echo __('Rent for Driver App'); ?></h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->


                                        <div class="box-body">
                                            <div class="form-group has-feedback">
                                                <?php echo $this->Form->input(
                                                    'driver_rent',
                                                    [
                                                        'label' => 'Amount',
                                                        'class' => 'form-control',
                                                        'required' => 'required',
                                                        'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                                    ]
                                                ); ?>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <label for="despatch-type">Select Weekly/Monthly</label>
                                                <select class="form-control" id="despatch-type"
                                                        name="data[Charge][driver_rent_type]">
                                                    <option value="week">Week</option>
                                                    <option value="month">Month</option>
                                                </select>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--                            <legend></legend>-->
                            <!--                            --><?php
                            //                            echo $this->Form->input('user_id');
                            //                            echo $this->Form->input('minimum_charge');
                            //                            echo $this->Form->input('per_minute_charge');
                            //                            echo $this->Form->input('divert_call_charge');
                            //                            echo $this->Form->input('despatch_rent');
                            //                            echo $this->Form->input('despatch_rent_type');
                            //                            echo $this->Form->input('driver_rent');
                            //                            echo $this->Form->input('driver_rent_type');
                            //
                            ?>
                            <!--                            --><?php //echo $this->Form->end(__('Submit')); ?>








                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>


                            </form>
                        </div>
                    </div>
                </div>
        </section>

    </div>
</div>






















