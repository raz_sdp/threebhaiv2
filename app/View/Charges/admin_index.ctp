<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo __('Branch Charges'); ?></h3>
                        <button class="btn btn-warning btn-lg pull-right"><?php echo $this->Html->link('Add New ', array('controller' => 'charges', 'action' => 'add', 'admin' => true, 'prefix' => 'admin'))?></button>
                    </div>
                    <div class="box-body">

                            <div class="table-responsive col-md-12">
                                <table cellpadding="0" cellspacing="0" id="example2" class="table table-bordered table-hover">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('user_id','Branch Name'); ?></th>
			<th><?php echo $this->Paginator->sort('minimum_charge'); ?></th>
			<th><?php echo $this->Paginator->sort('per_minute_charge'); ?></th>
			<th><?php echo $this->Paginator->sort('divert_call_charge','Divert the call charge'); ?></th>
			<th><?php echo $this->Paginator->sort('despatch_rent'); ?></th>
			<th><?php echo $this->Paginator->sort('despatch_rent_type'); ?></th>
			<th><?php echo $this->Paginator->sort('driver_rent'); ?></th>
			<th><?php echo $this->Paginator->sort('driver_rent_type'); ?></th>
        <th><?php echo $this->Paginator->sort('Divert the passenger call'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($charges as $charge): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($charge['User']['company_name'], array('controller' => 'users', 'action' => 'profile', $charge['User']['id'])); ?>
		</td>
		<td><?php echo h($charge['Charge']['minimum_charge']); ?>&nbsp;</td>
		<td><?php echo h($charge['Charge']['per_minute_charge']); ?>&nbsp;</td>
		<td><?php echo h($charge['Charge']['divert_call_charge']); ?>&nbsp;</td>
		<td><?php echo h($charge['Charge']['despatch_rent']); ?>&nbsp;</td>
		<td><?php echo h($charge['Charge']['despatch_rent_type']); ?>&nbsp;</td>
		<td><?php echo h($charge['Charge']['driver_rent']); ?>&nbsp;</td>
		<td><?php echo h($charge['Charge']['driver_rent_type']); ?>&nbsp;</td>
        <?php $change = $charge['Charge']['call_allow']='driver'?'Driver':'Owner' ?>
        <td><?php echo h($change); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $charge['Charge']['id']),['class' => 'btn btn-primary']); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $charge['Charge']['id']), array('class' => 'btn btn-danger','confirm' => __('Are you sure you want to delete # %s?', $charge['User']['company_name'].' charge'))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
                                </table>
                            </div>


                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>	</p>
                        <div class="clearfix"></div>
                        <div class="paging">
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
                                echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
                                echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
                                ?>
                            </ul>

                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->


            <!-- /.box -->
        </div>
        <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>