
<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Edit Settings'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->


                        <div class="box-body">
                            <?php echo $this->Form->create('Setting', array('type' => 'file')); ?>
	<?php echo $this->Form->input('id');?>
<!--                            --><?php //if(AuthComponent::user()['type'] = 'vendor'){ ?>
<!--                            <div class="col-md-12 form-group">-->
<!--                                --><?php //echo $this->Form->input('admin_percentage', array('label' => 'Admin Percentage %','class'=>'form-control')); ?>
<!--                            </div>-->
<!--                            --><?php //} else { ?>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('admin_percentage', array('label' => 'Admin Percentage %','class'=>'form-control')); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('free_credit_driver', array('label' => 'Free Credit Amount to be Given for Sign Up to Drivers £','class'=>'form-control')); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('admin_percentage_cash', array('label' => 'Admin Percentage % for Cash Payment','class'=>'form-control')); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('number_rent', array('label' => 'Set Branch User Phone Number Rent','class'=>'form-control')); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('promoter_percentage', array('label' => 'Promoter Percentage %','class'=>'form-control')); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('social_sharing_msg', array('label' => 'Set Dialog on the Social Service','class'=>'form-control')); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('admin_email', array('label' => 'Set Admin Email','class'=>'form-control')); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('balance_trans', array('label' => 'Set Minimum Transferable Balance £','class'=>'form-control')); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('driver_min_balance', array('label' => 'Set Minimum Balance £ Required By Driver to Get a Job','class'=>'form-control')); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <?php echo $this->Form->input('driver_msg', array('label' => 'Set Small Note for Driver','class'=>'form-control')); ?>
                            </div>
<!--                            --><?php //} ?>
                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                            </form>


                        </div>
                    </div>
                </div>
        </section>

    </div>
</div>

