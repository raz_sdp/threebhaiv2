<style>
    form div.checkbox{
        float: left;
        clear: none;
        width: 300px;
    }
</style>

<div class="content-wrapper">
<!-- Main content -->
<section class="content">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-warning">
<div class="box-header with-border">
    <h3 class="box-title">Admin Edit User</h3>
</div>
<!-- /.box-header -->
<!-- form start -->
<!--<form role="form">-->

<div class="box-body">
<?php
//pr($this->request->data);die;

echo $this->Form->create('Setting', array('type' => 'file')); ?>
<?php echo $this->Form->input('id'); ?>
<div class="row">
<div class="col-md-6">
    <div class="form-group">
        <?php echo $this->Form->input('itunes_link', array(
            'class' => 'form-control',
            'required'=>'required',
            'placeholder' => 'Set iTunes Link',
            'label' => 'Set iTunes Link'
        )); ?>
    </div>

    <div class="form-group">
        <?php echo $this->Form->input('googleplay_link', array(
            'class' => 'form-control',
            'label'=>'Set Google Play Link',
            'required'=>'required',
            'placeholder' => 'Set Google Play Link'
        )); ?>
    </div>
    <div class="form-group">
        <?php echo $this->Form->input('facebook_link', array(
            'class' => 'form-control',
            'label'=>'Set Facebook Link',
            'required'=>'required',
            'placeholder' => 'Set Facebook Link'
        )); ?>
    </div>
    <div class="form-group">

        <?php echo $this->Form->input('twitter_link', array(
            'class' => 'form-control',
            'label' => 'Set Twitter Link',
            'required'=>'required',
            'placeholder' => 'Set Twitter Link'
        )); ?>
    </div>

        <div class="form-group">

            <?php echo $this->Form->input('website_contact_email', array(
                'class' => 'form-control',
                'label' => 'Set Website Contact Email Id',
                'placeholder' => 'Set Website Contact Email Id',
                'required'=>'required'
            )); ?>

        </div>
<div class="form-group">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <button type="submit" class="btn btn-primary btn-block">Save</button>
    </div>
</div>
</form>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
