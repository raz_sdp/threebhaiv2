<?php
$option = array('Sandbox' => 'Sandbox', 'Live' => 'Live', 'Mock' => 'Mock');
?>

<div class="wrapper">
    <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="row">
    <!-- left column -->
    <div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo __('Admin Edit Settings'); ?></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->


    <div class="box-body">
<?php echo $this->Form->create('Setting', array('type' => 'file')); ?>
<?php echo $this->Form->input('id');?>
    <div class="col-md-12 form-group">
        <?php echo $this->Form->input('live_username', array('label' => 'Live Username','class'=>'form-control')); ?>
    </div>
        <div class="col-md-12 form-group">
            <?php echo $this->Form->input('paypal_username_api', array('label' => 'API Username for Live','class'=>'form-control')); ?>
        </div>
        <div class="col-md-12 form-group">
            <?php echo $this->Form->input('paypal_password', array('label' => 'API Password for Live','class'=>'form-control')); ?>
        </div>
        <div class="col-md-12 form-group">
            <?php echo $this->Form->input('paypal_signature', array('label' => 'API Signature for Live','class'=>'form-control')); ?>
        </div>
        <div class="col-md-12 form-group">
            <?php echo $this->Form->input('paypal_clientId', array('label' => 'Client Id for Live','class'=>'form-control')); ?>
        </div>
        <div class="col-md-12 form-group">
            <?php echo $this->Form->input('paypal_secret', array('label' => 'Secret Key for Live','class'=>'form-control')); ?>
        </div>
        <div class="col-md-12 form-group">
            <?php echo $this->Form->input('sandbox_username', array('label' => 'Sandbox Username','class'=>'form-control')); ?>
        </div>
        <div class="col-md-12 form-group">
            <?php echo $this->Form->input('sandbox_clientId', array('label' => 'Client Id for Sandbox','class'=>'form-control')); ?>
        </div>
        <div class="col-md-12 form-group">
            <?php echo $this->Form->input('sandbox_secret', array('label' => 'Secret Key for Sandbox','class'=>'form-control')); ?>
        </div>
        <div class="col-md-12 form-group">
            <?php echo $this->Form->input('sandbox_username_api', array('label' => 'Sandbox API Username','class'=>'form-control')); ?>
        </div>
        <div class="col-md-12 form-group">
            <?php echo $this->Form->input('sandbox_password', array('label' => 'Sandbox API Password','class'=>'form-control')); ?>
        </div>
        <div class="col-md-12 form-group">
            <?php echo $this->Form->input('sandbox_signature', array('label' => 'Sandbox API Signature','class'=>'form-control')); ?>
        </div>
        <div class="col-md-12 form-group">
            <?php echo $this->Form->input('paypal_mode', array('options' => $option, 'empty' => '(Select One)')); ?>
        </div>
        <div class="form-group">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary btn-block">Save</button>
            </div>
        </div>
        </form>


    </div>
    </div>
    </div>
    </section>

    </div>
</div>