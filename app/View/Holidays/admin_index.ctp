<div class="holidays index">
	<div class="AddNewButton"><?php echo $this->Html->link('Add new Holiday', array('controller' => 'holidays', 'action' => 'add', 'admin' => true, 'prefix' => 'admin'))?></div>
	<div class="AddNewButton"><?php echo $this->Html->link('Holiday Types', array('controller' => 'holidaytypes', 'action' => 'index', 'admin' => true, 'prefix' => 'admin'))?></div>
	<h2><?php echo __('Holidays'); ?></h2>
    <div class="table-responsive">
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('holidaytype_id','Holiday Type'); ?></th>
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th><?php echo $this->Paginator->sort('is_repeat','Repeat Every Year'); ?></th>
			<th class="actions"><?php echo __(''); ?></th>
	</tr>
	<?php foreach ($holidays as $holiday): ?>
	<tr>
		<td>
			<?php echo h($holiday['Holidaytype']['holiday_type']); ?>&nbsp;
		</td>
		<?php  
		$date = date_create($holiday['Holiday']['holiday']);
		?>
		<td><?php echo date_format($date,'d/m/Y'); ?>&nbsp;</td>
		<td>
			<?php echo h($holiday['Holiday']['is_repeat']); ?>&nbsp;
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $holiday['Holiday']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $holiday['Holiday']['id']), null, __('Are you sure you want to delete %s?', $holiday['Holidaytype']['holiday_type'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
        </div>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>