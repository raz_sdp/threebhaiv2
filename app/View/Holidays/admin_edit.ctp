<div class="holidays form">
<?php echo $this->Form->create('Holiday'); ?>
	<fieldset>
		<legend><?php echo __('Edit Holiday'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('holidaytype_id', array('label' => 'Holiday Type'));
		echo $this->Form->input('date');
		echo $this->Form->input('is_repeat', array('label' => 'Repeat Every Year'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>