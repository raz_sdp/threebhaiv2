


<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Edit Zone '); ?>  </h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body">

                            <?php

                            echo $this->Form->create('Zone' , array( "data-toggle"=>"validator", "role" =>"form"));
                                    echo $this->Form->input('id');
                             ?>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('type', array(
                                        'type' => 'hidden',
                                        'required'=>'required',
                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    )); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('name', array(
                                        'class' => 'form-control',
                                        'required'=>'required',
                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    ));?>
                                </div>
                            <?php if($this->request->data['Zone']['type'] != 'vr_zone') {?>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('voip_phone_no',array('options'=> $existing_purchase_twilio_numbers,'style' => 'width:250px; height: 20px'));
                                    ;?>
                                </div>

                            <?php }?>

                            <?php if ($this->request->data['Zone']['type'] == 'vr_zone') { ?>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('req_timeout', array(
                                        'label' => 'Try Each Logged In Driver for Number of Seconds ',
                                        'class' => 'form-control',
                                        'required'=>'required',
                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    )); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('pickup_lead_time', array(
                                        'label' => 'How many minutes before the pick up time the job should be offered to the driver ',
                                        'class' => 'form-control',
                                        'required'=>'required',
                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    )); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('remain_loggedin_on_accept', array(
                                        'label' => 'After Accepting a Job Leave Drivers Logged IN ',
                                        'checked' => true,
                                    )); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('reject_threshold', array(
                                        'label' => 'Log Drivers OUT after number of Ignore or Reject Jobs :',
                                        'class' => 'form-control',
                                        'required'=>'required',
                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    )); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('allow_inactive_time', array(
                                        'label' => 'Log Drivers Out after number of Inactive Minute',
                                        'class' => 'form-control',
                                        'required'=>'required',
                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    )); ?>
                                </div>
                            <?php } ?>

                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4"> <button type="submit" class="btn btn-primary btn-block">Save</button></div>
                            </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>












<!--

<div class="zones form">
<?php /*echo $this->Form->create('Zone'); */?>
	<fieldset>
		<legend><?php /*echo __('Admin Edit Zone'); */?></legend>
	<?php
/*		echo $this->Form->input('id');
		echo $this->Form->input('type', array('type' => 'hidden'));
		echo $this->Form->input('name',array('style' => 'width: 250px; height: 20px' ));
		if($this->request->data['Zone']['type'] != 'vr_zone') {
		echo $this->Form->input('voip_phone_no',array('options'=> $existing_purchase_twilio_numbers,'style' => 'width:250px; height: 20px'));
		}	
		if($this->request->data['Zone']['type'] == 'vr_zone') {
	        echo $this->Form->input('req_timeout', array('label' => 'Try Each Logged In Driver for Number of Seconds'));
	        echo $this->Form->input('pickup_lead_time', array('label' => 'How many minutes before the pick up time the job should be offered to the driver'));
	        echo $this->Form->input('remain_loggedin_on_accept', array('label' => 'After Accepting a Job Leave Drivers Logged IN', 'checked' => true));
	        echo $this->Form->input('reject_threshold', array('label' => 'Log Drivers OUT after number of Ignore or Reject Jobs'));
	        echo $this->Form->input('allow_inactive_time', array('label' => 'Log Drivers Out after number of Inactive Minute'));
	    	}
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
--><?php /*echo $this->element('menu'); */?>
