<?php
echo $this->Html->css(['zones']);
?>

<?php
$zone_types = array(
    'vr_zone' => 'VR Zone',
    'phonecall_gps_zone' => 'Phone GPS Zone',
    'phonecall_zone' => 'Phonecall Zone'

);
if ($zone_type != 'phonecall_zone') {
    ?>
    <script type="text/javascript">
        var polygons = JSON.parse('<?php echo json_encode($polygons);?>');
    </script>

<?php } ?>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Zones</h3>
                        <button class="btn btn-warning btn-lg pull-right"><?php echo $this->Html->link('Add new ' . $zone_types[$zone_type], array('controller' => 'zones', 'action' => 'add', 'admin' => true, 'prefix' => 'admin', $zone_type)) ?></button>
                    </div>

                    <div class="box-body">
                        <div class="zones index">

                            <?php
                            echo $this->Form->input('zone_type', array('options' => $zone_types,
                                        'selected' => $zone_type,
                                        'class' => 'form-control',
                                        'style' => 'max-width: 155px;'
                                    )
                                ) . '<br>';

                            if ($zone_type != 'phonecall_zone') {
                                ?>
                                <div class="form-group">
                                    <div id="main-map">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            <?php } ?>
                            <div class="table-responsive">

                                <table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
                                    <tr>
                                        <th>Name</th>
                                        <?php if ($zone_type != 'vr_zone') { ?>
                                            <th>VOIP Phone No</th>
                                        <?php } ?>
                                        <th class="actions"></th>
                                    </tr>
                                    <?php foreach ($zones as $zone): ?>
                                        <tr id="zone_<?php echo $zone['Zone']['id']; ?>">
                                            <td><?php echo h($zone['Zone']['name']); ?>&nbsp;</td>
                                            <?php if ($zone_type != 'vr_zone') { ?>
                                                <td><?php echo h($zone['Zone']['voip_phone_no']); ?></td>
                                            <?php } ?>
                                            <td class="actions">
                                                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $zone['Zone']['id']), ['class' => 'btn btn-info']); ?>
                                                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $zone['Zone']['id']), ['class' => 'btn btn-danger'], __('Are you sure you want to delete %s?', $zone['Zone']['name'])); ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                                </div>

                        </div>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<script type="text/javascript">
    // zone type selection drop down change fn
    $(function () {
        $('#zone_type').on('change', function () {
            location.href = ROOT + 'admin/zones/index/' + $(this).val();
        });
    });
    console.log(polygons);
</script>

<?php echo $this->Html->script(array('http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBhlUzD8xgohlHKuw7QJ2y62MO2zjbDuP8', 'markerwithlabel_packed.js', 'zones_index')); ?>