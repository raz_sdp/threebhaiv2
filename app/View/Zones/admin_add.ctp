<?php
echo $this->Html->css(['zones']);
?>
<script type="text/javascript">
    // zone type selection drop down change fn
    $(function () {
        $('#ZoneType').on('change', function () {
            location.href = ROOT + 'admin/zones/add/' + $(this).val();
        });
    });
</script>
<?php
$zone_types = array(
    'vr_zone' => 'VR Zone',
    'phonecall_gps_zone' => 'Phone GPS Zone',
    'phonecall_zone' => 'Phonecall Zone'

);
if ($zone_type != 'phonecall_zone') {
    ?>
    <script type="text/javascript">
        var polygons = JSON.parse('<?php echo json_encode($polygons);?>');
        //console.log(polygons)
    </script>
<?php
}
echo $this->Html->script(array('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry&sensor=false&key=AIzaSyBhlUzD8xgohlHKuw7QJ2y62MO2zjbDuP8', 'polygon.min', 'markerwithlabel_packed', 'zones_add'));
?>


<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add '); ?> <?php echo($zone_types[$zone_type]); ?> </h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body">
                            <?php echo $this->Form->create('Zone',array( "data-toggle"=>"validator", "role" =>"form"));
                            if ($zone_type != 'phonecall_zone') {
                                ?>
                                <table style="width:200px;">
                                    <tr>
                                        <td>
                                            <div id="hand_b"/>
                                        </td>
                                        <td>
                                            <div id="shape_b"/>
                                        </td>
                                        <td style="width:390px">
                                            <form id="options">
                                                <!--<label for="radiusInput">Radius</label> <input type="text" value="5" id="radiusInput" name="radiusInput"> km-->
                                            </form>
                                        </td>
                                    </tr>
                                </table>
                            <?php
                            }
                            if ($zone_type != 'phonecall_zone') {
                                ?>

                                <!-- map	-->
                            <div class="form-group">
                                <div id="main-map">
                                </div>
                            </div>
                                <div class="clearfix"></div>
                            <?php
                            }
                            ?>
                            <div class="form-group has-feedback">

                                <?php echo $this->Form->input('name',  array(
                                    'class' => 'form-control',
                                    'id'=>'inputName',
                                    'required'=>'required',
                                    'placeholder' => 'name',
                                    'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                )); ?>

                            </div>

                            <?php if ($zone_type == 'vr_zone') { ?>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('req_timeout', array(
                                        'label' => 'Try Each Logged In Driver for Number of Seconds ',
                                        'class' => 'form-control',
                                        'required'=>'required',
                                        'id'=>'inputName',

                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    )); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('pickup_lead_time', array(
                                        'label' => 'How many minutes before the pick up time the job should be offered to the driver ',
                                        'class' => 'form-control',
                                        'required'=>'required',
                                        'id'=>'inputName',
                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'

                                        )); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('remain_loggedin_on_accept', array(
                                        'label' => 'After Accepting a Job Leave Drivers Logged IN ',
                                        'required'=>'required',
                                        'id'=>'inputName',
                                        'checked' => true,
                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'

                                    )); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('reject_threshold', array(
                                        'label' => 'Log Drivers OUT after number of Ignore or Reject Jobs :',
                                        'required'=>'required',
                                        'id'=>'inputName',
                                        'class' => 'form-control',
                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'

                                    )); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('allow_inactive_time', array(
                                        'label' => 'Log Drivers Out after number of Inactive Minute',
                                        'class' => 'form-control',
                                        'required'=>'required',
                                        'id'=>'inputName',
                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    )); ?>
                                </div>
                            <?php } ?>

                            <?php if ($zone_type != 'vr_zone') { ?>
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('voip_phone_no', array(
                                        'label' => 'VOIP Phone No',
                                        'required'=>'required',
                                        'id'=>'inputName',
                                        'class' => 'form-control',
                                        'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                    ))
                                    ; ?>
                                </div>
                            <?php } ?>

                            <div class="form-group has-feedback">
                                <?php
                                echo $this->Form->input('type', array(
                                    'options' => $zone_types,
                                    'required'=>'required',
                                    'id'=>'inputName',
                                    'class' => 'form-control',
                                    'selected' => $zone_type,
                                    'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                                ));
                                ?>
                                <?php
                                echo $this->Form->input('polygon', array('type' => 'hidden'));
                                echo $this->Form->input('polygon_points', array('type' => 'hidden'));
                                ?>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>



