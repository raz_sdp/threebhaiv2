<div class="aclsmanages view">
<h2><?php echo __('Aclsmanage'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($aclsmanage['Aclsmanage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($aclsmanage['Aclsmanage']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Acl'); ?></dt>
		<dd>
			<?php echo $this->Html->link($aclsmanage['Acl']['id'], array('controller' => 'acls', 'action' => 'view', $aclsmanage['Acl']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Access'); ?></dt>
		<dd>
			<?php echo h($aclsmanage['Aclsmanage']['access']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Aclsmanage'), array('action' => 'edit', $aclsmanage['Aclsmanage']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Aclsmanage'), array('action' => 'delete', $aclsmanage['Aclsmanage']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $aclsmanage['Aclsmanage']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Aclsmanages'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aclsmanage'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Acls'), array('controller' => 'acls', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Acl'), array('controller' => 'acls', 'action' => 'add')); ?> </li>
	</ul>
</div>
