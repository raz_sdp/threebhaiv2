<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('Aclsmanage', array('type' => 'file', "data-toggle"=>"validator", "role" =>"form")); ?>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('type', array('label'=>'Type', 'class'=>'form-control', 'required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>'));?>
                                    <!--                                --><?php //echo $this->Form->input('zone_id', array('options' => $zones, 'label' => 'Phonecall and Phone GPS Zones', 'class' => 'form-control', 'required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('acl_id', array('label'=>'ACL', 'class'=>'form-control', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>'));?>
                                    <!--                                --><?php //echo $this->Form->input('req_timeout', array('label' => 'Try Each Logged In Driver for Number of Seconds', 'class' => 'form-control', 'required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>

                                </div>
                            </div>
                              <div class="col-md-12">
                            <div class="form-group">
                                <?php echo $this->Form->input('access', array('label'=>'Access', 'class'=>'checkboxmargin'));?>
<!--                                --><?php //echo $this->Form->input('remain_loggedin_on_accept', array('label' => 'After Accepting a Job Leave Drivers Logged IN','class' => 'checkboxmargin', 'checked' => true)); ?>
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                            </from>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
