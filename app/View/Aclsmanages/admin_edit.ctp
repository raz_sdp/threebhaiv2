<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Edit Acl'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('Aclsmanage', array('type' => 'file', "data-toggle"=>"validator", "role" =>"form")); ?>
                        <div class="box-body">
                            <?php echo $this->Form->input('id');?>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('type', array( 'label' => 'Type', 'class' => 'form-control', 'required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
                            </div>
                            <div class="form-group has-feedback">
                                <?php echo $this->Form->input('acl_id', array('label' => 'ACL', 'class' => 'form-control', 'required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('access', array('Access','class'=>'checkboxmargin', 'checked' => true)); ?>
                            </div>

                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                            </from>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
