
<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Admin Fee Records</h3>
                        <h3 class=" pull-right">Total Amount <?php echo $this->Number->currency(trim($sum_amount[0][0]['total'],'-'), 'GBP');?></h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                        <table cellpadding="0" cellspacing="0" class="table table-bordered table-striped">
                            <tr>
                                <th><?php echo $this->Paginator->sort('created', 'Transaction Date'); ?></th>
                                <th><?php echo $this->Paginator->sort('user_id', "Driver's Name"); ?></th>
                                <th><?php echo $this->Paginator->sort('amount'); ?></th>
                                <th><?php echo $this->Paginator->sort('booking_id', 'Booking Id'); ?></th>
                            </tr>
                            <?php // pr($sum_amount); ?>
                            <?php foreach ($records as $record): ?>
                                <tr>
                                    <td>
                                        <?php $created = date_create($record['Transaction']['created']);
                                        echo date_format($created, 'd/m/Y g:i A'); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Html->link($record['User']['name'], array('controller' => 'users','action' => 'edit', $record['User']['id'])); ?>
                                    </td>
                                    <td><?php
                                        $fee = trim($record['Transaction']['amount'], '-');
                                        echo $this->Number->currency($fee, 'GBP');
                                        ?>&nbsp;
                                    </td>
                                    <td>
                                        <?php echo $this->Html->link($record['Transaction']['booking_id'], array('controller' => 'bookings','action' => 'view', $record['Transaction']['booking_id'])); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                            </div>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>	</p>
                        <div class="clearfix"></div>
                        <div class="paging">
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
                                echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
                                echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
                                ?>
                            </ul>
                        </div>





                    </div>
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div
