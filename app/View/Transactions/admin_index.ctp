<?php
$service = [
    'free_credit' => 'Free credit given by admin',
    'booking_fee' => 'Charge for booking transaction',
    'vendor_number_rent' => 'Charge for using number',
    'paypal' => 'Cash in from Paypal'
]
?>
<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Transactions</h3>
<!--                        <button class="btn btn-warning btn-lg pull-right">--><?php //echo $this->Html->link('View Admin Fee Record ', array('action' => 'paymentrecord', 'admin' => true, 'prefix' => 'admin'))?><!--</button>-->
                    </div>
                    <div class="box-body">

                        <?php echo $this->Form->create('Transaction', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
                        <div class=" col-md-12">
                            <div class="form-group col-md-10">
                            <select class="form-control" name="keyword" id="keyword">
                                <option value="">Select One</option>
                                <option value="passenger"<?php echo $keyword == 'passenger' ? 'selected' : '' ?>>Passenger</option>
                                <option value="driver"<?php echo $keyword == 'driver' ? 'selected' : '' ?>>Driver</option>
                                <option value="vendor"<?php echo $keyword == 'vendor' ? 'selected' : '' ?>>Branch User</option>
                            </select>

                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary pull-right" type="submit">Go</button>
                            </div>
                        </div>
                        </form>
                        <div class="clearfix"></div>
                        <div class="table-responsive">

                            <table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
                                <tr>
                                    <th><?php echo $this->Paginator->sort('name'); ?></th>
                                    <th><?php echo $this->Paginator->sort('type'); ?></th>
                                    <th><?php echo $this->Paginator->sort('service'); ?></th>
                                    <th><?php echo $this->Paginator->sort('amount'); ?></th>
                                    <th><?php echo $this->Paginator->sort('timestamp'); ?></th>
<!--                                    <th class="actions">--><?php //echo __('Action'); ?><!--</th>-->
                                </tr>
                                <?php // pr($transactions); ?>
                                <?php foreach ($transactions as $transaction): ?>
                                    <tr>
                                        <td>
                                            <?php echo h($transaction['User']['name']); ?>
                                        </td>
                                        <td>
                                            <?php echo ucfirst($transaction['User']['type']); ?>
                                        </td>
                                        <td>
                                            <?php echo empty($service[$transaction['Transaction']['service']])?'Transaction happen':$service[$transaction['Transaction']['service']]; ?>
                                        </td>
                                        <td><?php echo $this->Number->currency($transaction['Transaction']['amount'], 'GBP');
                                            ?>&nbsp;
                                        </td>
                                        <td>
                                            <?php echo h($transaction['Transaction']['timestamp']); ?>
                                        </td>
<!--                                        <td class="actions">-->
<!--                                            --><?php //// echo $this->Html->link(__('Add'), array('controller' => 'top_ups', 'action' => 'add', 'admin' => true, 'prefix' => 'admin', $transaction['TopUp']['user_id'])); ?>
<!--                                            --><?php //echo $this->Html->link(__('View'), array('controller' => 'users','admin' => true, 'action' => 'admin_topup', $transaction['User']['id']), ['class' => 'btn btn-info']); ?>
<!--                                        </td>-->
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>	</p>
                        <div class="clearfix"></div>
                        <div class="paging">
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
                                echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
                                echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
                                ?>
                            </ul>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->


            <!-- /.box -->
        </div>
        <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
