<script type="text/javascript">
// zone type selection drop down change fn
$(function(){
	$('#short_description').on('change', function(){
		location.href = ROOT + 'admin/transactions/reqtransferamount/' + $(this).val();
	});
	$('a.pay').on('click', function(e){
		e.preventDefault();
		var link = $(this).prop('href');
		$('#pay_form').prop('action', link);
		var note=prompt("Please enter note","Bank transfer");

		if (note!=null)
		{
			$('#note').val(note);
			$('#pay_form').submit();
		}
	})
});
</script>
<?php
$types = array(
	'transfer_pending' => 'Pending',
	'transfer_paid'	=> 'Approved'
);
?>
<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<!-- <div class="box-header">
					<h3 class="box-title">Users</h3>
				</div> -->
				<div class="box-body">
					<form id="pay_form" action="" method="post" style="display:none;">
						<input type="hidden" value="" id="note" name="data[Transaction][note_box]">
					</form>
					<div class="transaction index">
						<h2><?php echo __('Balance Transfer Status'); ?></h2>
						<?php
						echo $this->Form->input('short_description', array('label' => 'Transfer Status&nbsp;', 'options' => $types,
						'selected' => $type,
						'class' => 'form-control',
						'style' => 'max-width: 155px;'
					)
				)."<br>"; ?>
				<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
					<tr>
						<th><?php echo $this->Paginator->sort('user_id'); ?></th>
						<th><?php echo $this->Paginator->sort('amount', 'Balance'); ?></th>
						<th><?php echo $this->Paginator->sort('short_description', 'Transfer Status'); ?></th>
						<?php if($type!= 'transfer_pending') { ?>
							<th><?php echo $this->Paginator->sort('note_box'); ?></th>
						<?php } ?>
						<th><?php echo $this->Paginator->sort('timestamp','Transaction Date'); ?></th>
						<th class="actions"><?php echo __(''); ?></th>
					</tr>
					<?php foreach ($transactions as $transaction): ?>
						<tr>
							<td>
								<?php echo h($transaction['User']['name']); ?>
							</td>
							<td><?php echo $this->Number->currency(trim($transaction['Transaction']['amount'],'-'), 'GBP'); ?>&nbsp;</td>
							<td>
								<?php
								$val = $transaction['Transaction']['short_description'];
								echo $types[$val];
								?>
							</td>
							<?php if($type!= 'transfer_pending') { ?>
								<td><?php echo h($transaction['Transaction']['note_box']); ?>&nbsp;</td>
							<?php } ?>
							<td>
								<?php
								$date_time = date_create($transaction['Transaction']['timestamp']);
								echo date_format($date_time,'d/m/Y');
								?>
							</td>
							<td class="actions">
								<?php if($type != 'transfer_paid') {
									echo $this->Html->link(__('Mark as Paid'), array( 'action' => 'markaspay', $transaction['Transaction']['id']), array('div' => false, 'class' => 'e_d pay btn btn-warning'));
								}
								?>
								<?php echo $this->Html->link(__('Detail'), array('controller' => 'users','admin' => true, 'action' => 'admin_topup', $transaction['User']['id']),['class' => 'btn btn-info']); ?>
								<?php /* echo $this->Form->postLink(__('Delete'), array('action' => 'markaspay_delete', $transaction['Transaction']['id']), null, __('Are you sure you want to delete the Transfer record for %s?', $transaction['User']['name'])); */ ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
				<p class="pull-right">
					<?php
					echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					));
					?>	</p>
					<div class="clearfix"></div>
					<div class="paging">
						<ul class="pagination pagination-sm no-margin pull-right">
							<?php
							echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
							echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
							echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
							?>
						</ul>
					</div>
				</div>


			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->


		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
