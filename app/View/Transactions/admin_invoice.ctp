<?php
$decriptions = [
    'top_up' => 'Your account credited from paypal',
    'vendor_number_rent' => 'Phone number purchase or rent',
    'booking_fee' => 'Get money from driver for complete job'
]
?>
<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Invoice of <?php echo $user['User']['name'] ?></h3>
                    </div>
                    <div class="box-body">


                        <div class="table-responsive">

                            <table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
                                <tr>
                                    <th><?php echo $this->Paginator->sort('service'); ?></th>
                                    <th><?php echo $this->Paginator->sort('short_description'); ?></th>
                                    <th><?php echo $this->Paginator->sort('amount', 'Amount (£)'); ?></th>
                                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                                    <th><?php echo $this->Paginator->sort('is_paid'); ?></th>
                                    <th><?php echo $this->Paginator->sort('timestamp', 'Date'); ?></th>
                                </tr>
                                <?php // pr($transactions); ?>
                                <?php foreach ($histories as $history): ?>
                                    <tr>
                                        <td>
                                            <?php echo h($history['Transaction']['service']); ?>
                                        </td>
                                        <td><?php echo($decriptions[$history['Transaction']['short_description']]);
                                            ?>&nbsp;
                                        </td>
                                        <td>
                                            <?php echo h($history['Transaction']['amount']); ?>
                                        </td>
                                        <td>
                                            <?php echo($history['Transaction']['short_description'] == 'top_up' || $history['Transaction']['short_description'] == 'booking_fee' ? 'Credit' : 'Debit'); ?>
                                        </td>

                                        <td>
                                            <?php echo $history['Transaction']['is_paid'] == '1' ? 'Yes' : 'No' ?>
                                        </td>
                                        <?php $date = date_create($history['Transaction']['timestamp']) ?>
                                        <td>
                                            <?php echo date_format($date, 'd/m/Y'); ?>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>    </p>

                        <div class="clearfix"></div>
                        <div class="paging">
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                echo "<li>" . $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) . "</li>";
                                echo "<li>" . $this->Paginator->numbers(array('separator' => '')) . "</li>";
                                echo "<li>" . $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) . "</li>";
                                ?>
                            </ul>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->


            <!-- /.box -->
        </div>
        <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
