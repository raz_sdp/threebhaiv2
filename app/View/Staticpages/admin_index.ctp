<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">CMS</h3>
                        <button class="btn btn-warning btn-lg pull-right"><?php echo $this->Html->link('Add new Page', array('action' => 'add', 'admin' => true, 'prefix' => 'admin'))?></button>
                    </div>

                    <div class="box-body">
					<div class="staticpages index">

                        <div class="table-responsive">
						<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
							<tr>
								<th><?php echo $this->Paginator->sort('id'); ?></th>
								<th><?php echo $this->Paginator->sort('title'); ?></th>
								<th><?php echo $this->Paginator->sort('content'); ?></th>
								<th><?php echo $this->Paginator->sort('sort_order'); ?></th>
								<th><?php echo $this->Paginator->sort('menu_title'); ?></th>
								<th><?php echo $this->Paginator->sort('created'); ?></th>
								<th class="actions"><?php echo __('Actions'); ?></th>
							</tr>
							<?php foreach ($staticpages as $staticpage): ?>
								<tr>
									<td><?php echo h($staticpage['Staticpage']['id']); ?>&nbsp;</td>
									<td><?php echo h($staticpage['Staticpage']['title']); ?>&nbsp;</td>
									<td><?php echo h($staticpage['Staticpage']['content']); ?>&nbsp;</td>
									<td><?php echo h($staticpage['Staticpage']['sort_order']); ?>&nbsp;</td>
									<td><?php echo h($staticpage['Staticpage']['menu_title']); ?>&nbsp;</td>
									<td><?php echo date_format(date_create($staticpage['Staticpage']['created']),'d/m/Y g:i A'); ?>&nbsp;</td>
									<td class="actions">
										<?php // echo $this->Html->link(__('View'), array('action' => 'view', $staticpage['Staticpage']['id'])); ?>
										<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $staticpage['Staticpage']['id']), ['class' => 'btn btn-info']); ?>
										<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $staticpage['Staticpage']['id']), ['class' => 'btn btn-danger'], __('Are you sure you want to delete # %s?', $staticpage['Staticpage']['title'])); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</table>
                            </div>
						<p class="pull-right">
							<?php
							echo $this->Paginator->counter(array(
								'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
							));
							?>	</p>
							<div class="clearfix"></div>
							<div class="paging">
								<ul class="pagination pagination-sm no-margin pull-right">
									<?php
									echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
									echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
									echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
									?>
								</ul>
							</div>
						</div>


					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->


				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
