<div class="wrapper">
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo __('Edit Page'); ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <?php echo $this->Form->create('Staticpage',array('type'=>'file')); ?>
                    <div class="box-body">
                        <div class="form-group">

                            <h3><?php echo $this->Form->input('menu_title',array('class'=>'form-control'));?></h3>

                        </div>
                        <div class="form-group">

                            <?php echo $this->Form->input('title',array('class'=>'form-control'));?>

                        </div>
                        <div class="form-group">
                            <?php   echo $this->Form->input('content', array('type' => 'textarea', 'class' => 'form-control'));?>

                        </div>
                        <div class="form-group">
                            <?php   echo $this->Form->input('sort_order', array( 'class' => 'form-control'));?>

                        </div>
                        <div class="form-group ">
                            <?php   echo $this->Form->input('slug', array( 'class' => 'form-control','disabled'));?>

                        </div>
                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary btn-block">Save</button>
                            </div>
                        </div>
                        </from>

                    </div>

                </div>

            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
</div>





<!--
<div class="staticpages form">
<?php /*echo $this->Form->create('Staticpage'); */?>
	<fieldset>
		<legend><?php /*echo __('Edit Page'); */?></legend>
	<?php
/*		echo $this->Form->input('id');
		echo $this->Form->input('menu_title');
		echo $this->Form->input('title');
		echo $this->Form->input('content', array('type' => 'textarea', 'class' => 'content_page'));
		echo $this->Form->input('sort_order');
		echo $this->Form->input('slug');
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
--><?php /*echo $this->element('menu'); */?>
