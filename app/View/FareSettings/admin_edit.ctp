<style>
    form div.checkbox {
        float:;
        clear:;
    }

    .checkbox {
        width: 20%;
        float: left;
    }

</style>


<?php
echo $this->Html->script(array('add_fare.js?10'));
echo $this->Html->script('fare_holidays', array('inline' => false));

?>

<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content --><?php echo $this->Form->create('FareSetting', array('type' => 'file')); ?>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Edit Fare By Miles'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->


                        <div class="box-body">



                            <div class="form-group">
                                <?php echo $this->Form->input('zone_id', array('empty' => 'Default', 'class' => 'form-control')); ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('note', array('label' => 'Note For Passenger & Driver', 'class' => 'form-control')); ?>
                            </div>
                            <div class="form-group">

                                <?php echo $this->Form->input('holidaytype_id', array('label' => 'Holiday Type', 'class' => 'form-control', 'empty' => '(Not A Holiday)')); ?>

                            </div>


                            <div class="form-group col-md-12">
                                <?php
                                $weekdays = array('0' => 'Sunday', '1' => 'Monday', '2' => 'Tuesday', '3' => 'Wednesday', '4' => 'Thrusday', '5' => 'Friday', '6' => 'Saturday');?>

                                <?php
                                if($this->request->data['FareSetting']['weeks'] != Null) {
                                    $day = trim($this->request->data['FareSetting']['weeks'],'#');
                                    $wks = explode('#', $day);

                                    ?>
                                    <?php foreach($weekdays as $key => $weekday) { ?>
                                        <label class="checkbox-inline days">
                                            <input name="data[FareSetting][weeks][]" type="checkbox" value="<?php echo $key ?>" <?php echo in_array($key,$wks) ? 'checked':'' ?>> <?php echo $weekday?>
                                        </label>
                                    <?php } } else {
                                    echo $this->Form->input('week_no', array('label'=> false, 'multiple' => 'checkbox','options' => $weekdays, 'div' => false));
                                } ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('time_from', array('label' => 'Start Time'));; ?>

                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('time_to', array('label' => 'End Time')); ?>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-body js-panel-box">
                                    <div class='AddMoreClass'>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php echo $this->Form->input('distance', array('label' => ' Mile Up To', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php echo $this->Form->input('s_4', array('label' => 'Base Fare', 'class' => 'form-control fareClass', 'style' => '', 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Standard Car</h3>
                                            </div>

                                            <div class="panel-body">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <?php echo $this->Form->input('s_5', array('label' => ' Up To 5 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">

                                                    <?php echo $this->Form->input('s_6', array('label' => 'Up To 6 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>
                                                </div>
                                                <div class="col-md-3">

                                                    <?php echo $this->Form->input('s_7', array('label' => ' Up To 7 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>

                                                </div>
                                                <div class="col-md-3">

                                                    <?php echo $this->Form->input('s_8', array('label' => 'Up To 8 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>

                                                </div>

                                            </div>
                                        </div>

                                        <div class="panel panel-info">

                                            <div class="panel-heading">
                                                <h3 class="panel-title">Estate Car</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-4">

                                                    <?php echo $this->Form->input('e_4', array('label' => ' Up To 4 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>

                                                </div>
                                                <div class="col-md-4">

                                                    <?php echo $this->Form->input('e_5', array('label' => 'Up To 5 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>

                                                </div>
                                                <div class="col-md-4">

                                                    <?php echo $this->Form->input('e_6', array('label' => 'Up To 6 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>

                                                </div>
                                                <div class="col-md-4">

                                                    <?php echo $this->Form->input('e_7', array('label' => ' Up To 7 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>

                                                </div>
                                                <div class="col-md-4">

                                                    <?php echo $this->Form->input('e_8', array('label' => ' Up To 8 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>

                                                </div>

                                            </div>
                                        </div>

                                        <div class="panel panel-info">

                                            <div class="panel-heading">
                                                <h3 class="panel-title">Wheelchair</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-3">

                                                    <?php echo $this->Form->input('w_3', array('label' => ' Up To 3 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>

                                                </div>
                                                <div class="col-md-3">

                                                    <?php echo $this->Form->input('w_4', array('label' => 'Up To 4 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>

                                                </div>
                                                <div class="col-md-3">

                                                    <?php echo $this->Form->input('w_5', array('label' => 'Up To 5 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>

                                                </div>
                                                <div class="col-md-3">

                                                    <?php echo $this->Form->input('w_6', array('label' => ' Up To 6 Passengers', 'class' => 'form-control', 'style' => '', 'div' => false)); ?>

                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <?php echo $this->Form->input('Add More', array('type' => 'button', 'label' => false, 'value' => 'Add More', 'id' => 'AddMore', 'class'=> 'btn btn-primary btn-lg')); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                            </from>


                        </div>
                    </div>
                </div>
        </section>

    </div>
</div>


<!--
<style>
	form div.checkbox{
		float: left;
		clear: none;
	}
</style>
<?php
/*	echo $this->Html->script(array('add_fare.js?10'));
	echo $this->Html->script('fare_holidays', array('inline'=>false));
*/?>
<div class="fareSettings form">
<?php /*echo $this->Form->create('FareSetting'); */?>
	<fieldset>
		<legend><?php /*echo __('Admin Edit Fare By Miles'); */?></legend>
	<?php
/*		echo $this->Form->input('id');
		echo $this->Form->input('zone_id', array('empty' => 'Default'));
		echo $this->Form->input('note', array('label' => 'Note For Passenger & Driver'));
		echo $this->Form->input('holidaytype_id', array('label' => 'Holiday Type','empty' => '(Not A Holiday)'));
		$weekdays =  array('0' => 'Sunday', '1'=> 'Monday','2' => 'Tuesday','3' => 'Wednesday','4' => 'Thrusday','5' => 'Friday','6' => 'Saturday');
		if($this->request->data['FareSetting']['weeks'] != Null) {
            $day = trim($this->request->data['FareSetting']['weeks'],'#');
            $wks = explode('#', $day);
            echo $this->Form->input('weeks', array('label' => false ,'multiple' => 'checkbox', 'selected' => $wks ,'options' => $weekdays));
        } else {
            echo $this->Form->input('weeks', array('label'=> false, 'multiple' => 'checkbox','options' => $weekdays));
        }
		echo $this->Form->input('time_from', array('label' => 'Start Time'));
		echo $this->Form->input('time_to', array('label' => 'End Time'));
		echo $this->Form->input('fare_setting_type',array('value' => $this->request->data['FareSetting']['fare_setting_type'], 'type'=>'hidden'));
		*/?><div class='AddMoreClass'><?php
/*		*/?>Mile Up To <?php
/*		echo $this->Form->input('distance',array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][distance][]','div' => false));
		*/?>&nbsp;&nbsp;<span class='fareClass'>Base Fare</span> <?php
/*		echo $this->Form->input('s_4', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => 'fare_id', 'name' => 'data[FareSetting][s_4][]', 'div' => false));
		*/?><br><br><label><strong>Standard Car</strong></label>5 Passengers <?php
/*		echo $this->Form->input('s_5', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_5][]', 'div' => false));
			*/?>&nbsp;&nbsp;6 Passengers <?php
/*		echo $this->Form->input('s_6', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_6][]', 'div' => false));
		*/?>&nbsp;&nbsp;7 Passengers <?php
/*		echo $this->Form->input('s_7', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_7][]', 'div' => false));
		*/?>&nbsp;&nbsp;8 Passengers <?php
/*		echo $this->Form->input('s_8', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_8][]', 'div' => false));
		*/?><br><br><label><strong>Estate Car</strong></label><?php
/*		*/?>4 Passengers <?php
/*		echo $this->Form->input('e_4', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_4][]', 'div' => false));
		*/?>&nbsp;&nbsp;5 Passengers <?php
/*		echo $this->Form->input('e_5', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_5][]', 'div' => false));
		*/?>&nbsp;&nbsp;6 Passengers <?php
/*		echo $this->Form->input('e_6', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_6][]', 'div' => false));
		*/?>&nbsp;&nbsp;7 Passengers <?php
/*		echo $this->Form->input('e_7', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_7][]', 'div' => false));
		*/?>&nbsp;&nbsp;8 Passengers <?php
/*		echo $this->Form->input('e_8', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_8][]', 'div' => false));
		*/?><br><br><label><strong>Wheelchair</strong></label>
		3 Passengers <?php
/*		echo $this->Form->input('w_3', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][w_3][]','div' => false));
		*/?>&nbsp;&nbsp;4 Passengers <?php
/*		echo $this->Form->input('w_4', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][w_4][]','div' => false));
		*/?>&nbsp;&nbsp;5 Passengers <?php
/*		echo $this->Form->input('w_5', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][w_5][]', 'div' => false));
		*/?>&nbsp;&nbsp;6 Passengers <?php
/*		echo $this->Form->input('w_6', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][w_6][]', 'div' => false));
		*/?></div><?php
/*		$i = 1;
		foreach ($children as $child) {
		*/?><br><br>Mile Up To <?php
/*		echo $this->Form->input('distance',array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][distance][]', 'value' => $child['FareSetting']['distance'], 'div' => false));
		*/?> Per Mile <?php
/*			echo $this->Form->input('s_4', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_4][]', 'value' =>$child['FareSetting']['s_4'],'div' => false));
		*/?><br><br><label><strong>Standard Car</strong></label>5 Passengers <?php
/*		echo $this->Form->input('s_5', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_5][]', 'value' => $child['FareSetting']['s_5'], 'div' => false));
			*/?>&nbsp;&nbsp;6 Passengers <?php
/*		echo $this->Form->input('s_6', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_6][]', 'value' => $child['FareSetting']['s_6'], 'div' => false));
		*/?>&nbsp;&nbsp;7 Passengers<?php
/*		echo $this->Form->input('s_7', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_7][]', 'value' => $child['FareSetting']['s_7'], 'div' => false));
		*/?>&nbsp;&nbsp;8 Passengers <?php
/*		echo $this->Form->input('s_8', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_8][]', 'value' => $child['FareSetting']['s_8'], 'div' => false));
		*/?><br><br><label><strong>Estate Car</strong></label><?php
/*		*/?>4 Passengers <?php
/*		echo $this->Form->input('e_4', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_4][]', 'value' => $child['FareSetting']['e_4'], 'div' => false));
		*/?>&nbsp;&nbsp;5 Passengers <?php
/*		echo $this->Form->input('e_5', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_5][]', 'value' => $child['FareSetting']['e_5'], 'div' => false));
		*/?>&nbsp;&nbsp;6 Passengers <?php
/*		echo $this->Form->input('e_6', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_6][]', 'value' => $child['FareSetting']['e_6'], 'div' => false));
		*/?>&nbsp;&nbsp;7 Passengers <?php
/*		echo $this->Form->input('e_7', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_7][]', 'value' => $child['FareSetting']['e_7'], 'div' => false));
		*/?>&nbsp;&nbsp;8 Passengers <?php
/*		echo $this->Form->input('e_8', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_8][]', 'value' => $child['FareSetting']['e_8'], 'div' => false));
		*/?><br><br><label><strong>Wheelchair</strong></label>
		3 Passengers <?php
/*		echo $this->Form->input('w_3', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][w_3][]', 'value' => $child['FareSetting']['w_3'],'div' => false));
		*/?>&nbsp;&nbsp;4 Passengers <?php
/*		echo $this->Form->input('w_4', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][w_4][]', 'value' => $child['FareSetting']['w_4'],'div' => false));
		*/?>&nbsp;&nbsp;5 Passengers <?php
/*		echo $this->Form->input('w_5', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][w_5][]', 'value' => $child['FareSetting']['w_5'], 'div' => false));
		*/?>&nbsp;&nbsp;6 Passengers <?php
/*		echo $this->Form->input('w_6', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][w_6][]', 'value' => $child['FareSetting']['w_6'], 'div' => false));
		*/?><br><br><?php
/*		}
		echo $this->Form->input('Add More',array('type' => 'button', 'label' => false, 'value' =>'Add More', 'id' => 'AddMore'));
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
--><?php /*echo $this->element('menu'); */?>
