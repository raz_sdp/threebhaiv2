<div class="fareSettings form">
<?php echo $this->Form->create('FareSetting'); ?>
	<fieldset>
		<legend><?php echo __('Add Fare Setting'); ?></legend>
	<?php
		echo $this->Form->input('from_postcode');
		echo $this->Form->input('from_airport');
		echo $this->Form->input('to_postcode');
		echo $this->Form->input('to_airport');
		echo $this->Form->input('distance');
		echo $this->Form->input('weeks');
		echo $this->Form->input('time_from');
		echo $this->Form->input('time_to');
		echo $this->Form->input('s_4');
		echo $this->Form->input('s_5');
		echo $this->Form->input('s_6');
		echo $this->Form->input('s_7');
		echo $this->Form->input('s_8');
		echo $this->Form->input('e_4');
		echo $this->Form->input('e_5');
		echo $this->Form->input('e_6');
		echo $this->Form->input('e_7');
		echo $this->Form->input('e_8');
		echo $this->Form->input('w_3');
		echo $this->Form->input('w_4');
		echo $this->Form->input('w_5');
		echo $this->Form->input('w_6');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Fare Settings'), array('action' => 'index')); ?></li>
	</ul>
</div>
