<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Fixed Fare</h3>
                        <button class="btn btn-warning btn-lg pull-right"><?php echo $this->Html->link('Add Fixed Fare', array('controller' => 'fare_settings', 'action' => 'fixedadd', 'admin' => true, 'prefix' => 'admin'))?></button>

                    </div>

                    <div class="box-body">
					<div class="fareSettings index">

                        <div class="table-responsive">
						<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
							<tr>
								<th><?php echo $this->Paginator->sort('pick_up', 'From'); ?></th>
								<th><?php echo $this->Paginator->sort('destination', 'To'); ?></th>
								<th><?php echo $this->Paginator->sort('holidaytype_id','Holiday Type'); ?></th>
								<th><?php echo $this->Paginator->sort('weeks','Weekdays'); ?></th>
								<th><?php echo $this->Paginator->sort('time_from', 'Start Time'); ?></th>
								<th><?php echo $this->Paginator->sort('time_to', 'End Time'); ?></th>
								<th><?php echo $this->Paginator->sort('s_4','Base Fare'); ?></th>
								<th><?php echo $this->Paginator->sort('created'); ?></th>
								<th class="actions"><?php echo __(''); ?></th>
							</tr>
							<?php // pr($fareSettings);?>
							<?php foreach ($fareSettings as $fareSetting): ?>
								<tr>
									<td><?php echo h($fareSetting['FareSetting']['pick_up']); ?>&nbsp;</td>
									<td><?php echo h($fareSetting['FareSetting']['destination']); ?>&nbsp;</td>
									<td><?php if($fareSetting['Holidaytype']['holiday_type'] != Null) {
										echo h($fareSetting['Holidaytype']['holiday_type']);
									} else echo h('N/A');
									?></td>

									<?php if($fareSetting['FareSetting']['weeks'] != Null) {
										$weekdays =  array('0' => 'Sunday', '1' => 'Monday','2' => 'Tuesday','3' => 'Wednesday','4' => 'Thrusday','5' => 'Friday','6' => 'Saturday');
										$day = trim($fareSetting['FareSetting']['weeks'], '#');
										$wks = explode('#', $day);
										?>
										<td><?php foreach ($wks as $key) {
											echo $weekdays [$key];?>
											<br><?php
										}
										?>&nbsp;</td>
									<?php } else { ?>
										<td><?php echo ('N/A'); ?>&nbsp;</td>
									<?php }
									$c_date = date_create($fareSetting['FareSetting']['created']);
									$s_time = date_create($fareSetting['FareSetting']['time_from']);
									$e_time = date_create($fareSetting['FareSetting']['time_to']);
									?>
									<td><?php echo date_format($s_time, 'g:i A'); ?>&nbsp;</td>
									<td><?php echo date_format($e_time, 'g:i A'); ?>&nbsp;</td>
									<td><?php echo $this->Number->currency($fareSetting['FareSetting']['s_4'], 'GBP'); ?>&nbsp;</td>
									<td><?php echo date_format($c_date,'d/m/Y'); ?>&nbsp;</td>
									<td class="actions">
										<?php echo $this->Html->link(__('Edit'), array('action' => 'fixededit', $fareSetting['FareSetting']['id']),['class' => 'btn btn-info']); ?>
										<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $fareSetting['FareSetting']['id']), ['class' => 'btn btn-danger'], __('Are you sure you want to delete settings for %s to %s?', $fareSetting['FareSetting']['pick_up'], $fareSetting['FareSetting']['destination'])); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</table>
                            </div>
						<p class="pull-right">
							<?php
							echo $this->Paginator->counter(array(
								'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
							));
							?>	</p>
							<div class="clearfix"></div>
							<div class="paging">
								<ul class="pagination pagination-sm no-margin pull-right">
									<?php
									echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
									echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
									echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
									?>
								</ul>
							</div>
						</div>


					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->


				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
