<div class="phonebooks view">
<h2><?php echo __('Phonebook'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($phonebook['Phonebook']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($phonebook['User']['name'], array('controller' => 'users', 'action' => 'view', $phonebook['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Number'); ?></dt>
		<dd>
			<?php echo h($phonebook['Phonebook']['number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rent'); ?></dt>
		<dd>
			<?php echo h($phonebook['Phonebook']['rent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($phonebook['Phonebook']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($phonebook['Phonebook']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Phonebook'), array('action' => 'edit', $phonebook['Phonebook']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Phonebook'), array('action' => 'delete', $phonebook['Phonebook']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $phonebook['Phonebook']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Phonebooks'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phonebook'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
