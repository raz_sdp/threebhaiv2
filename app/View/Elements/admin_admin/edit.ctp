<?php
/**
 * Created by PhpStorm.
 * User: mukul
 * Date: 6/28/18
 * Time: 11:03 AM
 */ ?>
<div class="row">
    <div class="col-md-12">
        <div class="form-group has-feedback">
            <?php
            echo $this->Form->input('name', array(
                'class' => 'form-control',
                'id' => 'inputName',
                'type' => 'textfile',
                'placeholder' => 'name',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'

            ));
            ?>

        </div>

        <div class="form-group has-feedback">
            <?php echo $this->Form->input('email', array(
                'class' => 'form-control',
                'id' => 'inputName',
                'placeholder' => 'email',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>
        <div class="form-group has-feedback">
            <?php echo $this->Form->input('address', array(
                'class' => 'form-control',
                'id' => 'inputName',
                'placeholder' => 'address',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'

            )); ?>
        </div>
        <div class="form-group has-feedback ">
            <?php echo $this->Form->input('mobile', array(
                'class' => 'form-control',
                // 'id'=>'inputName',
                'placeholder' => 'mobile',
                'type' => 'number',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>

        <div class="form-group">
            <?php echo $this->Form->input('password', array(
                'class' => 'form-control',
                'value' => '',
                'id' => 'inputName',
                'required' => '',
                'placeholder' => 'Enter new password')); ?>
        </div>
    </div>
</div>