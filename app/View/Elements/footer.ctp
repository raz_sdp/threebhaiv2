<script type="text/javascript" >
    var itunes_link='<?php echo $header_links[0]['Setting']['itunes_link']?>';
    var android='<?php echo $header_links[0]['Setting']['googleplay_link']?>';

</script>
<div data-type="background" data-speed="10" class="row partthree" id="third">
    <div id="download" class="container aboutpart des">
        <h2 id="about" style="color: <?php echo !empty($footer['color']) ? $footer['color'] : '#ffd800'; ?> !important;">
            <?php if(!empty($footer['logo_icon'])){ ?>
                <img src="<?php echo $this->Html->url($footer['logo_icon'], true)?>">&nbsp;
            <?php } ?>
            <?php echo $footer['title']; ?>
        </h2>
        <div id="aboutdetails">
            <?php echo $footer['content']; ?>
        </div>
    </div>

    <div class="app-dwn">
        <button class="dwn-iphone">test</button>
        <button class="dwn-and">test</button>
    </div>
    <div class="downloading-bnt">
        <div class="downloading">

            <?php echo $this->Html->button(array('class' => 'dwn-iphone'));?>
            <?php echo $this->Html->button(array('class' => 'dwn-and'));?>

        </div>
    </div>
</div>
<div data-type="background" data-speed="10" class="row footer">
    <div class="container-fluid">
        <div class="s-icon text-center">
            <?php echo $this->Html->link($this->Html->image('message.png', array('alt' => 'CakePHP', 'id'=>'msg')),'mailto:'. $header_links[0]['Setting']['website_contact_email'], array('escape' => false));?>
            <?php echo $this->Html->link($this->Html->image('facebook.png', array('alt' => 'CakePHP', 'id'=>'fb')),$header_links[0]['Setting']['facebook_link'], array('escape' => false, 'target' => '_blank'));?>
            <?php echo $this->Html->link($this->Html->image('twitter.png', array('alt' => 'CakePHP', 'id'=>'twtr')),$header_links[0]['Setting']['twitter_link'], array('escape' => false, 'target' => '_blank'));?>
            <br/>
            <br/>
            <br/>
            <a class="footer_button" href="<?php echo $this->Html->url('/vendor_registration'); ?>">Company Registration</a>
            &nbsp;&nbsp;
            <a class="footer_button" href="<?php echo $this->Html->url('/distributor_registration'); ?>">Distributor Registration</a>
        </div>

        <div class="footertext">

            <?php
            $linkHtmls1st = array();
            $linkHtmls2nd = array();
            //    echo '<pre>';
            //    print_r($links);
            //    echo '</pre>';
            foreach ($links as $link) {
                if($link['Staticpage']['slug'] == 'contact'){
                    $lnk = $this->Html->link($link['Staticpage']['menu_title'],'mailto:'. $header_links[0]['Setting']['website_contact_email']);
                }
                else if($link['Staticpage']['slug'] == 'login' || $link['Staticpage']['slug'] == 'signup') {
                    $lnk = $this->Html->link($link['Staticpage']['menu_title'], '#', array('class' => $link['Staticpage']['slug'].'-link'));
                }
                else if($link['Staticpage']['slug'] == 'vendor_login'){
                    $lnk = '<a target="_blank" href ="'.$this->Html->url('/admin/users/login').'">'.$link['Staticpage']['menu_title'].'</a>';
                }
                else {
                    $lnk = $this->Html->link($link['Staticpage']['menu_title'], array('controller' => 'pages', 'action' => 'display', $link['Staticpage']['slug']));
                }

                if($link['Staticpage']['sort_order'] >= 100 && $link['Staticpage']['sort_order'] < 200)
                    $linkHtmls1st[] = $lnk;
                else
                    $linkHtmls2nd[] = $lnk;

            }?>
            <style>
                .footer_button{
                    background-color: #e4b057;
                    border: none;
                    padding: 15px !important;
                    font-weight: bolder;
                    font-size: 15px;
                    /*box-shadow: 0px 0px 15px #ffd800;*/
                    border-radius: 50px;
                    text-decoration: none !important;
                    color: #000;
                }
                .footer_button:hover{
                    color: #000;
                }
            </style>
            <div class="col-md-2 text-right">

            </div>
            <div class="col-md-8">
                <div class="f-link text-center">
                    <?php echo implode($linkHtmls1st);?>
                </div>
                <div class="f-link text-center">
                    <?php echo implode($linkHtmls2nd);?>
                </div>
            </div>
            <div class="col-md-2 text-center">

            </div>
            <div class="row"></div>
            <div class="copy-writh">
                <p class="text-center copyright">&copy; <?php echo date('Y'); ?> CabbieAppUK Limited. All Rights Reserved.</p>
            </div>

        </div>
    </div>
    <div class="car"></div>
    <div>