
<header class="main-header">
<!-- Logo -->
<a href="index2.html" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>T</b>B</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">ThreeBhai</span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
<!-- Sidebar toggle button-->
<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
</a>

<div class="navbar-custom-menu">
<ul class="nav navbar-nav">

<!-- Notifications: style can be found in dropdown.less -->

<!-- User Account: style can be found in dropdown.less -->
<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
<!--        <img src="img/user2-160x160.jpg" class="user-image" alt="User Image">-->
        <?php
        if(AuthComponent::user(['type']) == 'vendor'){?>
            <img class="user-image" src="<?php echo empty(AuthComponent::user()['image'])? $this->Html->url('/img/avatar/default.png'):$this->Html->url('/files/vendor_infos/'.AuthComponent::user()['image']) ?>" alt="" >
        <?php } else { ?>
            <img class="user-image" src="<?php echo $this->Html->url('/img/avatar/default.png') ?>" alt="" >
        <?php } ?>
        <span class="hidden-xs"><?php echo(AuthComponent::user(['name'])); ?></span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
<!--            <img src="img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
            <?php
            if(AuthComponent::user(['type']) == 'vendor'){?>
                <img class="user-image" src="<?php echo empty(AuthComponent::user()['image'])? $this->Html->url('/img/avatar/default.png'):$this->Html->url('/files/vendor_infos/'.AuthComponent::user()['image']) ?>" alt="" >
            <?php } else { ?>
                <img class="user-image" src="<?php echo $this->Html->url('/img/avatar/default.png') ?>" alt="" >
            <?php } ?>
<!--            --><?php //echo $this->Html->image('/img/avatar/default.png', ['class' => 'user-image','alt' => ''])?>
            <p>
                <?php if(AuthComponent::user(['type']) == 'vendor'){ ?>
                <?php echo AuthComponent::user(['name']) ?></br>
                    <?php //echo $this->Html->image('/img/company_name.png', ['class' => 'user-image','alt' => ''])?>
                <?php echo AuthComponent::user(['company_name']) ?>
                <small><?php echo AuthComponent::user(['branch_number'])?></small>
                <?php } else { ?>
                    <?php echo AuthComponent::user(['name']) ?></br>
                    <?php echo AuthComponent::user(['type']) ?>
                    <small><?php echo AuthComponent::user(['email'])?></small>

                <?php } ?>
            </p>
        </li>
        <!-- Menu Body -->
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="<?php echo $this->Html->url('/admin/users/profile', true)?>" class="btn btn-default btn-flat">Profile</a>
            </div>
            <div class="pull-right">
                <a href="<?php echo $this->Html->url('/admin/users/logout', true)?>" class="btn btn-default btn-flat">Sign out</a>
            </div>
        </li>
    </ul>
</li>
<!-- Control Sidebar Toggle Button -->
</ul>
</div>
</nav>
</header>
