
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel" style="height: 90px;">
            <div class="pull-left image" style="margin-top: 15px">
                <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
                <?php
                if(AuthComponent::user(['type']) == 'vendor'){?>
                    <img class="img-circle" src="<?php echo empty(AuthComponent::user()['image'])? $this->Html->url('/img/avatar/default.png'):$this->Html->url('/files/vendor_infos/'.AuthComponent::user()['image']) ?>" alt="" >
                <?php } else { ?>
                    <img class="img-circle" src="<?php echo $this->Html->url('/img/avatar/default.png') ?>" alt="" >
                <?php } ?>
            </div>
            <?php
            if(AuthComponent::user(['type']) =='admin'){

            ?>
            <div class="pull-left info">
                <p><?php echo AuthComponent::user(['name']) ?></p>
                <a href="" class="btn btn-warning">online</a>

            </div>
            <?php } else {
                $wallet = AppHelper::wallet($id = AuthComponent::user(['id']))
                ?>
            <div class="pull-left info">
                <p>A/c Balance &nbsp;<span style="font-size: 18px;"><?php echo!empty($wallet['Wallet']['available_balance'])? $wallet['Wallet']['available_balance']:0; ?></span></p>
                <a href="<?php echo $this->Html->url('/admin/users/profile', true) ?>" class="btn btn-warning">Top Up</a>
                </div>
            <?php } ?>
        </div>
        <!-- search form -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

            <!-- Manage Dashboard-->
            <li>
                <a href="<?php echo $this->Html->url("/dashboard", true) ?>">
                    </i> <span>Dashboard</span>
                </a>
            </li>
            <!--/Manage Dashboard-->
            <?php
            //pr($options);die;
            $ctrl = strtolower($this->params['controller']);
            $action = explode('_', strtolower($this->params['action']));
            @$parameter = strtolower($this->params['pass']['0']);

            //pr($parameter);die;

            $options = AppHelper::getMenus();
            @$charge_id = AppHelper::get_charge_id();
            //pr($options);

            foreach ($options as $key => $menu):
                //pr($menu);die;
                //$menu = array_shift($menu);
                 $class = explode('/', $menu);
                //pr($class);die;
                ?>
                <li class="<?php echo $ctrl == $class['0'] && $action['1'] == $class['1'] && @$parameter == @$class['2'] ? 'active' :'' ?>">
<!--                    --><?php
                    $res = $menu == 'charges/edit' ? 'charges/edit/'.$charge_id['Charge']['id']:$menu;
                    ?>
                    <a href="<?php echo $this->Html->url('/admin/' .$res , true); ?>">
                        <span><?php echo $key; ?></span>
                    </a>
                </li>
            <?php endforeach; ?>
            <!--Manage Users-->
<!--            <li class="treeview --><?php //echo $ctrl == 'users' ? 'active menu-open' : '' ?><!--">-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-users"></i> <span>Manage Users</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--                </a>-->
<!--                <ul class="treeview-menu">-->
<!--                    <li class="--><?php //echo $ctrl == 'users' && $action == 'admin_add' ? 'active' : '' ?><!--"><a-->
<!--                            href="--><?php //echo $this->Html->url("admin/users/add", true) ?><!--"><i-->
<!--                                class="fa fa-user-plus"></i> Add New</a></li>-->
<!--                    <li class="--><?php //echo $ctrl == 'users' && $action == 'admin_index' ? 'active' : '' ?><!--"><a-->
<!--                            href="--><?php //echo $this->Html->url("/admin/users/index", true) ?><!--"><i-->
<!--                                class="fa fa-list"></i>List</a></li>-->
<!--                </ul>-->
<!--            </li>-->
            <!--/Manage Users-->


            <!-- Manage Lectures   -->
            <!-- <li class="treeview <?php //echo $ctrl=='lectures' ? 'active menu-open' : ''?> ">
    <a href="#">
        <i class="fa  fa-file"></i> <span>Manage Lectures</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php //echo $ctrl=='lectures'&& $action=='admin_add' ? 'active' : ''?>"><a href="<?php //echo $this->Html->url("/admin/lectures/add", true) ?>"><i
                    class="fa fa-plus"></i> Add New</a></li>
        <li class="<?php //echo $ctrl=='lectures' && $action=='admin_index'? 'active' : ''?>"><a href="<?php //echo $this->Html->url("/admin/lectures/index", true) ?>"><i
                    class="fa fa-list"></i>List</a></li>
    </ul>
</li> -->
            <!-- /Manages Lectures-->


            <!-- Manage Question   -->

            <!--/Manages Question-->

            <!--Manages Subjects    -->

            <!--/Manages Subjects    -->
            <!--Manages Result   -->


            <!--/Manages Tests Question    -->


            <!--Manages Usersanswer    -->


            <!--<li class="treeview <?php /*echo $ctrl=='usersanswers' ? 'active menu-open' : ''*/ ?>">
    <a href="#">
        <i class="fa fa-pencil-square-o"></i> <span>Manage Usersanswer</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php /*echo $ctrl=='usersanswer'&& $action=='admin_add' ? 'active' : ''*/ ?>""><a href="<?php /*echo $this->Html->url("/admin/Usersanswers/add", true) */ ?>"><i
                    class="fa fa-plus"></i> Add New</a></li>
        <li class="<?php /*echo $ctrl=='usersanswer'&& $action=='admin_index' ? 'active' : ''*/ ?>""><a href="<?php /*echo $this->Html->url("/admin/Usersanswers/index", true) */ ?>"><i
                    class="fa fa-list"></i>List</a></li>
    </ul>
</li>-->

            <!--/Manages Usersanswer    -->


        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
