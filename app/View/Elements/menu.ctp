<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Admins'), array('controller' => 'users', 'action' => 'index', 'admin' => true, 'admin')); ?></li>
		<li><?php echo $this->Html->link(__('Drivers'), array('controller' => 'users', 'action' => 'index', 'admin' => true, 'driver')); ?></li>
		<li><?php echo $this->Html->link(__('Passengers'), array('controller' => 'users', 'action' => 'index', 'admin' => true, 'passenger')); ?></li>
		<li><?php echo $this->Html->link(__('Zones'), array('controller' => 'zones', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('Twilio Numbers'), array('controller' => 'zones', 'action' => 'available_phone_numbers', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('Geophonebooks'), array('controller' => 'geophonebooks', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('VR Zone Settings'), array('controller' => 'vr_settings', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('Phone Setting'), array('controller' => 'phone_settings', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('Bookings'), array('controller' => 'bookings', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('Cancelled Bookings'), array('controller' => 'archived_bookings', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('Call Logs'), array('controller' => 'phone_queues', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('Bars'), array('controller' => 'bars', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('Transactions'), array('controller' => 'transactions', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('Balance Transfer Status'), array('controller' => 'transactions', 'action' => 'reqtransferamount', 'admin' => true, 'transfer_pending')); ?></li>
<!--		<li>--><?php //echo $this->Html->link(__('Rent Items'), array('controller' => 'rent_items', 'action' => 'index', 'admin' => true)); ?><!--</li>-->
		<li><?php echo $this->Html->link(__('Fixed Fare'), array('controller' => 'fare_settings', 'action' => 'fixedindex', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('Fare By Miles'), array('controller' => 'fare_settings', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('Settings'), array('controller' => 'settings', 'action' => 'edit', 'admin' => true, 1)); ?></li>
		<li><?php echo $this->Html->link(__('Paypal Settings'), array('controller' => 'settings', 'action' => 'paypal', 'admin' => true, 1)); ?></li>
		<li><?php echo $this->Html->link(__('Driver Decline Reasons'), array('controller' => 'driver_decline_reasons', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('Holidays'), array('controller' => 'holidaytypes', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('CMS'), array('controller' => 'staticpages', 'action' => 'index', 'admin' => true)); ?></li>
		<li><?php echo $this->Html->link(__('CMS Links'), array('controller' => 'settings', 'action' => 'share_links', 'admin' => true, 1)); ?></li>
		<li><?php echo $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout', 'admin' => true)); ?></li>
	</ul>
</div>