<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/18/18
 * Time: 11:55 AM
 */
?>
<div class="row">
    <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6">
        <div class="form-group has-feedback has-feedback">
            <label class="control-label">Full Name<sup
                    style="color: red;">*</sup> </label>
            <input name="data[User][name]" type="text" class="form-control" id=""
                   placeholder="Your Full name" required="required">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>

        <div class="form-group">
            <label class="control-label">Image </label>
            <input autocomplete="off" name="data[User][image]" type="file" class="form-control">
        </div>

        <div class="form-group has-feedback has-feedback">
            <label class="control-label">Company Name (<span style="color: royalblue">Name to display in Drivers & Passengers App</span>)<sup
                    style="color: red;">*</sup> </label>
            <input name="data[User][company_name]" type="text" class="form-control" id=""
                   placeholder="Your business name" required="required">

             <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback has-feedback">
            <label class="control-label">Company Id (Unique Name)<sup style="color: red;">*</sup> </label>
            <input data-remote="<?php echo $this->Html->url('/users/validate_input', true); ?>"
                   data-error="Bruh, that email address is invalid"
                   name="data[User][company_slug]" type="text" class="form-control" id=""
                   placeholder="e.g. cabbieappuk" required="required">
            <sub style="color: darkblue;">For multiple words use '-' and lowercase words</sub>

             <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <!--<div class="form-group has-feedback">
            <label class="control-label">App Name <br/><sub>Drivers & Passengers</sub><sup style="color: red;">*</sup> </label>
            <input autocomplete="off" type="text" class="form-control"  placeholder="Name to display in apps"
                   required="required">

             <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>-->
        <div class="form-group has-feedback">
            <label class="control-label">Email<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" data-remote="<?php echo $this->Html->url('/users/validate_input', true); ?>"
                   type="email" class="form-control"  name="data[User][email]" data-error="Invalid Email address or this email already exist."
                   placeholder="For login & reset password" required="required">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group has-feedback">
            <label class="control-label">User ID<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" data-remote="<?php $this->Html->url('/users/validate_input', true); ?>"
                   type="text" class="form-control"  name="data[User][username]" data-error="This User ID already exist."
                   placeholder="e.g: riley" required="required">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group has-feedback">
            <label class="control-label">Paaword<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][password]" type="password" class="form-control"
                    placeholder="password">
        </div>

        <!--<div class="form-group has-feedback">
            <label class="control-label">Password<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" type="password" name="data[User][password]" class="form-control" id="password"
                   placeholder="Password" required="required">

             <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label class="control-label">Cofirm Password<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" name="confirm_password" type="password" class="form-control" id=""
                   placeholder="Retype Password" required="required" data-match="#password"
                   data-match-error="Whoops, these don't match">

             <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>-->
        <div class="form-group has-feedback">
            <label class="control-label">Phone No.<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" data-remote="<?php echo $this->Html->url('/users/validate_input', true); ?>" name="data[User][mobile]" type="text" class="form-control" data-error="This phone no already exist."
                   placeholder="Mobile or land line" required="required">

             <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
             <div class="help-block with-errors"></div>
        </div>        

    </div>
    <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6">
        <div class="form-group has-feedback">
            <label class="control-label">Business Address<sup style="color: red;">*</sup> </label>
            <input onkeyup="addNewAddress('address')" autocomplete="off" name="data[User][address]" type="text" data-remote="<?php echo $this->Html->url('/users/validate_address', true) ?>" class="form-control" id="address"
                   placeholder="Full Address" data-error="It's not a valid address" required="required">
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label class="control-label">Hackney Carriage Taxi Driver <span class="des"
                                                                            style="color: #fff;">or</span>
                Private Hire Operator License Number<sup style="color: red;">*</sup> </label>

            <input autocomplete="off" name="data[User][vendor_license_number]" type="text" class="form-control"
                    placeholder="License Number" required="required">

             <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

        </div>
        <div class="form-group has-feedback">
            <label class="control-label">License Expiry Date<sup style="color: red;">*</sup> </label>

            <div class="input-group date" data-date-format="dd.mm.yyyy">
                <input  type="text" class="form-control" name="data[User][vendor_license_exp_date]" id="vendor_license_exp_date" placeholder="Pick a Date" required="required">
                <div class="input-group-addon" >
                    <span class="glyphicon glyphicon-calendar"></span>
                </div>
            </div>
            <!--<input autocomplete="off" name="data[User][vendor_license_exp_date]" type="text" class="form-control hasDatepicker"
                   id="datetext" placeholder="Pick a Date" required="required">-->


             <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        </br>
        <div class="form-group has-feedback">
            <label class="control-label">Licensing Authority<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][vendor_license_authority]" type="" class="form-control" id=""
                   placeholder="License issued by" required="required">

             <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label class="control-label">Picture of the license<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][vendor_license_image]" type="file" class="form-control" id=""
                   placeholder="">
        </div>
        <div class="form-group has-feedback">
            <label class="control-label">Authorised Person Name<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][vendor_license_holder_name]" type="" class="form-control"
                    placeholder="License Holder Name">
        </div>

        <div class="form-group has-feedback">
            <label class="control-label">Branch No.<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" data-remote="<?php echo $this->Html->url('/users/validate_input', true); ?>" name="data[User][branch_number]" type="text" class="form-control" data-error="This Branch number already exist."
                   placeholder="Branch Number" required="required">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>
        </div>
        
        <!--<div class="form-group has-feedback">
            <label class="control-label">Promoter Code <br/><sup class="" style="color: #fff;">(Recomended)</sup><sup
                    style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][promoter_code]" type="text" class="form-control" id=""
                   placeholder="Ask who told you to join">
        </div>-->
    </div>
</div>