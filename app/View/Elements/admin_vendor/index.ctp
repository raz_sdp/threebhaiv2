<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/18/18
 * Time: 1:55 PM
 */
//pr($users);die;
?>
<div class="table-responsive col-md-12">
<table cellpadding="0" cellspacing="0" id="example2" class="table table-bordered table-hover">
    <tr>
        <th><?php echo $this->Paginator->sort('Branch Name'); ?></th>
        <th><?php echo $this->Paginator->sort('Branch Number'); ?></th>
        <th><?php echo $this->Paginator->sort('Active Drivers'); ?></th>
        <th><?php echo $this->Paginator->sort('Phone Numbers'); ?></th>
        <th><?php echo $this->Paginator->sort('A/C Balance'); ?></th>
        <th><?php echo $this->Paginator->sort('Enabled'); ?></th>

        <th class="actions"><?php echo __('Actions'); ?></th>

    </tr>
    <?php foreach ($users as $user): ?>
        <?php //pr($user); ?><?php //endforeach; die;?>
        <tr>
            <td><?php echo h($user['User']['company_name']); ?>&nbsp;</td>
            <td><?php echo h($user['User']['branch_number']); ?>&nbsp;</td>
            <?php $active_driver = AppHelper::active_driver($user['User']['branch_number']); ?>
            <td><?php echo h($active_driver); ?>&nbsp;</td>
            <?php $number = AppHelper::active_number($user['User']['id']) ?>
            <td><?php echo !empty($number)?$number:'0' ?>&nbsp;</td>
            <?php $balance = AppHelper::wallet($user['User']['id']) ?>
            <td><span style="font-size: 18px;"><?php echo !empty($balance)?' '.$balance['Wallet']['available_balance']:' 0' ?></span>&nbsp;</td>
            <td><?php echo $user['User']['is_enabled'] == '1'? 'Yes' : 'No'; ?>&nbsp;</td>

<!--            --><?php //if($type == 'driver') { ?>
<!--                <td>--><?php ////echo h($user['User']['ref_promoter_code']); ?><!--&nbsp;</td>-->
<!--                <td>--><?php //echo h($user['User']['running_distance']); ?><!--&nbsp;</td>-->
<!--                <td>--><?php //echo h($user['User']['voip_no']); ?><!--&nbsp;</td>-->
<!--            --><?php //} ?>
<!--            <td>-->
<!--                --><?php //if($type == 'driver'){
//                    if(!empty($user['User']['is_enabled'])) {
//                        echo h('Yes');
//                    } else {
//                        echo h('No');
//                    }
//                }
//                ?>
<!--                &nbsp;-->
<!--            </td>-->
            <td class="actions">
                <?php $value = $user['User']['is_enabled'] == '1'? '0' : '1'; ?>
                <?php echo $this->Html->link(__($user['User']['is_enabled'] == '1'? 'Disable' : 'Enable'), array('action' => 'EnableOrDisable_member',$user['User']['id'],$value,'vendor'),['class' => 'btn btn-primary']); ?>
<!--                <button class="btn btn-primary" id="status"><a href="" >--><?php //echo $user['User']['is_enabled'] == '1'? 'Disable' : 'Enable'; ?><!--</a></button>-->
                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'], $type), ['class' => 'btn btn-info']); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'admin_delete', $user['User']['id']), ['class' => 'btn btn-danger'],null, __('Are you sure you want to delete %s?', $user['User']['name'])); ?>
                <?php
                    echo $this->Html->link(__('Give Free Credit'), array('action' => 'freetoup', $user['User']['id']), ['class' => 'btn btn-success']);
                ?>
                <?php echo $this->Html->link(__('Permissions'), array('controller' => 'users_acls','action' => 'index', $user['User']['id']), ['class' => 'btn btn-warning']); ?>
                <?php echo $this->Html->link(__('Invoice'), array('controller' => 'transactions','action' => 'invoice', $user['User']['id']), ['class' => 'btn btn-success']); ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
</div>
