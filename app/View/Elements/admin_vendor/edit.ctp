<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/30/18
 * Time: 12:48 PM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/18/18
 * Time: 11:55 AM
 */
?>
<div class="row">
    <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6">
        <div class="form-group has-feedback has-feedback">
            <label class="control-label">Full Name<sup
                    style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][name]" type="text" class="form-control" id=""
                   placeholder="Your Full name" required="required" value="<?php echo $this->data['User']['name']?>">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group">
            <label class="control-label">Image </label>
            <input autocomplete="off" name="data[User][image]" type="file" class="form-control">
        </div>
        <div class="form-group">
            <?php echo $this->Html->image('/files/vendor_infos/'.$this->data['User']['image'], ['width' => '200', 'height' => '200'])?>
        </div>
        <div class="form-group has-feedback has-feedback">
            <label class="control-label">Company Name (<span style="color: royalblue">Name to display in Drivers & Passengers App</span>)<sup
                    style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][company_name]" type="text" class="form-control" id=""
                   placeholder="Your business name" required="required" value="<?php echo $this->data['User']['company_name']?>">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback has-feedback">
            <label class="control-label">Company Id (Unique Name)<sup style="color: red;">*</sup> </label>
            <input data-remote="<?php echo $this->Html->url('/users/validate_input', true); ?>"
                   data-error="Bruh, that email address is invalid" autocomplete="off"
                   name="data[User][company_slug]" type="text" class="form-control" id=""
                   placeholder="e.g. cabbieappuk" disabled required="required" value="<?php echo $this->data['User']['company_slug']?>">
            <sub style="color: darkblue;">For multiple words use '-' and lowercase words</sub>

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label class="control-label">Business Address<sup style="color: red;">*</sup> </label>
            <input onkeyup="addNewAddress('address')" autocomplete="off" name="data[User][address]" type="text" class="form-control" id="address"
                   placeholder="Full Address" required="required" value="<?php echo $this->data['User']['address']?>">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label class="control-label">Branch Number<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][branch_number]" type="text" class="form-control" id=""
                   placeholder="Branch Number" disabled required="required" value="<?php echo $this->data['User']['branch_number']?>">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label class="control-label">Password</label>
            <input autocomplete="off" name="data[User][password]" type="password" class="form-control" id=""
                   placeholder="" value="">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>

    </div>
    <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6">
        <div class="form-group has-feedback">
            <label class="control-label">Hackney Carriage Taxi Driver <span class="des"
                                                                            style="color: #fff;">or</span>
                Private Hire Operator License Number<sup style="color: red;">*</sup> </label>

            <input autocomplete="off" name="data[User][vendor_license_number]" type="text" class="form-control"
                   placeholder="License Number" required="required" value="<?php echo $this->data['User']['vendor_license_number']?>">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

        </div>

        <div class="form-group has-feedback">
            <label class="control-label">License Expiry Date<sup style="color: red;">*</sup> </label>

            <div class="input-group date" data-date-format="dd.mm.yyyy">
                <?php
                $myDateTime = DateTime::createFromFormat('Y-m-d', $this->request->data['User']['vendor_license_exp_date']);
                $this->request->data['User']['vendor_license_exp_date'] = $myDateTime->format('d/m/Y');
                ?>
                <input  type="text" class="form-control" name="data[User][vendor_license_exp_date]" id="vendor_license_exp_date" placeholder="Pick a Date" required="required" value="<?php echo $this->data['User']['vendor_license_exp_date']?>">
                <div class="input-group-addon" >
                    <span class="glyphicon glyphicon-calendar"></span>
                </div>
            </div>
            <!--<input autocomplete="off" name="data[User][vendor_license_exp_date]" type="text" class="form-control hasDatepicker"
                   id="datetext" placeholder="Pick a Date" required="required">-->


            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label class="control-label">Licensing Authority<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][vendor_license_authority]" type="" class="form-control" id=""
                   placeholder="License issued by" required="required" value="<?php echo $this->data['User']['vendor_license_authority']?>">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <br>
        <div class="form-group has-feedback">
            <label class="control-label">Picture of the license<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][vendor_license_image]" type="file" class="form-control" id=""
                   placeholder="">
        </div>
        <div class="form-group has-feedback">
            <label class="control-label">Authorised Person Name<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][vendor_license_holder_name]" type="" class="form-control"
                   placeholder="License Holder Name" value="<?php echo $this->data['User']['vendor_license_holder_name']?>">
        </div>
        <div class="form-group has-feedback has-feedback">
            <label class="control-label">Email<sup style="color: red;">*</sup> </label>
            <input data-remote="<?php echo $this->Html->url('/users/validate_input', true); ?>"
                   data-error="Bruh, that email address is invalid" autocomplete="off"
                   name="data[User][email]" type="email" class="form-control" id=""
                   placeholder="e.g. example@domain'com" required="required" value="<?php echo $this->data['User']['email']?>">


            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
       
    </div>
</div>