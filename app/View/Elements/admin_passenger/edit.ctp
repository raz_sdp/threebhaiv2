<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/8/18
 * Time: 10:49 AM
 */ ?>
<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/18/18
 * Time: 11:55 AM
 */
?>
<div class="row">
    <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6">
        <div class="form-group has-feedback has-feedback">
            <label class="control-label">Full Name<sup
                    style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][name]" type="text" class="form-control" id=""
                   placeholder="Your Full name" value="<?php echo $this->request->data['User']['name'] ?>" required="required">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label class="control-label">Password<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][password]" type="password" class="form-control" id="inputPassword"
                   placeholder="Password" required="required">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label class="control-label">Email<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" data-remote="<?php echo $this->Html->url('/users/validate_input', true); ?>"
                   type="email" class="form-control"  name="data[User][email]" data-error="Invalid Email address or this email already exist."
                   placeholder="For login & reset password" value="<?php echo $this->request->data['User']['email'] ?>" required="required">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>
        </div>

    </div>
    <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6">
        <div class="form-group has-feedback">
            <label class="control-label">User ID<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" data-remote="<?php $this->Html->url('/users/validate_input', true); ?>"
                   type="text" class="form-control"  name="data[User][username]" value="<?php echo $this->request->data['User']['username'] ?>"
                   placeholder="e.g: riley" required="required">
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>



        <div class="form-group has-feedback">
            <label class="control-label">Phone No.<sup style="color: red;">*</sup> </label>
            <input autocomplete="off" name="data[User][mobile]" type="text" class="form-control" value="<?php echo $this->request->data['User']['mobile'] ?>"
                   placeholder="Mobile or land line" required="required">

            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>

        <?php if (AuthComponent::user(['type']) == 'admin') {?>
            <div class="form-group has-feedback">
                <label>Select Branch</label>
                <select name="data[User][branch_number]" class="form-control">
                    <?php foreach($branches as $branch){ ?>
                        <option value="<?php echo $branch['User']['branch_number'] ?>" <?php echo $branch['User']['branch_number']== $this->request->data['User']['branch_number'] ? 'selected' : ''?>><?php echo $branch['User']['company_name'] ?></option>
                    <?php } ?>
                </select>


                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            </div>
        <?php   }// pr($branches); ?>
    </div>
</div>