<script type="text/javascript">
    <?php if(empty($auth)){ ?>
    var loged_in = false;
    <?php } else { ?>
    var loged_in = true;
    <?php } ?>
</script>

<?php
echo $this->Html->script(array('popup_login', 'popup_signup', 'passenger_booking'));
?>
<div id="overlay"></div>
<div class="container-fluid">
    <div class="section-one" id="intro">
        <div class="container">
            <div class="row">
                <div class="icon">
                    <?php echo $this->Html->link($this->Html->image('message.png', array('alt' => 'CakePHP', 'id'=>'msg')),'mailto:'. $header_links[0]['Setting']['website_contact_email'], array('escape' => false));?>

                    <?php echo $this->Html->link($this->Html->image('facebook.png', array('alt' => 'CakePHP', 'id'=>'fb')),$header_links[0]['Setting']['facebook_link'], array('escape' => false, 'target' => '_blank'));?>

                    <?php echo $this->Html->link($this->Html->image('twitter.png', array('alt' => 'CakePHP', 'id'=>'twtr')),$header_links[0]['Setting']['twitter_link'], array('escape' => false, 'target' => '_blank'));?>

                    <a href="<?php echo $this->Html->url('/admin/users/login'); ?>" class="login" target="_blank">Login</a>

                </div>
            </div>
        </div>
        <div class="bg-color-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-6  col-sm-6 col-xs-6">
                        <h2 class="text-center">The BEST Taxi and Mini Cab Booking App in the UK.</h2>
                    </div>
                    <div class="col-md-6  col-sm-6 col-xs-6" id="logo">
                        <?php echo $this->Html->link($this->Html->image('logo.png', array('alt' => 'CakePHP')),'home', array('escape' => false));?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-two" id="second">
        <div class="text-form">
            <div class="container">
                <div class="row">
                    <div class="bg-color" style="margin: 0 0 20px 0;">
                        <form class="form-horizontal signUp" role="form" data-toggle="validator" method ="post" action="<?php echo $this->Html->url('/users/vendor_registration')?>">
                            <input type="hidden" name="data[User][type]" value="distributor">
                            <h3 class="signlabel">You're almost done!</h3>
                            <div class="col-md-8 col-md-offset-2"><?php echo $this->Session->flash(); ?></div>
                            <div class="row"></div>
                            <div class="des">
                                <p><b>Register a free account to get access to buy local phone numbers for your business</b><br/><b>Pay As you go account with top up balance by paypall & Card payments!</b></p>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Full Name</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="data[User][name]" required="required"  type="text" class="form-control" id="" placeholder="Name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Full Address</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="data[User][address]" required="required" type="text" class="form-control" id="" placeholder="Address">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Email</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off"  type="email" required="required" class="form-control" id="" name="data[User][email]" placeholder="For Login & Reset Password">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Password</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" type="password" name="data[User][password]" class="form-control" id="password" placeholder="Password" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Cofirm Password</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="confirm_password" required="required" type="password" class="form-control" id="" placeholder="Retype Password" required="required" data-match="#password" data-match-error="Whoops, these don't match">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-1 text-center">
                                <img src="<?php echo $this->Html->url?>img/partition.png" alt="CakePHP" id="partition">
                            </div>
                            <div class="col-md-5 text-left">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Phone Number</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="data[User][mobile]" required="required" type="text" class="form-control" id="" placeholder="Phone Number">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Promoter Code <br/><sup class="" style="color: #fff;">(Recomended)</sup></label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="data[User][promoter_code]" type="text" class="form-control" id="" placeholder="Ask your referrer">
                                    </div>
                                </div>
                                <label class="">Don't have a promoter code? <a href="#">Click Here</a></label>
                                <br/>
                                <br/>
                                <label class="pull-left"><a href="#">Check the Terms and Condition here</a></label>
                                <br/>
                                <br/>
                                <button type="submit" style="background-color: #000; border: none; margin-left: -10px;"><?php echo $this->Html->image('signupbtn.png', array('alt' => 'CakePHP', 'id'=>'')) ?></button>
                                <br/><sup class="" style="color: #fff;">(Make money by promoting the business)</sup>
                            </div>
                            <div class="row"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="popup" style="display:none">
        <div class="header">
            <?php echo $this->Html->image('delete-btn.png', array('alt' => 'CakePHP', 'class' => 'delete')) ?>
            <h1 class="popup-font text-center">You're almost done!</h1>
            <p class="text-center">To complete your booking you need to have an account with us.</p>
            <p class="text-center">You can access your account online or via our App.</p>

        </div>
        <div class="functionality">

        </div>
        <?php //echo $this->Html->image('partition.png', array('alt' => 'CakePHP', 'id'=>'partition')) ?>

        <div class="col-md-5 col-sm-5">
            <div class="row">
                <h3 class="loginlabel">Login</h3>
                <form class="form-horizontal login" role="form">
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Email</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Password</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="password" class="form-control" id="loginpassword" placeholder="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left"></label>
                        <div class="col-sm-8">
                            <?php echo $this->Html->image('loginbtn.png', array('alt' => 'CakePHP', 'id'=>'loginimg')) ?>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="col-md-2 col-sm-2 text-center">
            <?php echo $this->Html->image('partition.png', array('alt' => 'CakePHP', 'id'=>'partition')) ?>
        </div>
        <div class="col-md-5 col-sm-5">
            <div class="row">
                <form class="form-horizontal signUp" role="form">
                    <h3 class="signlabel">Sign Up</h3>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Name</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="text" class="form-control" id="name" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Mobile No</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="text" class="form-control" id="mobileno" placeholder="Mobile No">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Email</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="email" class="form-control" id="signemail" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Password</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="password" class="form-control" id="signpassword" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Cofirm Password</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="password" class="form-control" id="cp" placeholder="Retype Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left"></label>
                        <div class="col-sm-8">
                            <?php echo $this->Html->image('signupbtn.png', array('alt' => 'CakePHP', 'id'=>'signimg')) ?>
                        </div>
                    </div>
            </div>

            </form>
        </div>
    </div>
    <div class="wait-message" style="display:none">
        <h2>Please wait...</h2>
    </div>
    <?php echo $this->element('footer', array('footer' => $footer, 'links' => $links)); ?>
