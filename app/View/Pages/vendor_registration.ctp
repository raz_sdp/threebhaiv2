<script type="text/javascript">
    <?php if(empty($auth)){ ?>
    var loged_in = false;
    <?php } else { ?>
    var loged_in = true;
    <?php } ?>
</script>

<?php
//echo $this->Html->script(array('popup_login', 'popup_signup', 'passenger_booking'));
?>
<div id="overlay"></div>
<div class="container-fluid">
    <div class="section-one" id="intro">
        <div class="container">
            <div class="row">
                <div class="icon">
                    <?php echo $this->Html->link($this->Html->image('message.png', array('alt' => 'CakePHP', 'id'=>'msg')),'mailto:'. $header_links[0]['Setting']['website_contact_email'], array('escape' => false));?>

                    <?php echo $this->Html->link($this->Html->image('facebook.png', array('alt' => 'CakePHP', 'id'=>'fb')),$header_links[0]['Setting']['facebook_link'], array('escape' => false, 'target' => '_blank'));?>

                    <?php echo $this->Html->link($this->Html->image('twitter.png', array('alt' => 'CakePHP', 'id'=>'twtr')),$header_links[0]['Setting']['twitter_link'], array('escape' => false, 'target' => '_blank'));?>

                    <a href="<?php echo $this->Html->url('/admin/users/login'); ?>" class="login" target="_blank">Login</a>

                </div>
            </div>
        </div>
        <div class="bg-color-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-6  col-sm-6 col-xs-6">
                        <h2 class="text-center">The BEST Taxi and Mini Cab Booking App in the UK.</h2>
                    </div>
                    <div class="col-md-6  col-sm-6 col-xs-6" id="logo">
                        <?php echo $this->Html->link($this->Html->image('logo.png', array('alt' => 'CakePHP')),'home', array('escape' => false));?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-two" id="second">
        <div class="text-form">
            <div class="container ">
                <div class="row">
                    <div class="bg-color" style="margin: 0 0 20px 0;">
                        <form class="form-horizontal signUp" role="form" data-toggle="validator" method ="post" action="<?php echo $this->Html->url('/users/vendor_registration')?>" enctype="multipart/form-data">
                            <input type="hidden" name="data[User][type]" value="vendor" >
                            <h3 class="signlabel">You're almost done!</h3>
                            <div class="col-md-8 col-md-offset-2"><?php echo $this->Session->flash(); ?></div>
                            <div class="row"></div>
                            <div class="des">
                                <p><b>Register to get your own despatch system and drivers app for you business</b><br/><b>Pay As you go account with top up balance by paypall & free trial</b></p>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group has-feedback">
                                    <label class="col-sm-4 control-label label-left">Company Name</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="data[User][company_name]" type="text" class="form-control" id="" placeholder="Your business name" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <label class="col-sm-4 control-label label-left">Company Id</label>
                                    <div class="col-sm-8">
                                        <input data-remote="<?php echo $this->Html->url('/users/validate_input', true);?>" data-error="Bruh, that email address is invalid" autocomplete="off" name="data[User][company_slug]" type="text" class="form-control" id="" placeholder="e.g. cabbieappuk" required="required">
                                        <sub style="color: #fff;">For multiple words use '-' and lowercase words</sub>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">App Name <br/><sub>Drivers & Passengers</sub></label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off"  type="text" class="form-control" id="" placeholder="Name to display in apps" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Email</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" data-remote="<?php $this->Html->url('/users/validate_input', true);?>" type="email" class="form-control" id="" name="data[User][email]" placeholder="For login & reset password"  required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Password</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" type="password" name="data[User][password]" class="form-control" id="password" placeholder="Password" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Cofirm Password</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="confirm_password" type="password" class="form-control" id="" placeholder="Retype Password" required="required" data-match="#password" data-match-error="Whoops, these don't match">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Phone No.</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="data[User][mobile]" type="text" class="form-control" id="" placeholder="Mobile or land line"  required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-1 text-center">
                                <img src="<?php echo $this->Html->url('/img/partition.png', true)?>" alt="CakePHP" id="partition">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Business Address</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="data[User][address]" type="text" class="form-control" id="" placeholder="Full Address"  required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Hackney Carriage Taxi Driver <span class="des" style="color: #fff;">or</span> Private Hire Operator License Number</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="data[User][vendor_license_number]" type="text" class="form-control" id="" placeholder="License Number"  required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">License Expiry Date</label>
                                    <div class="col-sm-8 col-xs-7 date-input">
                                        <input autocomplete="off" name="data[User][vendor_license_exp_date]" type="text" class="form-control" id="datetext" placeholder="Pick a Date" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Licensing Authority</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="data[User][vendor_license_authority]" type="" class="form-control" id="" placeholder="License issued by" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Picture of the license</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="data[User][vendor_license_image]" type="file" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Authorised Person Name</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="data[User][vendor_license_holder_name]" type="" class="form-control" id="" placeholder="License Holder Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label label-left">Promoter Code <br/><sup class="" style="color: #fff;">(Recomended)</sup></label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" name="data[User][promoter_code]" type="text" class="form-control" id="" placeholder="Ask who told you to join">
                                    </div>
                                </div>
                            </div>
                            <div class="row"></div>
                            <div class="col-md-6 text-left">
                                <label class="">Don't have a promoter code? <a href="#">Click Here</a></label>
                            </div>
                            <div class="col-md-6 text-left">
                                <label class="pull-left"><a href="#">Check the Terms and Condition here</a></label>
                                <br/>
                                <br/>
                                <button type="submit" style="background-color: #000; border: none; margin-left: -10px;"><?php echo $this->Html->image('signupbtn.png', array('alt' => 'CakePHP', 'id'=>'')) ?></button>
                            </div>
                            <div class="row"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="wait-message" style="display:none">
    <h2>Please wait...</h2>
</div>
<?php echo $this->element('footer', array('footer' => $footer, 'links' => $links)); ?>
