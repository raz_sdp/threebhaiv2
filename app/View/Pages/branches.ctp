<style type="text/css">
    body{
        background-color: #ffffff !important;
        position: relative;
        overflow-x: hidden;
    }
</style>
<table class="table table-responsive">
    <tr>
        <th>Company Name</th>
        <th>Branch Number</th>
    </tr>
    <?php
    //pr($branches);
    foreach ($branches as $branch) {
        ?>
        <tr>
            <td><?php echo $branch['company_name']?></td>
            <td><?php echo $branch['branch_number']?></td>
        </tr>
    <?php
    }
    ?>
</table>
