<script type="text/javascript">
    <?php if(empty($auth)){ ?>
    var loged_in = false;
    <?php } else { ?>
    var loged_in = true;
    <?php } ?>
</script>
<style>
    .carousel-inner > .item > img,
    .carousel-inner > .item > a > img {
        margin: auto;
        height: 300px;
    }
</style>

<?php echo $this->Html->script(array('popup_login', 'popup_signup', 'passenger_booking')); ?>
<div id="overlay"></div>
<div class="container-fluid">
    <div class="section-one" id="intro" style="height: 375px !important;">
        <div class="container">
            <div class="row">
                <div class="icon">
                    <?php echo $this->Html->link($this->Html->image('message.png', array('alt' => 'CakePHP', 'id'=>'msg')),'mailto:'. $header_links[0]['Setting']['website_contact_email'], array('escape' => false));?>

                    <?php echo $this->Html->link($this->Html->image('facebook.png', array('alt' => 'CakePHP', 'id'=>'fb')),@$header_links[0]['Setting']['facebook_link'], array('escape' => false, 'target' => '_blank'));?>

                    <?php echo $this->Html->link($this->Html->image('twitter.png', array('alt' => 'CakePHP', 'id'=>'twtr')),@$header_links[0]['Setting']['twitter_link'], array('escape' => false, 'target' => '_blank'));?>

                    <a href="#" class="login-link login">Login</a>

                </div>
            </div>
        </div>
        <div class="bg-color-overlay" style="bottom: -50px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6  col-sm-6 col-xs-6">
                        <h2 class="text-center">The BEST Taxi and Mini Cab Booking App in the UK.</h2>
                    </div>
                    <div class="col-md-6  col-sm-6 col-xs-6" id="logo">
                        <?php echo $this->Html->link($this->Html->image('logo.png', array('alt' => 'CakePHP')),'home', array('escape' => false));?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        $n = 0;
        foreach($home_content as $segment){
            $n++;
            $cont = $segment['HomePage'];
//            echo '<pre>';
//            print_r($cont);
//            echo '</pre>';
    ?>
        <div class="" id="<?php echo $n%2 == 0 ? 'third':'second'?>">
            <div class="bg_trans">
                <div class="container" style="padding-bottom: 15px;">
                    <div id="download" class="container aboutpart des">
                        <h2 id="about">
                            <?php echo $cont['title']; ?>
                        </h2>
                        <div id="aboutdetails">
                            <?php echo $cont['content']; ?>
                        </div>
                    </div>
                    <?php
                        $slider_imgages = json_decode($cont['images']);
                    ?>
                    <div id="cr<?php echo $n;?>" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <?php
                            $key = '';
                            if(!empty($cont['images'])){
                                foreach($slider_imgages as $key=>$sld_img){
                            ?>
                                    <li data-target="#cr<?php echo $n;?>" data-slide-to="<?php echo $key; ?>" class="<?php echo $key == 0 ? 'active' : '';?>"></li>
                            <?php
                                }}
                                if(!empty($cont['video_iframe'])){
                            ?>
                                <li data-target="#cr<?php echo $n;?>" data-slide-to="<?php echo $key == '' ? 0 : $key+1; ?>" class="<?php echo $key == '' ? 'active' : ''; ?>"></li>
                            <?php } ?>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php
                            $key = '';
                            if(!empty($cont['images'])){
                                foreach($slider_imgages as $key=>$sld_img){ ?>
                                    <div class="item <?php echo $key == 0 ? 'active' : '';?>">
                                        <img src="<?php echo $this->Html->url('/home_pic/'.$sld_img, true)?>">
    <!--                                <div class="carousel-caption"></div>-->
                                    </div>
                                <?php
                                }
                            }
                                if(!empty($cont['video_iframe'])){
                            ?>
                            <div class="item <?php echo $key == '' ? 'active' : ''; ?>">
                                <iframe width="100%" height="300px" src="<?php echo $cont['video_iframe']; ?>" frameborder="0" allowfullscreen="1"></iframe>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#cr<?php echo $n;?>" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#cr<?php echo $n;?>" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>

    <div class="wait-message" style="display:none">
        <h2>Please wait...</h2>
    </div>

    <!--      pop up login      -->
    <div class="popup" style="display:none">
        <div class="header">
            <?php echo $this->Html->image('delete-btn.png', array('alt' => 'CakePHP', 'class' => 'delete')) ?>
            <h1 class="popup-font text-center">You're almost done!</h1>
            <p class="text-center">To complete your booking you need to have an account with us.</p>
            <p class="text-center">You can access your account online or via our App.</p>

        </div>
        <div class="functionality">

        </div>
        <?php //echo $this->Html->image('partition.png', array('alt' => 'CakePHP', 'id'=>'partition')) ?>

        <div class="col-md-5 col-sm-5">
            <div class="row">
                <h3 class="loginlabel">Login</h3>
                <form class="form-horizontal login" role="form">
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Email</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Password</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="password" class="form-control" id="loginpassword" placeholder="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left"></label>
                        <div class="col-sm-8">
                            <?php echo $this->Html->image('loginbtn.png', array('alt' => 'CakePHP', 'id'=>'loginimg')) ?>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="col-md-2 col-sm-2 text-center">
            <?php echo $this->Html->image('partition.png', array('alt' => 'CakePHP', 'id'=>'partition')) ?>
        </div>
        <div class="col-md-5 col-sm-5">
            <div class="row">
                <form class="form-horizontal signUp" role="form">
                    <h3 class="signlabel">Sign Up</h3>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Name</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="text" class="form-control" id="name" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Mobile No</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="text" class="form-control" id="mobileno" placeholder="Mobile No">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Email</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="email" class="form-control" id="signemail" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Password</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="password" class="form-control" id="signpassword" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Cofirm Password</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="password" class="form-control" id="cp" placeholder="Retype Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left"></label>
                        <div class="col-sm-8">
                            <?php echo $this->Html->image('signupbtn.png', array('alt' => 'CakePHP', 'id'=>'signimg')) ?>
                        </div>
                    </div>
            </div>

            </form>
        </div>
    </div>
    <!--      pop up login      -->
    <script>
        $('.carousel').carousel({
            pause: 'hover'
        });
    </script>

    <?php echo $this->element('footer', array('footer' => $footer, 'links' => $links)); ?>
