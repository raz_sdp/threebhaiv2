<script type="text/javascript">
<?php if(empty($auth)){ ?>
  var loged_in = false;
  <?php } else { ?>
    var loged_in = true;
    <?php } ?>
    </script>

    <?php
        echo $this->Html->script(array('popup_login', 'popup_signup', 'passenger_booking'));
    ?>
    <div id="overlay"></div>
    <div class="container-fluid">
     <div class="section-one" id="intro">
       <div class="container">
         <div class="row">
           <div class="icon">
             <?php echo $this->Html->link($this->Html->image('message.png', array('alt' => 'CakePHP', 'id'=>'msg')),'mailto:'. $header_links[0]['Setting']['website_contact_email'], array('escape' => false));?> 

             <?php echo $this->Html->link($this->Html->image('facebook.png', array('alt' => 'CakePHP', 'id'=>'fb')),$header_links[0]['Setting']['facebook_link'], array('escape' => false, 'target' => '_blank'));?>  
             
             <?php echo $this->Html->link($this->Html->image('twitter.png', array('alt' => 'CakePHP', 'id'=>'twtr')),$header_links[0]['Setting']['twitter_link'], array('escape' => false, 'target' => '_blank'));?>
             
             <a href="#" class="login-link login">Login</a> 
             
           </div>
         </div>
       </div>
       <div class="bg-color-overlay">
         <div class="container">
           <div class="row">
             <div class="col-md-6  col-sm-6 col-xs-6">
               <h2 class="text-center">The BEST Taxi and Mini Cab Booking App in the UK.</h2>
             </div>
             <div class="col-md-6  col-sm-6 col-xs-6" id="logo">
               <?php echo $this->Html->link($this->Html->image('logo.png', array('alt' => 'CakePHP')),'home', array('escape' => false));?>
             </div>
           </div>
         </div>
       </div>
     </div>
     <div class="section-two" id="second">
       <div class="text-form">
        <?php echo $this->Html->image('taxi.png', array('alt' => 'CakePHP', 'id'=>'taxi')) ?>           
        <div class="container ">
         <div class="row">
           <div class="bg-color">        
             <form class="form-horizontal" role="form"> 
              <div class="part-one row">
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label class="col-sm-4 col-xs-5 control-label label-left">
                    <?php echo $this->Html->image('location.png', array('alt' => 'CakePHP', 'id'=>'loc')) ?>
                    Pick Up</label>
                    <div class="col-sm-8 col-xs-7">
                      <input autocomplete="off"  type="text" class="form-control postcode-auto-complete" id="pickup" placeholder="Pick Up">
                    </div>
                  </div>
                  <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-right destina-tion">
                    
                    <label class="col-sm-5 col-md-5 col-xs-6 control-label">
                     <?php echo $this->Html->image('location.png', array('alt' => 'CakePHP', 'id'=>'loc1')) ?>
                     Destination</label>
                     <div class="col-sm-7 col-xs-6 col-md-7 padding-right">
                      <input autocomplete="off" type="text" class="form-control postcode-auto-complete destination" placeholder="Destination">
                    </div>
                    
                  </div> 
                  <div class="form-group col-md-6 col-sm-6 col-xs-12">
                   
                    <label class="col-sm-10 col-md-7 col-xs-10 control-label label-left add-via">
                      <?php echo $this->Html->image('add.png', array('alt' => 'CakePHP', 'id'=>'add')) ?>
                      Add Another Destination</label>
                      <div class="col-sm-2 col-xs-2 col-md-2">
                        <!-- <input autocomplete="off"  type="text" class="form-control" id="inputPassword3" placeholder="Password"> -->
                      </div>
                      
                    </div> 
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 another-des padding-right" style="display:none">
                      <div class="row" id="via">
                        
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 add-des" style="margin-left:0">              
                          <label class="col-sm-4 col-xs-5 col-md-5 control-label">
                           <?php echo $this->Html->image('location.png', array('alt' => 'CakePHP', 'id'=>'add')) ?>
                            Destination </label>
                           <div class="col-sm-7 col-xs-6 col-md-7 padding-right">
                            <input autocomplete="off"  type="text" class="form-control postcode-auto-complete" placeholder="Add Destination">
                          </div>               
                        </div>
                        
                        
                      </div>
                    </div> 
                  </div>              
                  <div class="part-tow row">
                    <div class="form-group col-md-2 col-sm-3 col-xs-12 padding-right">

                    <div class="checkbox" id="asap">
                      <label>
                        <?php echo $this->Html->image('date.png', array('alt' => 'CakePHP', 'id'=>'datepic')) ?>
                        <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?> 
                        <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?> 
                        
                        <input autocomplete="off"  type="checkbox" value="1"> Soon as possible
                      </label>
                                  
                    </div>
                  </div>
                    <div class="form-group col-md-2 col-sm-3 col-xs-12 padding-right Date">
                     
                      <label class="col-sm-4 col-xs-5 control-label label-left">
                        Date</label>
                        <div class="col-sm-8 col-xs-7 date-input">
                          <input autocomplete="off"  type="text" class="form-control" id="datetext" placeholder="Date">
                        </div>
                        
                      </div> 
                      <div class="form-group col-md-2 col-sm-3 col-xs-12 padding-right time">
                       
                        <label class="col-sm-4 col-xs-5 control-label label-left">                
                          Time</label>
                          <div class="col-sm-8 col-xs-7 time-input">
                            <input autocomplete="off"  type="text" class="form-control" id="timetext" name="pick_up_time" placeholder="Pick Up Time">
                          </div>
                          
                        </div> 
                        <div class="form-group col-md-3 col-sm-3 col-xs-12 padding-right pessengers">
                          
                          <label class="col-sm-6 col-md-7 col-xs-5 control-label label-left">
                            
                            No of pessengers</label>
                            <div class="col-sm-6 col-md-5 col-xs-5">
                              <select class="form-control" id="passngrno">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                              </select>
                            </div>
                            
                          </div> 
                          <div class="form-group col-md-3 col-sm-3 col-xs-12 Luggage">
                            
                            <label class="col-sm-6 col-xs-5 control-label label-left">                
                              No of Luggage</label>
                              <div class="col-sm-6 col-xs-7">
                                <select class="form-control" id="luggage" >
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              
                            </div> 
                          </div>
                          <div class="part-three row">
                             <div class="form-group col-md-2 col-sm-4 col-xs-6">
                              
                              <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                <div class="checkbox" id="1seat">
                                  <label>
                                    <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic', 'style' => 'display : none;')) ?> 
                                    <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check', 'style' => 'display : inline;')) ?> 
                                    
                                    <input autocomplete="off" id="1seat"  type="checkbox" checked="checked" value="4 Seats"> 1-4 Seats
                                  </label>
                                </div>
                                
                              </div>
                            </div>
                            <div class="form-group col-md-2 col-sm-4 col-xs-6">
                              
                              <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                <div class="checkbox">
                                  <label>
                                    <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?> 
                                    <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?> 
                                    
                                    <input autocomplete="off"  type="checkbox" value="5 Seats"> 5 Seats
                                  </label>
                                </div>
                                
                              </div>
                            </div>
                            <div class="form-group col-md-2 col-sm-4 col-xs-6">
                             
                              <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                <div class="checkbox">
                                  <label>
                                    <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?> 
                                    <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?> 
                                    
                                    <input autocomplete="off"  type="checkbox" value="6 Seats"> 6 Seats
                                  </label>
                                  
                                </div>
                              </div>
                            </div>
                            <div class="form-group col-md-2 col-sm-4 col-xs-6">
                              
                              <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                <div class="checkbox">
                                  <label>
                                    <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?> 
                                    <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?> 
                                    
                                    <input autocomplete="off"  type="checkbox" value="7 Seats"> 7 Seats
                                  </label>
                                </div>
                                
                              </div>
                            </div>
                            <div class="form-group col-md-2 col-sm-4 col-xs-6">
                              
                              <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                <div class="checkbox">
                                  <label>
                                    <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?> 
                                    <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?> 
                                    
                                    <input autocomplete="off"  type="checkbox" value="8 Seats"> 8 Seats
                                  </label>
                                </div>
                                
                              </div>
                            </div>
                            <div class="form-group col-md-2 col-sm-4 col-xs-6">
                              
                              <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                <div class="checkbox">
                                  <label>
                                    <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?> 
                                    <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?> 
                                    
                                    <input autocomplete="off"  type="checkbox" value="Estate"> Estate
                                  </label>
                                </div>
                                
                              </div>
                            </div>
                            <div class="form-group col-md-2 col-sm-4 col-xs-6">
                              
                              <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                <div class="checkbox">
                                 <label>
                                  <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?> 
                                  <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?> 
                                  
                                  <input autocomplete="off"  type="checkbox" value="Wheelchair"> Wheelchair
                                </label>
                              </div>
                            </div>
                            
                          </div>
                        </div>
                        <div class="last-part row">
                         <div class="form-group col-md-12 col-sm-12 col-xs-12">                 
                          <label class="col-sm-5 col-md-3 col-xs-5 control-label label-left">
                            <?php echo $this->Html->image('qstn.png', array('alt' => 'CakePHP', 'id'=>'note')) ?>
                            Additional Note to driver</label>
                            <div class="col-sm-7 col-md-9 col-xs-7 padding-right">
                              <input autocomplete="off"  type="email" class="form-control" id="addNote" placeholder="Note to driver">
                            </div>
                          </div>
                        </div>
                        <button type="submit" class="buy-now">Submit</button>
                      </form>            
                    </div>
                    <button class="buy-now1">t</button>

                  </div>
                </div>
              </div>
            </div>
            <div class="popup" style="display:none">
              <div class="header">                
                <?php echo $this->Html->image('delete-btn.png', array('alt' => 'CakePHP', 'class' => 'delete')) ?>     
                <h1 class="popup-font text-center">You're almost done!</h1>                              
                <p class="text-center">To complete your booking you need to have an account with us.</p>
                <p class="text-center">You can access your account online or via our App.</p>
                
              </div>
              <div class="functionality">
                
              </div>   
              <?php //echo $this->Html->image('partition.png', array('alt' => 'CakePHP', 'id'=>'partition')) ?>
              
              <div class="col-md-5 col-sm-5">
                <div class="row">
                 <h3 class="loginlabel">Login</h3>
                 <form class="form-horizontal login" role="form">
                  <div class="form-group">
                    <label class="col-sm-4 control-label label-left">Email</label>
                    <div class="col-sm-8">
                      <input autocomplete="off"  type="email" class="form-control" id="email" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label label-left">Password</label>
                    <div class="col-sm-8">
                      <input autocomplete="off"  type="password" class="form-control" id="loginpassword" placeholder="password">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label label-left"></label>
                    <div class="col-sm-8">
                     <?php echo $this->Html->image('loginbtn.png', array('alt' => 'CakePHP', 'id'=>'loginimg')) ?> 
                   </div>
                 </div>
                 
               </form>                    
             </div>
           </div> 
           <div class="col-md-2 col-sm-2 text-center">
            <?php echo $this->Html->image('partition.png', array('alt' => 'CakePHP', 'id'=>'partition')) ?>
          </div>
          <div class="col-md-5 col-sm-5">
            <div class="row">
             <form class="form-horizontal signUp" role="form">
              <h3 class="signlabel">Sign Up</h3>
              <div class="form-group">
                <label class="col-sm-4 control-label label-left">Name</label>
                <div class="col-sm-8">
                  <input autocomplete="off"  type="text" class="form-control" id="name" placeholder="Name">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label label-left">Mobile No</label>
                <div class="col-sm-8">
                  <input autocomplete="off"  type="text" class="form-control" id="mobileno" placeholder="Mobile No">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label label-left">Email</label>
                <div class="col-sm-8">
                  <input autocomplete="off"  type="email" class="form-control" id="signemail" placeholder="Email">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label label-left">Password</label>
                <div class="col-sm-8">
                  <input autocomplete="off"  type="password" class="form-control" id="signpassword" placeholder="Password">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label label-left">Cofirm Password</label>
                <div class="col-sm-8">
                  <input autocomplete="off"  type="password" class="form-control" id="cp" placeholder="Retype Password">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label label-left"></label>
                <div class="col-sm-8">
                 <?php echo $this->Html->image('signupbtn.png', array('alt' => 'CakePHP', 'id'=>'signimg')) ?>
               </div>
             </div>
           </div>
           
         </form>
       </div>   
     </div>
   </div>
   <div class="wait-message" style="display:none">
    <h2>Please wait...</h2>
  </div> 
  <?php echo $this->element('footer', array('footer' => $footer, 'links' => $links)); ?>
