<?php
echo $this->Html->css(array('jquery-ui', 'style-ui'));
echo $this->Html->script(array('jquery-ui'));
$types = array(
    'driver' => 'Driver',
    'passenger' => 'Passenger',
    'vendor' => 'Branch User',
    'admin' => 'Admin'

);
?>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpt_cHVYOuA_WAmkdjLZ33g9bHrCmXEj8&v=3&libraries=geometry,places"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->Html->url('/map/map-icons.css'); ?>">
<script type="text/javascript" src="<?php echo $this->Html->url('/map/map-icons.js'); ?>"></script>
<script type="text/javascript">
    // autocomplete bug fix
    $(window).on('load', function () {
        setTimeout(function () {
            $('input#UserPasswordx').val('').css('background-color', '#FFFFFF !important');
            $('input#UserName').val($('input#UserName').data('value')).css('background-color', '#FFFFFF !important');
        }, 1000);
    });
</script>


<div class="content-wrapper">
<!-- Main content -->
<section class="content">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">
<div class="box-header with-border">
    <h3 class="box-title">Admin Add <?php echo @$types[$type] ?></h3>
</div>
<!-- /.box-header -->
<!-- form start -->
<!--<form role="form">-->

<div class="box-body">
<?php echo $this->Form->create('User', array('type' => 'file', "data-toggle" => "validator", "role" => "form")); ?>

<?php
if ($type == 'vendor') {
    echo $this->element('admin_vendor/add');
} else if ($type == 'passenger') {
    echo $this->element('admin_passenger/add');
} else if ($type == 'admin'){
    echo $this->element('admin_admin/add');
}else{
    ?>
    <div class="row">
    <div class="col-md-6">
        <div class="form-group has-feedback">
            <?php
            echo $this->Form->input('name', array(
                'class' => 'form-control',
                'id' => 'name',
                'type' => 'textfile',
                'placeholder' => 'name',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'

            ));
            ?>

        </div>

        <div class="form-group has-feedback">
            <?php
            echo $this->Form->input('username', array(
                'class' => 'form-control',
                'id' => 'username',
                'type' => 'textfile',
                'placeholder' => 'Username',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'

            ));
            ?>

        </div>

        <div class="form-group has-feedback">
            <?php echo $this->Form->input('email', array(
                'class' => 'form-control',
                'id' => 'email',
                'placeholder' => 'email',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>
        <div class="form-group has-feedback">
            <?php echo $this->Form->input('address', array(
                'class' => 'form-control',
                'onkeyup'=> 'addNewAddress("address")',
                'id' => 'address',
                'data-remote' => $this->Html->url('/users/validate_address', true),
                'data-error' => 'It\'s not a valid address' ,
                'placeholder' => 'address',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'

            )); ?>
        </div>
        <div class="form-group has-feedback ">
            <?php echo $this->Form->input('mobile', array(
                'class' => 'form-control',
                 'id'=>'mobile',
                'placeholder' => 'mobile',
                'type' => 'number',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>

        <div class="form-group  has-feedback ">
            <?php echo $this->Form->input('password', array(
                'class' => 'form-control',
                'id' => 'password',
                'placeholder' => 'password',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
        </div>


        <div class="form-group  has-feedback ">
            <?php echo $this->Form->input('voip_no', array(
                'class' => 'form-control',
                'id' => 'voip_no',
                'placeholder' => 'voip',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
        </div>

        <!--        <div class="form-group">-->
        <!---->
        <!--            --><?php //echo $this->Form->input('ref_promoter_code', array('class' => 'form-control', 'type' => 'select', 'options' => $ref_prom, 'empty' => '(Select One)')); ?>
        <!---->
        <!--        </div>-->



        <!--<div class="form-group">

            <?php /*echo $this->Form->input('code', array('class' => 'form-control', 'value' => $type, 'type' => 'hidden')); */ ?>
        </div>

        <div class="form-group">

            <?php /*echo $this->Form->input('type', array('value' => $type, 'type' => 'hidden')); */ ?>Account Holds Name
        </div>-->

<!--        <div class="form-group  has-feedback ">-->
<!---->
<!--            --><?php //echo $this->Form->input('account_holder_name', array(
//                'class' => 'form-control',
//                'id' => 'account_holder_name',
//                'placeholder' => 'Account Holds Name',
//                'required' => 'required',
//                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
<!--        </div>-->
<!--        <div class="form-group  has-feedback ">-->
<!---->
<!--            --><?php //echo $this->Form->input('account_no', array(
//                'class' => 'form-control',
//                'id' => 'account_no',
//                'placeholder' => 'account_no',
//                'required' => 'required',
//                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
<!--        </div>-->
<!---->
<!--        <div class="form-group  has-feedback ">-->
<!---->
<!--            --><?php //echo $this->Form->input('sort_code', array(
//                'class' => 'form-control',
//                'id' => 'sort_code',
//                'type' => 'number',
//                'placeholder' => 'sort_code',
//                'required' => 'required',
//                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
<!--        </div>-->
<!--        <div class="form-group  has-feedback ">-->
<!---->
<!--            --><?php //echo $this->Form->input('iban', array(
//                'class' => 'form-control',
//                'id' => 'iban',
//                'placeholder' => 'iban',
//                'required' => 'required',
//                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
<!--        </div>-->
<!--        <div class="form-group  has-feedback ">-->
<!---->
<!--            --><?php //echo $this->Form->input('swift', array(
//                'class' => 'form-control',
//                'id' => 'swift',
//                'placeholder' => 'swift code',
//                'required' => 'required',
//                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
<!--        </div>-->
        <div class="form-group  has-feedback ">

            <?php echo $this->Form->input('imagex', array(
                'class' => 'form-control',
                'id' => 'imagex',
                'label' => 'Your Image',
                'type' => 'file',
                'placeholder' => '',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
        </div>
        <div class="form-group  has-feedback ">

            <?php echo $this->Form->input('badge_imagex', array(
                'class' => 'form-control',
                'id' => 'badge_imagex',
                'placeholder' => '',
                'label' => 'Image of Driver Badge',
                'type' => 'file',
                'required' => 'required',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>


<!--        <div class="form-group  has-feedback ">-->
<!---->
<!--            --><?php // echo $this->Form->input('cab_type', array(
//                'class' => 'form-control',
//                'id' => 'cab_type',
//                'label' => 'Cab Type',
//                'required' => 'required',
//                'options' => array('Private Hire' => 'Private Hire',
//                    'Hackney Carriage' => 'Hackney Carriage'), 'empty' => '(Select One)',
//                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
//
//            ));?>
<!--        </div>-->




        <div class="form-group  has-feedback ">

            <?php echo $this->Form->input('vehicle_type', array(
                'class' => 'form-control',
                'id' => 'vehicle_type',
                'required' => 'required',
                'label' => 'Car Type',
                'options' => array('Saloon car' => 'Saloon Car', '5/6 Seater' => '5/6 Seater', '7/8 Seater' => '7/8 Seater', 'Estate Car' => 'Estate Car','Wheelchair'=> 'Wheelchair','Private Hire' => 'Private Hire',
                    'Hackney Carriage' => 'Hackney Carriage','Other' => 'Other'),
                'empty' => '(Select One)',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>

    </div>

    <div class="col-md-6">

        <div class="form-group  has-feedback ">

            <?php echo $this->Form->input('registration_plate_no', array(
                'class' => 'form-control',
                'required' => 'required',
                'id' => 'registration_plate_no',
                'label' => 'Registration Plate No',
                'placeholder' => 'registration_plate_no',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>
<!--        <div class="form-group  has-feedback ">-->
<!---->
<!--            --><?php // echo $this->Form->input('no_of_seat', array(
//                'class' => 'form-control',
//                'required' => 'required',
//                'id' => 'no_of_seat',
//                'options' => array('Saloon car' => 'Saloon Car', '5/6 Seater' => '5/6 Seater', '7/8 Seater' => '7/8 Seater', 'Estate Car' => 'Estate Car'),
//                'empty' => '(Select One)',
//                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
//
//            ));?>
<!--        </div>-->
<!--        <div class="checkbox">-->
<!--            <label>-->
<!--                --><?php //echo $this->Form->input('is_wheelchair', array('label' => 'Wheelchair Access?', 'type' => 'checkbox', 'value' => '1')); ?>
<!--            </label>-->
<!--        </div>-->

        <div class="form-group  has-feedback ">

            <?php echo $this->Form->input('vehicle_licence_no', array(
                'label' => 'Vehicle Licence No',
                'class' => 'form-control',
                'required' => 'required',
                'id' => 'vehicle_licence_no',
                'placeholder' => 'vehicle_licence_no',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>
        <div class="form-group  has-feedback ">

            <?php echo $this->Form->input('vehicle_licence_imagex', array(
                'class' => 'form-control',
                'required' => 'required',
                'id' => 'vehicle_licence_imagex',
                'label' => 'Vehicle licence image',
                'class' => 'form-control', 'type' => 'file',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>

        <div class="form-group  has-feedback ">

            <?php echo $this->Form->input('insurance_certificate', array(
                'label' => 'Insurance Date Of Expiry',
                'id' => 'insurance_certificate',
                'class' => 'form-control',
                'required' => 'required',
                //'type' => 'datetime',
                'id' => 'inputName',
                'placeholder' => 'insurance certificate',
                'value' => date('Y/m/d'), 'width:150px; height:30px; padding:0',
                //'class' => 'form-control datePicker',
                'type' => '',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'

            )); ?>
        </div>
        <div class="form-group  has-feedback ">

            <?php echo $this->Form->input('insurance_certificate_imagex', array(
                'label' => 'Insurance Certificate Image',
                'class' => 'form-control',
                'required' => 'required',
                'type' => 'file',
                'class' => 'form-control',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>


        <div class="form-group  has-feedback ">
            <?php echo $this->Form->input('running_distance', array(
                'class' => 'form-control',
                'required' => 'required',
                'type' => 'number',
                'label' => 'Running Distance (miles)',
                'placeholder' => '',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>
        <div class="form-group  has-feedback ">
            <?php echo $this->Form->input('promoter_percentage', array(
                'class' => 'form-control',
                'required' => 'required',
                'type' => 'number',
                'label' => 'Override Promoter Percentage %',
                'placeholder' => '',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>

        <div class="form-group  has-feedback ">
            <?php echo $this->Form->input('admin_percentage', array(
                'class' => 'form-control',
                'required' => 'required',
                'type' => 'number',
                'label' => 'Override Admin Percentage %',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>

        <div class="form-group  has-feedback ">
            <?php echo $this->Form->input('admin_percentage_cash', array(
                'class' => 'form-control',
                'required' => 'required',
                'type' => 'number',
                'label' => 'Override Admin Percentage % For Cash Payment',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>
        <?php if (AuthComponent::user(['type']) == 'admin') {?>
        <div class="form-group  has-feedback ">
            <label>Select Branch</label>
            <select name="data[User][branch_number]" class="form-control">
                <?php foreach($branches as $branch){ ?>
                <option value="<?php echo $branch['User']['branch_number'] ?>"><?php echo $branch['User']['company_name'] ?></option>
                <?php } ?>
            </select>
        </div>

<?php   }// pr($branches); ?>
        <div class="checkbox">
            <label>
                <?php echo $this->Form->input('despatch_job', array('label' => 'Ticked to allow this driver on Despatch Job', 'value' => '1')); ?>
            </label>
        </div>
    </div>


    </div>
    <div class="col-md-12">
        <h2 class="row">VR Zone (If ticked NOT allowed)</h2>
        <?php
        #pr($vr_zones);die;
        foreach ($vr_zones as $key => $sing_zone) {
            ?>
            <div class="col-md-4 col-sm-6 col-lg-2 col-xs-6">
<!--                --><?php
//                echo $this->Form->input('Zone.id.', array('hiddenField' => false, 'label' => $sing_zone, 'type' => 'checkbox', 'multiple' => 'checkbox', 'value' => $key));
//                ?>
                <label class="checkbox-inline">
                    <input type="checkbox"  name="data[Zone][id][]" value="<?php echo $key ?>"> <?php echo $sing_zone ?>
                </label>
            </div>
        <?php
        }
        ?>

    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <h2 class="row">Phone GPS Zone (If ticked allowed)</h2>
        <?php
        #pr($vr_zones);die;
        foreach ($phonecall_gps_zones as $key1 => $call_zone) {
            ?>
            <div class="col-md-4 col-sm-6 col-lg-3 col-xs-6">
                <label class="checkbox-inline">
                    <input type="checkbox" name="data[Zone][id][]" value="<?php echo $key1 ?>"> <?php echo $call_zone ?>
                </label>
            </div>
        <?php
        }
        ?>
    </div>
    <div class="clearfix"></div>
<!--    <div class="form-group  has-feedback ">-->
<!--        --><?php
//        echo $this->Form->input('Zone.id.', array(
//            'class' => 'form-control',
//            'hiddenField' => false,
//            'class' => 'form-control',
//            'required' => 'required',
//            'label' => '<h2>Phonecall Zone (If selected allowed)</h2>',
//            'type' => 'select',
//            'options' => $phonecall_zones, 'empty' => '(Select One)',
//            'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
//
//        ));
//        ?>
<!--    </div>-->
    <div class="form-group">
        <label for="zone_2"><h2>Phonecall Zone (If selected allowed)</h2></label>
        <select name="data[Zone][id]" class="form-control" id="zone_2">
            <?php foreach ($phonecall_zones as $key => $phonecall_zone ){ ?>
            <option value="<?php echo $key ?>"><?php echo $phonecall_zone ?></option>
            <?php } ?>
        </select>
    </div>
<?php
}
?>
<div class="form-group">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <button type="submit" class="btn btn-primary btn-block">Save</button>
    </div>
</div>
</form>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<script>
    function addNewAddress(location) {
        var text = { componentRestrictions: {} };
        var input = document.getElementById(location);
        autocomplete = new google.maps.places.Autocomplete(input, text);
    }
</script>


