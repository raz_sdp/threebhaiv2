<?php

/*App::uses('HttpSocket', 'Network/Http');
$HttpSocket = new HttpSocket();
//pr(AuthComponent::user());
$group_id = AuthComponent::user(['group_id']);
$results = $HttpSocket->get(Router::url("/subjects/get_lectures/".$group_id
    , true));
$tests = json_decode($results->body, true);*/

?>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!--    <section class="content-header">-->
<!--        <img src="--><?php //echo $this->Html->url('/img/an_bannar.png')?><!--" height="100%" width="100%">-->
<!---->
<!--    </section>-->

<!-- Main content -->
<section class="content">
 <div class="row">
        <div class="col-lg-4 col-xs-6 <?php echo AuthComponent::user(['type'])=='vendor' ? 'hide' : '' ?>">
            <!-- small box -->
            <div class="small-box branchmanagers-dashboard">
                <div class="inner">
                    <h3><?php echo $branchusers ?></h3>
                    <p>Branch Users</p>
                </div>
                <div class="icon">
                    <i class="fa fa-user-secret"></i>
                </div>
                <a href="<?php echo $this->Html->url("/admin/users/index/vendor", true) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box passengers-dashboard">
                <div class="inner">
                    <h3><?php echo $passengers ?></h3>
                    <p>Passengers</p>

                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="<?php echo $this->Html->url("/admin/users/index/passenger", true) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
     <div class="col-lg-4 col-xs-6">
         <!-- small box -->
         <div class="small-box drivers-dashboard">
             <div class="inner">
                 <h3><?php echo $drivers ?></h3>

                 <p>Drivers</p>
             </div>
             <div class="icon">
                 <i class="fa fa-taxi"></i>
             </div>
             <a href="<?php echo $this->Html->url("/admin/users/index/driver", true) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
     </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bookings-dashboard">
                <div class="inner">
                    <h3><?php echo $bookings ?></h3>

                    <p>Bookings</p>
                </div>
                <div class="icon">
                    <i class="fa fa-calendar-check-o"></i>
                </div>
                <a href="<?php echo $this->Html->url("/admin/bookings/index", true) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6 <?php echo AuthComponent::user(['type'])=='vendor'?'hide':''?>">
            <!-- small box -->
            <div class="small-box zones-dashboard">
                <div class="inner">
                    <h3><?php echo $zones ?></h3>

                    <p>Zones</p>
                </div>
                <div class="icon">
                    <i class="fa fa-map-o"></i>
                </div>
                <a href="<?php echo $this->Html->url("/admin/zones/index", true) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
<!--        col-->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box transactions-dashboard">
                <div class="inner">
                    <h3><?php echo $transactions ?></h3>

                    <p>Transactions</p>
                </div>
                <div class="icon">
                    <i class="fa fa-money"></i>
                </div>
                <a href="<?php echo $this->Html->url("/admin/transactions/index", true) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>


     <div class="col-lg-4 col-xs-6 <?php echo AuthComponent::user(['type'])=='vendor'?'hide':''?>">
         <!-- small box -->
         <div class="small-box admin-setting-dashboard <?php echo AuthComponent::user(['type'])=='vendor'?'hide':''?>">
             <div class="inner">
                 <h3>Settings</h3>

                 <p>Menu Permission</p>
             </div>
             <div class="icon">
                 <i class="fa fa-cog"></i>
             </div>
             <a href="<?php echo $this->Html->url("/admin/aclsmanages/index/vendor", true) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
     </div>
    <!-- ./col -->
</div>
<!-- /.row -->
<!-- Main row -->
<!-- /.row (main row) -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->