<div class="users view">
<h2><?php  echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($user['User']['address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mobile'); ?></dt>
		<dd>
			<?php echo h($user['User']['mobile']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Home No'); ?></dt>
		<dd>
			<?php echo h($user['User']['home_no']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Enabled'); ?></dt>
		<dd>
			<?php echo h($user['User']['is_enabled']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Priority'); ?></dt>
		<dd>
			<?php echo h($user['User']['priority']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($user['User']['type']); 
			$type = $user['User']['type'];
			?>
			&nbsp;
		</dd>

<?php	if($type != 'passenger'){ ?>
		<dt><?php  echo __('Vehicle'); ?></dt>
		<dd>
			<?php echo h($user['User']['vehicle_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Running Distance'); ?></dt>
		<dd>
			<?php echo h($user['User']['running_distance']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Virtual rank'); ?></dt>
		<dd>
			<?php echo h($user['User']['virtual_rank']); ?>
			&nbsp;
		</dd>
		<dt><?php // echo __('Zones'); ?></dt>
		<dd>
			<?php?>
			<?php // echo $this->Html->link($user['Zone']['name'], array('controller' => 'zones', 'action' => 'view', $user['Zone']['id'])); ?>
			&nbsp;
		</dd>


	<?php	} ?>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php echo $this->element('menu'); ?>