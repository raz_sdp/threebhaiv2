<?php
	if($forgot_pwd_token != 'success') {
?>
<div class="pwreset">
	<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Reset password'); ?></legend>
		<?php
			echo $this->Form->input('password');
			echo $this->Form->input('confirm_password', array('type' => 'password'));
		?>
	</fieldset>
	<?php echo $this->Form->end('submit'); ?>
</div>
<?php
}
?>