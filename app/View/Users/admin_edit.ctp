<?php
$types = array(
    'driver' => 'Driver',
    'passenger' => 'Passenger',
    'vendor' => 'Branch Users',
    'admin'	=> 'Admin'

);
?>
<style>
	form div.checkbox{
		float: left;
		clear: none;
		width: 300px;
	}
</style>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpt_cHVYOuA_WAmkdjLZ33g9bHrCmXEj8&v=3&libraries=geometry,places"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->Html->url('/map/map-icons.css'); ?>">
<script type="text/javascript" src="<?php echo $this->Html->url('/map/map-icons.js'); ?>"></script>
<script type="text/javascript">
<?php
echo $this->Html->css(array('jquery-ui', 'style-ui'));
echo $this->Html->script(array('jquery-ui'));
?>

<script type="text/javascript">
	$(function(){
		$('#insurance_certificate').datepicker({dateFormat : 'dd/mm/yy'});
	});

	// autocomplete bug fix
	$(window).load(function() {
		setTimeout(function () {
			$('input#UserPasswordx').val('').css('background-color', '#FFFFFF !important');
	       	$('input#UserName').val($('input#UserName').data('value')).css('background-color', '#FFFFFF !important');
   		}, 1000);
	});
</script>
<?php
echo $this->Html->script(array('image_hover')); ?>

<div class="content-wrapper">
<!-- Main content -->
<section class="content">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">
<div class="box-header with-border">
    <h3 class="box-title">Admin Edit User</h3>
</div>
<!-- /.box-header -->
<!-- form start -->
<!--<form role="form">-->

<div class="box-body">
<?php
//pr($this->request->data);die;

echo $this->Form->create('User', array('type' => 'file' , "data-toggle"=>"validator", "role" =>"form")); ?>
<?php echo $this->Form->input('id');
if($type =='admin'){
    echo $this->element('admin_admin/edit');
}else if($type =='vendor') {
    echo $this->element('admin_vendor/edit');
}else if($type =='passenger') {
    echo $this->element('admin_passenger/edit');
}else{
?>
<div class="row">
    <div class="col-md-6">
        <div class="form-group has-feedback">
            <?php echo $this->Form->input('name', array(
                'class' => 'form-control',
                'id'=>'UserName',
                'id'=>'inputName',
                'required'=>'required',
                'placeholder' => 'name',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>

        <div class="form-group has-feedback">
            <?php echo $this->Form->input('email', array(
                'class' => 'form-control',
                'type'=>'email' ,
                'id'=>'inputName',
                'required'=>'required',
                'placeholder' => 'email',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>
    <div class="form-group has-feedback">
        <?php echo $this->Form->input('address', array(
            'class' => 'form-control',
            'id' => 'address',
            'onkeyup'=> 'addNewAddress("address")',
            'data-remote' => $this->Html->url('/users/validate_address', true),
            'data-error' => 'It\'s not a valid address' ,
            'placeholder' => 'address',
            'required' => 'required',
            'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'

        )); ?>
    </div>
        <div class="form-group has-feedback ">

            <?php echo $this->Form->input('mobile', array(
                'class' => 'form-control',
                'label' => 'Mobile',
                'id'=>'inputName',
                'type'=>'text',
                'required'=>'required',
                'placeholder' => 'mobile',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>

        <?php if($this->request->data['User']['type'] == 'driver') {
            ?>

        <div class="form-group has-feedback">

            <?php echo $this->Form->input('voip_no', array(
                'class' => 'form-control',
                'label' => 'Phone Number for Answering Passengers Phone Calls (VOIP No)',
                'placeholder' => 'voip_no',
                'required'=>'required',
                'id'=>'inputName',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>

         </div>

       <?php }?>



        <!--        <div class="form-group">-->
        <!---->
        <!--            --><?php //echo $this->Form->input('ref_promoter_code', array('class' => 'form-control', 'type' => 'select', 'options' => $ref_prom, 'empty' => '(Select One)')); ?>
        <!---->
        <!--        </div>-->

        <div class="form-group has-feedback">

            <?php echo $this->Form->input('password', array(
                'class' => 'form-control',
                'id'=>'UserPasswordx',
                'id'=>'inputName',
                'required'=>'required',
                'type' => 'password',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>

        </div>


      <!--  <div class="form-group">

            <?php /*echo $this->Form->input('code', array('class' => 'form-control', 'value' => $type, 'type' => 'hidden')); */?>
        </div>

        <div class="form-group">

            <?php /*echo $this->Form->input('type', array('value' => $type, 'type' => 'hidden')); */?>
        </div>-->
        <?php if($this->request->data['User']['type'] == 'driver'){?>




        <div class="form-group has-feedback">

            <?php echo $this->Form->input('imagex', array(
                'class' => 'form-control',
                'label' => 'Your Image',
                'id'=>'inputName',
                //'required'=>'required',
                'type' => 'file',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
            <p><img src="<?php echo $this->Html->url('/files/driver_infos/'.$this->request->data['User']['image'],true); ?>" height="100px" width="100px"></p>
        </div>

        <div class="form-group has-feedback">

            <?php echo $this->Form->input('badge_imagex', array(
                'class' => 'form-control',
                'label' => 'Image of Driver Badge',
                //'required'=>'required',
                'id'=>'inputName',
                'type' => 'file',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>

            <p><img src="<?php echo $this->Html->url('/files/driver_infos/'.$this->request->data['User']['badge_image'],true); ?>" height="100px" width="100px"></p>
        </div>



<!--        <div class="form-group has-feedback">-->
<!---->
<!--            --><?php // echo $this->Form->input('cab_type',
//                array('class' => 'form-control',
//                    'label' => 'Cab Type', 'options' => array('Private Hire' => 'Private Hire',
//                    'Hackney Carriage' => 'Hackney Carriage'),
//                    'id'=>'inputName',
//                    'required'=>'required',
//                    'empty' => '(Select One)',
//                    'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
//                ));?>
<!--        </div>-->

        <div class="form-group  has-feedback ">

            <?php echo $this->Form->input('vehicle_type', array(
                'class' => 'form-control',
                'id' => 'vehicle_type',
                'required' => 'required',
                'label' => 'Car Type',
                'options' => array('Saloon car' => 'Saloon Car', '5/6 Seater' => '5/6 Seater', '7/8 Seater' => '7/8 Seater', 'Estate Car' => 'Estate Car','Wheelchair'=> 'Wheelchair','Private Hire' => 'Private Hire',
                    'Hackney Carriage' => 'Hackney Carriage','Other' => 'Other'),
                'empty' => '(Select One)',
                'after' => ' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>

        </div>

        <div class="col-md-6">


        <div class="form-group has-feedback">

            <?php echo $this->Form->input('registration_plate_no', array(
                'class' => 'form-control',
                'label' => 'Registration Plate No',
                'id'=>'inputName',
                'required'=>'required',
                'placeholder' => 'registration_plate_no',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
                )); ?>
        </div>



<!--        <div class="form-group has-feedback">-->
<!---->
<!--            --><?php // echo $this->Form->input('no_of_seat', array(
//                'class' => 'form-control',
//                'id'=>'inputName',
//                'required'=>'required',
//                'options' => array('2' => '2 Seats', '4' => '4 Seats', '5' => '5 Seats', '6' => '6 Seats', '7' => '7 Seats', '8' => '8 Seats'),
//                'empty' => '(Select One)',
//                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
//
//            ));?>
<!--        </div>-->
<!--        <div class="checkbox" style="width: 100%;">-->
<!--            <label>-->
<!--                --><?php //echo $this->Form->input('is_wheelchair', array('label' => 'Wheelchair Access?', 'type' => 'checkbox', 'value' => '1')); ?>
<!--            </label>-->
<!--        </div>-->

        <div class="form-group has-feedback">

            <?php echo $this->Form->input('vehicle_licence_no', array(
                'label' => 'Vehicle Licence No',
                'class' => 'form-control',
                'id'=>'inputName',
                'required'=>'required',
                'placeholder' => 'vehicle_licence_no',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>



        <div class="form-group has-feedback">

            <?php echo $this->Form->input('vehicle_licence_imagex', array(
                'label' => 'Vehicle licence image',
                'id'=>'inputName',
                //'required'=>'required',
                'type'=>'file'
            ,'class'=>'form-control',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
            <p><img src="<?php echo $this->Html->url('/files/driver_infos/'.$this->request->data['User']['vehicle_licence_image'],true); ?>" height="100px" width="100px"></p>
        </div>

        <div class="form-group has-feedback">

            <?php echo $this->Form->input('insurance_certificate', array(
                'class'=>'form-control',
                'label' => 'Insurance Date Of Expiry',
                'id' => 'insurance_certificate','value' => date('d/m/Y'),
                'id'=>'inputName',
                'required'=>'required',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>',
                'width:150px; height:30px; padding:0','type'=>'text')); ?>
        </div>
        <div class="form-group has-feedback">

            <?php echo $this->Form->input('insurance_certificate_imagex', array(
                'label' => 'Insurance Certificate Image',
                'id'=>'inputName',
                //'required'=>'required',
                'type' => 'file',
                'class' => 'form-control',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
            <p><img src="<?php echo $this->Html->url('/files/driver_infos/'.$this->request->data['User']['insurance_certificate_image'],true); ?>" height="100px" width="100px"></p>
        </div>

        <div class="form-group has-feedback">

            <?php echo $this->Form->input('running_distance', array(
                'class' => 'form-control',
                'label' => 'Running Distance (miles)',
                'id'=>'inputName',
                'required'=>'required',
                'placeholder' => '',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>
        <div class="form-group has-feedback">

            <?php echo $this->Form->input('promoter_percentage', array(
                'class' => 'form-control',
                'required'=>'required',
                'label' => 'Override Promoter Percentage %',
                'id'=>'inputName',
                'placeholder' => '',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>

        <div class="form-group has-feedback">
            <?php echo $this->Form->input('admin_percentage', array(
                'class' => 'form-control',
                'id'=>'inputName',
                'required'=>'required',
                'label' => 'Override Admin Percentage %',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>

        <div class="form-group has-feedback">
            <?php echo $this->Form->input('admin_percentage_cash', array(
                'class' => 'form-control',
                'required'=>'required',
                'label' => 'Override Admin Percentage % For Cash Payment',
                'after'=>' <span class="glyphicon form-control-feedback" aria-hidden="true"></span>'
            )); ?>
        </div>


        <div class="checkbox">
            <label>
                <?php echo $this->Form->input('despatch_job', array('label' => 'Ticked to allow this driver on Despatch Job', 'value' => '1')); ?>
            </label>
        </div>
    </div>


</div>
<div class="clearfix"></div>
    <div class="col-md-12">
        <h2 class="row">VR Zone (If ticked NOT allowed)</h2>
        <?php
        #pr($vr_zones);die;
        foreach ($vr_zones as $key => $sing_zone) {
            ?>
            <div class="col-md-4 col-sm-6 col-lg-2 col-xs-6">
                <!--                --><?php
                //                echo $this->Form->input('Zone.id.', array('hiddenField' => false, 'label' => $sing_zone, 'type' => 'checkbox', 'multiple' => 'checkbox', 'value' => $key));
                //                ?>
                <label class="checkbox-inline">
                    <input type="checkbox"  name="data[Zone][id][]" value="<?php echo $key ?>" <?php echo in_array($key, $zone_ids)? 'checked': '' ?> > <?php echo $sing_zone ?>
                </label>
            </div>
        <?php
        }
        ?>

    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <h2 class="row">Phone GPS Zone (If ticked allowed)</h2>
        <?php
        #pr($vr_zones);die;
        foreach ($phonecall_gps_zones as $key1 => $call_zone) {
            ?>
            <div class="col-md-4 col-sm-6 col-lg-3 col-xs-6">
                <label class="checkbox-inline">
                    <input type="checkbox" name="data[Zone][id][]" value="<?php echo $key1 ?>" <?php echo in_array($key1, $zone_ids)? 'checked': '' ?>> <?php echo $call_zone ?>
                </label>
            </div>
        <?php
        }
        ?>
    </div>
    <div class="clearfix"></div>
        <div class="form-group">
            <label for="zone_2"><h2>Phonecall Zone (If selected allowed)</h2></label>
            <select name="data[Zone][id]" class="form-control" id="zone_2">
                <?php foreach ($phonecall_zones as $key => $phonecall_zone ){ ?>
                    <option value="<?php echo $key ?>"><?php echo $phonecall_zone ?></option>
                <?php } ?>
            </select>
        </div>    <?php }?>
<?php } ?>
<div class="form-group">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <button type="submit" class="btn btn-primary btn-block">Save</button>
    </div>
</div>
</form>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>

<script>
    function addNewAddress(location) {
        var text = { componentRestrictions: {} };
        var input = document.getElementById(location);
        autocomplete = new google.maps.places.Autocomplete(input, text);
    }
</script>





































