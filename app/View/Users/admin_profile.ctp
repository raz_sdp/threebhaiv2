

<!DOCTYPE html>
<html>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<div class="content-wrapper">

<section class="content-header">
    <h1>
        User Profile
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User profile</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

<div class="row">
<div class="col-md-3">

    <!-- Profile Image -->
    <div class="box box-primary">
        <div class="box-body box-profile">
            <?php
            if(AuthComponent::user(['type']) == 'vendor'){?>
                <img class="profile-user-img img-responsive img-circle" src="<?php echo empty($user['User']['image'])? $this->Html->url('/img/avatar/default.png'):$this->Html->url('/files/vendor_infos/'.$user['User']['image']) ?>" alt="" >
            <?php } else { ?>
                <img class="profile-user-img img-responsive img-circle" src="<?php echo $this->Html->url('/img/avatar/default.png') ?>" alt="" >
            <?php } ?>
            <h3 class="profile-username text-center">
                <?php
                if($user['User']['type']=='vendor'){
                    echo($user['User']['company_name']);
                } else {
                    echo($user['User']['name']);
                }
                ?>
            </h3>
<?php
$job = [
    'admin' => 'ADMIN',
    'vendor'    => 'Director'
];
$role = $user['User']['type'];
?>
            <p class="text-muted text-center"><?php echo($job[$role])?><br>
            <?php
            if ($role != 'admin') {
                $paypal_url ='https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
            $paypal_id ='pay@threebhai.com';
            ?>



            <form action="<?php echo $paypal_url ?>" role="form" method="post" id="paypal_form">
                <input type="hidden" name="cmd" value="_xclick" />
                <input type="hidden" name="business" value="<?php echo($paypal_id); ?>">
                <input type="hidden" name="no_note" value="1" />
                <input type="hidden" name="currency_code" value="GBP">
                <input type="hidden" name="first_name" value="<?php echo AuthComponent::user()['name'] ?>" />
                <input type="hidden" name="payer_email" value="<?php echo AuthComponent::user()['email']?>" />
                <input type="hidden" name="item_number" value="top up" />
                <input type='hidden' name='notify_url' value='<?php echo $this->Html->url('/admin/users/paypal_success', true); ?>'>
                <input type="hidden" name="cancel_return" value="<?php echo $this->Html->url('/admin/users/paypal_cancel', true); ?>">
                <input type="hidden" name="return" value="<?php echo $this->Html->url('/admin/users/paypal_success', true); ?>">

                <div class="form-group">
                    <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                    <div class="input-group">
                        <input type="text" name="amount" class="form-control" id="exampleInputAmount" placeholder="Amount">
                        <div class="input-group-btn"><button type="submit" class=" btn btn-warning">Top Up</button></div>
                    </div>
                </div>

                </form>
            <?php } ?>
            


            </p>

            <?php
            $wallet = AppHelper::wallet($id = AuthComponent::user(['id']))
            ?>
            <ul class="list-group list-group-unbordered">

                <?php if($role == 'vendor') {?>
                    <li class="list-group-item">
                        <b>Total Top Up</b> <a class="pull-right"><?php echo(isset($wallet['Wallet']['total_balance'])?$wallet['Wallet']['total_balance']:'0') ?></a>
                    </li>
                <li class="list-group-item">
                    <b>Total Spent</b> <a class="pull-right"><?php echo(isset($wallet['Wallet']['total_withdraw'])?$wallet['Wallet']['total_withdraw']:' 0') ?></a>
                </li>


                <li class="list-group-item">
                    <b>A/C Balance</b> <a class="pull-right"><?php echo(isset($wallet['Wallet']['available_balance'])?$wallet['Wallet']['available_balance']:' 0') ?></a>
                </li>
                <?php } ?>

                <li class="list-group-item">
                    <b>Address</b> <a class="pull-right"><?php echo($user['User']['address']) ?></a>
                </li>
                <li class="list-group-item">
                        <b>Mobile</b> <a class="pull-right"><?php echo($user['User']['mobile']) ?></a>
                </li>
                <li class="list-group-item">
                    <b>Mail</b> <a class="pull-right"><?php echo($user['User']['email']) ?></a>
                </li>
                <?php
                $date = $user['User']['created'];
                $d    = explode(' ', $date)
                ?>


                <li class="list-group-item">
                    <b>Member Since</b> <a class="pull-right"><?php echo($d[0]) ?></a>
                </li>

            </ul>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <!-- About Me Box -->

    <!-- /.box -->
</div>
<!-- /.col -->
<div class="col-md-9">
<div class="nav-tabs-custom">
<ul class="nav nav-tabs">
    <li class="active"><a href="#settings" data-toggle="tab">Update Profile</a></li>
</ul>
<div class="tab-content">


<div class=" active tab-pane" id="settings">
    <form class="form-horizontal" method="post">
        <?php if($user['User']['type']=='vendor'){ ?>
            <div class="form-group">
                <label for="inputcompany" class="col-sm-2 control-label">Company Name</label>

                <div class="col-sm-10">
                    <input type="text" name="data[User][company_name]" value="<?php echo($user['User']['company_name']) ?>" class="form-control" id="inputcompany" placeholder="Company Name">
                </div>
            </div>

            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">User ID</label>

                <div class="col-sm-10">
                    <input type="text" name="data[User][username]" value="<?php echo($user['User']['username']) ?>" class="form-control" id="inputName" placeholder="Name" disabled>
                </div>
            </div>
        <?php } ?>
        <div class="form-group">
            <label for="inputName" class="col-sm-2 control-label">Name</label>

            <div class="col-sm-10">
                <input type="text" name="data[User][fullname]" value="<?php echo($user['User']['name']) ?>" class="form-control" id="inputName" placeholder="Name">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="col-sm-2 control-label">Email</label>

            <div class="col-sm-10">
                <input type="email" name="data[User][email]" value="<?php echo($user['User']['email']) ?>" class="form-control" id="inputEmail" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <label for="inputaddress" class="col-sm-2 control-label">Address</label>

            <div class="col-sm-10">
                <input type="text" name="data[User][address]" value="<?php echo($user['User']['address']) ?>" class="form-control" id="inputaddress" placeholder="Address">
            </div>
        </div>

        <div class="form-group">
            <label for="inputimage" class="col-sm-2 control-label">Image </label>
            <div class="col-sm-10">
            <input autocomplete="off" name="data[User][image]" type="file" id="inputimage" class="form-control">
            </div>
        </div>
<!--        <div class="form-group">-->
<!--            <label for="inputcity" class="col-sm-2 control-label">Company Name</label>-->
<!---->
<!--            <div class="col-sm-10">-->
<!--                <input class="form-control" name="data[User][company_name]" value="--><?php //echo($user['User']['company_name']) ?><!--" id="inputcity" placeholder="City">-->
<!--            </div>-->
<!--        </div>-->
        <div class="form-group">
            <label for="inputmobile" class="col-sm-2 control-label">Mobile</label>

            <div class="col-sm-10">
                <input class="form-control" name="data[User][mobile]" value="<?php echo($user['User']['mobile']) ?>" id="inputmobile" placeholder="Mobile Number">
            </div>
        </div>
        <div class="form-group">
            <label for="inputpassword" class="col-sm-2 control-label">Password</label>

            <div class="col-sm-10">
                <input type="text" name="data[User][password]" class="form-control" id="inputpassword" placeholder="Enter New Password">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
<!-- /.tab-pane -->
</div>
<!-- /.tab-content -->
</div>
<!-- /.nav-tabs-custom -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- Control Sidebar -->
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


</body>
</html>
