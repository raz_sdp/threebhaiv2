

<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('Transaction', array('type' => 'file', "data-toggle"=>"validator", "role" =>"form")); ?>
                        <div class="box-body">
                            <div class="col-md-12">
                                <div class="form-group has-feedback">
                                    <?php echo $this->Form->input('amount', array('label'=>'Free Credit Amount', 'class'=>'form-control', 'required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>'));?>
                                    <!--                                --><?php //echo $this->Form->input('zone_id', array('options' => $zones, 'label' => 'Phonecall and Phone GPS Zones', 'class' => 'form-control', 'required'=>'required', 'after'=>'<span class="glyphicon form-control-feedback" aria-hidden="true"></span>')); ?>
                                   <?php echo $this->Form->input('short_description',array('value' => 'free_credit', 'type'=>'hidden'));
                                    echo $this->Form->input('service',array('value' => 'free_credit', 'type'=>'hidden'));?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>
                            </from>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!-- /.row -->
    </section>
    <!-- /.content -->
</div>




























<!--<div class="topUps form">-->
<?php //echo $this->Form->create('Transaction'); ?>
<!--	<fieldset>-->
<!--		<legend>Give Free Credit To --><?php //echo ($user[0]['User']['name']); ?><!-- (--><?php //echo $user[0]['User']['type']; ?><!--) </legend>-->
<!--	--><?php
//		echo $this->Form->input('amount',array('label' => 'Free Credit Amount'));
//		echo $this->Form->input('short_description',array('value' => 'free_credit', 'type'=>'hidden'));
//		echo $this->Form->input('service',array('value' => 'free_credit', 'type'=>'hidden'));
//	?>
<!--	</fieldset>-->
<?php //echo $this->Form->end(__('Submit')); ?>
<!--</div>-->
<?php //echo $this->element('menu'); ?>