<div class="users index">
	<h2><?php echo __('Users'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('mobile'); ?></th>
			<th><?php echo $this->Paginator->sort('password'); ?></th>
			<th><?php echo $this->Paginator->sort('is_enabled'); ?></th>
			<th><?php echo $this->Paginator->sort('code'); ?></th>
			<th><?php echo $this->Paginator->sort('priority'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('vehicle_id'); ?></th>
			<th><?php echo $this->Paginator->sort('running_distance'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['mobile']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['password']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['is_enabled']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['code']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['priority']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['type']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($user['Vehicle']['id'], array('controller' => 'vehicles', 'action' => 'view', $user['Vehicle']['id'])); ?>
		</td>
		<td><?php echo h($user['User']['running_distance']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['created']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Billings'), array('controller' => 'billings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Billing'), array('controller' => 'billings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Booking Settings'), array('controller' => 'booking_settings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Booking Setting'), array('controller' => 'booking_settings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fare Settings'), array('controller' => 'fare_settings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fare Setting'), array('controller' => 'fare_settings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Notification Logs'), array('controller' => 'notification_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Notification Log'), array('controller' => 'notification_logs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Queues'), array('controller' => 'queues', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Queue'), array('controller' => 'queues', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rejects'), array('controller' => 'rejects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reject'), array('controller' => 'rejects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Request Logs'), array('controller' => 'request_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Request Log'), array('controller' => 'request_logs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Top Ups'), array('controller' => 'top_ups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Top Up'), array('controller' => 'top_ups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Trackings'), array('controller' => 'trackings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tracking'), array('controller' => 'trackings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Trouble Logs'), array('controller' => 'trouble_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Trouble Log'), array('controller' => 'trouble_logs', 'action' => 'add')); ?> </li>
	</ul>
</div>
