<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Edit User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('email');
		echo $this->Form->input('mobile');
		echo $this->Form->input('password');
		echo $this->Form->input('is_enabled');
		echo $this->Form->input('code');
		echo $this->Form->input('priority');
		echo $this->Form->input('type');
		echo $this->Form->input('vehicle_id');
		echo $this->Form->input('running_distance');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Billings'), array('controller' => 'billings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Billing'), array('controller' => 'billings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Booking Settings'), array('controller' => 'booking_settings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Booking Setting'), array('controller' => 'booking_settings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fare Settings'), array('controller' => 'fare_settings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fare Setting'), array('controller' => 'fare_settings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Notification Logs'), array('controller' => 'notification_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Notification Log'), array('controller' => 'notification_logs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Queues'), array('controller' => 'queues', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Queue'), array('controller' => 'queues', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rejects'), array('controller' => 'rejects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reject'), array('controller' => 'rejects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Request Logs'), array('controller' => 'request_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Request Log'), array('controller' => 'request_logs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Top Ups'), array('controller' => 'top_ups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Top Up'), array('controller' => 'top_ups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Trackings'), array('controller' => 'trackings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tracking'), array('controller' => 'trackings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Trouble Logs'), array('controller' => 'trouble_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Trouble Log'), array('controller' => 'trouble_logs', 'action' => 'add')); ?> </li>
	</ul>
</div>
