<?php
echo $this->Html->css(array('jquery-ui', 'style-ui'));
echo $this->Html->script(array('jquery-ui'));
?>
<script type="text/javascript">
    //fn for from(calender)-to(calender) to show transaction records for a specified records
    $(function () {
        $('#go').on('click', function () {
            location.href = $('#created').val();
        });
        $('#show').on('click', function () {
            location.href = ROOT + 'admin/users/topup/<?php echo $user_id; ?>/' + $('#from').data('mysql') + '/' + $('#to').data('mysql');
        });

        $("#from, #to").datepicker({
            dateFormat: 'dd/mm/yy',
            onClose: function (dateText) {
                $(this).data('mysql', dateText.split('/').reverse().join('-'));
            }
        });
    });
</script>
<style type="text/css">

    .test {
        margin: 0 5px;
    }

    .test:first-child {
        float: left;
        width: 45%;
        background: #fff;
        color: #2c6877;
        font-family: 'Gill Sans', 'lucida grande', helvetica, arial, sans-serif;
        font-size: 165%;
    }

    .test:last-child {
        float: right;
        width: 45%;
        color: #2c6877;
        font-family: 'Gill Sans', 'lucida grande', helvetica, arial, sans-serif;
        font-size: 120%;
    }
</style>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Transaction Record for <?php echo $user['User']['name']; ?>
                            (<?php echo $user['User']['type']; ?>)</h3>

                    </div>

                    <div class="box-body">
                        <div class="users index">
                            <div class="users index">
                                <?php
                                $sh_des = array('free_credit' => 'Free Credit', 'admin_payment' => 'Admin Payment', 'top_up' => 'Top Up', 'taxi_fare' => 'Taxi Fare', 'promoter_profit' => 'Promoter Profit', 'promoter_fee' => 'Promoter Fee', 'transfer_pending' => 'Transfer Request Pending', 'transfer_paid' => 'Transfer Paid Complete', 'billing_twillio' => 'Twilio Bill', 'admin_fee' => 'Admin Fee', 'rent_fee' => 'Rent Fee', 'rent_profit' => 'Rent Profit');
                                $ser = array('paypal' => 'PayPal', 'card' => 'Card Payment', 'credit_transfer' => 'Credit Transfer', 'cash_in_hand' => 'Cash in Hand', 'admin_payment' => ' ', 'promoter_fee' => ' ');
                                ?>
                                <?php if ($transactions[0]['User']['type'] == 'passenger') { ?>
                                    <h3>
                                        Balance <?php echo $this->Number->currency(round($sum_amount[0][0]['total'], 2), 'GBP'); ?></h3>
                                <?php } ?>
                                <?php if ($transactions[0]['User']['type'] != 'passenger') { ?>
                                    <div class="content">
                                        <div class="test">
                                            Actual Balance* <?php
                                            echo $this->Number->currency(round($actual_balance['total'], 2), 'GBP');
                                            ?><br>
                                            Available Balance* <?php
                                            echo $this->Number->currency(round($available_balance['total'], 2), 'GBP');
                                            ?>
                                            <br>
                                        </div>
                                        <div class="test">
                                            Account Holder's
                                            Name: <?php echo $transactions[0]['User']['account_holder_name']; ?> <br>
                                            Account No : <?php echo $transactions[0]['User']['account_no']; ?><br>
                                            Sort Code: <?php echo $transactions[0]['User']['sort_code']; ?><br>
                                            IBAN: <?php echo $transactions[0]['User']['iban']; ?><br>
                                            SWIFT/BIC code: <?php echo $transactions[0]['User']['swift']; ?><br>
                                        </div>
                                    </div>
                                    *Balance does not include Cash in Hand.
                                    <br>Remaining Free Credit Balance <?php echo $this->Number->currency(round($available_balance['remaining_free_credit'], 2), 'GBP'); ?>
                                    <br>
                                <?php } ?>
                                <br>
                                <?php
                                $options = array('30' => 'Last 30 Days', '90' => 'Last 90 Days', '180' => 'Last 180 Days');
                                $links = array();
                                $selected = null;
                                foreach ($options as $key => $value) {
                                    $link = $this->Html->url(array($user_id, $key));
                                    $links[$link] = $value;
                                    if ($limit_created == $key) {
                                        $selected = $link;
                                    }
                                }?>
                                <div><label for="created">Select one Transaction Period</label></div>
                                <div class="col-md-12">
                                    <div
                                        class="col-md-11 form-group"> <?php echo $this->Form->input('created', array('options' => $links,'selected' =>$selected, 'class' => 'form-control', 'label' => false, 'div' => false)); ?></div>
                                    <div
                                        class="col-md-1"> <?php echo $this->Form->input('Go', array('id' => 'go', 'type' => 'button', 'class' => 'go btn btn-primary', 'div' => false, 'label' => false)); ?></div>
                                </div>
                                <br>
                                <br><strong>OR</strong>
                                <br><br>

                                <div class="col-md-12">
                                    <div class="col-md-1">From</div>
                                    <div class="col-md-4">
                                        <?php
                                        $fr = $from === null ? strtotime('-30 days') : strtotime($from);
                                        echo $this->Form->input('from', array('value' => date('d/m/Y', $fr), 'type' => 'text', 'label' => false, 'data-mysql' => date('Y-m-d', $fr), 'class' => 'form-control', 'div' => false, 'style' => 'width:175px; height:30px; padding:0')); ?></div>
                                    <div class="col-md-1">To</div>
                                    <div class="col-md-4">
                                        <?php
                                        $t = $to === null ? strtotime('today') : strtotime($to);
                                        echo $this->Form->input('to', array('value' => date('d/m/Y', $t), 'type' => 'text', 'label' => false, 'data-mysql' => date('Y-m-d', $t), 'class' => 'form-control', 'div' => false, 'style' => 'width:175px;height:30px; padding:0'));?></div>
                                    <div class="col-md-1">
                                        <?php echo $this->Form->input('Show', array('id' => 'show', 'type' => 'button', 'class' => 'go btn btn-primary', 'div' => false, 'label' => false));
                                        ?></div>
                                </div>
                                <br>
                                <br>
                                <br>

                                <table cellpadding="0" cellspacing="0" id="example2"
                                       class="table table-bordered table-hover">
                                    <tr>
                                        <th>Transaction Date</th>
                                        <th>Description</th>
                                        <th>Service</th>
                                        <th>Deposite</th>
                                        <th>Expense</th>
                                        <th>Booking Id</th>
                                        <th>
                                            <?php
                                            if ($transactions[0]['User']['type'] != 'passenger') {
                                                ?>
                                                Referred User
                                            <?php } ?>
                                        </th>
                                    </tr>
                                    <?php // pr($transactions); ?>
                                    <?php foreach ($transactions as $index => $transaction):
                                        if ($transaction['Transaction']['short_description'] != 'transfer_pending') {
                                            $negative_check = strstr($transaction['Transaction']['amount'], '-');
                                            ?>
                                            <tr>
                                                <td><?php
                                                    //$time = $transaction['Transaction']['created'];
                                                    $date_time = date_create($transaction['Transaction']['created']);
                                                    echo date_format($date_time, 'd/m/Y g:i A');
                                                    //echo h($ttt);
                                                    ?>&nbsp;</td>
                                                <td><?php
                                                    $val = $transaction['Transaction']['short_description'];
                                                    echo $sh_des[$val];
                                                    ?>&nbsp;</td>
                                                <td><?php
                                                    $val = $transaction['Transaction']['service'];
                                                    echo $ser[$val];
                                                    ?>&nbsp;</td>
                                                <?php if ($negative_check == false) { ?>
                                                    <td><?php echo $this->Number->currency($transaction['Transaction']['amount'], 'GBP'); ?>
                                                        &nbsp;</td>
                                                    <td><?php echo 'N/A'; ?> </td>
                                                <?php } else { ?>
                                                    <td><?php echo 'N/A'; ?> </td>
                                                    <?php $amount = trim($transaction['Transaction']['amount'], '-'); ?>
                                                    <td><?php echo $this->Number->currency($amount, 'GBP'); ?>
                                                        &nbsp;</td>
                                                <?php } ?>
                                                <td>
                                                    <?php if (empty($transaction['Transaction']['booking_id'])) {
                                                        echo 'N/A';
                                                    } else {
                                                        echo $this->Html->link($transaction['Transaction']['booking_id'], array('controller' => 'bookings', 'action' => 'view', $transaction['Transaction']['booking_id']));
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php if ($transaction['User']['type'] != 'passenger') {
                                                        if (empty($transaction['Transaction']['referred_user_id'])) {
                                                            echo 'N/A';
                                                        } else {
                                                            echo $this->Html->link($transaction['ReferredUser']['name'], array('action' => 'edit', $transaction['Transaction']['referred_user_id']));
                                                        }
                                                    }
                                                    ?>
                                                </td>

                                            </tr>
                                        <?php
                                        }
                                    endforeach; ?>
                                </table>
                                <p class="pull-right">
                                    <?php
                                    echo $this->Paginator->counter(array(
                                        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                    ));
                                    ?>    </p>

                                <div class="clearfix"></div>
                                <div class="paging">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <?php
                                        echo "<li>" . $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')) . "</li>";
                                        echo "<li>" . $this->Paginator->numbers(array('separator' => '')) . "</li>";
                                        echo "<li>" . $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')) . "</li>";
                                        ?>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
