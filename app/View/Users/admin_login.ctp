<div class="login-box">
    <div class="login-logo" style="margin:0 0 0 13%">

        <a href="<?php echo $this->Html->url('/')?>">
            <?php echo $this->Html->image('/img/backgroundshadow.png',['class'=>'img-responsive','height' => '','width'=> '80%',
    'margin-left'=> '18px'])?>
        </a>
        <!--<a href="/"><b>Adission</b>Ninja</a>-->
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <?php echo $this->Form->create('User',array('class'=>'form-group'));?>
        <div class="form-group has-feedback">
            <!--<input type="email" class="form-control" placeholder="Email">-->
            <?php
            echo $this->Form->input('email',array('class'=>'form-control','label'=>false,'placeholder'=>'Email','div'=>array('class'=>'form-group')));?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <!--<input type="password" class="form-control" placeholder="Password">-->
            <?php
            echo $this->Form->input('password',array('class'=>'form-control','label'=>false,'placeholder'=>'Password', 'div'=>array('class'=>'form-group')));?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">

            <!-- /.col -->
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>
            <!-- /.col -->
        </div>
        <br>
        <p class="login-box-msg"><a href="#">I forgot my password</a></p>
        </form>


    </div>
    <!-- /.login-box-body -->
