<?php //pr($this->params);?>

<?php
	$types = array(
				'driver' => 'Driver',
				'passenger' => 'Passenger',
				'vendor' => 'Branch Users',
				'admin'	=> 'Admin'

			);
?>
<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-warning">
					<div class="box-header with-border">
                            <h3 class="box-title"><?php echo __(@$types[$type]); ?></h3>
                            <button class="btn btn-warning btn-lg pull-right"><?php echo $this->Html->link('Add New '. $types[$type], array('controller' => 'users', 'action' => 'add', 'admin' => true, 'prefix' => 'admin', $type))?></button>
                        </div>
					<div class="box-body">

                        <div class="users index">


                            <?php
                            if($type=='vendor') echo $this->element('admin_vendor/index');
                            else{
                                ?>
                                <div class="table-responsive row">
                                    <table cellpadding="0" cellspacing="0" id="example2" class="table table-bordered table-hover">
                                        <tr>
                                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('name'); ?></th>
                                            <th><?php echo $this->Paginator->sort('email'); ?></th>
                                            <th><?php echo $this->Paginator->sort('username'); ?></th>
                                            <th><?php echo $this->Paginator->sort('mobile'); ?></th>
                                            <th><?php echo $this->Paginator->sort('address'); ?></th>
                                            <th><?php echo $this->Paginator->sort('home_no'); ?></th>
                                            <?php if($type!= 'passenger') { ?>
                                                <th><?php //echo $this->Paginator->sort('promoter_code'); ?></th>
                                            <?php } ?>
                                            <?php if($type == 'driver') { ?>
                                                <th><?php //echo $this->Paginator->sort('promoter_code'); ?></th>
                                                <th><?php //echo $this->Paginator->sort('ref_promoter_code','Reference Promoter Code'); ?></th>
                                                <th><?php echo $this->Paginator->sort('running_distance','Running Distance (miles)'); ?></th>
                                                <th><?php echo $this->Paginator->sort('voip_no','Phone No for Phonecall zone(VOIP No)'); ?></th>
                                            <?php } ?>
                                            <th> <?php if($type == 'driver') { ?>
                                                    <?php echo $this->Paginator->sort('is_enabled','Enabled');
                                                } ?></th>
                                            <th class="actions"><?php echo __('Actions'); ?></th>

                                        </tr>
                                        <?php foreach ($users as $user): ?>
                                            <?php //pr($user);die ?>
                                            <tr>
                                                <td><?php echo h('1000'.$user['User']['id']); ?>&nbsp;</td>
                                                <td><?php echo h($user['User']['name']); ?>&nbsp;</td>
                                                <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
                                                <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
                                                <td><?php echo h($user['User']['mobile']); ?>&nbsp;</td>
                                                <td><?php echo h($user['User']['address']); ?>&nbsp;</td>
                                                <td><?php echo h($user['User']['home_no']); ?>&nbsp;</td>
                                                <?php if($type!= 'passenger') { ?>
                                                    <td><?php //echo h($user['User']['promoter_code']); ?>&nbsp;</td>
                                                <?php } ?>
                                                <?php if($type == 'driver') { ?>
                                                    <td><?php //echo h($user['User']['promoter_code']); ?>&nbsp;</td>
                                                    <td><?php //echo h(@$user['User']['ref_promoter_code']); ?>&nbsp;</td>
                                                    <td><?php echo h($user['User']['running_distance']); ?>&nbsp;</td>
                                                    <td><?php echo h($user['User']['voip_no']); ?>&nbsp;</td>
                                                <?php } ?>
                                                <td>
                                                    <?php if($type == 'driver'){
                                                        if(!empty($user['User']['is_enabled'])) {
                                                            echo h('Yes');
                                                        } else {
                                                            echo h('No');
                                                        }
                                                    }
                                                    ?>
                                                    &nbsp;
                                                </td>
                                                <td class="actions">
                                                    <?php
                                                    if ($type == 'driver') {
                                                        if($user['User']['is_enabled'] == 0){
                                                            echo $this->Html->link(__('Enable'), array( 'action' => 'enable_driver', $user['User']['id']), array('div' => false, 'class' => 'e_d btn btn-default'));
                                                        } else {
                                                            echo $this->Html->link(__('Disable'), array('action' => 'disable_driver', $user['User']['id']), array('div' => false, 'class' => 'd_d btn btn-default'));
                                                        }
                                                    }
                                                    ?>
                                                    <?php
                                                    if($type=='driver')
                                                        echo $this->Html->link(__('Give Free Credit'), array('action' => 'freetoup', $user['User']['id']), ['class' => 'btn btn-success']);
                                                    ?>
                                                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'],$type), ['class' => 'btn btn-info']); ?>
                                                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'admin_delete', $user['User']['id']), ['class' => 'btn btn-danger'], __('Are you sure you want to delete %s?', $user['User']['name'])); ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            <?php
                            }
                            ?>

                            <p class="pull-right">
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?>	</p>
                            <div class="clearfix"></div>
                            <div class="paging">
                                <ul class="pagination pagination-sm no-margin pull-right">
                                    <?php
                                    echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
                                    echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
                                    echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
                                    ?>
                                </ul>

                            </div>
                        </div>
                    </div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->


					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>


<?php //echo $this->element('menu'); ?>
