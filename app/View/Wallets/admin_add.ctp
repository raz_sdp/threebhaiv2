<div class="wallets form">
<?php echo $this->Form->create('Wallet'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Wallet'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('total_balance');
		echo $this->Form->input('total_withdraw');
		echo $this->Form->input('available_balance');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Wallets'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
