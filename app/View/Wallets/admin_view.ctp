<div class="wallets view">
<h2><?php echo __('Wallet'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($wallet['Wallet']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($wallet['User']['name'], array('controller' => 'users', 'action' => 'view', $wallet['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Balance'); ?></dt>
		<dd>
			<?php echo h($wallet['Wallet']['total_balance']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Withdraw'); ?></dt>
		<dd>
			<?php echo h($wallet['Wallet']['total_withdraw']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Available Balance'); ?></dt>
		<dd>
			<?php echo h($wallet['Wallet']['available_balance']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($wallet['Wallet']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($wallet['Wallet']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Wallet'), array('action' => 'edit', $wallet['Wallet']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Wallet'), array('action' => 'delete', $wallet['Wallet']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $wallet['Wallet']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Wallets'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Wallet'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
