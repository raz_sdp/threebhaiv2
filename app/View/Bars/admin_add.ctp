<div class="bars form">
<?php echo $this->Form->create('Bar'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Bar'); ?></legend>
	<?php
		echo $this->Form->input('userId1');
		echo $this->Form->input('userId2');
		echo $this->Form->input('is_requested');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>