<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bars</h3>
                    </div>

                    <div class="box-body">
					<div class="bars index">
						<div class="table-responsive">
						<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
							<tr>
								<th><?php echo $this->Paginator->sort('userId1','Who'); ?></th>
								<th><?php echo $this->Paginator->sort('userId2','Whom'); ?></th>
								<th><?php echo $this->Paginator->sort('created'); ?></th>
								<th class="actions"><?php echo __(''); ?></th>
							</tr>
							<?php foreach ($bars as $bar): ?>
								<tr>
									<td><?php echo h($bar['User1']['name']).' ('. $bar['User1']['type'] .')';?>&nbsp;</td>
									<td><?php echo h($bar['User2']['name']).' ('. $bar['User2']['type'] .')';?>&nbsp;</td>

									<?php
									$created_date = date_create($bar['Bar']['created']);
									?>
									<td><?php echo date_format($created_date,'d/m/Y'); ?>&nbsp;</td>
									<td class="actions">
										<?php echo $this->Form->postLink(__('Un-Bar'), array('action' => 'delete', $bar['Bar']['id']), ['class' => 'btn btn-danger'], __('Are you sure you want to un-bar %s?', $bar['User2']['name'])); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</table>
                        </div>
						<p class="pull-right">
							<?php
							echo $this->Paginator->counter(array(
								'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
							));
							?>	</p>
							<div class="clearfix"></div>
							<div class="paging">
								<ul class="pagination pagination-sm no-margin pull-right">
									<?php
									echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
									echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
									echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
									?>
								</ul>
							</div>
						</div>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->


				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
