<?php echo $this->Html->script('bar');
$this->Session->read('Auth.User');
$id = $this->Session->read('Auth.User.id');
?>
<div class="users form">
<?php echo $this->Form->create('Bar'); ?>
	<fieldset>
		<legend><?php echo __('Bar a driver'); ?></legend>
	<?php
		echo $this->Form->input('userId1', array('type' => 'hidden', 'value' => $id ));
		echo $this->Form->input('userId2', array('label' => 'Bar the driver', 'value' => '1,2,3'));
		echo $this->Form->input('who_to_whom', array('type' => 'hidden', 'value' => 'p2d'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>