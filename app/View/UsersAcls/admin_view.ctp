<div class="usersAcls view">
<h2><?php echo __('Users Acl'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($usersAcl['UsersAcl']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersAcl['User']['name'], array('controller' => 'users', 'action' => 'view', $usersAcl['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Acl'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersAcl['Acl']['id'], array('controller' => 'acls', 'action' => 'view', $usersAcl['Acl']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($usersAcl['UsersAcl']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($usersAcl['UsersAcl']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Users Acl'), array('action' => 'edit', $usersAcl['UsersAcl']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Users Acl'), array('action' => 'delete', $usersAcl['UsersAcl']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $usersAcl['UsersAcl']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Users Acls'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users Acl'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Acls'), array('controller' => 'acls', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Acl'), array('controller' => 'acls', 'action' => 'add')); ?> </li>
	</ul>
</div>
