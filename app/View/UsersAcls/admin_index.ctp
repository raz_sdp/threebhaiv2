<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Individual Branch User's Menu Access</h3>
                    </div>
                    <?php
                    $vendor_id = $this->params['pass'][0];
                    echo $this->Form->create('UsersAcl',['url' => "add/$vendor_id"]);
                    ?>
                    <div class="box-body">
                        <!--<div class="form-group">
                            <?php /*echo $this->Form->input('group_id', ['class' => 'form-control js-group', 'value' => $group_id]); */?>
                        </div>-->

                        <div class="form-group js-question-box table-responsive" style="max-height: 600px;">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <th style="width:20px;"><input type="checkbox"></th>
                                <th>Select Menu for the Branch User's</th>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($aclsmanages as  $aclsmanage) {
                                    //pr($aclsmanage);die;
                                    $acl_id = $aclsmanage['Acl']['id'];
                                    $checked = '';
                                    $class = '';
                                    if (in_array($acl_id, $selected_acls)) {
                                        $checked = 'checked';
                                        $class = 'success';
                                    }
                                    ?>
                                    <tr class="<?php echo $class ?>">
                                        <td><input type="checkbox" value="<?php echo  $acl_id?>"
                                                   name="data[UsersAcl][acl_id][]" <?php echo $checked ?>>
                                        </td>
                                        <td><?php echo $aclsmanage['Acl']['permission_name'] ?></td>
                                    </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <button class="btn btn-warning btn-lg btn-block">Save Selected Menus</button>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    </form>
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>



