<div class="archivedBookings form">
<?php echo $this->Form->create('ArchivedBooking'); ?>
	<fieldset>
		<legend><?php echo __('Add Archived Booking'); ?></legend>
	<?php
		echo $this->Form->input('pick_up');
		echo $this->Form->input('pick_up_lat');
		echo $this->Form->input('pick_up_lng');
		echo $this->Form->input('destination');
		echo $this->Form->input('destination_lat');
		echo $this->Form->input('destination_lng');
		echo $this->Form->input('destination_postcode');
		echo $this->Form->input('destination_airport');
		echo $this->Form->input('pick_up_postcode');
		echo $this->Form->input('pick_up_airport');
		echo $this->Form->input('zone_id');
		echo $this->Form->input('total_distance');
		echo $this->Form->input('distance_google');
		echo $this->Form->input('at_time');
		echo $this->Form->input('persons');
		echo $this->Form->input('car_type');
		echo $this->Form->input('via');
		echo $this->Form->input('fare');
		echo $this->Form->input('real_fare');
		echo $this->Form->input('is_paid');
		echo $this->Form->input('requester');
		echo $this->Form->input('customer_phn_no');
		echo $this->Form->input('acceptor');
		echo $this->Form->input('accept_time');
		echo $this->Form->input('hunting_no');
		echo $this->Form->input('status');
		echo $this->Form->input('special_requirement');
		echo $this->Form->input('bar_driver');
		echo $this->Form->input('note_to_driver');
		echo $this->Form->input('booking_type');
		echo $this->Form->input('is_pre_book');
		echo $this->Form->input('is_sap_booking');
		echo $this->Form->input('notify_admin');
		echo $this->Form->input('route');
		echo $this->Form->input('recurring');
		echo $this->Form->input('recurring_start');
		echo $this->Form->input('recurring_end');
		echo $this->Form->input('no_of_luggages');
		echo $this->Form->input('from_web');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Archived Bookings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Zones'), array('controller' => 'zones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Zone'), array('controller' => 'zones', 'action' => 'add')); ?> </li>
	</ul>
</div>
