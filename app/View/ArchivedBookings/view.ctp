<div class="archivedBookings view">
<h2><?php echo __('Archived Booking'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pick Up'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['pick_up']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pick Up Lat'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['pick_up_lat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pick Up Lng'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['pick_up_lng']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destination'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['destination']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destination Lat'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['destination_lat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destination Lng'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['destination_lng']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destination Postcode'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['destination_postcode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destination Airport'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['destination_airport']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pick Up Postcode'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['pick_up_postcode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pick Up Airport'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['pick_up_airport']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zone'); ?></dt>
		<dd>
			<?php echo $this->Html->link($archivedBooking['Zone']['name'], array('controller' => 'zones', 'action' => 'view', $archivedBooking['Zone']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Distance'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['total_distance']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Distance Google'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['distance_google']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('At Time'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['at_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Persons'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['persons']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Car Type'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['car_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Via'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['via']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fare'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['fare']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Real Fare'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['real_fare']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Paid'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['is_paid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Requester'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['requester']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Phn No'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['customer_phn_no']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Acceptor'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['acceptor']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Accept Time'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['accept_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hunting No'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['hunting_no']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Special Requirement'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['special_requirement']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bar Driver'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['bar_driver']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note To Driver'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['note_to_driver']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Booking Type'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['booking_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Pre Book'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['is_pre_book']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Sap Booking'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['is_sap_booking']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notify Admin'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['notify_admin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Route'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['route']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Recurring'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['recurring']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Recurring Start'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['recurring_start']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Recurring End'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['recurring_end']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No Of Luggages'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['no_of_luggages']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('From Web'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['from_web']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Archived Booking'), array('action' => 'edit', $archivedBooking['ArchivedBooking']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Archived Booking'), array('action' => 'delete', $archivedBooking['ArchivedBooking']['id']), null, __('Are you sure you want to delete # %s?', $archivedBooking['ArchivedBooking']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Archived Bookings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Archived Booking'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Zones'), array('controller' => 'zones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Zone'), array('controller' => 'zones', 'action' => 'add')); ?> </li>
	</ul>
</div>
