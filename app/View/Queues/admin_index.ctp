<?php
	$res = array('cancelled' => 'Cancelled', 'accepted' => 'Accepted', 'auto_cancelled' => 'Auto Cancelled', 'timeout'=> 'Timeout', 'cleared' => 'Cleared', 'rejected' => 'Rejected', 'traffic' => 'Traffic', 'broken' =>'Broken', 'forwarded' => 'Forwarded', 'ignored' => 'Timeout');
	?>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Queue Record for Booking Id <?php echo $queues[0]['Queue']['booking_id'] ?></h3>

                    </div>

                    <div class="box-body">
<div class="queues index">
<!--	<h2>--><?php //echo __('Queue Record for Booking Id '); echo ($queues[0]['Queue']['booking_id']); ?><!--</h2>-->
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
	<tr>
			<th><?php echo $this->Paginator->sort('user_id','Driver'); ?></th>
			<th><?php echo $this->Paginator->sort('request_time'); ?></th>
			<th><?php echo $this->Paginator->sort('response_time'); ?></th>
			<th><?php echo $this->Paginator->sort('response'); ?></th>
			<th><?php echo $this->Paginator->sort('user_comment'); ?></th>
			<th><?php echo $this->Paginator->sort('driver_decline_reason_id'); ?></th>
			<th><?php echo $this->Paginator->sort('pob_time', 'Passenger on Board Time'); ?></th>
			<th><?php echo $this->Paginator->sort('ondoor_time', "On Passenger's Door"); ?></th>
			<th><?php echo $this->Paginator->sort('no_show_time'); ?></th>
			
	</tr>
	<?php foreach ($queues as $queue): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($queue['User']['name'], array('controller' => 'users', 'action' => 'edit', $queue['User']['id'])); ?>
		</td>
		<td><?php 
			if (!empty($queue['Queue']['request_time'])){
				$date_time = date_create($queue['Queue']['request_time']);
				echo date_format($date_time,'d/m/Y g:i A');
			} else {
				echo 'N/A';
			}
		?>&nbsp;</td>
		<td><?php
		 	if (!empty($queue['Queue']['response_time'])) {
				$date_time = date_create($queue['Queue']['response_time']);
				echo date_format($date_time,'d/m/Y g:i A');
			} else {
				echo 'N/A';
			}
		?>&nbsp;</td>
		<td><?php 
			if (!empty($queue['Queue']['response'])) {
				$val = $queue['Queue']['response'];
				echo $res[$val]; 
			} else {
				echo 'N/A';
			}
		?>&nbsp;</td>
		<td><?php if (!empty($queue['Queue']['user_comment'])) {
				echo h($queue['Queue']['user_comment']); 
			} else {
				echo 'N/A';
			}
		?>&nbsp;</td>
		<td><?php 
			if (!empty($queue['Queue']['driver_decline_reason_id'])) {
				echo h($queue['DriverDeclineReason']['reason']); 
			} else {
				echo 'N/A';
			}
		?>&nbsp;</td>
		<td><?php  
			if (!empty($queue['Queue']['pob_time'])) {
				$date_time = date_create($queue['Queue']['pob_time']);
				echo date_format($date_time,'d/m/Y g:i A');
			} else {
				echo 'N/A';
			}
		?>&nbsp;</td>
		<td><?php
			if (!empty($queue['Queue']['ondoor_time'])) {
				$date_time = date_create($queue['Queue']['ondoor_time']);
				echo date_format($date_time,'d/m/Y g:i A');
			} else {
				echo 'N/A';
			}
		?>&nbsp;</td>
		<td><?php 
			if (!empty($queue['Queue']['no_show_time'])) {
				$date_time = date_create($queue['Queue']['no_show_time']);
				echo date_format($date_time,'d/m/Y g:i A');
			} else {
				echo 'N/A';
			}
		?>&nbsp;</td>

	</tr>
<?php endforeach; ?>
	</table>
    <p class="pull-right">
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="clearfix"></div>
    <div class="paging">
        <ul class="pagination pagination-sm no-margin pull-right">
            <?php
            echo "<li>".$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'))."</li>";
            echo "<li>".$this->Paginator->numbers(array('separator' => ''))."</li>";
            echo "<li>".$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'))."</li>";
            ?>
        </ul>
    </div>
    <!-- /.box-body -->
</div>
                        <!-- /.box -->


                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
