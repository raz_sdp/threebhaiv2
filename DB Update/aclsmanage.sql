-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2018 at 08:40 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cabbieapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `aclsmanage`
--

CREATE TABLE `aclsmanage` (
  `id` bigint(20) NOT NULL,
  `type` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acl_id` int(11) DEFAULT NULL,
  `access` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Role Permit ACL';

--
-- Dumping data for table `aclsmanage`
--

INSERT INTO `aclsmanage` (`id`, `type`, `acl_id`, `access`) VALUES
(3, 'vendor', 75, 1),
(4, 'vendor', 21, 1),
(5, 'vendor', 20, 1),
(6, 'vendor', 19, 1),
(7, 'vendor', 18, 1),
(8, 'vendor', 17, 1),
(9, 'vendor', 16, 1),
(10, 'vendor', 15, 1),
(11, 'vendor', 14, 1),
(12, 'vendor', 13, 1),
(13, 'vendor', 12, 1),
(14, 'vendor', 4, 1),
(15, 'vendor', 5, 1),
(16, 'vendor', 6, 1),
(17, 'vendor', 7, 1),
(18, 'vendor', 8, 1),
(19, 'vendor', 9, 1),
(20, 'vendor', 10, 1),
(21, 'vendor', 11, 1),
(22, 'vendor', 63, 1),
(23, 'vendor', 70, 1),
(24, 'vendor', 71, 1),
(25, 'vendor', 66, 1),
(26, 'vendor', 64, 1),
(27, 'vendor', 73, 1),
(28, 'vendor', 72, 1),
(29, 'vendor', 65, 1),
(30, 'vendor', 84, 1),
(31, 'vendor', 135, 1),
(32, 'vendor', 133, 1),
(33, 'vendor', 132, 1),
(34, 'vendor', 134, 1),
(35, 'vendor', 131, 1),
(36, 'vendor', 139, 1),
(37, 'vendor', 95, 1),
(38, 'vendor', 138, 1),
(39, 'vendor', 30, 1),
(40, 'vendor', 27, 1),
(41, 'vendor', 26, 1),
(42, 'vendor', 25, 1),
(43, 'vendor', 24, 1),
(44, 'vendor', 23, 1),
(45, 'vendor', 22, 1),
(46, 'vendor', 31, 1),
(47, 'vendor', 32, 1),
(48, 'vendor', 33, 1),
(49, 'vendor', 34, 1),
(50, 'vendor', 28, 1),
(51, 'vendor', 35, 1),
(52, 'vendor', 36, 1),
(53, 'vendor', 37, 1),
(54, 'vendor', 124, 1),
(55, 'vendor', 121, 1),
(56, 'vendor', 119, 1),
(57, 'vendor', 118, 1),
(58, 'vendor', 117, 1),
(59, 'vendor', 120, 1),
(60, 'admin', 75, 1),
(61, 'admin', 21, 1),
(62, 'admin', 20, 1),
(63, 'admin', 19, 1),
(64, 'admin', 18, 1),
(65, 'admin', 17, 1),
(66, 'admin', 16, 1),
(67, 'admin', 15, 1),
(68, 'admin', 14, 1),
(69, 'admin', 13, 1),
(70, 'admin', 12, 1),
(71, 'admin', 4, 1),
(72, 'admin', 5, 1),
(73, 'admin', 6, 1),
(74, 'admin', 7, 1),
(75, 'admin', 8, 1),
(76, 'admin', 9, 1),
(77, 'admin', 10, 1),
(78, 'admin', 11, 1),
(79, 'admin', 63, 1),
(80, 'admin', 70, 1),
(81, 'admin', 71, 1),
(82, 'admin', 66, 1),
(83, 'admin', 64, 1),
(84, 'admin', 73, 1),
(85, 'admin', 72, 1),
(86, 'admin', 65, 1),
(87, 'admin', 84, 1),
(88, 'admin', 135, 1),
(89, 'admin', 133, 1),
(90, 'admin', 132, 1),
(91, 'admin', 134, 1),
(92, 'admin', 131, 1),
(93, 'admin', 139, 1),
(94, 'admin', 95, 1),
(95, 'admin', 138, 1),
(96, 'admin', 30, 1),
(97, 'admin', 27, 1),
(98, 'admin', 26, 1),
(99, 'admin', 25, 1),
(100, 'admin', 24, 1),
(101, 'admin', 23, 1),
(102, 'admin', 22, 1),
(103, 'admin', 31, 1),
(104, 'admin', 32, 1),
(105, 'admin', 33, 1),
(106, 'admin', 34, 1),
(107, 'admin', 28, 1),
(108, 'admin', 35, 1),
(109, 'admin', 36, 1),
(110, 'admin', 37, 1),
(111, 'admin', 124, 1),
(112, 'admin', 121, 1),
(113, 'admin', 119, 1),
(114, 'admin', 118, 1),
(115, 'admin', 117, 1),
(116, 'admin', 120, 1),
(117, 'vendor', 140, 1),
(118, 'vendor', 142, 1),
(119, 'administrator', 143, 1),
(120, 'administrator', 144, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aclsmanage`
--
ALTER TABLE `aclsmanage`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roleID_2` (`type`,`acl_id`),
  ADD UNIQUE KEY `role_id` (`type`,`acl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aclsmanage`
--
ALTER TABLE `aclsmanage`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
