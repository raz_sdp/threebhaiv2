/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : cabbieappv2

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2018-07-11 15:29:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `acls`
-- ----------------------------
DROP TABLE IF EXISTS `acls`;
CREATE TABLE `acls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permission_key` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orderid` int(11) DEFAULT NULL,
  `parent` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permKey` (`permission_key`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of acls
-- ----------------------------
INSERT INTO `acls` VALUES ('4', 'Admin', 'users/index/admin', '1', '1');
INSERT INTO `acls` VALUES ('5', 'Drivers', 'users/index/driver', '2', '1');
INSERT INTO `acls` VALUES ('6', 'Passengers', 'users/index/passenger', '3', '1');
INSERT INTO `acls` VALUES ('7', 'Edit Acl', 'aclsmanages/index', '50', '1');
INSERT INTO `acls` VALUES ('8', 'Zone', 'zones/index#', '51', '0');
INSERT INTO `acls` VALUES ('9', 'VR Zone Settings', 'vr_settings/index', '8', '1');
INSERT INTO `acls` VALUES ('10', 'Phone Setting', 'phone_settings/index', '9', '1');
INSERT INTO `acls` VALUES ('11', 'Bookings', 'bookings/index', '10', '1');
INSERT INTO `acls` VALUES ('12', 'Cancelled Bookings', 'archived_bookings/index', '11', '0');
INSERT INTO `acls` VALUES ('13', 'Call Logs', 'phone_queues/index', '12', '1');
INSERT INTO `acls` VALUES ('14', 'Bars', 'bars/index', '13', '1');
INSERT INTO `acls` VALUES ('15', 'Transactions', 'transactions/index', '14', '1');
INSERT INTO `acls` VALUES ('16', 'Balance Transfer Status', 'transactions/reqtransferamount/', '15', '0');
INSERT INTO `acls` VALUES ('17', 'Fixed Fare', 'fare_settings/fixedindex', '16', '1');
INSERT INTO `acls` VALUES ('18', 'Fare By Miles', 'fare_settings/index', '17', '1');
INSERT INTO `acls` VALUES ('19', 'Settings', 'settings/edit/1', '18', '1');
INSERT INTO `acls` VALUES ('20', 'Paypal Setting', 'settings/paypal/1', '19', '1');
INSERT INTO `acls` VALUES ('21', 'Driver Decline Reasons', 'driver_decline_reasons/index', '20', '1');
INSERT INTO `acls` VALUES ('22', 'Holidays', 'holidaytypes/index', '21', '1');
INSERT INTO `acls` VALUES ('23', 'CMS', 'staticpages/index', '22', '1');
INSERT INTO `acls` VALUES ('24', 'CMS Link', 'settings/share_links/1', '23', '1');
INSERT INTO `acls` VALUES ('25', 'Logout', 'users/logout', '100', '1');
INSERT INTO `acls` VALUES ('26', 'Update Custom Info', 'user/user_custom', null, '0');
INSERT INTO `acls` VALUES ('27', 'Package Info', 'user/user_package', null, '0');
INSERT INTO `acls` VALUES ('28', 'Change User Role ', 'user/user_role', null, '0');
INSERT INTO `acls` VALUES ('30', 'Delete User', 'user/user_delete', null, '0');
INSERT INTO `acls` VALUES ('31', 'Add New Access Control', 'user/roles', null, '0');
INSERT INTO `acls` VALUES ('32', 'Edit Access Control List', 'user/create_role', null, '0');
INSERT INTO `acls` VALUES ('33', 'Edit Access Control List', 'user/rename_role', null, '0');
INSERT INTO `acls` VALUES ('34', 'Delete Access Control', 'user/delete_role', null, '0');
INSERT INTO `acls` VALUES ('35', 'Update General Settings', 'setting', null, '0');
INSERT INTO `acls` VALUES ('36', 'Developer Tools', 'setting/developer_guide', null, '0');
INSERT INTO `acls` VALUES ('37', 'Lisence Information', 'setting/lic_info', null, '0');
INSERT INTO `acls` VALUES ('57', 'Task Management', 'task', null, '0');
INSERT INTO `acls` VALUES ('58', 'Create Task', 'task/new', null, '0');
INSERT INTO `acls` VALUES ('59', 'Task Rescheduleing', 'task/reschedule', null, '0');
INSERT INTO `acls` VALUES ('60', 'Task Archiving', 'task/archive', null, '0');
INSERT INTO `acls` VALUES ('62', 'Manage Everyone Task', 'task/everyone', null, '0');
INSERT INTO `acls` VALUES ('63', 'Utilities', 'utilities', null, '0');
INSERT INTO `acls` VALUES ('64', 'Email Template', 'utilities/email_template', null, '0');
INSERT INTO `acls` VALUES ('65', 'Locaion Management', 'utilities/location', null, '0');
INSERT INTO `acls` VALUES ('66', 'Newsletter', 'utilities/newsletter', null, '0');
INSERT INTO `acls` VALUES ('70', 'Testimonial ', 'utilities/testimonial', null, '0');
INSERT INTO `acls` VALUES ('71', 'Module Manager', 'utilities/module', null, '0');
INSERT INTO `acls` VALUES ('72', 'Clean Up Cachce', 'utilities/cleanup', null, '0');
INSERT INTO `acls` VALUES ('73', 'Database Management', 'utilities/database', null, '0');
INSERT INTO `acls` VALUES ('75', 'Dashboard', 'dashboard', null, '0');
INSERT INTO `acls` VALUES ('84', 'Deleted User', 'user/delete', null, '0');
INSERT INTO `acls` VALUES ('95', 'User Sentmail', 'user/user_sentmail', null, '0');
INSERT INTO `acls` VALUES ('117', 'Help', 'help', null, '0');
INSERT INTO `acls` VALUES ('118', 'Contact Online', 'help/online', null, '0');
INSERT INTO `acls` VALUES ('119', 'User Manual', 'help/manual', null, '0');
INSERT INTO `acls` VALUES ('120', 'License Info', 'help/info', null, '0');
INSERT INTO `acls` VALUES ('121', 'FAQs', 'help/faq', null, '0');
INSERT INTO `acls` VALUES ('124', 'My Query', 'help/query', null, '0');
INSERT INTO `acls` VALUES ('125', 'FAQ Managemtn', 'product/faq', null, '0');
INSERT INTO `acls` VALUES ('126', 'faq add', 'product/faq_add', null, '0');
INSERT INTO `acls` VALUES ('131', 'Attandance', 'user/attandance', null, '0');
INSERT INTO `acls` VALUES ('132', 'Salary Payable', 'user/salary_payable', null, '0');
INSERT INTO `acls` VALUES ('133', 'Payout', 'user/payout', null, '0');
INSERT INTO `acls` VALUES ('134', 'Login Entry', 'user/login_entry', null, '0');
INSERT INTO `acls` VALUES ('135', 'User Attendance ', 'user/user_attan', null, '0');
INSERT INTO `acls` VALUES ('138', 'Manage Bank Holidays', 'user/bank_holiday', null, '0');
INSERT INTO `acls` VALUES ('139', 'Manage Office Day Type', 'user/daytype', null, '0');
INSERT INTO `acls` VALUES ('140', 'Zones', 'zones/index', '6', '1');
INSERT INTO `acls` VALUES ('141', 'Acl', 'acls', '51', '0');
INSERT INTO `acls` VALUES ('142', 'Twilio Numbers', 'zones/available_phone_numbers', '7', '1');
INSERT INTO `acls` VALUES ('143', 'Branch Users', 'users/index/vendor', '4', '1');
INSERT INTO `acls` VALUES ('144', 'Distributors', 'users/index/distributor', '5', '1');
INSERT INTO `acls` VALUES ('145', 'Despatch Job', '/despatch_job', null, '1');
INSERT INTO `acls` VALUES ('146', 'Add Driver', 'users/add/driver', null, '0');
INSERT INTO `acls` VALUES ('147', 'Edit Driver', 'users/edit/driver', null, '0');
INSERT INTO `acls` VALUES ('148', 'Add Passenger', 'users/add/passenger', null, '0');
INSERT INTO `acls` VALUES ('149', 'Edit Passenger', 'users/edit/passenger', null, '0');
INSERT INTO `acls` VALUES ('150', 'Add VR Zone Setting', 'vr_settings/add', null, '0');
INSERT INTO `acls` VALUES ('151', 'Edit VR Zone Setting', 'vr_settings/edit', null, '0');
INSERT INTO `acls` VALUES ('152', 'Edit Phone Setting', 'phone_settings/edit', null, '0');
INSERT INTO `acls` VALUES ('153', 'Add Phone Setting', 'phone_settings/add', null, '0');
INSERT INTO `acls` VALUES ('154', 'Call Log View', 'phone_queues/view', null, '0');
INSERT INTO `acls` VALUES ('155', 'Transaction View', 'users/topup', null, '0');
INSERT INTO `acls` VALUES ('156', 'Add Fixed Fare', 'fare_settings/fixedadd', null, '0');
INSERT INTO `acls` VALUES ('157', 'Edit Fixed Fare', 'fare_settings/fixededit', null, '0');
INSERT INTO `acls` VALUES ('158', 'Add Fare By Mile', 'fare_settings/add', null, '0');
INSERT INTO `acls` VALUES ('159', 'Edit Fare By Mile', 'fare_settings/edit', null, '0');
INSERT INTO `acls` VALUES ('160', 'Add Driver Decline Reason\r\n', 'driver_decline_reasons/add', null, '0');
INSERT INTO `acls` VALUES ('161', 'Edit Driver Decline Reason', 'driver_decline_reasons/edit', null, '0');
INSERT INTO `acls` VALUES ('162', 'Add Holiday', 'holidaytypes/add', null, '0');
INSERT INTO `acls` VALUES ('163', 'Edit Holiday', 'holidaytypes/edit', null, '0');
INSERT INTO `acls` VALUES ('164', 'Add CMS', 'staticpages/add', null, '0');
INSERT INTO `acls` VALUES ('165', 'Edit CMS', 'staticpages/edit', null, '0');
INSERT INTO `acls` VALUES ('166', 'Add VR Zone', 'zones/add/vr_zone', null, '0');
INSERT INTO `acls` VALUES ('169', 'Add Phone GPS Zone', 'zones/add/phonecall_gps_zone', null, '0');
INSERT INTO `acls` VALUES ('170', 'Add Phonecall Zone', 'zones/add/phonecall_zone', null, '0');
INSERT INTO `acls` VALUES ('171', 'Edit Zone', 'zones/edit', null, '0');
INSERT INTO `acls` VALUES ('172', 'Edit Vendor', 'users/edit/vendor', null, '0');
INSERT INTO `acls` VALUES ('173', 'Add Vendor', 'users/add/vendor', null, '0');
INSERT INTO `acls` VALUES ('174', 'View Admin Fee Records', 'transactions/paymentrecord', null, '0');
